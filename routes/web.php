<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () { return redirect('/login'); });

 Route::get('/home', function () { return redirect()->route('home'); });


Route::group(['middleware' => ['web','auth','permission:access_panel'], 'prefix' => 'panel'], function () {

    Route::group(['middleware' => ['permission:access_admin'], 'prefix' => 'admin'], function () { 

            Route::get('home', 'AdminController@index')->name('admin.index');

    });


    Route::get('ajax/setup-alert', 'SiteController@setupAlert')->name('setupAlert');



    /*ORGANIZATION ROUTES*/

    Route::get('ajax/organization/OrganizationWithID', 'OrganizationController@OrganizationWithID')->name('OrganizationWithID');
    Route::post('ajax/organization/setOrganization', 'OrganizationController@setOrganizationViaAjax')->name('setOrganizationViaAjax');
    Route::get('organization/select', 'OrganizationController@select')->name('OrgSelect');    
 

    /*PROJECT ROUTES*/
  
    Route::get('project/select', 'ProjectController@select')->name('ProjectSelect');
    Route::get('ajax/project/ProjectWithID', 'ProjectController@ProjectWithID')->name('ProjectWithID');
    Route::post('ajax/project/setProject', 'ProjectController@setProjectViaAjax')->name('setProjectViaAjax');
    Route::get('ajax/stage/department', 'StageController@listByDepartment')->name('StageByDepartment');
    Route::get('ajax/stage/rate', 'StageController@standardRate')->name('StageStandardRate');

    Route::group(['middleware' => ['OrgProjectIfSet']],function () {
            
            /*ITEMS ROUTES*/
            Route::post('ajax/item/incharge', 'ItemController@itemIncharge')->name('itemIncharge');

            /*SUPPLIER AJAX*/

            Route::get('ajax/supplier/items', 'ItemController@supplierItems')->name('supplierItems');
            Route::get('ajax/supplier/balance', 'SupplierController@balance')->name('supplier-balance');
            Route::post('ajax/supplier/remove-item', 'ItemController@RemoveSupplierItem')->name('supplier-item-remove');
            Route::get('ajax/supplier/grn/unpaid', 'OrderGrnController@supplierGrn')->name('supplier-grn-unpaid');

            /*CONTRACTOR*/

            Route::get('ajax/contractor/department', 'ContractorController@listByDepartment')->name('ContractorByDepartment');
            Route::get('ajax/contractor/balance', 'ContractorController@balance')->name('contractor-balance');
            Route::get('ajax/contractor/unpaid-task', 'ContractorController@unpaidTask')->name('contractor-unpaid-task');

            /*OTHER ROUTES*/
            Route::get('home', 'SiteController@index')->name('home');



            /*PROJECT ROUTES*/
            Route::group(['prefix' => 'project'], function () {

                
                Route::get('product-property/list', 'ProductPropertyController@listProductProperties')->name('ProjectPropertyList');
                Route::get('product-category/list', 'ProductCategoryController@getProductCategoriesviaAjax')->name('ProductCategoriesviaAjax');
                Route::get('product-category/manage/{id}', 'ProductCategoryController@show')->name('ShowProductCategory');
                Route::post('product-category/assign', 'ProductCategoryController@assignPropertyHeader')->name('assignPropertyHeader');
                Route::post('add-product-catrgory-product', 'ProductCategoryController@AddProdToCategory')->name('AddProdToCategory');
                Route::post('delete-product-catrgory-product', 'ProductCategoryController@DeleteProdFromCategory')->name('DeleteProdFromCategory');

                Route::post('product/action', 'ProductController@action')->name('product-action');

               
                Route::resource('project-managers','ProjectManagerController');
                Route::resource('product','ProductController');
                Route::resource('product-category','ProductCategoryController');
                Route::resource('product-property/header','ProductPropertyHeaderController');
                Route::resource('product-property','ProductPropertyController');
                Route::resource('product-items','ProductItemController');
               

            });

            Route::get('order/view/{order}', 'OrderController@show')->name('view-order');
            Route::get('order/grn/prepare/{Orderid}', 'OrderController@prepareGrn')->name('prepare-grn');

            Route::get('user/login/logs','LoginLogsController@logs')->name('login-logs');
            Route::get('user/exists','UserController@checkUser')->name('user-exists');

            Route::get('inventory/items','InventoryController@items')->name('inventory-items');
            


            Route::post('payments/pay','PaymentController@pay')->name('payments-pay');

            Route::get('payments/transaction','PaymentController@viewTransaction')->name('payments-transaction');

            Route::get('payments/transaction/bank/{id}','PaymentController@bankTransaction')->name('bank-transaction');

            Route::get('payments/material-payment/create','PaymentController@materialPayment')->name('material-payment');
            Route::get('payments/contractor-payment/create','PaymentController@contractorPayment')->name('contractor-payment');
            Route::get('payments/service-payment/create','PaymentController@servicePayment')->name('service-payment');
            Route::get('payments/external-payment/create','PaymentController@externalPayment')->name('external-payment');
            Route::get('payments/external-income/create','PaymentController@externalIncome')->name('external-income');
            Route::get('payments/customer-payment/create','PaymentController@customerPayment')->name('customer-payment');

            Route::get('payments/view/{paymentid}','PaymentController@show')->name('show-payment');

            Route::get('payments/receipt/{paymentid}','PaymentController@receipt')->name('receipt');
            Route::get('payments/voucher/{paymentid}','PaymentController@voucher')->name('voucher');

            Route::any('bank-accounts/ajax','BankAccountController@ajax')->name('banks-ajax');
            Route::any('order/ajax','OrderController@ajax')->name('order-ajax');

            Route::post('change-request/update','ChangeRequestController@updateChangeRequest')->name('update-cr');
            
            Route::any('customer/products','ProductCustomerController@customerProductsCombo')->name('customer-products-combo');
            Route::any('product/cost','ProductController@cost')->name('product-cost');
            Route::any('product/upgrades','ProductController@upgrades')->name('product-upgrades');

            Route::get('reports','ReportController@index')->name('reports');
            Route::any('reports/generate','ReportController@generate')->name('report-generate');


            Route::resource('order','OrderController');
            Route::resource('ordergrn','OrderGrnController');
            Route::resource('supplier','SupplierController');
            Route::resource('contractor','ContractorController');
            Route::resource('department','DepartmentController');
            Route::resource('item','ItemController');
            Route::resource('tax','TaxController');
            Route::resource('staff','StaffController');
            Route::resource('stages','StageController');
            Route::resource('product-stages','ProductStageController');
            Route::resource('customer','CustomerController');
            Route::resource('material-transfer','MaterialTransferController');
            Route::resource('inventory','InventoryController');
            Route::resource('bank-accounts','BankAccountController');
            Route::resource('payments','PaymentController');
            Route::resource('product-customer','ProductCustomerController');
            Route::resource('change-request','ChangeRequestController');

    });

     Route::resource('organization','OrganizationController');
     Route::resource('project','ProjectController');
    


    
});