@extends('master')

@section('title','Dashboard')

@section('pagecontent')


@if(isset($project))

    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Projects Dashboard</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">

                    <div class="row">
                        <div class="col-md-12"><div class="setup-alert"></div></div>
                    </div>
                    


        <div class="container-fluid ks-rows-section">

            <div class="row ks-widgets-collection">

                <div class="col-lg-3">
                    <div class="ks-dashboard-widget ks-widget-amount-statistics ks-default">
                        <div class="ks-statistics">
                            <span class="ks-amount" data-count-up="{{ $project->orders->count() }}">0</span>
                            <span class="ks-text">Customers</span>
                        </div>                       
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="ks-dashboard-widget ks-widget-amount-statistics ks-default">
                        <div class="ks-statistics">
                            <span class="ks-amount" data-count-up="{{ $project->products->count() }}">0</span>
                            <span class="ks-text">Total Products</span>
                        </div>
                    </div>
                </div>
      

                 
                  <div class="col-lg-3">
                    <div class="ks-dashboard-widget ks-widget-amount-statistics ks-default">
                        <div class="ks-statistics">
                            <span class="ks-amount" data-count-up="{{ $project->products->where('payment_id', null)->count() }}">0</span>
                            <span class="ks-text">Product Available</span>
                        </div>
                        
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="ks-dashboard-widget ks-widget-amount-statistics ks-default">
                        <div class="ks-statistics">
                            <span class="ks-amount" data-count-up="{{ $project->products->where('payment_id', '>', '0')->count() }}">0</span>
                            <span class="ks-text">Product Sold</span>
                        </div>
                        
                    </div>
                </div>
             
            </div>
            
            <div class="row ks-widgets-collection">
                
                <div class="col-lg-6">
                    <div id="ks-line-chart-panel" class="card panel">
                        <h5 class="card-header">
                            Products Available

                        </h5>
                        <div class="card-block ">
                        <div class="ks-statistics row">
                            

                            @if($project->productcategories->count() > 0)

                               @foreach($project->productcategories as $ProductCategory)

                               <div class="ks-item col-sm-4">
                                <div class="ks-amount">{{ $ProductCategory->products->count() }}</div>
                                <div class="ks-text">{{ $ProductCategory->product_category_name }}</div>
                            </div>

                                
                               @endforeach

                            @else
                                No Product Categories Available! &nbsp;&nbsp; <a href="{{ route('product-category.create') }}">Create Product Category</a>
                            @endif            
                          
                        </div>
                        </div>
                    </div>

                       <div id="ks-bar-chart-panel" class="card panel">
                        <h5 class="card-header">
                            Recent Orders
                        </h5>
                        <div class="card-block">
                           @if($project->orders->count() > 0)

                                        <table class="table table-condensed table=bordered table-hover">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>Order Code</th>
                                                    <th>Invoice Total</th>
                                                    <th>Created On</th>
                                                    <th>Manage</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($project->orders->sortByDesc('id')->take(5) as $Order)
                                                    <tr>
                                                        <td><strong>{{ $Order->code }}</strong></td>
                                                        <td>{{ $Order->invoice_total }}</td>
                                                        <td>{{ $Order->created_at }}</td>
                                                        <td><a href="{{ URL::action('OrderController@show',$Order->code) }}" class="btn btn-sm btn-primary" target="_blank">View</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>

                                        </table>

                                    @else
                                        No orders yet!
                                    @endif
                        </div>
                    </div>
                </div>


         

                <div class="col-lg-6">


                    <div id="ks-line-chart-panel" class="card panel">
                        <h5 class="card-header">
                            Project Details

                        </h5>
                        <div class="card-block">
                           
                                        <div class="ks-text-block">
                                        <dt>Project Name:</dt>
                                        <dd>{{ $project->proj_code }} - {{ $project->proj_name }}</dd>
                                     
                                        <dt>Address:</dt>
                                        <dd>{{ $project->address_one }}</dd>
                                        <dd>{{ $project->address_two }}</dd>
                                            
                                        <dt>City:</dt>
                                        <dd>{{ $project->city }}</dd>

                                        <dt>State:</dt>
                                        <dd>{{ $project->state }}</dd>

                                        <dt>Pin Code:</dt>
                                        <dd>{{ $project->pincode }}</dd>


                                        </div>
                                
                                    
                        </div>
                    </div>
                

                 
                </div>
                   <div class="col-lg-6">
                       
                   </div>
            </div>
            <div class="row ks-widgets-collection">
                
                    

            </div>

          
        </div>
    
                </div>
            </div>
        </div>
    </div>

    @endif

@endsection
