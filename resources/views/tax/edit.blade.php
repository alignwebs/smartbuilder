@extends('master')

@section('title','Edit Tax')

@section('pagecontent')



    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Edit Tax</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-4 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                   Edit Tax Details
                                    </h5>
                                    <div class="card-block">
                                    
                                    {{ Form::open(['method' => 'PUT','action'=>['TaxController@update', $tax->id]]) }}    

                                        <div class="form-group">
                                            {{ Form::text('name', $tax->name,['class'=>'form-control', 'placeholder'=>"Enter Tax Name", 'required'=>'']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::number('percent', $tax->percent,['class'=>'form-control', 'placeholder'=>"Enter Tax Percentage", 'required'=>'', 'min'=>'0','max'=>'100', 'step'=>'0.05']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::text('description', $tax->description,['class'=>'form-control', 'placeholder'=>"Enter Tax Description"]) }}
                                        </div>

                                        <br>

                                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                        
                                            
                                    {{ Form::close() }}
                                    </div>
                                </div>

                                

                            
                            
                        </div>

                         
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
