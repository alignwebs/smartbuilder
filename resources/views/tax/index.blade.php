@extends('master')

@section('title','Tax')

@section('pagecontent')



    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Tax</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-8 ks-panels-column-section">

                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                    Taxes List
                                    </h5>
                                    <div class="card-block">
                                        
                                        @if($Taxes->count() > 0)
                                            
                                            <table class="table table-condensed table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Tax Name</th>
                                                    <th>Percentage</th>
                                                    <th>Description</th>
                                                    <th width="100">Options</th>
                                                </tr>
                                            </thead>
                                            @foreach($Taxes as $tax)
                                                <tr>
                                                    <td>{{ $tax->name }}</td>
                                                    <td>{{ $tax->percent }} %</td>
                                                    <td>{{ $tax->description }}</td>
                                                    <td>{!! deleteEditModelBtn("tax",$tax->id, $tax->name) !!} </td>
                                                </tr>
                                            @endforeach
                                            </table>

                                        @endif
                                    
                                    </div>
                                </div>

                                

                            
                            
                        
                            </div>

                            <div class="col-lg-4 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                    Add New Tax
                                    </h5>
                                    <div class="card-block">
                                    
                                    {{ Form::open(['action'=>'TaxController@store']) }}    

                                        <div class="form-group">
                                            {{ Form::text('name', '',['class'=>'form-control', 'placeholder'=>"Enter Tax Name", 'required'=>'']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::number('percent', '',['class'=>'form-control', 'placeholder'=>"Enter Tax Percentage", 'required'=>'', 'min'=>'0','max'=>'100', 'step'=>'0.05']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::text('description', '',['class'=>'form-control', 'placeholder'=>"Enter Tax Description"]) }}
                                        </div>

                                        <br>

                                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                                        
                                            
                                    {{ Form::close() }}
                                    </div>
                                </div>

                                

                            
                            
                        </div>

                         
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
