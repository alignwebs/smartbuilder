   <!-- BEGIN HEADER -->
<nav class="navbar ks-navbar">
    <!-- BEGIN HEADER INNER -->
    <!-- BEGIN LOGO -->
    <div href="javascript:void(0)" class="navbar-brand">
        <!-- BEGIN RESPONSIVE SIDEBAR TOGGLER -->
        <a href="javascript:void(0)" class="ks-sidebar-toggle" style="display: inline !Important"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <a href="javascript:void(0)" class="ks-sidebar-mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <!-- END RESPONSIVE SIDEBAR TOGGLER -->
        <a href="javascript:void(0)" class="ks-logo">{{ config('app.name') }}</a>

    </div>
    <!-- END LOGO -->

    <!-- BEGIN MENUS -->
    <div class="ks-wrapper">
        <nav class="nav navbar-nav">
            <!-- BEGIN NAVBAR MENU -->
            <div class="ks-navbar-menu">
                
                @if(!empty(Session::get('selectedOrg')))
                <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                       {{ getOrganization('org_name') }}
                    </a>
                  @hasanyrole('admin|owner')  
                    <div class="dropdown-menu ks-info" aria-labelledby="Preview">
                        <a class="dropdown-item ks-active" href="{{ URL::route('OrgSelect') }}">Change Organization</a>
                        <a class="dropdown-item ks-active" href="{{ URL::route('organization.index') }}">List All Organizations</a>
                        <a class="dropdown-item ks-active" href="{{ URL::route('organization.create') }}">Add New Organization</a>
                    </div>
                  @endhasanyrole
                </div>


                @if(!empty(getProject('proj_name')))

                 <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                       {{ getProject('proj_name') }}
                    </a>
                     @hasanyrole('admin|owner')  
                    <div class="dropdown-menu ks-info" aria-labelledby="Preview">
                        <a class="dropdown-item ks-active" href="{{ URL::route('ProjectSelect') }}">Change Project</a>
                        <a class="dropdown-item ks-active" href="{{ URL::route('project.index') }}">List All Projects</a>
                        <a class="dropdown-item ks-active" href="{{ URL::route('project.create') }}">Add New Projects</a>
                    </div>
                    @endhasanyrole
                </div>
                
                @endif

                @else
                 <div class="nav-item nav-link ">
                    <a href="{{ URL::to('home') }}" class="btn btn-success">Select Organization</a>
                </div>
                @endif


            </div>
            <!-- END NAVBAR MENU -->

            <!-- BEGIN NAVBAR ACTIONS -->
            <div class="ks-navbar-actions">
        
                <!-- BEGIN NAVBAR USER -->
                <div class="nav-item dropdown ks-user">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-avatar">
                            <img src="assets/img/avatars/placeholders/ava-64.png" width="36" height="36">
                        </span> &nbsp;&nbsp;
                        {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                      
                         <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                             <span class="fa fa-sign-out ks-icon" aria-hidden="true"></span>
                            <span>Logout</span>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                       
                    </div>
                </div>
                <!-- END NAVBAR USER -->
            </div>
            <!-- END NAVBAR ACTIONS -->
        </nav>

        <!-- BEGIN NAVBAR ACTIONS TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-actions-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="fa fa-ellipsis-h ks-icon ks-open"></span>
                <span class="fa fa-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR ACTIONS TOGGLER -->

        <!-- BEGIN NAVBAR MENU TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-menu-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="fa fa-th ks-icon ks-open"></span>
                <span class="fa fa-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR MENU TOGGLER -->
    </div>
    <!-- END MENUS -->
    <!-- END HEADER INNER -->
</nav>
<!-- END HEADER -->