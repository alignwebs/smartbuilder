

<!-- BEGIN DEFAULT SIDEBAR -->

<div class="ks-column ks-sidebar ks-info">

    <div class="ks-wrapper">

        

        @if(!empty(getOrganization('org_logo')))

        <div class="org-logo">

            <img src="{{ asset('storage/'.getOrganization('org_logo')) }}" alt="">

        </div>

        @endif



        <ul class="nav nav-pills nav-stacked">

            



            <li class="nav-item">

                <a class="nav-link"  href="{{ URL::Route('home') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Dashboard</span>

                </a>

                

            </li>







            

            <li class="nav-item">

                <a class="nav-link"  href="{{ URL::Action('OrderController@index') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Orders</span>

                </a>

                

            </li>



            <li class="nav-item">

                <a class="nav-link"  href="{{ route('material-transfer.index') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Material Transfer</span>

                </a>

                

            </li>



            <li class="nav-item">

                <a class="nav-link"  href="{{ route('inventory.index') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Inventory</span>

                </a>

                

            </li>



            <li class="nav-item">

                <a class="nav-link"  href="{{ URL::Route('stages.index') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Stages</span>

                </a>

                

            </li>

            

            <li class="nav-item">

                <a class="nav-link"  href="{{ URL::Route('customer.index') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Customers</span>

                </a>

                

            </li>





            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Products</span>

                </a>

                

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::Action('ProductController@index') }}">Manage Products</a>

                    <a class="dropdown-item" href="{{ URL::Action('ProductPropertyController@create') }}">Product Properties</a>

                    <a class="dropdown-item" href="{{ URL::Action('ProductPropertyHeaderController@index') }}">Product Property Headers</a>

                    

                    

                    <a class="dropdown-item" href="{{ URL::Action('ProductCategoryController@create') }}">Product Categories</a>

                </div>

            </li>



            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Project Payments</span>

                </a>

                

                <div class="dropdown-menu">

                    <a class="dropdown-item ks-active" href="{{ route('payments.index') }}">View All</a>          

                    <a class="dropdown-item ks-active" href="{{ route('material-payment') }}">Material Payment</a>   

                    <a class="dropdown-item ks-active" href="{{ route('contractor-payment') }}">Contractor Payment</a>

                    <a class="dropdown-item ks-active" href="{{ route('service-payment') }}">Service Payment</a>

                    <a class="dropdown-item ks-active" href="{{ route('external-payment') }}">External Payment</a>

                    <a class="dropdown-item ks-active" href="{{ route('external-income') }}">External Receipts</a>

                    <a class="dropdown-item ks-active" href="{{ route('customer-payment') }}">Customer Payment</a>

                </div>

                

                

            </li>


            <li class="nav-item">

                <a class="nav-link"  href="{{ URL::Route('reports') }}" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Reports</span>

                </a>

                

            </li>


            



            @hasanyrole('admin|owner')

            

            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Suppliers</span>

                </a>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::route('supplier.create') }}">Add New</a>

                    <a class="dropdown-item" href="{{ URL::route('supplier.index') }}">List All</a>

                </div>

            </li>

            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Contractors</span>

                </a>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::route('contractor.create') }}">Add New</a>

                    <a class="dropdown-item" href="{{ URL::route('contractor.index') }}">List All</a>

                </div>

            </li>



            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Departments</span>

                </a>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::route('department.create') }}">Add New</a>

                    <a class="dropdown-item" href="{{ URL::route('department.index') }}">List All</a>

                </div>

            </li>



            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Items</span>

                </a>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::route('item.create') }}">Add New</a>

                    <a class="dropdown-item" href="{{ URL::route('item.index') }}">List All</a>

                </div>

            </li>



            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>System</span>

                </a>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::route('tax.index') }}">Tax</a>

                </div>

                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::route('staff.index') }}">Staff</a>

                </div>



                <div class="dropdown-menu">

                    <a class="dropdown-item" href="{{ URL::Action('ProjectManagerController@index') }}">Project Managers</a>

                </div>

                

                

            </li>



            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle"  href="javascript:void(0)" role="button" aria-haspopup="true" aria-expanded="false">

                 

                    <span>Accounts</span>

                </a>

                

                <div class="dropdown-menu">

                    <a class="dropdown-item ks-active" href="{{ URL::route('bank-accounts.index') }}">Bank Accounts</a>                

                </div>

                

                

            </li>



            



            @else





            @endhasanyrole



        </ul>

    </div>

</div>

<!-- END DEFAULT SIDEBAR -->