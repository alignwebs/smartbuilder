@extends('master')

@section('title','Payment')

@section('pagecontent')

	<style>
		#bank-details { text-align: center; }
		#bank-details .title { font-size: 18px }
		#bank-details .balance { font-size: 22px }


		#supplier-stats { display: none }
	</style>


  <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Payment: #{{ $payment->id }}</h3>
                <div class="pull-right">
                <a href="{{ route('voucher',$payment->id) }}" target="_blank" class="btn btn-default">View Voucher</a>&nbsp;&nbsp;
                <a href="{{ route('receipt',$payment->id) }}" target="_blank" class="btn btn-default">View Receipt</a>
                	
                </div>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<iframe src="{{ route('receipt',$payment->id) }}" style="width:100%; height: 550px" frameborder="0"></iframe>
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')
	
	<script></script>

@endpush