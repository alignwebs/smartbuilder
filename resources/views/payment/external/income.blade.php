@extends('master')

@section('title','New External Income')

@section('pagecontent')

	<style>
		#bank-details {} }
		#bank-details .title { font-size: 18px }
		#bank-details .balance { font-size: 15px }


	</style>


  <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>New External Income</h3>
                <button type="button" onclick="pay()" class="btn btn-primary pull-right">Submit</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')
						
						{{ Form::open(['id'=>'payment_frm']) }}
						
						<div class="row">
							

							<div class="col-lg-5">
							
						<!-- Bank NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Primary Details</h5>
			                         <div class="form-group row">
			                                    <label for="bank_account" class="col-sm-4 form-control-label">Bank Account</label>
			                                    <div class="col-sm-8">
													{{ Form::select('bank_account',$BankAccounts, null, ['id'=>'bank_account','class'=>'form-control', 'placeholder' => 'Select Bank Account', 'onchange'=>'getBankBalance(this)']) }}
			                                    </div>
			                           </div>

			                           <div class="form-group row">
			                                    <label for="" class="col-sm-4 form-control-label">Bank Balance</label>
			                                    <div class="col-sm-8">
												 <div id="bank-details">
			                    
			                        	<div class="balance">Rs. 0/-</div>
			                        </div>

			                                    </div>
			                           </div>

			                          
		                            	<div class="form-group row">
		                                    <label for="payment_type" class="col-sm-4 form-control-label">Payment Type</label>
		                                    <div class="col-sm-8">
												{{ Form::select('payment_type',$PaymentTypes, $payment_type_id, ['class'=>'form-control', 'placeholder' => 'Select Payment Type']) }}
		                                    </div>
		                                </div>
										
										<div class="form-group row">
		                                    <label for="payment_method" class="col-sm-4 form-control-label">Payment Method</label>
		                                    <div class="col-sm-8">
												{{ Form::select('payment_method',$PaymentMethods, null, ['id'=>'payment_method','class'=>'form-control', 'placeholder' => 'Select Payment Method']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="payment_method_ref" class="col-sm-4 form-control-label">Payment Reference No.</label>
		                                    <div class="col-sm-8">
												{{ Form::text('payment_method_ref', '', ['class'=>'form-control', 'id'=>'payment_method_ref']) }}
		                                    </div>	
		                                </div>


		                                <div class="form-group row">
		                                    <label for="total_payable_amount" class="col-sm-4 form-control-label">Receieved Amount</label>
		                                    <div class="col-sm-8">
												{{ Form::number('total_payable_amount', '', ['class'=>'form-control', 'id'=>'total_payable_amount']) }}
		                                    </div>	
		                                </div>

		                                 
		                                <div class="form-group row">
		                                    <label for="date" class="col-sm-4 form-control-label">Date</label>
		                                    <div class="col-sm-8">
												{{ Form::text('date', date('Y-m-d'), ['class'=>'form-control', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=> date('Y-m-d') , 'required'=>'']) }}
		                                    </div>	
		                                </div>
		                            	



		                        </div>
		                    </div>

		                    <!-- END Bank NAME BLOCK -->
							
			            </div>


			            <div class="col-md-7">
			            	
			            	<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">External Details</h5>
			                         <div class="form-group row">
			                                    <label for="External_payment_type" class="col-sm-4 form-control-label">Purpose</label>
			                                    <div class="col-sm-8">
													{{ Form::text('purpose', '', ['id'=>'purpose','class'=>'form-control']) }}
			                                    </div>
			                         </div>

			                         <div class="form-group row">
		                                    <label for="paid_to" class="col-sm-4 form-control-label">Receieved From:</label>
		                                    <div class="col-sm-8">
												{{ Form::text('paid_by', '', ['class'=>'form-control']) }}
		                                    </div>	
		                                </div>



			                         <div class="form-group row">
		                                    <label for="approved_by" class="col-sm-4 form-control-label">Approved By:</label>
		                                    <div class="col-sm-8">
												{{ Form::text('approved_by', '', ['class'=>'form-control']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row">
		                                    <label for="remark" class="col-sm-4 form-control-label">Remark (if any)</label>
		                                    <div class="col-sm-8">
												{{ Form::text('remark', '', ['class'=>'form-control']) }}
		                                    </div>	
		                                </div>




		                        </div>
		                    </div>

			            </div>


						</div>
						{{ Form::hidden('type', 'transaction') }}
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')
	
	<script>

		function getBankBalance(e)
		{
			var bank_id = $(e).val();

			$.getJSON("{{ route('banks-ajax') }}", {type:"account_details", id:bank_id}, function (json) {

				$('#bank-details .title').html(json.name+" - "+json.bank_name+" - "+json.account_number);

				$('#bank-details .balance').html(json.balance);
				
				reinitSelect()

			});
		}

	
		function pay()
		{
			
			if($('#bank_account').val() < 1)
				alert("Select a Bank Account");
			else if($('#payment_method').val() < 1)
				alert("Select a Payment Method");
			else if($('#purpose').val() == "")
				alert("Enter purpose");
			else if($('#total_payable_amount').val() < 1)
				alert("Enter Payable Amount");
			else if($('#bank_account').val() > 0) {

					swal({
					  title: "Proceed with Payment?",
					  text: "Press OK to Pay or press cancel.",
					  type: "info",
					  showCancelButton: true,
					  closeOnConfirm: false,
					  showLoaderOnConfirm: true
					}, function () {

					    var success = swal({
							  title: "Transaction Successful!",
							  text: "",
							  type: "success",
							  confirmButtonClass: "btn-success"
							})

					    paymentSubmit()
			 
					});

			}
		}

		function paymentSubmit()
		{
			var form = $('#payment_frm');

			var fields = form.serialize();

			var url = "{{ route('payments-pay') }}";

			$.post(url,fields, function (data) {

				window.location.href = data;
			})

		}

	

		function reinitSelect()
		{
			$('select').select2();
		}

	</script>

@endpush