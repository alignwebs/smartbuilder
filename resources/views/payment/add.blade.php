@extends('master')

@section('title','New Payment')

@section('pagecontent')

	<style>
		#bank-details { text-align: center; }
		#bank-details .title { font-size: 18px }
		#bank-details .balance { font-size: 22px }


		#supplier-stats { display: none }
	</style>


  <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Make New Payment</h3>
                <button type="button" onclick="paymentSubmit()" class="btn btn-success pull-right">Submit</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')
						
						{{ Form::open(['id'=>'payment_frm']) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- Bank NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Primary Details</h5>
			                         <div class="form-group row">
			                                    <label for="suplr_name" class="col-sm-4 form-control-label">Bank Account</label>
			                                    <div class="col-sm-8">
													{{ Form::select('bank_account',$BankAccounts, null, ['class'=>'form-control', 'placeholder' => 'Select Bank Account', 'onchange'=>'getBankBalance(this)']) }}
			                                    </div>
			                           </div>

		                            	<div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Payment Type</label>
		                                    <div class="col-sm-8">
												{{ Form::select('payment_type',$PaymentTypes, null, ['class'=>'form-control', 'placeholder' => 'Select Payment Type', 'onchange'=>'getSubTypes(this)']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Payment Sub Type</label>
		                                    <div class="col-sm-8 paymentsubtype">
												{{ Form::select('payment_sub_type',[], null, ['class'=>'form-control', 'placeholder' => 'Select Payment Sub Type']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label"></label>
		                                    <div class="col-sm-8 subtypes_prop">
												
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label"></label>
		                                    <div class="col-sm-8 subtypes_prop2">
												
		                                    </div>
		                                </div>
		                            	
		                               <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Payable Amount</label>
		                                    <div class="col-sm-8">
												{{ Form::number('payable_amount', '', ['class'=>'form-control', 'id'=>'payable_amount', 'readonly'=>'true']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row advance_amount" >
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Advance Amount</label>
		                                    <div class="col-sm-8">
												{{ Form::number('advance_amount', '', ['class'=>'form-control', 'id'=>'advance_amount']) }}
		                                    </div>	
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Remark (if any)</label>
		                                    <div class="col-sm-8">
												{{ Form::text('remark', '', ['class'=>'form-control']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Date</label>
		                                    <div class="col-sm-8">
												{{ Form::text('date', date('Y-m-d'), ['class'=>'form-control', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=> date('Y-m-d') , 'required'=>'']) }}
		                                    </div>	
		                                </div>
		                            	



		                        </div>
		                    </div>

		                    <!-- END Bank NAME BLOCK -->

							
			            </div>

			            <div class="col-md-6">
			            	

						<!-- Bank NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Selected Bank Account Details</h5>
			                        <div id="bank-details">
			                        	<div class="title">Select Bank Account</div>
			                        	<div class="balance">Rs. 0/-</div>
			                        </div>
		                        </div>
		                    </div>

		                    <!-- END Bank NAME BLOCK -->


		                    <div class="card" id="supplier-stats">
		                        <div class="card-block">
		                            <h5 class="card-title">Supplier Payments Stats</h5>
			                        <div id="target">
			                        	<div class="row">
			                        		<div class="col-sm-4">
			                        			<label>Due Amount:</label>
			                        			<div class="due"></div>
			                        		</div>
			                        		<div class="col-sm-4">
			                        			<label>Paid Amount:</label>
			                        			<div class="paid"></div>
			                        		</div>
			                        		<div class="col-sm-4">
			                        			<label>Total Amount:</label>
			                        			<div class="total"></div>
			                        		</div>
			                        		<div class="col-sm-4">
			                        			<label>Advance Amount:</label>
			                        			<div class="advance"></div>
			                        		</div>
			                        	</div>
			                        </div>
		                        </div>
		                    </div>


							
			            
			            </div>

						</div>
						{{ Form::hidden('type', 'transaction') }}
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')
	
	<script>

	hideAdvanceText()


		function getSubTypes(e)
		{
			var payment_type = $(e).val();

			$.get("{{ route('payments-ajax') }}", {type:"subtypes", id:payment_type}, function (data) {

				resetSubType()
				$('.paymentsubtype').html(data)

				
				reinitSelect()

			});
		}

		function getSubTypeProp(e)
		{
			var sub_payment_type = $(e).val();


			$.get("{{ route('payments-ajax') }}", {type:"fetch_subtypes_prop", id:sub_payment_type}, function (data) {
				
				resetSubType()

				$('.subtypes_prop').html(data)


				reinitSelect()

			});
		}


		function getBankBalance(e)
		{
			var bank_id = $(e).val();

			$.getJSON("{{ route('banks-ajax') }}", {type:"account_details", id:bank_id}, function (json) {

				$('#bank-details .title').html(json.name+" - "+json.bank_name+" - "+json.account_number);

				$('#bank-details .balance').html(json.balance);
				

				reinitSelect()

			});
		}

		function getOrderSupplier(e)
		{
			var id = $(e).val();

			$.get("{{ route('order-ajax') }}", {type:"suppliersByOrder", id:id}, function (data) {

				
				$('.subtypes_prop2').html(data)
				

				reinitSelect()

			});
		}

		function getOrderGrn(e)
		{
			$.get("{{ route('order-ajax') }}", {type:"ordersByGrn"}, function (data) {

				
				$('.subtypes_prop').html(data)
				

				reinitSelect()

			});
		}



		function getSupplierStats(e)
		{
			var id = $(e).val();

			var order_id = $('select[name="order"]').val();

			

			$.getJSON("{{ route('payments-ajax') }}", {type:"supplier-stats", id:id, order:order_id}, function (json) {
				
					var grn_total	=	json.supplierStats.grn_total;
					var grn_paid	=	json.supplierStats.grn_paid;
					var grn_due		=	json.supplierStats.grn_due;

					var order_total		=	json.supplierStats.order_total;
					var order_due		=	json.supplierStats.order_due;
					var order_paid		=	json.supplierStats.order_paid;

					var advance		=	json.supplierStats.advance;

					if(order_id != "")
					{
						$('#supplier-stats .due').html(order_due);
						$('#supplier-stats .paid').html(order_paid);
						$('#supplier-stats .total').html(order_total);
						$('#supplier-stats .advance').html(advance);
						
						setPayableAmount(order_due);
					}
					else
					{
						$('#supplier-stats .due').html(grn_due);
						$('#supplier-stats .paid').html(grn_paid);
						$('#supplier-stats .total').html(grn_total);
						$('#supplier-stats .advance').html(advance);
						
						setPayableAmount(grn_due);
						
					}

					
					showAdvanceText();

					$('#supplier-stats').fadeIn()

			});
		}

		function paymentSubmit()
		{
			var form = $('#payment_frm');

			var fields = form.serialize();

			var url = "{{ route('payments-ajax') }}";

			$.post(url,fields, function (data) {

				 if(data.transaction_id != "")
				 {
				 	window.location.href = "{{ route('payments-transaction') }}/"+transaction_id;
				 }
				 else
				 {
				 	alert('something went wrong')
				 }

			})

		}

		function showAdvanceText()
		{
			$('.advance_amount').val(0).show()
		}

		function hideAdvanceText()
		{
			$('.advance_amount').val(0).hide()
		}


		function setPayableAmount(amount)
		{

			$('#payable_amount').val(parseFloat(amount).toFixed(2))
		}

		function reinitSelect()
		{
			$('select').select2();
		}

		function resetSubType()
		{
			$('.subtypes_prop').html("")
				$('.subtypes_prop2').html("")

				hideAdvanceText()
		}
	</script>

@endpush