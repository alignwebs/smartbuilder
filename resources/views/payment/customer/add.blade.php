@extends('master')

@section('title','New Customer Payment')

@section('pagecontent')

	<style>
		#bank-details {} }
		#bank-details .title { font-size: 18px }
		#bank-details .balance { font-size: 15px }


		#upgrades-container { clear: both; margin-top: 20px; }
	</style>


  <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>New Customer Payment</h3>
                <button type="button" onclick="pay()" class="btn btn-primary pull-right">Submit</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')
						
						{{ Form::open(['id'=>'payment_frm']) }}
						
						<div class="row">
							

							<div class="col-lg-5">
							
						<!-- Bank NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Primary Details</h5>
			                         <div class="form-group row">
			                                    <label for="bank_account" class="col-sm-4 form-control-label">Bank Account</label>
			                                    <div class="col-sm-8">
													{{ Form::select('bank_account',$BankAccounts, null, ['id'=>'bank_account','class'=>'form-control', 'placeholder' => 'Select Bank Account', 'onchange'=>'getBankBalance(this)']) }}
			                                    </div>
			                           </div>

			                           <div class="form-group row">
			                                    <label for="" class="col-sm-4 form-control-label">Bank Balance</label>
			                                    <div class="col-sm-8">
												 <div id="bank-details">
			                    
			                        	<div class="balance">Rs. 0/-</div>
			                        </div>

			                                    </div>
			                           </div>

			                          
		                            	<div class="form-group row">
		                                    <label for="payment_type" class="col-sm-4 form-control-label">Payment Type</label>
		                                    <div class="col-sm-8">
												{{ Form::select('payment_type',$PaymentTypes, $payment_type_id, ['class'=>'form-control', 'placeholder' => 'Select Payment Type']) }}
		                                    </div>
		                                </div>
										
										<div class="form-group row">
		                                    <label for="payment_method" class="col-sm-4 form-control-label">Payment Method</label>
		                                    <div class="col-sm-8">
												{{ Form::select('payment_method',$PaymentMethods, null, ['id'=>'payment_method','class'=>'form-control', 'placeholder' => 'Select Payment Method']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="payment_method_ref" class="col-sm-4 form-control-label">Payment Reference No.</label>
		                                    <div class="col-sm-8">
												{{ Form::text('payment_method_ref', '', ['class'=>'form-control', 'id'=>'payment_method_ref']) }}
		                                    </div>	
		                                </div>


		                               <div class="form-group row">
		                                    <label for="payable_amount" class="col-sm-4 form-control-label">Payable Amount</label>
		                                    <div class="col-sm-8">
												{{ Form::number('payable_amount', '', ['class'=>'form-control', 'id'=>'payable_amount', 'readonly'=>'true']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row">
		                                    <label for="total_payable_amount" class="col-sm-4 form-control-label">Total Payable Amount</label>
		                                    <div class="col-sm-8">
												{{ Form::number('total_payable_amount', '', ['class'=>'form-control', 'id'=>'total_payable_amount']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row">
		                                    <label for="paid_to" class="col-sm-4 form-control-label">Paid To:</label>
		                                    <div class="col-sm-8">
												{{ Form::text('paid_to', '', ['class'=>'form-control']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row">
		                                    <label for="remark" class="col-sm-4 form-control-label">Remark (if any)</label>
		                                    <div class="col-sm-8">
												{{ Form::text('remark', '', ['class'=>'form-control']) }}
		                                    </div>	
		                                </div>

		                                <div class="form-group row">
		                                    <label for="date" class="col-sm-4 form-control-label">Date</label>
		                                    <div class="col-sm-8">
												{{ Form::text('date', date('Y-m-d'), ['class'=>'form-control', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=> date('Y-m-d') , 'required'=>'']) }}
		                                    </div>	
		                                </div>
		                            	



		                        </div>
		                    </div>

		                    <!-- END Bank NAME BLOCK -->
								
					
							
			            </div>


			            <div class="col-md-7">
			            	
			            	<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Customer Details</h5>
			                         <div class="form-group row">
			                                    <label for="supplier_id" class="col-sm-4 form-control-label">Customer</label>
			                                    <div class="col-sm-8">
													{{ Form::select('customer_id',$Customers, null, ['class'=>'form-control', 'placeholder' => 'Select Customer', 'onchange'=>'getCustomerProducts(this)']) }}
			                                    </div>
			                         </div>

			                         <div class="form-group row">
			                                    <label for="supplier_id" class="col-sm-4 form-control-label">Product</label>
			                                    <div class="col-sm-8">
													 <div id="products_container"></div>
			                                    </div>
			                         </div>

			                           <div class="form-group row">
			                                    <label for="supplier_id" class="col-sm-4 form-control-label">Type</label>
			                                    <div class="col-sm-8">
													{{ Form::select('prod_pay_type',['product'=>'Product Payment', 'upgrade' => 'Upgrade Charges'], null, ['class'=>'form-control','id'=>'type', 'placeholder' => 'Select Customer', 'onchange'=>'getProductPaymentType(this)']) }}
			                                    </div>
			                         </div>

			                         <div id="upgrades-container"></div>

										
			                          

		                        </div>
		                    </div>

			            </div>


						</div>
						{{ Form::hidden('type', 'transaction') }}
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')
	
	<script>

		var items = [];

		function getBankBalance(e)
		{
			var bank_id = $(e).val();

			$.getJSON("{{ route('banks-ajax') }}", {type:"account_details", id:bank_id}, function (json) {

				$('#bank-details .title').html(json.name+" - "+json.bank_name+" - "+json.account_number);

				$('#bank-details .balance').html(json.balance);
				

				reinitSelect()

			});
		}

		function getCustomerProducts(e)
		{
			var id = $(e).val();
			
			$.get("{{ route('customer-products-combo') }}", { 'customer_id':id}, function (data) {
				
				$('#products_container').html(data);

				reinitSelect();
					
			});
			
		}

		function getProductPaymentType(e)
		{
			var type = $(e).val();

			$('#upgrades-container').html('')

			if(type == 'product')
			{
				var product_id = $('#product_id').val();

				$.get('{{ route('product-cost') }}', {product_id:product_id}, function (data) {

					setPayableAmount($.trim(data));
					setTotalPayableAmount()

				})
			}
			else if(type == 'upgrade')
			{
				var product_id = $('#product_id').val();

				$.get('{{ route('product-upgrades') }}', {product_id:product_id}, function (data) {

					$('#upgrades-container').html(data)

				})
			}
		}

	
		function updateItems()
		{
			items = [];

			$('#upgrades-container table .item:checked').each(function () {

					var taskid = $(this).attr('data-taskid');
					var price = $(this).attr('data-price');

					var item = {"taskid":taskid, "price":price};

					items.push(item)

			})

			 calculateTotal()

			//console.log(items)
		}

		function calculateTotal()
		{
			var total = 0;

			  $.each(items, function (index,value) { 

			  		total = +total + +value.price;

			  })


			 setPayableAmount(total)
		}

		function pay()
		{
			
			if($('#bank_account').val() < 1)
				alert("Select a Bank Account");
			else if($('#payment_method').val() < 1)
				alert("Select a Payment Method");
			else if($('#prod_pay_type').val() == 'upgrade' && items.length < 1)
			{
					alert("Select a Upgrade Items");
			}
			else if($('#bank_account').val() > 0 && $('#payable_amount').val() > 0) {

			

					swal({
					  title: "Proceed with Payment?",
					  text: "Press OK to Pay or press cancel.",
					  type: "info",
					  showCancelButton: true,
					  closeOnConfirm: false,
					  showLoaderOnConfirm: true
					}, function () {

					    var success = swal({
							  title: "Transaction Successful!",
							  text: "",
							  type: "success",
							  confirmButtonClass: "btn-success"
							})

					    paymentSubmit()
			 
					});

			}
		}

		function paymentSubmit()
		{
			var form = $('#payment_frm');

			var fields = form.serialize();

			var data = JSON.stringify(items);

			var url = "{{ route('payments-pay') }}";

			$.post(url,fields+"&items="+data, function (data) {

				window.location.href = data;
			})

		}

		function setTotalPayableAmount()
		{
			var payable = $('#payable_amount').val();
			
			$('#total_payable_amount').val(payable)
			
		}


	
		function setPayableAmount(amount)
		{
			$('#payable_amount').val(parseFloat(amount).toFixed(2))
			setTotalPayableAmount()
		}

		function reinitSelect()
		{
			$('select').select2();
		}

		function resetSubType()
		{
			$('.subtypes_prop').html("")
				$('.subtypes_prop2').html("")

				hideAdvanceText()
		}

		$('#payable_amount, #advance_amount').on('change', function () {
			setTotalPayableAmount()
		})
	</script>

@endpush