@extends('master')

@section('title','Payments')

@section('pagecontent')


  <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Payments Transactions:</h3>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">

						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">List of all the payment transactions of this project</h5>

		                            @if($Payments->count() > 0)

							<table class="table datatable">
								<thead>
									<tr>
										<th>Payment ID</th>
										<th>Bank</th>
										<th>Payment Type</th>
										<th>Credit/Debit</th>
										<th>Amount</th>
										<th>Remark</th>
										<th>Date</th>
		
									</tr>
								</thead>
						
								<tbody>
									@foreach($Payments as $Payment)

										<tr>
											<td><a href="{{ route('show-payment', $Payment->id) }}">{{ @$Payment->gen_payment_id }}</a></td>
											<td>{{ @$Payment->bank->name }}</td>
											<td>{{ @$Payment->paymentType->name }}</td>
											<td>{{ @$Payment->type }}</td>
											<td>{{ @currency($Payment->amount) }}</td>
											<td>{{ @$Payment->remark }}</td>
											<td>{{ @$Payment->date }}</td>
										</tr>
					
									@endforeach
								</tbody>

							</table>
						@else


						@endif
		                        </div>
		                </div>
						
						
						
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')
	
	<script></script>

@endpush