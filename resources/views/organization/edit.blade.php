@extends('master')

@section('title','Edit Organization')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">
                <h3>Edit Organization: {{ $organization['org_name'] }}</h3>
                <button type="button" onclick="$('#org_add_frm').submit()" class="btn btn-success pull-right">Update</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['method'=>'PUT','action' => ['OrganizationController@update', $organization->id], 'id'=>'org_add_frm', 'files'=>true]) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- ORGANIZATION NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Organization Basic Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="org_code" class="col-sm-3 form-control-label">Org Code</label>
		                                    <div class="col-sm-9">
												{{ Form::text('org_code', $organization->org_code,['class'=>'form-control', 'disabled'=>'disabled']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="org_name" class="col-sm-3 form-control-label">Org Name</label>
		                                    <div class="col-sm-9">
												{{ Form::text('org_name', $organization->org_name,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_logo" class="col-sm-3 form-control-label">Org Logo</label>
		                                    <div class="col-sm-9">
												{{ Form::file('org_logo', ['class'=>'form-control']) }}
		                                    </div>
		                                </div>



		                        </div>
		                    </div>

		                    <!-- END ORGANIZATION NAME BLOCK -->

		                    <!-- ORGANIZATION OTHER DETAILS BLOCK -->
		                    <div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Other Details</h5>
		                            

		                                 <div class="form-group row">
		                                    <label for="org_other_tin" class="col-sm-3 form-control-label">TIN</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_other_tin',$organization->org_tin,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_other_billing" class="col-sm-3 form-control-label">Billing Address</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_other_billing',$organization->org_billing_address,['class'=>'form-control']) }}
		                                    </div>
		                                </div>


		                        </div>
		                    </div>
		                    <!-- END ORGANIZATION OTHER DETAILS BLOCK -->

			            </div>

			            <div class="col-lg-6">

			            <!-- ORGANIZATION DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Organization Address Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="org_address_one" class="col-sm-3 form-control-label">Address 1</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_address_one',$organization->org_address_one,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_address_two" class="col-sm-3 form-control-label">Address 2</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_address_two',$organization->org_address_two,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_city" class="col-sm-3 form-control-label">City</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_city',$organization->org_city,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_district" class="col-sm-3 form-control-label">District</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_district',$organization->org_district,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="org_state" class="col-sm-3 form-control-label">State</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::select('org_state', $indiaStates, $organization->org_state, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_pincode" class="col-sm-3 form-control-label">Pin Code</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_pincode',$organization->org_pincode,['class'=>'form-control']) }}
		                                    </div>
		                                </div>





		                         
		                        </div>
		                    </div>
							  <!-- END ORGANIZATION DETAILS BLOCK -->

							


			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection