@extends('master')

@section('title','List Organizations')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Organizations</h3>
            
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $organizations->count() }} Organizations in your user account</h5>
									
									@if($organizations->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													<th>Org Code</th>
													<th>Org Name</th>
													<th>Projects</th>
													<th>City</th>
													<th>District</th>
													<th>State</th>
													
												</tr>
											</thead>

											<tbody>
												@foreach($organizations as $organization)
													<tr>
														<td><strong>{{ $organization->org_code }}</strong></td>
														<td><a href="{{ URL::action('OrganizationController@show',$organization->id) }} }}"><strong>{{ $organization->org_name }}</strong></a></td>
														<td>{{ $organization->projects->count() }} Projects</td>
														<td>{{ $organization->org_city }}</td>
														<td>{{ $organization->org_district }}</td>
														<td>{{ $organization->org_state }}</td>
														
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast One organization
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection