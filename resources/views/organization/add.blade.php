@extends('master')

@section('title','Add Organization')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Add Organization</h3>
                <button type="button" onclick="$('#org_add_frm').submit()" class="btn btn-success pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => 'OrganizationController@store', 'id'=>'org_add_frm', 'files'=>true]) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- ORGANIZATION NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Organization Basic Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="org_code" class="col-sm-3 form-control-label">Org Code</label>
		                                    <div class="col-sm-9">
												{{ Form::text('org_code', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="org_name" class="col-sm-3 form-control-label">Org Name</label>
		                                    <div class="col-sm-9">
												{{ Form::text('org_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="org_logo" class="col-sm-3 form-control-label">Org Logo</label>
		                                    <div class="col-sm-9">
												{{ Form::file('org_logo', ['class'=>'form-control']) }}
		                                    </div>
		                                </div>



		                        </div>
		                    </div>

		                    <!-- END ORGANIZATION NAME BLOCK -->

							<!-- ORGANIZATION CONTACT PERSON BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Contact Person</h5>
		                          
		                            	
		                            	 <div class="form-group row">
		                                    <label for="org_contact_name" class="col-sm-3 form-control-label">Name</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_contact_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                 <div class="form-group row">
		                                    <label for="org_contact_phone" class="col-sm-3 form-control-label">Phone</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_contact_phone','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                <div class="form-group row">
		                                    <label for="org_contact_email" class="col-sm-3 form-control-label">Email</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::email('org_contact_email','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                 <div class="form-group row">
		                                    <label for="org_contact_district" class="col-sm-3 form-control-label">District</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_contact_district','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                   <div class="form-group row">
		                                    <label for="org_contact_state" class="col-sm-3 form-control-label">State</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::select('org_contact_state', $indiaStates, null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_contact_pincode" class="col-sm-3 form-control-label">Pin Code</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_contact_pincode','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>


		                        </div>
		                    </div>
		                    <!-- END ORGANIZATION CONTACT PERSON BLOCK -->

			            </div>

			            <div class="col-lg-6">

			            <!-- ORGANIZATION DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Organization Address Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="org_address_one" class="col-sm-3 form-control-label">Address 1</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_address_one','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_address_two" class="col-sm-3 form-control-label">Address 2</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_address_two','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_city" class="col-sm-3 form-control-label">City</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_city','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_district" class="col-sm-3 form-control-label">District</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_district','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="org_state" class="col-sm-3 form-control-label">State</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::select('org_state', $indiaStates, null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_pincode" class="col-sm-3 form-control-label">Pin Code</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_pincode','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>





		                         
		                        </div>
		                    </div>
							  <!-- END ORGANIZATION DETAILS BLOCK -->

							 <!-- ORGANIZATION OTHER DETAILS BLOCK -->
		                    <div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Other Details</h5>
		                            

		                                 <div class="form-group row">
		                                    <label for="org_other_tin" class="col-sm-3 form-control-label">TIN</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_other_tin','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="org_other_billing" class="col-sm-3 form-control-label">Billing Address</label>
		                                    <div class="col-sm-9">
		                                       	{{ Form::text('org_other_billing','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>


		                        </div>
		                    </div>
		                    <!-- END ORGANIZATION OTHER DETAILS BLOCK -->


			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection