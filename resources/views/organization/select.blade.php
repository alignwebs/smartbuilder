@extends('master')

@section('title','Select Organization')

@section('pagecontent')
       <div class="ks-page">
    
    <div class="ks-body">
       
        <div class="card panel panel-default light ks-panel ks-confirm">
            <div class="card-block">
                <div class="ks-header" style="margin-top:0 ">Select an Organization</div>
                <div class="ks-description">

                    <div class="form-group">
                            <select class="form-control ks-select" id="orgList" required></select>
                    </div>

                    <button type="button" class="btn btn-primary" onclick="setOrganization()">GO</button>
                    <hr>

                    <a href="{{ URL::action('OrganizationController@create') }}" class="btn btn-block btn-default">Add New Organization</a>
                 
                </div>  
            </div>
        </div>

        
    </div>
    
</div>
@endsection

@push('scripts')

    <script>
        $(document).ready(function () {
            organizationList()
        } )

        function organizationList()
        {
            $.get('{{ URL::route('OrganizationWithID') }}', function (json) {

                $.each(json, function (i, item){

                    $('<option value="'+item.id+'">'+item.org_name+' - '+item.org_code+'</option>').appendTo('#orgList')
                    
                })

            })

            $('#orgList').select2();
        }

        function setOrganization()
        {
            var org_id = $('#orgList').val();

            if(org_id > 0)
            {
                $.post('{{ URL::route('setOrganizationViaAjax') }}', {'org_id':org_id, "_token": '{{ csrf_token() }}'}, function (data) {

                        if($.trim(data) == "")
                            window.location.href = "{{ URL::route('ProjectSelect') }}";
                        else
                            alert(data);

                })    
            }
            else
                alert("Please select or create an organization.")

            
        }

    </script>

@endpush