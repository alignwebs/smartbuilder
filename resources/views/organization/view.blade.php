@extends('master')

@section('title', $organization['org_name'])

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Organization Details</h3>

                <div class="pull-right">
                  <a href="{{ URL::action('OrganizationController@edit', $organization->id) }}" data-method="delete" class="btn btn-default ">Edit</a>
                &nbsp;&nbsp;
               <a href="{{ URL::action('OrganizationController@destroy', $organization->id) }}" data-method="delete" class="btn btn-default pull-right delete-ajax">Delete</a>
                  
                </div>

            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body ks-tabs-page-container">
                
                <div class="ks-tabs-container-description">
                    <h3>{{ $organization['org_code'] }} - {{ $organization['org_name'] }}</h3>
                    <p>{{  $organization['org_city']}}, {{  $organization['org_state']}}</p>
                </div>
     
			<ul class="nav ks-nav-tabs ks-tabs-page-default">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-toggle="tab" data-target="#projects">
                            Organization Projects
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#basic">
                            Basic Details
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#contacts">
                            Contacts
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active ks-column-section" id="projects" role="tabpanel">
                    	 @if($organization->projects->count() > 0)
                          
                        <table class="table datatable">
                      <thead>
                        <tr>
                          <th>Project Code</th>
                          <th>Project Name</th>
                          <th>City</th>
                          <th>District</th>
                          <th>State</th>
                          <th>Manage</th>
                        </tr>
                      </thead>

                      <tbody>
                        @foreach($organization->projects as $project)
                          <tr>
                            <td><strong>{{ $project->proj_code }}</strong></td>
                            <td><strong>{{ $project->proj_name }}</strong></td>
                            <td>{{ $project->city }}</td>
                            <td>{{ $project->district }}</td>
                            <td>{{ $project->state }}</td>
                            <td><a href="{{ URL::action('ProjectController@edit',$project->id) }}" class="btn btn-sm btn-primary">Manage</a></td>
                          </tr>
                        @endforeach
                      </tbody>

                    </table>


                       @endif
                    </div>


                    <div class="tab-pane" id="basic" role="tabpanel">
                    <div class="row">
                    	<div class="col-md-6">
                    		
                      <table class="table">
                      		<tbody>
                      			<tr>
                      				<th>Address:</th>
                      				<td>{{ $organization->org_address_one }} {{ $organization->org_address_two }}</td>
                      			</tr>
                      			<tr>
                      				<th>City:</th>
                      				<td>{{ $organization->org_city }}</td>
                      			</tr>
                      			<tr>
                      				<th>District:</th>
                      				<td>{{ $organization->org_district }}</td>
                      			</tr>
                      			<tr>
                      				<th>State:</th>
                      				<td>{{ $organization->org_state }} - {{ $organization->org_pincode }}</td>
                      			</tr>
                      			<tr>
                      				<th>TIN</th>
                      				<td>{{ $organization->org_tin }}</td>
                      			</tr>

                            <tr>
                              <th>Billing Address</th>
                              <td>{{ $organization->org_billing_address }}</td>
                            </tr>
                      			
                      		</tbody>
                      </table>
                    	</div>
                    </div>
                    </div>


                    <div class="tab-pane" id="contacts" role="tabpanel">
                        @if($organization->contacts->count() > 0)
							<table class="table table-condensed table-bordered datatable">
								
								<thead>
									<tr>
										<th>Name</th>
										<th>Phone</th>
										<th>Email</th>
										<th>Address</th>
			
										<th>State</th>
										<th>Pincode</th>
									</tr>
								</thead>
								<tbody>
									@foreach($organization->contacts as $Contact)
										<tr>
											<td>{{ $Contact->name }}</td>
											<td>{{ $Contact->phone }}</td>
											<td>{{ $Contact->email }}</td>
											<td>{{ $Contact->address }}</td>
											<td>{{ $Contact->state }}</td>
											<td>{{ $Contact->pincode }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						@else
							No contact available for this organization.
                        @endif
                    </div>
                </div>
		

   </div>
	</div>
				</div>
@endsection

