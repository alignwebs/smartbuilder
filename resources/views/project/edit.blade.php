@extends('master')

@section('title','Edit Project')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Edit Project Details: {{ $Project->proj_name }} [#{{ $Project->proj_code }}]</h3>
                <button type="button" onclick="$('#proj_update_frm').submit()" class="btn btn-success pull-right">Update</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => ['ProjectController@update', $Project->id], 'id'=>'proj_update_frm', 'method'=>'PUT']) }}
						
						<div class="row">
							
			            <div class="col-lg-6">

			            <!-- Project DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Project Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="proj_address_one" class="col-sm-2 form-control-label">Address 1</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('proj_address_one',$Project->address_one,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="proj_address_two" class="col-sm-2 form-control-label">Address 2</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('proj_address_two',$Project->address_two,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="proj_city" class="col-sm-2 form-control-label">City</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('proj_city',$Project->city,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="proj_district" class="col-sm-2 form-control-label">District</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('proj_district',$Project->district,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="proj_state" class="col-sm-2 form-control-label">State</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::select('proj_state', $indiaStates, $Project->state, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="proj_pincode" class="col-sm-2 form-control-label">Pin Code</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('proj_pincode',$Project->pincode,['class'=>'form-control']) }}
		                                    </div>
		                                </div>





		                         
		                        </div>
		                    </div>
							  <!-- END Project DETAILS BLOCK -->

			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection