@extends('master')

@section('title','Select Project')

@section('pagecontent')
       <div class="ks-page">
    
    <div class="ks-body">
       
        <div class="card panel panel-default light ks-panel ks-confirm">
            <div class="card-block">

                <div class="ks-header" style="margin-top:0 ">
                    <div>{{ Session('selectedOrg')['org_name'] }}</div>
                    <hr>
                    <p>Select A Project</p>
                </div>
                <div class="ks-description">

                <div class="form-group">
                        <select class="form-control ks-select" id="projList" required></select>
                </div>

                <button type="button" class="btn btn-primary" onclick="setProject()">GO</button>
                <hr>

                <a href="{{ URL::action('ProjectController@create') }}" class="btn btn-block btn-default">Add New Project</a>
                 
                </div>
                
            </div>
        </div>

        
    </div>
    
</div>

@endsection

@push('scripts')

    <script>
        $(document).ready(function () {
            projectList()
        } )

        function projectList()
        {
            $.get('{{ URL::route('ProjectWithID') }}', function (json) {

                $.each(json, function (i, item){

                    $('<option value="'+item.id+'">'+item.proj_name+' - '+item.proj_code+'</option>').appendTo('#projList')
                    
                })

            })

            $('#projList').select2();
        }

        function setProject()
        {
            var proj_id = $('#projList').val();

            if(proj_id > 0)
            {
                $.post('{{ URL::route('setProjectViaAjax') }}', {'proj_id':proj_id, "_token": '{{ csrf_token() }}'}, function (data) {

                        if($.trim(data) == "")
                            window.location.href = "{{ URL::to('/') }}";
                        else
                            alert(data);

                })    
            }
            else
                alert("Please select or add a Project.")

            
        }

    </script>

@endpush