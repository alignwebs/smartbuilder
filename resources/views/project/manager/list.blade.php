@extends('master')

@section('title','List Project Roles Users')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">
              <h3>List Project Roles Users: <small>{{ getProject('proj_name') }}</small></h3>
              <a href="{{ URL::Action('ProjectManagerController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $ProjectManagers->count() }} project manager added to this project</h5>
									
									@if($ProjectManagers->count() > 0)

										<table class="table datatable table-condensed table-bordered table-hover">
											<thead>
												<tr>
													<th width="220">Name</th>
													<th>Phone</th>
													<th>Email</th>
													<th>District</th>
													<th>State</th>
													
													<th>Options</th>
												</tr>
											</thead>

											<tbody>
												@foreach($ProjectManagers as $manager)
													<tr>
														<td><strong><a href="{{ URL::action('ProjectManagerController@show', $manager->user->id)  }}">{{ $manager->user->name }}</a></strong></td>
														<td>{{ @$manager->user->detail->phone }}</td>
														<td>{{ $manager->user->email }}</td>
														<td>{{ @$manager->user->detail->district }}</td>
														<td>{{ @$manager->user->detail->state }}</td>
														<td>{!! deleteEditModelBtn("project-managers",$manager->user->id, $manager->user->name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										No Project Managers Added!
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection