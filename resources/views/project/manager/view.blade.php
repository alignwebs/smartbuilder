@extends('master')

@section('title','Project Manager Details')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>{{ $User->user->name }} <small>Project Manager Details</small> </h3>
              
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')
						
						<div class="row">
							<div class="col-lg-12">

						<!-- ORGANIZATION NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                        	<h5 class="card-title">Login Stamps</h5>

		                        	<table class="table" id="loginLogs">

		                        		<thead>
		                        			<tr>
		                        				<th>#</th>
		                        				<th>Event</th>
		                        				<th>IP Address</th>
		                        				<th>Timestamp</th>
		                        			</tr>
		                        		</thead>
		                        		
		                        	</table>
		                        </div>
		                    </div>

		                    <!-- END ORGANIZATION NAME BLOCK -->

			            </div>

			            <div class="col-lg-6">
			            	
			            </div>
						</div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection

@push('scripts')

	<script>
		$(function() {
		    $('#loginLogs').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!! route('login-logs') !!}?user_id={{ $User->user->id }}',
		        columns: [
		            { data: 'id', name: 'id' },
		            { data: 'event', name: 'event' },
		            { data: 'ip_address', name: 'ip_address' },
		            { data: 'created_at', name: 'timestamp' }
		        ]
		    });
		});
</script>

@endpush