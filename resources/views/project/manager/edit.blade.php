@extends('master')

@section('title','Edit Project Manager')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

             <h3>Edit Project Manager <small>{{ $User->user->name }}</small></h3>
              <a href="javascript:void(0)" onclick="$('#add_frm').submit()"  class="btn btn-success pull-right">Update</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">

						@include('common.pagetop')
						
   {{ Form::open(['action' => ['ProjectManagerController@update',$User->user_id], 'id'=>'add_frm','method'=>'PUT']) }}
						<div class="row">
							<div class="col-md-6">
								<div class="card">
									
		                        <div class="card-block">
		                     
		                            <h5 class="card-title">Personal Details</h5>
		                            	
		                            
		                            	 <div class="form-group row">
		                                    <label for="name" class="col-sm-2 form-control-label">Name</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('name', $User->user->name, ['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                 <div class="form-group row">
		                                    <label for="phone" class="col-sm-2 form-control-label">Phone</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('phone', $User->user->detail->phone,['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                              
		                                
		                                 <div class="form-group row">
		                                    <label for="address" class="col-sm-2 form-control-label">Address</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('address', $User->user->detail->address,['class'=>'form-control']) }}
		                                    </div>
		                                </div>


		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">District</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('district', $User->user->detail->district,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                   <div class="form-group row">
		                                    <label for="state" class="col-sm-2 form-control-label">State</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::select('state', getindiaStates(), $User->user->detail->state, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

										<div class="form-group row">
		                                    <label for="role_title" class="col-sm-2 form-control-label">State</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::select('role_title', ['Project Manager'=>'Project Manager','Project Co-ordinator'=>'Project Co-ordinator'],  $User->user->detail->role_title, ['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <input type="hidden" name="projectmanager" value="true">
										<input type="hidden" name="redir" value="{{ URL::route('project-managers.index') }}">
		                     
		                        </div>
		                
								</div>
							</div>
							<div class="col-md-6">
								
								<div class="card">
									<div class="card-block">
										
   <h5 class="card-title">Login Details</h5>
		
         

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address: </label> {{ $User->user->email }}

                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">New Password:</label>

                                Click <b>Forgot Password</b> at Login page.
                        </div>

                        
                    
                    
								
							
									</div>
								</div>
							</div>
						</div>
						   {{ Form::close() }}

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection