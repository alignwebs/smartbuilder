@extends('master')

@section('title','List Projects')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Projects</h3>
            
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $projects->count() }} projects in selected organization</h5>
									
									@if($projects->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													<th>Project Code</th>
													<th>Project Name</th>
													<th>City</th>
													<th>District</th>
													<th>State</th>
													<th>Manage</th>
												</tr>
											</thead>

											<tbody>
												@foreach($projects as $project)
													<tr>
														<td><strong>{{ $project->proj_code }}</strong></td>
														<td><strong>{{ $project->proj_name }}</strong></td>
														<td>{{ $project->city }}</td>
														<td>{{ $project->district }}</td>
														<td>{{ $project->state }}</td>
														<td>{!! deleteEditModelBtn("project",$project->id, $project->proj_name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast One project
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection