<h3 align="center">Payment Voucher</h3>
<br>

<table class="table">
		<tr>
		<td ><label>Payment #:</label> {{ $payment->gen_payment_id }}</td>

		<td align="right"><label>Date:</label> {{ $payment->date }}</td>
	</tr>
</table>
<table class="table">

	<tr>
		<td width="80"><label>Amount Paid To:</label></td>
		<td class="hasborder">{{ $payment->paid_to }}</td>
		<td  width="10"><label>of</label></td>
		<td class="hasborder" width="150">{{ currency($payment->amount) }}</td>
	</tr>
	<tr>
		<td><label>in words</label></td>
		<td colspan="3" class="hasborder">{{ currency_to_word($payment->amount) }} only</td>
	</tr>
	<tr>
		<td><label>by</label></td>
		<td class="hasborder" colspan="3">{{ $payment->paymentMethod->name }}</td>
	</tr>
	<tr>
		<td><label>for</label></td>
		<td class="hasborder" colspan="3">{{ $payment->paymentType->name }}</td>
	</tr>

</table>