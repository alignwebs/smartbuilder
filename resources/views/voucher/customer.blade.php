<h3 align="center">Receipt Voucher</h3>

<table class="table">
		<tr>
		<td ><label>Payment #:</label> {{ $payment->gen_payment_id }}</td>

		<td align="right"><label>Date:</label> {{ $payment->date }}</td>
	</tr>
</table>
<table class="table">

	<tr>
		<td width="100"><label>Amount Rcvd. From:</label></td>
		<td class="hasborder">{{ $payment->customer_payment->first()->customer->name }}</td>
		<td  width="10"><label>of</label></td>
		<td class="hasborder" width="150">{{ currency($payment->amount) }}</td>
	</tr>
	<tr>
		<td><label>in words</label></td>
		<td colspan="3" class="hasborder">{{ currency_to_word($payment->amount) }} only</td>
	</tr>
	<tr>
		<td><label>by</label></td>
		<td class="hasborder" colspan="3">{{ $payment->paymentMethod->name }}</td>
	</tr>

</table>