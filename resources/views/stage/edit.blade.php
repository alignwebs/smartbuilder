@extends('master')



@section('title','Edit Stage')



@section('pagecontent')



  <div class="ks-column ks-page">



        <div class="ks-header">

            <section class="ks-title">

                <h3>Stages: {{ $Stage->name }}</h3>

                 <button type="button" onclick="$('#add_frm').submit()" class="btn btn-success pull-right">Update</button>

            </section>

        </div>



		<div class="ks-content">

            <div class="ks-body">

				

				<div class="container-fluid">

				<div class="row">

					<div class="col-lg-12">

						

						@include('common.pagetop')



						{{ Form::open(['method'=>'PUT','action' => ['StageController@update', $Stage->id], 'id'=>'add_frm']) }}

						

						<div class="row">

							<div class="col-lg-12">



							<div class="card">

		                        <div class="card-block">

		                            <h5 class="card-title">Edit Stage <small>Enter new stage details</small></h5>



		                            	<div class="row">

		                            		<div class="col-sm-4">

		                            			<label for="code" class="form-control-label">Select Department</label>

		                            			{{ Form::select('department', $Departments, $Stage->department_id, ['class'=>'form-control']) }}

		                            		</div>

		                            		<div class="col-sm-4">

		                            			<label for="code" class="form-control-label">Select Product Category</label>

		                            			{{ Form::select('productcategory', $ProductCategory, $Stage->product_category_id, ['class'=>'form-control']) }}

		                            		</div>

		                            	</div>



		                            	<div class="row">

		                            		<div class="col-sm-2">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Stage Code</label>

				                                    {{ Form::text('code', $Stage->code,['class'=>'form-control']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-2">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Stage Name</label>

				                                    {{ Form::text('name', $Stage->name,['class'=>'form-control']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-2">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Standard Rate</label>

				                                    {{ Form::number('rate', $Stage->rate,['class'=>'form-control']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-3">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Description</label>

				                                    {{ Form::text('description', $Stage->description,['class'=>'form-control input-sm']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-3">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Comments</label>

				                                    {{ Form::text('comments', $Stage->comment,['class'=>'form-control']) }}

				                                </div>

		                            		</div>





		                            

		                            	</div>		                         		

		                         		

		                            	

		                           

		                        </div>

		                    </div>



		                    <!-- END Item NAME BLOCK -->

			            </div>



						</div>

						{{ Form::close() }}


					</div>

						

			     </div>



			            

		        </div>



   </div>

	</div>

				</div>

@endsection