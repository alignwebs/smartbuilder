@extends('master')



@section('title','Stages')



@section('pagecontent')



  <div class="ks-column ks-page">



        <div class="ks-header">

            <section class="ks-title">

                <h3>Stages</h3>

                 <button type="button" onclick="$('#add_frm').submit()" class="btn btn-success pull-right">Add Stage</button>

            </section>

        </div>



		<div class="ks-content">

            <div class="ks-body">

				

				<div class="container-fluid">

				<div class="row">

					<div class="col-lg-12">

						

						@include('common.pagetop')



						{{ Form::open(['action' => 'StageController@store', 'id'=>'add_frm']) }}

						

						<div class="row">

							<div class="col-lg-12">



							<div class="card">

		                        <div class="card-block">

		                            <h5 class="card-title">Add New Stage <small>Enter new stage details</small></h5>



		                            	<div class="row">

		                            		<div class="col-sm-4">

		                            			<label for="code" class="form-control-label">Select Department</label>

		                            			{{ Form::select('department', $Departments, null, ['class'=>'form-control']) }}

		                            		</div>

		                            		<div class="col-sm-4">

		                            			<label for="code" class="form-control-label">Select Product Category</label>

		                            			{{ Form::select('productcategory', $ProductCategory, null, ['class'=>'form-control']) }}

		                            		</div>

		                            	</div>



		                            	<div class="row">

		                            		<div class="col-sm-2">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Stage Code</label>

				                                    {{ Form::text('code', '',['class'=>'form-control']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-2">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Stage Name</label>

				                                    {{ Form::text('name', '',['class'=>'form-control']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-2">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Standard Rate</label>

				                                    {{ Form::number('rate', '',['class'=>'form-control']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-3">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Description</label>

				                                    {{ Form::text('description', '',['class'=>'form-control input-sm']) }}

				                                </div>

		                            		</div>

		                            		<div class="col-sm-3">

		                            			<div class="form-group">

				                                    <label for="code" class="form-control-label">Comments</label>

				                                    {{ Form::text('comments', '',['class'=>'form-control']) }}

				                                </div>

		                            		</div>





		                            

		                            	</div>		                         		

		                         		

		                            	

		                           

		                        </div>

		                    </div>



		                    <!-- END Item NAME BLOCK -->



							

			            </div>



						</div>

						{{ Form::close() }}





						<div class="row">

							

							<div class="col-lg-12">



							<div class="card">

		                        <div class="card-block">

		                            <h5 class="card-title">List of Stages <small>Total Stages: {{ $Stages->count() }}</small></h5>

									

									@if($Stages->count() > 0)



										<table class="table datatable">

											<thead>

												<tr>

													<th>Stage Code</th>

													<th>Stage Name</th>

													<th>Rate</th>

													<th>Department</th>

													<th>Product Category</th>

													<th>Description/Comment</th>

												</tr>

											</thead>



											<tbody>

												@foreach($Stages as $Stage)

													<tr>

														<td><strong>{{ $Stage->code }}</strong></td>

														<td><a href="{{ URL::action('StageController@edit',$Stage->id) }}"><strong>{{ $Stage->name }}</strong></a></td>

														<td>{{ $Stage->rate }}</td>

														<td>{{ @$Stage->department->name }}</td>

														<td>{{ @$Stage->productcategory->product_category_name }}</td>

														<td>{{ $Stage->description }}/{{ $Stage->comment }}</td>

														

													</tr>

												@endforeach

											</tbody>



										</table>



									@else

										No stage added!

									@endif

		                           

		                           

		                        </div>

		                    </div>



		                    <!-- END Item NAME BLOCK -->



							

			            </div>



							

						</div>





					</div>

						

			     </div>



			            

		        </div>



   </div>

	</div>

				</div>

@endsection