@extends('master')

@section('title','Material Transfer')

@section('pagecontent')


  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Project Material Transfer: <small>{{ getProject('proj_name') }}</small></h3>
                <div class="pull-right">
               		<button type="button" onclick="placeTransfer()" class="btn btn-success">Transfer</button>
                </div>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						<div class="row">
							<div class="col-lg-12">
						
						<!-- Order NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
								<h5 class="card-title">Product Options</h5>
		                        <div class="row">
		                        	<div class="col-sm-4">
		                        		<div class="form-group row">
		                                    <label for="org_code" class="col-sm-4 form-control-label">Transfer To:</label>
		                                    <div class="col-sm-8">
												{{ Form::select('to_product_id', $products, null, ['placeholder' => 'Select Product','class'=>'form-control']) }}
		                                    </div>
		                             	</div>	
		                        	</div>

		                        	
		                        </div>
		                        	 
		                        	
		                        </div>
		                    </div>

		                <!-- END Order NAME BLOCK -->

							
			            </div>
			            </div>

			            <div class="row">
	
						
			            <div class="col-lg-12">
			            	
			            	<div class="card">
		                        <div class="card-block">
		                        	<button type="button" class="btn btn-sm btn-default pull-right" onclick="itemSelect()">Add Items</button>
		                        	<h5 class="card-title">Transferring Items List</h5>

		                        	<table class="table table-condensed table-bordered table-hover" id="inventoryTransferItems">
                                    <thead>
	                                    <tr>
	                                    	<th>Department</th>
	                                        <th>Item Code</th>
	                                        <th>Item Name</th>
	                                        <th>Supplier Code</th>
	                                        <th>Supplier Name</th>
	                                        <th width="100">Transfer Qty</th>
	                                        <th width="100">In Stock</th>
	                                        <th>Delete</th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                    	
                                    </tbody>
                                </table>

	

		                        </div>
		                    </div>

			            </div>

			            
						</div>
				
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>


<div class="modal" tabindex="-1" role="dialog" id="itemSelect">
    <div class="modal-dialog modal-lg" style="max-width: 90% !Important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Material Transfer: Items Selection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="fa fa-close"></span>
                </button>
            </div>
            	
            <div class="modal-body">
            	<div class="row">
            	<div class="col-sm-1"><label for="">Select Department: </label></div>
            		<div class="col-sm-3">
            			{{ Form::select('department', $departments, null, ['placeholder' => 'Select Department','class'=>'form-control', 'onchange'=>'getItems(this)', 'id'=>'department']) }}
            		</div>
            	</div>
				<br>
            	<div id="inventoryItems"></div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-primary">Close</button>
            </div>
            
        </div>
</div>
</div>
@endsection


@push('scripts')


	<script>
		var items = [];

		function addItem(e)
		{
			var supplierid = $(e).attr('data-supplierid');
			var suppliercode = $(e).attr('data-suppliercode');
			var suppliername = $(e).attr('data-suppliername');

			var itemid = $(e).attr('data-itemid');
			var itemcode = $(e).attr('data-itemcode');
			var itemname = $(e).attr('data-itemname');
			var itemunit = $(e).attr('data-itemunit');

			var qtyavail = $(e).attr('data-qtyavail');

			var department = $('#itemSelect #department').val();
			var departmentname = $('#itemSelect #department option:selected').text();
			
			var item = {"supplierid":supplierid,"suppliercode":suppliercode,"suppliername":suppliername,"department":department,"departmentname":departmentname,"itemid":itemid,"itemcode":itemcode,"itemname":itemname,"itemunit":itemunit, "qtyavail":qtyavail, "qtyavailold":qtyavail};

			
			if(!searchItem('itemid',itemid))
				items.push(item)
			else
				alert('Item already in the list.')

			$(e).html('<span class="fa fa-tick" aria-hidden="true"></span> Selected')
			
			renderTable()

		}

		function searchItem(key,value)
		{
			var len = items.length;

			for(var i=0; i<len; i++)
			{
				if(items[i][key] == value)
					return true;
			}
		}

		function deleteItem(index)
	    {
	        items[index] = "";
	        renderTable()
	    }

	    function updateItem(key,index,e)
	    {
	    	var val = $(e).val();

	    	if(key == 'qtyavail')
	    	{
	    		var old = items[index]['qtyavailold'];
	    		console.log(val+" "+old)
	    		if(+val > +old)
	    			$(e).val(old)
	    		else
	    			items[index][key] = val;		
	    	}
	    	else
	    	{
	    		items[index][key] = val;
	    	}
	
	    	

	    	renderTable()
	    }

		function renderTable()
		{
			 if(items.length > 0)
		        {
		            $('#inventoryTransferItems tbody').html('');

		            var html = "";

		            $.each(items, function (index,value) {

		            
		            	if($(value).size() > 0)
		            	{

		            		html = '<tr><td>'+value.departmentname+'</td><td>'+value.itemcode+'</td><td>'+value.itemname+'</td><td>'+value.suppliercode+'</td><td>'+value.suppliername+'</td><td><input type="number" value="'+value.qtyavail+'" class="qtyavail" min="0"  onBlur="updateItem(\'qtyavail\','+index+',this)" ></td><td>'+value.qtyavailold+' '+value.itemunit+'</td><td><a href="javascript:" onclick="deleteItem('+index+')">Delete</a></td></tr>';

		            		 $(html).appendTo('#inventoryTransferItems tbody');

		            	}


		            })

		        }
			
		}

		function itemSelect()
		{
			$('#itemSelect').modal({backdrop:'static'},'show')
		}

		function getItems(e)
		{
			var department_id = $(e).val();

			if(department_id > 0)
			{
				$('#inventoryItems').html('Loading...')

				$.get('{{ route('inventory-items') }}', {'department_id':department_id}, function(data) {

						$('#inventoryItems').html(data).find('table').DataTable();

						//prettifyTable()


				})
			}
		}

		function placeTransfer()
		{
			if(items.length > 0 && $('select[name="to_product_id"]').val() != "") {

			

					swal({
					  title: "Confirm this Material Transfer?",
					  text: "Press OK to proceed transfer.",
					  type: "info",
					  showCancelButton: true,
					  closeOnConfirm: false,
					  showLoaderOnConfirm: true
					}, function () {

					    var success = swal({
							  title: "Material Transferred Successfully!",
							  text: "",
							  type: "success",
							  confirmButtonClass: "btn-success"
							})

					    transfer(success);
			 
					});

			}
		}

		function transfer(response)
		{
			var data = JSON.stringify(items);

			var product_id = $('select[name="to_product_id"]').val()

			$.post("{{ URL::action("ProductItemController@store") }}", {"items":data,"product_id": product_id , "type":"save" , "_token":"{{ csrf_token() }}"}, function (data) {

					response

					setTimeout(function () { window.location.href = data }, 1000 );

			});
		}

	</script>


@endpush