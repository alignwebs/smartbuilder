@extends('reports.master')


@section('content')

	@if($Customers->count() > 0)
		
		<table class="datatable table">
			<thead>
				<tr>
					<th>Customer ID</th>
					<th>Customer Name</th>
					<th>Phone No.</th>
					<th>Email</th>
					<th>Address</th>
					<th>Project Name</th>
					<th>Product Assigned</th>
	
				</tr>
			</thead>

			<tbody>
				@foreach($Customers as $Customer)

					<tr>
						<td>{{ $Customer->id }}</td>
						<td>{{ $Customer->name }}</td>
						<td>{{ $Customer->phone }}</td>
						<td>{{ $Customer->email }}</td>
						<td>{{ $Customer->address }}</td>
						<td>
							@php
								foreach($Customer->products as $product)
									$projects[] = $product->product->project->proj_name;

								


								$projects = array_unique($projects);

								foreach($projects as $project)
									echo $project;
							@endphp
						</td>
						<td>{{ $Customer->products->count() }}</td>
					</tr>

				@endforeach
			</tbody>
		</table>
	@else
		No customers found!
	@endif


@endsection