@extends('master')

@section('title','Reports')

@section('pagecontent')

<style>
	#DataTables_Table_0_wrapper .row:nth-child(1) { display: none }

	#report-container {}
	#report-container #report-header { text-align: center; margin: 20px 0; }
	#report-container #report-header .org-name { font-size: 20px; font-weight: bold; }
	#report-container #report-header .report-type { font-size: 16px; }
</style>

<div class="ks-column ks-page">
	<div class="ks-header">
		<section class="ks-title">
			<h3>Reports</h3>
		</section>
	</div>

	<div class="ks-content">
		<div class="ks-body">

			<div class="container-fluid">
				
				{{ Form::open(['method'=>'GET','route'=>'report-generate' , 'id'=>'report_frm']) }}

				<div class="row">
					<div class="col-lg-3">

						<div class="form-group row">
							<label for="payment_type" class="col-sm-4 form-control-label">Report Type</label>
							<div class="col-sm-8">
								{!! Form::select('report_type',$types, null, ['class'=>'form-control', 'placeholder' => 'Select Report Type']) !!}
							</div>
						</div>

					</div>

					<div class="col-lg-3">

						<div class="form-group row">
							<label for="payment_type" class="col-sm-4 form-control-label">Project</label>
							<div class="col-sm-8">
								{!! Form::select('project', ['' => 'All'] + $projects->toArray(), null, ['class'=>'form-control']) !!}
							</div>
						</div>

					</div>

					<div class="col-lg-3">
						<button type="submit" class="btn btn-primary">Generate</button>
					</div>
				</div>

				{{ Form::close() }}

				<div class="card">
					<div class="card-block">
					
						<div id="report-container">Generate a report to view</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
@endsection


@push('scripts')

<script>
	$('#report_frm').submit(function () {

		var action = $(this).attr('action');

		var fields = $(this).serialize();

		$.get(action, fields, function (data) {

			$('#report-container').html(data);

		})

		return false;
	})

</script>



@endpush