<div id="report-header">
	<div class="org-name">{{ getOrganization('org_code') }} - {{ getOrganization('org_name') }}</div>
	<div class="report-type">{{ $report_title->name }} Report - Generated on {{ Carbon\Carbon::now() }}</div>
</div>


<div id="report-content">
	@yield('content')
</div>