@if(sizeof($items) > 0)
	
	<table class="table table-condensed table-bordered table-hover">
		<thead>
			<th>Item Code</th>
			<th>Item Name</th>
	
			<th>Unit Price</th>
			<th>ROQ</th>
			<th>Tax Type</th>
			<th width="130"></th>

		</thead>

		<tbody>
			@foreach($items as $item)
				<tr>
					<td>{{ $item['item_code'] }}</td>
					<td>{{ $item['item_name'] }}</td>
					<td>{{ $item['item_unit_price'] }}</td>
					<td>{{ $item['roq'] }} {{ $item['item_unit'] }}</td>
					<td>{{ $item['tax_name'] }}</td>
					<td><a href="javascript:" data-supplierid="{{ $Supplier->id }}" data-suppliercode="{{ $Supplier->code }}" data-suppliername="{{ $Supplier->name }}" data-itemid="{{  $item['item_id'] }}" data-itemcode="{{  $item['item_code'] }}" data-itemname="{{  $item['item_name'] }}" data-itemunit="{{  $item['item_unit'] }}" data-itemunitprice="{{  $item['item_unit_price'] }}" data-discount="{{  $item['item_discount'] }}" data-roq="{{  $item['roq'] }}" data-taxname="{{  $item['tax_name'] }}"  data-tax="{{  $item['tax'] }}" onclick="addItem(this)">Add to cart</a></td>
				</tr>
			@endforeach
		</tbody>
	</table>

@else

	No items found for this supplier.

@endif