@if($incharges->count() > 0)
	
	<table class="table">
		<thead>
			<tr>
				<th>Staff Name</th>
				<th>Staff Code</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Address</th>
				<th>State</th>
			</tr>
		</thead>

		<tbody>
			@foreach($incharges as $incharge)
				<tr><td>{{ $incharge->staff->name }}</td><td>{{ $incharge->staff->code }}</td><td>{{ $incharge->staff->phone }}</td><td>{{ $incharge->staff->email }}</td><td>{{ $incharge->staff->address }}</td><td>{{ $incharge->staff->state }}</td></tr>
			@endforeach
		</tbody>

	</table>

@else

	No incharges found for this item.

@endif