@extends('master')

@section('title','List Project Items')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Items: <small>{{ getOrganization('org_name') }}</small></h3>
              <a href="{{ URL::Action('ItemController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Items->count() }} items added to this organization</h5>
									
									@if($Items->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													<th>Item Code</th>
													<th>Item Name</th>
													<th>Units</th>
													<th>Suppliers</th>
													<th>Tax</th>
													<th>Manage</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Items as $Item)
													<tr>
														<td><strong>{{ $Item->code }}</strong></td>
														<td><strong>{{ $Item->name }}</strong></td>
														<td>{{ $Item->unit }}</td>
														<td>
															@foreach($Item->suppliers as $row)

																<a  target="_blank"  class="link" href="{{ route('supplier.show',$row->supplier->id) }}">{{ $row->supplier->name }}</a>

															@endforeach
														</td>
														<td>{{ @$Item->tax->fullname }}</td>
														<td>{!! deleteEditModelBtn("item",$Item->id, $Item->name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast One item to project
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection