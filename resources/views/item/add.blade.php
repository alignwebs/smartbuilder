@extends('master')

@section('title','Add New Item')

@section('pagecontent')

  <div class="ks-column ks-page">

        <div class="ks-header">
            <section class="ks-title">
                <h3>Add New Item</h3>
                <button type="button" onclick="$('#item_add_frm').submit()" class="btn btn-success pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => 'ItemController@store', 'id'=>'item_add_frm']) }}
						
						<div class="row">
							<div class="col-lg-12">

							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Item's Primary Details</h5>
		                         		
		                         		<div class="form-group row">
		                                    <label for="code" class="col-sm-4 form-control-label">Select Suppliers</label>
		                                    <div class="col-sm-8">
												  	{{ Form::select('supplier[]', $suppliers, null, ['class'=>'form-control', 'multiple']) }}
		                                    </div>
		                                </div>
		                            	
		                            	<div class="form-group row">
		                                    <label for="code" class="col-sm-4 form-control-label">Item Code</label>
		                                    <div class="col-sm-8">
												{{ Form::text('code', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="name" class="col-sm-4 form-control-label">Item Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
										
										<div class="form-group row">
		                                    <label for="description" class="col-sm-4 form-control-label">Item Description</label>
		                                    <div class="col-sm-8">
												{{ Form::text('description','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                
		                                <div class="form-group row">
		                                    <label for="unit" class="col-sm-4 form-control-label">Item Unit</label>
		                                    <div class="col-sm-8">
												{{ Form::text('unit','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="unit_price" class="col-sm-4 form-control-label">Item Unit Price</label>
		                                    <div class="col-sm-8">
												{{ Form::number('unit_price','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="discount" class="col-sm-4 form-control-label">Item Discount %</label>
		                                    <div class="col-sm-8">
												{{ Form::number('discount',0,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="item_state" class="col-sm-4 form-control-label">Select Tax</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('tax', $tax, null , ['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="roq" class="col-sm-4 form-control-label">Item ROQ</label>
		                                    <div class="col-sm-8">
												{{ Form::text('roq','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="item_state" class="col-sm-4 form-control-label">Select Departments</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('departments[]', $departments, (isset($_GET['dep']) ? $_GET['dep'] : null) , ['class'=>'form-control', 'multiple']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="item_state" class="col-sm-4 form-control-label">Assign Staff</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('staff[]', $staff, null , ['class'=>'form-control', 'multiple']) }}
		                                    </div>
		                                </div>

		                              


		                        </div>
		                    </div>

		                    <!-- END Item NAME BLOCK -->

							
			            </div>

			            <div class="col-lg-6"></div>

						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection