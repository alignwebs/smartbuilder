
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <base href="{{ Request::root()  }}">
    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="libs/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/common.min.css">
    <link rel="stylesheet" type="text/css" href="assets/styles/pages/auth.min.css">
</head>
<body>
<div class="ks-page">

    <div class="ks-body">
        <div class="ks-logo">{{ config('app.name') }}</div>

        <div class="card panel panel-default light ks-panel ks-forgot-password">
            <div class="card-block">
            @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                <form class="form-container" role="form" method="POST" action="{{ route('password.email') }}">
                 {{ csrf_field() }}
 
                    <h4 class="ks-header">
                        Forgot Password
                        <span>Don't worry, this happens sometimes.</span>
                    </h4>

                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email" required autofocus>
                        <span class="icon-addon">
                            <span class="fa fa-at"></span>
                        </span>
                        </div>
                        @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">Send Password Reset Link</button>
                    </div>
                    <div class="ks-text-center">
                        <span class="text-muted">Remember It?</span> <a href="{{ URL::route('login') }}">Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
   
</div>

<script src="libs/jquery/jquery.min.js"></script>
<script src="libs/tether/js/tether.min.js"></script>
<script src="libs/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>