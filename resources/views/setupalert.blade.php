@if(sizeof($data) > 0)
	
	<div class="alert alert-danger">
		<h4>Setup the following modules:</h4>
		<ul>
			@foreach($data as $alert)
				<li><b>{{ ucwords($alert['title']) }}</b> - <a href="{{ $alert['link'] }}" target="_blank">Click to Setup</a></li>
			@endforeach
		</ul>
	</div>

@endif