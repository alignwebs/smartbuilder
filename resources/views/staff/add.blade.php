@extends('master')

@section('title','Add New Staff')

@section('pagecontent')

  <div class="ks-column ks-page">

        <div class="ks-header">
            <section class="ks-title">
                <h3>Add New Staff</h3>
                <button type="button" onclick="$('#item_add_frm').submit()" class="btn btn-success pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => 'StaffController@store', 'id'=>'item_add_frm']) }}
						
						<div class="row">
							
			            <div class="col-lg-6">

			            <!-- Item DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Enter Staff Contact Details</h5>
		                            
		                            	<div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Email</label>
		                                    <div class="col-sm-8">
												{{ Form::text('email','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Phone</label>
		                                    <div class="col-sm-8">
												{{ Form::text('phone','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Address</label>
		                                    <div class="col-sm-8">
												{{ Form::text('address','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                            
		                                <div class="form-group row">
		                                    <label for="item_state" class="col-sm-4 form-control-label">State</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('state', getIndiaStates(), null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                            
		                        </div>
		                    </div>
							  <!-- END Item DETAILS BLOCK -->



			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection