@extends('master')

@section('title','Edit Staff')

@section('pagecontent')

  <div class="ks-column ks-page">

        <div class="ks-header">
            <section class="ks-title">
                <h3>Edit Staff</h3>
                <button type="button" onclick="$('#item_add_frm').submit()" class="btn btn-success pull-right">Submit</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => ['StaffController@update', $Staff->id], 'method' => 'PUT' , 'id'=>'item_add_frm']) }}
						
						<div class="row">
							
			            <div class="col-lg-6">

			            <!-- Item DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Enter Staff Contact Details</h5>

		                            <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Code</label>
		                                    <div class="col-sm-8">
												{{ Form::text('code', $Staff->code,['class'=>'form-control', 'disabled']) }}
		                                    </div>
		                                </div>
		                            
		                            	<div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('name', $Staff->name,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Email</label>
		                                    <div class="col-sm-8">
												{{ Form::text('email', $Staff->email,['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Phone</label>
		                                    <div class="col-sm-8">
												{{ Form::text('phone', $Staff->phone,['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="item_name" class="col-sm-4 form-control-label">Address</label>
		                                    <div class="col-sm-8">
												{{ Form::text('address',  $Staff->address,['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                            
		                                <div class="form-group row">
		                                    <label for="item_state" class="col-sm-4 form-control-label">State</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('state', getIndiaStates(),  $Staff->state, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                            
		                        </div>
		                    </div>
							  <!-- END Item DETAILS BLOCK -->



			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection