@extends('master')

@section('title','List Staff')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Staff: <small>{{ getOrganization('org_name') }}</small></h3>
              <a href="{{ URL::Action('StaffController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Staff->count() }} Staff added to this organization</h5>
									
									@if($Staff->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													
													<th>Staff Code</th>
													<th>Staff Name</th>
													<th>Email</th>
													<th>Phone</th>
													<th>Address</th>
													<th>Manage</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Staff as $Staff)
													<tr>
														<td><strong>{{ $Staff->code }}</strong></td>
														<td><strong>{{ $Staff->name }}</strong></td>
														<td>{{ $Staff->email }}</td>
														<td>{{ $Staff->phone }}</td>
														<td>{{ $Staff->address }}</td>
														<td>{!! deleteEditModelBtn("staff",$Staff->id, $Staff->name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast one Staff
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection