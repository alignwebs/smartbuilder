@extends('master')

@section('title','List Customers')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">
              <h3>Project Customers <small>{{ getProject('proj_name') }}</small></h3>
              <a href="{{ URL::Action('CustomerController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Customers->count() }} customers added to this project</h5>
									
									@if($Customers->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													<th>Customer Name</th>
													<th>Phone</th>
													<th>Email</th>
													<th>City</th>
													<th>State</th>
													<th>Options</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Customers as $Customer)

													<tr>
														<td><a href="{{ route('customer.show',$Customer->id) }}">{{ $Customer->name }}</a></td>
														<td>{{ $Customer->phone }}</td>
														<td>{{ $Customer->email }}</td>
														<td>{{ $Customer->city }}</td>
														<td>{{ $Customer->state }}</td>
														<td>{!! deleteEditModelBtn("customer",$Customer->id, $Customer->name) !!}</td>
													</tr>

												@endforeach
											</tbody>

										</table>

									@else
										No Project Managers Added!
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection

