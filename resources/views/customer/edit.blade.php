@extends('master')

@section('title','Edit Customer')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

             <h3>Edit Customer <small>{{ getProject('proj_name') }}</small></h3>
              <a href="javascript:void(0)" onclick="$('#add_frm').submit()"  class="btn btn-primary pull-right">Submit</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">

						@include('common.pagetop')
						
   {{ Form::model($Customer, ['route' => ['customer.update', $Customer->id], 'method' => 'PUT', 'id'=>'add_frm']) }}
						<div class="row">
							<div class="col-md-6">
								<div class="card">
									
		                        <div class="card-block">
		                     
		                            <h5 class="card-title">Personal Details</h5>

		                            	<div class="form-group row">
		                                    <label for="name" class="col-sm-2 form-control-label">Name</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('name',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Email</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::email('email',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                <div class="form-group row">
		                                    <label for="phone" class="col-sm-2 form-control-label">Phone</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('phone',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                              
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">District</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('city',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="state" class="col-sm-2 form-control-label">State</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::select('state', getIndiaStates(), null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Secondary Name</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('name_sec',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Secondary Phone</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('phone_sec',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Secondary Email</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::email('email_sec',null,['class'=>'form-control']) }}
		                                    </div>
		                                </div>

										<input type="hidden" name="customer" value="true">
										<input type="hidden" name="redir" value="{{ URL::route('customer.index') }}">
		                     
		                        </div>
		                
								</div>
							</div>
							
						</div>
						   {{ Form::close() }}

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')

	<script>
		
		function checkUser(e)
		{
			/*var email = $(e).val();

			if(email != '')
			{
				$.get('{{ route('user-exists') }}', {'email':email}. function (data){



				})
			}*/
		}

	</script>

@endpush