@extends('master')

@section('title','Add Customer')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

             <h3>Add Customer <small>{{ getProject('proj_name') }}</small></h3>
              <a href="javascript:void(0)" onclick="$('#add_frm').submit()"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">

						@include('common.pagetop')
						
   {{ Form::open(['route' => 'register', 'id'=>'add_frm']) }}
						<div class="row">
							<div class="col-md-6">
								<div class="card">
									
		                        <div class="card-block">
		                     
		                            <h5 class="card-title">Personal Details</h5>

		                            	<div class="form-group row">
		                                    <label for="name" class="col-sm-2 form-control-label">Name</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Email</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::email('email_primary','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                <div class="form-group row">
		                                    <label for="phone" class="col-sm-2 form-control-label">Phone</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('phone','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                              
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">District</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('city','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="state" class="col-sm-2 form-control-label">State</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::select('state', getIndiaStates(), null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Secondary Name</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('name_sec','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Secondary Phone</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::text('phone_sec','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="district" class="col-sm-2 form-control-label">Secondary Email</label>
		                                    <div class="col-sm-10">
		                                       	{{ Form::email('email_sec','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

										<input type="hidden" name="customer" value="true">
										<input type="hidden" name="redir" value="{{ URL::route('customer.index') }}">
		                     
		                        </div>
		                
								</div>
							</div>
							<div class="col-md-6">
								
								<div class="card">
									<div class="card-block">
										
   <h5 class="card-title">Login Details</h5>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" onblur="checkCustomer()" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="password_fields">

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
					
                    		<p>Note: Customer login is created only one time</p>

         				</div>

         					<p class="message"></p>
                    
								
							
									</div>
								</div>
							</div>
						</div>
						   {{ Form::close() }}

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection


@push('scripts')

	<script>
		
		function checkCustomer()
		{
			var email = $('#email').val();

			var message = "";

			if(email != '')
			{
				if(validateEmail(email))
				{

					$.getJSON('{{ route('user-exists') }}', {'email':email, 'type':'customer'}, function (json){

						var status = $.trim(json.status);

						if(status == 'exists')
						{
							message = "Customer login details found as <b>"+json.customer.name+"</b> ! Customer will be added under this login account.";

							$('.password_fields').hide();


						}
						else if(status == 'locked')
						{
							message = "Cannot use this email for login. Please enter a new email.";

							$('.password_fields').show();
						}
						else
							$('.password_fields').show();

						$('.message').html(message)

					})
					
				}
			}
		}

	</script>

@endpush