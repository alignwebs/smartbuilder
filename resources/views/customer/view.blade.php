@extends('master')

@section('title', 'Customer Details')

@section('pagecontent')

<style>
  #modal .select2  { width:100% !important; }
</style>
  <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">

                <h3>Project Customer Details</h3>
             <button class="btn btn-primary pull-right" type="button" onclick="$('#modal').modal('show')">Assign Product</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body ks-tabs-page-container">
               <div class="ks-tabs-container-description">
                    <h3>{{ $Customer['name'] }}</h3>
                   
                </div>
     
			         <ul class="nav ks-nav-tabs ks-tabs-page-default">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-toggle="tab" data-target="#tab1">
                           Products
                          
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#tab2">
                            Change Requests
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#tab3">
                            Payments
                           
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
            <div class="tab-pane active ks-column-section" id="tab1" role="tabpanel">      
               

              @if($Customer->products->count() > 0)

                <table class="table datatable">
                  <thead>
                    <th>Product Name</th>
                    <th>Project</th>
                    <th>Organization</th>
                    <th>Remark</th>
                    <th></th>
                  </thead>

                  
                  @foreach($Customer->products as $row)

                    <tr>
                      <td>{{ $row->product->name_desc }}</td>
                      <td>{{ $row->product->project->proj_name.", ".$row->product->project->state }}</td>
                      <td>{{ $row->product->organization->org_name }}</td>
                      <td>{{ $row->remark }}</td>
                      <td>{!! deleteEditModelBtn("product-customer",$row->id, $row->product->product_name) !!}</td>
                    </tr>

                  @endforeach
                </table>



              @else
                No Products Assigned Yet!
              @endif
              
            </div>


            <div class="tab-pane" id="tab2" role="tabpanel">
                                      
    @if($ChangeRequests->count() > 0)

  <div class="timeline">

    @foreach($ChangeRequests->sortBy('date') as $ChangeRequest)
        
        @php

          $date = Carbon\Carbon::createFromFormat('Y-m-d',$ChangeRequest->date);
  
        @endphp
        <div class="entry">
          <div class="title">
            <h3>{{ $ChangeRequest->title }} <small>{{ $date->format('l jS F Y') }}</small> <span class="badge badge-default">{{ currency($ChangeRequest->charge) }}</span> <span class="badge badge-default">{{ $ChangeRequest->status }}</span> <span class="badge badge-default">{{ $ChangeRequest->status }}</span></h3>
            </div>
          <div class="body">

            <div class="row">
              <div class="col-sm-7">
                <p>{!! nl2br($ChangeRequest->instruction) !!}</p>    
              </div>

              <div class="col-sm-5">
                
                  @if($ChangeRequest->files->count() > 0)
                  <b>Files:</b>
                    <ul class="list-unstyled">
                      @foreach($ChangeRequest->files as $file)
                        <li><a target="_blank" href="{{ asset('storage/'.$file->file) }}" class="link-icon">Download / View File</a></li>
                      @endforeach
                      
                    </ul>
                  @endif
              </div>
            </div>
            
            
          </div>
        </div>

    @endforeach
</div>

    @else
      No Requests Avaialble.
    @endif




              

            </div>

            <div class="tab-pane" id="tab3" role="tabpanel"></div>
        </div>
		

   </div>
	</div>
				</div>







    <div class="modal fade" tabindex="-1" role="dialog" id="modal">
    <div class="modal-dialog modal-sm ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Assign Product to Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="fa fa-close"></span>
                </button>
            </div>
              {{ Form::open(['action'=>'ProductCustomerController@store']) }}
            <div class="modal-body">
             
                          <div class="row">
                              <div class="col-sm-12">


                              <div class="form-group">
                              <label for="code" class="form-control-label">Select Product</label>
                                  <div id="stage">
                                   {{ Form::select('product', $Products->pluck('name_desc','id'), [],['class'=>'form-control', 'required'=>'', 'placeholder'=>'Select Product']) }}
                                  </div>
                              </div>


                              <div class="form-group">
                              <label for="code" class="form-control-label">Date</label>
                                    {{ Form::text('date', date('Y-m-d'), ['class'=>'form-control', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=> date('Y-m-d') , 'required'=>'']) }}
                              </div>

                              <div class="form-group">
                              <label for="code" class="form-control-label">Remark</label>
                                  {{ Form::text('remark', null, ['class'=>'form-control']) }}
                              </div>

                          {{ Form::hidden('customer_id', $Customer->id) }}

                          </div>
                     
                  </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@endsection


@push('scripts')


@endpush