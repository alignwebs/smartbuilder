@extends('master')

@section('title','Add New Department')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Add New Department</h3>
                <button type="button" onclick="$('#suplr_add_frm').submit()" class="btn btn-success pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')
						
						{{ Form::open(['action' => 'DepartmentController@store', 'id'=>'suplr_add_frm']) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- Department NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Department's Primary Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="suplr_code" class="col-sm-4 form-control-label">Department Code</label>
		                                    <div class="col-sm-8">
												{{ Form::text('code', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Department Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                              


		                        </div>
		                    </div>

		                    <!-- END Department NAME BLOCK -->

							
			            </div>

			            <div class="col-lg-6">

			            <!-- Department DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Department Contact Person</h5>
		                          <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Person Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('contact_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Person Email</label>
		                                    <div class="col-sm-8">
												{{ Form::text('contact_email','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Person Phone</label>
		                                    <div class="col-sm-8">
												{{ Form::text('contact_phone','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Person Address</label>
		                                    <div class="col-sm-8">
												{{ Form::text('contact_address','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                            
		                                <div class="form-group row">
		                                    <label for="suplr_state" class="col-sm-4 form-control-label">State</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('contact_state', $indiaStates, null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                            
		                        </div>
		                    </div>
							  <!-- END Department DETAILS BLOCK -->



			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection