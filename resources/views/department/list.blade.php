@extends('master')

@section('title','List Departments')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Departments: <small>{{ getOrganization('org_name') }}</small></h3>
              <a href="{{ URL::Action('DepartmentController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Departments->count() }} departments added to this organization</h5>
									
									@if($Departments->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													
													
													<th>Department Name</th>
													<th>Project Items</th>
													<th>Manage</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Departments as $Department)
													<tr>
													
														<td><a href="{{ URL::action('DepartmentController@show',$Department->id) }}"><strong>{{ $Department->name }} - {{ $Department->code }}</strong></a></td>
														<td>{{ $Department->projectItems->count() }} items</td>
														<td>{!! deleteEditModelBtn("department",$Department->id, $Department->name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast one department
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection