@extends('master')

@section('title', $title)

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>{{ $title }} Department Details</h3>
           <a href="{{ URL::Action('ItemController@create') }}?dep={{ $Department->id }}"  class="btn btn-success pull-right">Add New Item</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						<div class="row">
							
			            <div class="col-lg-9">

			            <!-- Project DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                        	<h5 class="card-title">Department Items</h5>

		                        	@if($Items->count() > 0)
										
										<table class="table datatable">
											<thead>
												<tr>
													<th>Item Code</th>
													<th>Item Name</th>
													<th>Units</th>
													<th>Supplier</th>
													<th>Incharge</th>
													<th>Delete</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Items as $Item)

													<tr>
														<td><strong>{{ $Item->code }}</strong></td>
														<td><strong>{{ $Item->name }}</strong></td>
														<td>{{ $Item->unit }}</td>
														<td>{{ $Item->supplier->name }}</td>
														<td><button class="btn btn-sm btn-primary" onclick="viewItemIncharge('{{ $Item->id }}','Item Incharge for {{ $Item->name }} in {{ $title }} Department')">View</button></td>
														<td><a href="#" class="btn btn-sm btn-danger">Delete</a></td>
													</tr>
												@endforeach
											</tbody>

										</table>
									@else
										No items added yet!
		                        	@endif
		                        </div>
		                    </div>
							  <!-- END Project DETAILS BLOCK -->

			            </div>
						</div>
						
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection