@extends('master')

@section('title','Add New Contractor')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Add New Contractor</h3>
                <button type="button" onclick="$('#add_frm').submit()" class="btn btn-success pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => 'ContractorController@store', 'id'=>'add_frm']) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- Contractor NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Contractor's Primary Details</h5>
		                         
		                            	<div class="form-group row">
		                                    <label for="code" class="col-sm-4 form-control-label">Select Department</label>
		                                    <div class="col-sm-8">
												{{ Form::select('department', $Departments, null, ['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                            	 <div class="form-group row">
		                                    <label for="code" class="col-sm-4 form-control-label">Contractor Code</label>
		                                    <div class="col-sm-8">
												{{ Form::text('code', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="name" class="col-sm-4 form-control-label">Contractor Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                   <div class="form-group row">
		                                    <label for="email" class="col-sm-4 form-control-label">Contractor Email</label>
		                                    <div class="col-sm-8">
												{{ Form::text('email','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>
		                                 <div class="form-group row">
		                                    <label for="phone" class="col-sm-4 form-control-label">Contractor Phone</label>
		                                    <div class="col-sm-8">
												{{ Form::text('phone','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>
										
		                                   <div class="form-group row">
		                                    <label for="address" class="col-sm-4 form-control-label">Contractor Address</label>
		                                    <div class="col-sm-8">
												{{ Form::text('address','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                        </div>
		                    </div>

		                    <!-- END Contractor NAME BLOCK -->

							
			            </div>

			            <div class="col-lg-6"></div>

			            
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection