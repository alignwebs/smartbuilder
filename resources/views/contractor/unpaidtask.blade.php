@if($data->count() > 0)
	
	<p>{{ $data->count() }} unpaid tasks found.</p>
	<table class="table table-bordered datatable table-hover">
		<thead>
			<tr class="active">
				<th></th>
				<th>Product</th>
				<th>Stage</th>
				<th>Amount</th>
				
				<th>Date</th>
			</tr>
		</thead>

		<tbody>
			@foreach($data as $task)
				<tr>
					<td>{!! Form::checkbox('tasks[]', $task->id, null, ['class' => 'item', 'data-taskid' => $task->id ,'data-price' => $task->rate,'onchange' => 'updateItems()']) !!}</td>
					<td><a href="{{ route('product.show', $task->product_id) }}" target="_blank">{{ $task->product->product_name }}</a></td>
					<td>{{ $task->stage->name }}</td>
					<td>{{ currency($task->rate) }}</td>
					<td>{{ $task->date }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	

@else
	
	No unpaid orders!

@endif