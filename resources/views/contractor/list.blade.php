@extends('master')

@section('title','List Contractors')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Contractors: <small>{{ getOrganization('org_name') }}</small></h3>
              <a href="{{ URL::Action('ContractorController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Contractors->count() }} Contractors added to this organization</h5>
									
									@if($Contractors->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													
													<th>Contractor Code</th>
													<th>Contractor Name</th>
													<th>Email</th>
													<th>Phone</th>
													<th>Address</th>
													<th>Manage</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Contractors as $Contractor)
													<tr>
														<td><strong>{{ $Contractor->code }}</strong></td>
														<td><strong>{{ $Contractor->name }}</strong></td>
														<td>{{ $Contractor->email }}</td>
														<td>{{ $Contractor->phone }}</td>
														<td>{{ $Contractor->address }}</td>
														<td>{!! deleteEditModelBtn("contractor",$Contractor->id, $Contractor->name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast one Contractor
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection