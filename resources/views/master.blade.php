<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <title>@yield('title')</title>



    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>

    <meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}">

    <base href="{{ URL('/') }}/">



    <link rel="stylesheet" type="text/css" href="libs/bootstrap/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="libs/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">

    <link rel="stylesheet" type="text/css" href="libs/tether/css/tether.min.css">

    <link rel="stylesheet" type="text/css" href="assets/styles/common.min.css">

     <link rel="stylesheet" type="text/css" href="libs/jscrollpane/jquery.jscrollpane.css">

    <link rel="stylesheet" type="text/css" href="libs/flag-icon-css/css/flag-icon.min.css">

    <link rel="stylesheet" type="text/css" href="assets/styles/pages/auth.min.css">

   <link rel="stylesheet" type="text/css" href="assets/styles/fileicons.css">

    <!-- BEGIN THEME STYLES -->

    <link rel="stylesheet" type="text/css" href="assets/styles/themes/primary.min.css">

    

    <link rel="stylesheet" href="assets/styles/timeline.css">



    <!-- END THEME STYLES -->



    <link rel="stylesheet" type="text/css" href="assets/styles/widgets/panels.min.css">

<link rel="stylesheet" type="text/css" href="assets/scripts/charts/area/area.chart.min.css">

    <link rel="stylesheet" type="text/css" href="assets/scripts/charts/radial-progress/radial-progress.chart.min.css">

    <link rel="stylesheet" type="text/css" href="assets/styles/dashboard/projects.min.css">



    <link rel="stylesheet" type="text/css" href="libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->

    <link rel="stylesheet" type="text/css" href="assets/styles/libs/datatables-net/datatables.min.css"> <!-- customization -->

    <link rel="stylesheet" type="text/css" href="libs/select2/css/select2.min.css"> <!-- Original -->

    <link rel="stylesheet" type="text/css" href="assets/styles/libs/select2/select2.min.css"> <!-- Customization -->

    

    <link rel="stylesheet" type="text/css" href="libs/sweetalert/sweetalert.css"> <!-- original -->

    <link rel="stylesheet" type="text/css" href="assets/styles/libs/sweetalert/sweetalert.min.css"> <!-- customization -->



    <link rel="stylesheet" type="text/css" href="libs/prism/prism.css"> <!-- original -->

    <link rel="stylesheet" type="text/css" href="assets/styles/libs/bootstrap-notify/bootstrap-notify.min.css">

<link rel="stylesheet" type="text/css" href="libs/flatpickr/flatpickr.min.css"> <!-- original -->

<link rel="stylesheet" type="text/css" href="assets/styles/libs/flatpickr/flatpickr.min.css"> 

     <!-- customization -->

    

    <style>

    .org-logo {    text-align: center;

    padding: 20px;

    padding-bottom: 0;}



    .org-logo img {max-width: 190px;  }





        .submitBtn {     float: right; position: absolute; top: 0; z-index: 9; right: 14px; top: -79px; }

        tr.edited { background-color: #000 }



        .table thead th {     font-size: 11px; padding: 8px; }

        .table td { padding: 5px; }

        .table td:nth-child(1) { font-weight: bolder; }



        div.dataTables_wrapper>.row:first-child { border: none !important; }

            

        .ks-statistics  {}



        .ks-statistics .ks-item { text-align: center; margin-bottom: 20px }

        .ks-statistics .ks-amount { font-size: 24px; font-weight: 600; }

        .ks-statistics .ks-text { font-size: 14px }

        label {font-weight: bold;}



        

        a.link {text-decoration: underline;}

        a.link:hover { text-decoration: none !important; }

        .card .row.form-group { margin-bottom: -20px; }

        .card .row.form-group:last-child { margin-bottom: 20px }



        #preloader {    position: fixed;

        width: 100%;

        height: 100%;

        overflow: hidden;

        background: url('{{ asset('assets/img/loader.gif') }}')   rgba(255, 255, 255, 0.85);

        background-repeat: no-repeat;

        background-position: center center;

        z-index: 1;

        top: 0;

        left: 0;}





        @media(max-width: 1500px)

        {

            body {

                zoom: 90%;

            }

        }



    </style>

    @stack('css')

        

</head>

<body class="ks-navbar-fixed ks-sidebar-default ks-sidebar-fixed ks-theme-primary">

    

    @include('common.header')



  

        <div class="ks-container">

        

        @include('common.sidebar')

        

        <div id="preloader"></div>

        

        @include('flash::message')

        

        @yield('pagecontent')



        </div>

   





    @yield('footer')    

    

    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" id="infoModal">

    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title"></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true" class="fa fa-close"></span>

                </button>

            </div>

            <div class="modal-body"></div>

            <div class="modal-footer">

                <button type="button" class="btn btn-primary-outline ks-light" data-dismiss="modal">Close</button>

            </div>

        </div>

    </div>

</div>





  

<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="libs/jquery/jquery.min.js"></script>

<script src="libs/responsejs/response.min.js"></script>

<script src="libs/loading-overlay/loadingoverlay.min.js"></script>

<script src="libs/tether/js/tether.min.js"></script>

<script src="libs/bootstrap/js/bootstrap.min.js"></script>

<script src="libs/jscrollpane/jquery.jscrollpane.min.js"></script>

<script src="libs/jscrollpane/jquery.mousewheel.js"></script>

<script src="libs/flexibility/flexibility.js"></script>

<!-- END PAGE LEVEL PLUGINS -->



<!-- BEGIN THEME LAYOUT SCRIPTS -->

<script src="assets/scripts/common.js"></script>

<!-- END THEME LAYOUT SCRIPTS -->



<script src="libs/d3/d3.min.js"></script>

<script src="assets/scripts/charts/radial-progress/radial-progress.chart.js"></script>



<script src="libs/datatables-net/media/js/jquery.dataTables.min.js"></script>

<script src="libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>

<script src="libs/select2/js/select2.min.js"></script>

<script src="libs/bootstrap-notify/bootstrap-notify.min.js"></script>

<script src="libs/sweetalert/sweetalert.min.js"></script>

<script src="libs/count-up/count-up.min.js"></script>

<script src="libs/flatpickr/flatpickr.min.js"></script>

<script src="libs/prism/prism.js"></script>



<script type="application/javascript">

(function ($) {

    $(document).ready(function() {



        $('#flash-overlay-modal').modal();



        deleteCode()



        setupAlert()



        preloader('hide');



        $('input[name="date"]').flatpickr()



          $('select').select2();





         

   

        if($(window).width() > 1200 && $(window).width() < 1600)

         $('.ks-sidebar-toggle').click();



        $('.select2').css('width','100%')



        @if(!empty(Request::get('edited')))

         highlightEdited({{ Request::get('edited') }});  

        @endif



        @if(!empty(Request::get('created')))

         highlightCreated();  

        @endif



        @if(!empty(Request::get('deleted')))

         highlightDeleted();  

        @endif



        /*ADD CLASSES TO TABLE*/

        prettifyTable();

        /*END*/



        $('input').not('input[type=button], select, input.select2-search__field').each(function () {



                var $this = $(this)

                var label = $this.closest('div.form-group').find('label').html()



                $this.attr('placeholder',label)

                

        })







        $('.datatable').DataTable();



       

    });











    function deleteCode()

    {

        $('.deleteFrm').on('submit', function () {



            var title = $(this).find('.btn-delete').attr('data-title');



            var poll = confirm("Confirm Delete "+title+" ?");



            if(poll)

            {

                return true;

            }

            else

            {

                return false;

            }



        })

    }





    $('form').on('submit', function () {



        $(this).find('input.nospaces').each(function  (index) {

                var val = $(this).val()

                $(this).val(removeSpaces(val));

        })



        $(this).find('button[type=submit]').attr('disabled','disabled')



    })



    function removeSpaces(str)

    {

        str = str.replace(/\s+/g, '');



        return str;

    }



    function prettifyTable()

    {

        $(document).find('table.table').each(function (index) {

            $(this)

                    .addClass('ks-table-info')

                    .addClass('dt-responsive')

                    .addClass('nowrap')

                    .addClass('no-footer')

                    .addClass('dtr-inline')

        })

    }



    function highlightEdited(id)

    {

        var find = $('html').find('table tbody tr[data-id='+id+']');



        if(find.length > 0)

        {

            find.addClass('alert-success');



            $.notify({ 

            title: "Success:",

            message: "Highlighted item edited successfully!"

            }, 

            {

                type: 'success-active',

                offset: {

                    x: 30,

                    y: 70

                }

            });



        }

    }



    function highlightDeleted()

    {

     

            $.notify({ 

            title: "Deleted:",

            message: "Record has been deleted successfully!"

            }, 

            {

                type: 'danger-active',

                offset: {

                    x: 30,

                    y: 70

                }

            });



    }



    function highlightCreated()

    {

     

            $.notify({ 

            title: "Success:",

            message: "Record has been created successfully!"

            }, 

            {

                type: 'success-active',

                offset: {

                    x: 30,

                    y: 70

                }

            });



    }





    function preloader(option)

    {

        if(option == 'show')

        {  

            $('#preloader').show()

         }

        else

             $('#preloader').hide()

    }



     var options = {

            useEasing : true,

            useGrouping : true,

            separator : ',',

            decimal : '.',

            prefix : '',

            suffix : ''

        };



    $('.ks-amount[data-count-up]').each(function() {

            var countUp = parseInt($(this).data('count-up'));

            (new CountUp(this, 0, countUp, 0, 2, options)).start();

        });

   

    

    $( document ).ajaxStart(function() {

        if($(document).find('.modal.show').length == 0)

            preloader('show');



        $('.btn').attr('disabled','disabled')

    });



    $( document ).ajaxComplete(function() {

         preloader('hide');



         deleteCode()

          $('.datatable').DataTable();
          prettifyTable()

         $('.btn').removeAttr('disabled')

    });



})(jQuery);

</script>

<div class="ks-mobile-overlay"></div>



    @stack('scripts')

<script>

    function viewItemIncharge(id,title)

    {

        var url = "{{ URL::route('itemIncharge') }}";



        $.post(url, {"id":id, "_token":"{{ csrf_token() }}"}, function (data) {



                 $('#infoModal .modal-title').html(title);



                $('#infoModal .modal-body').html(data);



                $('#infoModal').modal('show')



        })

    }

</script>



<script>

$.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

});

$(document).on('click', 'a.delete-ajax', function(e) {

    e.preventDefault(); // does not go through with the link.



     var $this = $(this);







     swal({

            title: "Are You Sure?",

            text: "Press OK to Delete or press cancel.",

            type: "error",

            confirmButtonClass: "btn-danger",

            showCancelButton: true,

            closeOnConfirm: false,

            showLoaderOnConfirm: true



          }, function () {



               

                $.post({

                    type: $this.data('method'),

                    url: $this.attr('href')

                }).done(function (data) {

                   var success = swal({

                        title: "Deleted Successfully!",

                        text: "",

                        type: "success",

                        confirmButtonClass: "btn-success"

                      })

                    console.log(data);

                    window.location.href = data;

                });  

       

          });



    





});

</script>



<script>

    function setupAlert()

    {

        $.get("{{ URL::route('setupAlert') }}", function (data) {

            $('.setup-alert').html(data)

        })

    }



    function validateEmail(email) {

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        return re.test(email);

    }

</script>

</body>

</html>