@if($GrnItems->count() > 0)
	
	<p>{{ $GrnItems->count() }} unpaid items found.</p>
	<table class="table table-bordered datatable table-hover">
		<thead>
			<tr class="active">
				<th></th>
				<th>Order</th>
				<th>Item</th>
				<th>Qty</th>
				<th>Amount</th>
				<th>Date</th>
			</tr>
		</thead>

		<tbody>
			@foreach($GrnItems as $item)
				<tr>
					<td>{!! Form::checkbox('items[]', $item->id, null, ['class' => 'grnitem', 'data-grnid' => $item->id ,'data-price' => $item->total_price,'onchange' => 'updateItems()']) !!}</td>
					<td><a href="{{ route('view-order', $item->order->code) }}" target="_blank">{{ $item->order->code }}</a></td>
					<td>{{ $item->item->item_name }}</td>
					<td>{{ $item->qty }}</td>
					<td>{{ currency($item->total_price) }}</td>
					<td>{{ $item->created_at }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	

@else
	
	No unpaid orders!

@endif