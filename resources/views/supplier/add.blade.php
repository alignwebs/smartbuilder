@extends('master')

@section('title','Add New Supplier')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Add New Supplier</h3>
                <button type="button" onclick="$('#suplr_add_frm').submit()" class="btn btn-primary pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => 'SupplierController@store', 'id'=>'suplr_add_frm']) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- Supplier NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Supplier's Primary Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="suplr_code" class="col-sm-4 form-control-label">Supplier Code</label>
		                                    <div class="col-sm-8">
												{{ Form::text('suplr_code', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Supplier Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('suplr_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Supplier Email</label>
		                                    <div class="col-sm-8">
												{{ Form::text('suplr_email','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>
		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Supplier Phone</label>
		                                    <div class="col-sm-8">
												{{ Form::text('suplr_phone','',['class'=>'form-control nospaces']) }}
		                                    </div>
		                                </div>


		                        </div>
		                    </div>

		                    <!-- END Supplier NAME BLOCK -->

							<!-- Supplier MANAGER CONTACT PERSON BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Supplier Manager</h5>
		                          
		                            	
		                            	 <div class="form-group row">
		                                    <label for="suplr_contact_name" class="col-sm-4 form-control-label">Name</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_contact_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                 <div class="form-group row">
		                                    <label for="suplr_contact_phone" class="col-sm-4 form-control-label">Phone</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_contact_phone','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                <div class="form-group row">
		                                    <label for="suplr_contact_email" class="col-sm-4 form-control-label">Email</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::email('suplr_contact_email','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>	

		                                 <div class="form-group row">
		                                    <label for="suplr_contact_addr" class="col-sm-4 form-control-label">Address</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_contact_addr','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_contact_district" class="col-sm-4 form-control-label">District</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_contact_district','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                   <div class="form-group row">
		                                    <label for="suplr_contact_state" class="col-sm-4 form-control-label">State</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('suplr_contact_state', $indiaStates, null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_contact_pincode" class="col-sm-4 form-control-label">Pin Code</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_contact_pincode','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <input type="hidden" name="suplr_contact_role" value="Supplier Manager">


		                        </div>
		                    </div>
		                    <!-- END Supplier Manager CONTACT PERSON BLOCK -->

			            </div>

			            <div class="col-lg-6">

			            <!-- Supplier DETAILS BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Supplier Contact Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="suplr_address_one" class="col-sm-4 form-control-label">Address 1</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_address_one','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_address_two" class="col-sm-4 form-control-label">Address 2</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_address_two','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_city" class="col-sm-4 form-control-label">City</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_city','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_district" class="col-sm-4 form-control-label">District</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_district','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_state" class="col-sm-4 form-control-label">State</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::select('suplr_state', $indiaStates, null, ['placeholder' => 'Select a state','class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_pincode" class="col-sm-4 form-control-label">Pin Code</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_pincode','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>





		                         
		                        </div>
		                    </div>
							  <!-- END Supplier DETAILS BLOCK -->

							<!-- Supplier Coordinator CONTACT PERSON BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                        	<h5 class="card-title">Supplier Bank Details</h5>
		                         
		                            	
		                            	 <div class="form-group row">
		                                    <label for="suplr_meta_tin" class="col-sm-4 form-control-label">Tin No.</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('meta_tin','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_meta_bank_acc" class="col-sm-4 form-control-label">Bank Account No.</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_meta_bank_acc','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_meta_bank_name" class="col-sm-4 form-control-label">Bank Name</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_meta_bank_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_meta_bank_branch" class="col-sm-4 form-control-label">Bank Branch</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_meta_bank_branch','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_meta_bank_acc_type" class="col-sm-4 form-control-label">Bank Account Type</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_meta_bank_acc_type','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                 <div class="form-group row">
		                                    <label for="suplr_meta_bank_ifsc" class="col-sm-4 form-control-label">Bank IFSC Code</label>
		                                    <div class="col-sm-8">
		                                       	{{ Form::text('suplr_meta_bank_ifsc','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                        </div>
		                    </div>
		                    <!-- END Supplier CONTACT PERSON BLOCK -->



			            </div>
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection