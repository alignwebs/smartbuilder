@extends('master')

@section('title', $Supplier['name'])

@section('pagecontent')

<div class="ks-column ks-page">
  <div class="ks-header">

    <section class="ks-title">

      <h3>Supplier Details</h3>

    </section>
  </div>

  <div class="ks-content">
    <div class="ks-body ks-tabs-page-container">
     <div class="ks-tabs-container-description">
      <h3>{{ $Supplier['code'] }} - {{ $Supplier['name'] }}</h3>
      <p>Email: {{  @$Supplier['email']}} | Phone: {{  @$Supplier['phone']}} | City: {{  @$Supplier['city']}} | State: {{ @$Supplier['state'] }}</p>
    </div>

    <ul class="nav ks-nav-tabs ks-tabs-page-default">
      <li class="nav-item">
        <a class="nav-link active" href="#" data-toggle="tab" data-target="#items">
          Supplier Items
          <span class="badge badge-info badge-pill">{{ $Supplier->items->count() }}</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="tab" data-target="#bank">
          Bank Details
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="tab" data-target="#contacts">
          Contacts
          <span class="badge badge-info badge-pill">{{ $Supplier->contacts->count() }}</span>
        </a>
      </li>
    </ul>

    <div class="tab-content">
      <div class="tab-pane active ks-column-section" id="items" role="tabpanel">
        @if($Supplier->items->count() > 0)

        <table class="table datatable">
          <thead>
            <tr>
              <th>Item Code</th>
              <th>Item Name</th>
              <th>Units</th>
              <th>Supplier</th>
              <th>Delete</th>
            </tr>
          </thead>

          <tbody>
            @foreach($Supplier->items as $Item)
            <tr>
              <td><strong>{{ @$Item->item->code }}</strong></td>
              <td><strong>{{ @$Item->item->name }}</strong></td>
              <td>{{ @$Item->item->unit }}</td>
              <td>{{ @$Item->supplier->name }}</td>
              <td><a href="javascript:" class="btn btn-sm btn-danger btn-delete" onclick="removeSupplierItem({{ @$Item->item->id }}, {{ @$Item->supplier->id }})">Remove</a></td>
            </tr>
            @endforeach
          </tbody>

        </table>

        @endif
      </div>


      <div class="tab-pane" id="bank" role="tabpanel">
        <div class="row">
         <div class="col-md-6">

          <table class="table table-bordered">
            <tbody>
             <tr>
              <th>Bank Name</th>
              <td>{{ @$Supplier->meta_bank_name }}</td>
            </tr>
            <tr>
              <th>Bank Account No.</th>
              <td>{{ @$Supplier->meta_bank_acc }}</td>
            </tr>
            <tr>
              <th>Bank Branch</th>
              <td>{{ @$Supplier->meta_bank_branch }}</td>
            </tr>
            <tr>
              <th>Bank Account Type</th>
              <td>{{ @$Supplier->meta_bank_acc_type }}</td>
            </tr>
            <tr>
              <th>Bank IFSC Code</th>
              <td>{{ @$Supplier->meta_bank_ifsc }}</td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="tab-pane" id="contacts" role="tabpanel">
    @if($Supplier->contacts->count() > 0)
    <table class="table table-condensed table-bordered datatable">

      <thead>
       <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Address</th>
        <th>City</th>
        <th>District</th>
        <th>State</th>
        <th>Pincode</th>
      </tr>
    </thead>
    <tbody>
     @foreach($Supplier->contacts as $Contact)
     <tr>
       <td>{{ $Contact->name }}</td>
       <td>{{ $Contact->phone }}</td>
       <td>{{ $Contact->email }}</td>
       <td>{{ $Contact->address }}</td>
       <td>{{ $Contact->city }}</td>
       <td>{{ $Contact->district }}</td>
       <td>{{ $Contact->state }}</td>
       <td>{{ $Contact->pincode }}</td>
     </tr>
     @endforeach
   </tbody>
 </table>
 @else
 No contact available for this supplier.
 @endif
</div>
</div>


</div>
</div>
</div>
@endsection


@push('scripts')
  
  <script>
    function removeSupplierItem(item_id,supplier_id)
    {
      if(item_id > 0 && supplier_id > 0)
      {
         var poll = confirm("Confirm Remove ?");

         if(poll)
         {
            $.post('{{ route('supplier-item-remove') }}', {'item_id':item_id, 'supplier_id':supplier_id}, function (data) {

                  window.location.reload()

            })
         }
      }
    }
  </script>

@endpush