@extends('master')

@section('title','List Suppliers')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Suppliers: <small>{{ getOrganization('org_name') }}</small></h3>
            
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">List of all the suppliers in this organization</h5>
									
									@if($Suppliers->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													
													<th>Supplier Name</th>
													<th>Email</th>
													<th>Phone</th>
													<th>City</th>
													<th>State</th>
													<th></th>
												
												</tr>
											</thead>

											<tbody>
												@foreach($Suppliers as $Supplier)
													<tr>
														<td><a href="{{ URL::action('SupplierController@show',$Supplier->id) }}"><strong>{{ $Supplier->name }} - {{ $Supplier->code }}</strong></a></td>
														<td>{{ $Supplier->email }}</td>
														<td>{{ $Supplier->phone }}</td>
														<td>{{ $Supplier->city }}</td>
														<td>{{ $Supplier->state }}</td>
														<td>{!! deleteEditModelBtn('supplier', $Supplier->id, $Supplier->name) !!}</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast One Supplier
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection