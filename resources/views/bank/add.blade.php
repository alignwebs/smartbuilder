@extends('master')

@section('title','Add New Bank')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Add New Bank</h3>
                <button type="button" onclick="$('#suplr_add_frm').submit()" class="btn btn-primary pull-right">Add</button>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')
						
						{{ Form::open(['action' => 'BankAccountController@store', 'id'=>'suplr_add_frm']) }}
						
						<div class="row">
							<div class="col-lg-6">

						<!-- Bank NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">Bank's Primary Details</h5>
		                         
		                            	<div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Account Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>
		                            	
		                                <div class="form-group row">
		                                    <label for="suplr_name" class="col-sm-4 form-control-label">Bank Name</label>
		                                    <div class="col-sm-8">
												{{ Form::text('bank_name','',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                            	<div class="form-group row">
		                                    <label for="suplr_code" class="col-sm-4 form-control-label">Bank Account No.</label>
		                                    <div class="col-sm-8">
												{{ Form::text('account_number', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_code" class="col-sm-4 form-control-label">Bank IFSC Code</label>
		                                    <div class="col-sm-8">
												{{ Form::text('ifsc', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                                <div class="form-group row">
		                                    <label for="suplr_code" class="col-sm-4 form-control-label">Starting Balance</label>
		                                    <div class="col-sm-8">
												{{ Form::number('balance', '',['class'=>'form-control']) }}
		                                    </div>
		                                </div>

		                              


		                        </div>
		                    </div>

		                    <!-- END Bank NAME BLOCK -->

							
			            </div>

						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection