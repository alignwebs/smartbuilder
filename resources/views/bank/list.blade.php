@extends('master')

@section('title','List BankAccounts')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>List Bank Accounts: <small>{{ getProject('proj_name') }}</small></h3>
              <a href="{{ URL::Action('BankAccountController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							
							<div class="card">
			                        <div class="card-block">
			                            <h5 class="card-title">You have {{ $BankAccounts->count() }} Bank Accounts added to this project</h5>
										
										@if($BankAccounts->count() > 0)

											<table class="table datatable">
												<thead>
													<tr>
														
														
														<th>Account Name</th>
														<th>Bank Name</th>
														<th>Account No.</th>
														<th>IFSC</th>
														<th>Balance</th>
														<th>Last Updated</th>
														<th>Manage</th>
													</tr>
												</thead>

												<tbody>
													@foreach($BankAccounts as $BankAccount)
														<tr>
														
															<td><a href="{{ URL::action('BankAccountController@show',$BankAccount->id) }}"><strong>{{ $BankAccount->name }}</strong></a></td>
															<td>{{ $BankAccount->bank_name }}</td>
															<td>{{ $BankAccount->account_number }}</td>
															<td>{{ $BankAccount->ifsc }}</td>
															<td>{{ currency($BankAccount->balance) }}</td>
															<td>{{ $BankAccount->updated_at }}</td>
															<td>{!! deleteEditModelBtn("bank-accounts",$BankAccount->id, $BankAccount->name) !!}</td>
														</tr>
													@endforeach
												</tbody>

											</table>

										@else
											Please add atleast one department
										@endif
			                        </div>
			                </div>

						</div>
							
				     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection