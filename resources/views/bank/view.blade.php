@extends('master')

@section('title', $BankAccount->name)
 
@section('pagecontent')


<style>
	.quick-stats p { margin:0; padding: 0; font-size: 22px; }

	</style>
  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>{{ $BankAccount->name }} <small>Bank Account Details</small> </h3>
               <a href="{{ route('bank-accounts.edit', $BankAccount->id) }}" class="btn btn-primary pull-right">Edit Details</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						<div class="row quick-stats">
										<div class="col-md-3">
										<div class="card">
						                        <div class="card-block">
						                        	<h5 class="card-title">Current Balance</h5>
											
													<p>{{ currency($BankAccount->balance) }}</p>

						                        </div>
						                    </div>

											
										</div>
									</div>

						
						<div class="row">
							<div class="col-lg-12">
							
								
							

			            
								
							<div class="card">
		                        <div class="card-block">
		                        	<h5 class="card-title">Bank Transactions for {{ $BankAccount->name }}</h5>

		                        
		                        @if($transactions->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
		                        				<th>ID</th>
		                        				<th>User</th>
		                        				<th>Payment Method</th>
		                        				<th>Payment Type</th>
		                        				<th>Reference</th>
		                        				<th>Type</th>
		                        				<th>Amount</th>
		                        			</tr>
											</thead>

											<tbody>
												@foreach($transactions as $transaction)
													<tr>
														<td><strong><a href="{{ route('receipt', $transaction->id) }}" target="_blank">{{ @$transaction->gen_payment_id }}</a></strong></td>
														<td>{{ @$transaction->user->name }}</td>
														<td>{{ @$transaction->paymentMethod->name }}</td>
														<td>{{ @$transaction->paymentType->name }}</td>
														<td>{{ @$transaction->payment_method_ref }}</td>
														<td>{{ @$transaction->type }}</td>
														<td>{{ currency(@$transaction->amount) }}</td>
												</tr>
												@endforeach
											</tbody>

										</table>

									@else
										No transactions found for this bank account.
									@endif


		                        </div>
		                    </div>

		                    <!-- END ORGANIZATION NAME BLOCK -->

			            </div>

			            <div class="col-lg-6">
			            	
			            </div>
						</div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection

@push('scripts')


@endpush