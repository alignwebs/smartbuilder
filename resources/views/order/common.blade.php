
@push('scripts')

	<script>

		var items = [];

		function addItem(e)
		{
			var supplierid = $(e).attr('data-supplierid');
			var suppliercode = $(e).attr('data-suppliercode');
			var suppliername = $(e).attr('data-suppliername');

			var itemid = $(e).attr('data-itemid');
			var itemcode = $(e).attr('data-itemcode');
			var itemname = $(e).attr('data-itemname');
			var itemunit = $(e).attr('data-itemunit');
			var itemunitprice = $(e).attr('data-itemunitprice');
			var discount = $(e).attr('data-discount');
			var roq = $(e).attr('data-roq');
			
			var taxname = $(e).attr('data-taxname');
			var tax = $(e).attr('data-tax');

			var item = {"supplierid":supplierid,"suppliercode":suppliercode,"suppliername":suppliername,"itemid":itemid,"itemcode":itemcode,"itemname":itemname,"itemunit":itemunit,"itemqty":roq,"itemunitprice":itemunitprice,"itemdiscount":discount, "taxname":taxname,"tax":tax ,"itemnetunitprice":0, "total":0};

			items.push(item)

			renderTable()

		}

		function deleteItem(index)
	    {
	        items[index] = "";
	        renderTable()
	    }

	    function updateItem(key,index,e)
	    {
	    	var val = $(e).val();
	
	    	items[index][key] = val;

	    	renderTable()
	    }

		function renderTable()
		{
			 if(items.length > 0)
		        {
		            $('#orderitems tbody').html('');

		            var html = "";
		            var grand_total = 0;

		            $.each(items, function (index,value) {

		            	var discount = 0;
		            	var netunitprice = 0;
		            	var totalunitprice = 0;

		            	if($(value).size() > 0)
		            	{
		            		var netunitprice = parseFloat(value.itemunitprice);

		            		if(value.itemdiscount > 0)
		            		{
		            			discount = parseFloat(parseFloat(value.itemdiscount/100)*netunitprice).toFixed(2);
		            		}



		            		netunitprice = parseFloat(netunitprice - discount).toFixed(2);

		            		tax = parseFloat(parseFloat(value.tax/100)*netunitprice).toFixed(2);

		            		netunitprice = (+netunitprice + +tax).toFixed(2)

		            		totalunitprice = parseFloat(netunitprice * value.itemqty).toFixed(2);

		            		grand_total = parseFloat(+grand_total + +totalunitprice).toFixed(2);

		            		
		            		

		            		items[index]['itemnetunitprice'] = netunitprice;
		            		items[index]['total'] = totalunitprice;	

		            		html = '<tr><td>'+value.itemcode+'</td><td>'+value.itemname+'</td><td>'+value.suppliercode+'</td><td>'+value.suppliername+'</td><td><input type="number" value="'+value.itemqty+'" class="itemqty" min="0"  onBlur="updateItem(\'itemqty\','+index+',this)" ></td><td>'+value.itemunit+'</td><td><input type="number" value="'+value.itemunitprice+'" class="itemunitprice" min="0"  onBlur="updateItem(\'itemunitprice\','+index+',this)" ></td><td><input type="number" value="'+value.itemdiscount+'" onBlur="updateItem(\'itemdiscount\','+index+',this)" class="itemdiscount" min="0"></td><td><input type="number" value="'+value.tax+'" onBlur="updateItem(\'tax\','+index+',this)" class="tax" min="0"></td><td>'+netunitprice+'</td><td>'+totalunitprice+'<td><a href="javascript:" onclick="deleteItem('+index+')">Delete</a></td></tr>';

		            		 $(html).appendTo('#orderitems tbody');

		            	}


		            })

		            $('#grand-total').html("Rs. "+grand_total+"/-")

		        }
			
		}


		function getSupplierItems(e)
		{
			var supplier_id = $(e).val();
			$('#itemTarget').html("Loading items...")

			$.get("{{ URL::route('supplierItems') }}", {'supplier_id':supplier_id,'table':true}, function (data) {

					$('#itemTarget').html(data)
					$('#itemTarget table').dataTable()
			})
		}


		function placeOrder()
		{
			if(items.length > 0) {

			

					swal({
					  title: "Confirm Place This Order?",
					  text: "Press OK to place order or press cancel.",
					  type: "info",
					  showCancelButton: true,
					  closeOnConfirm: false,
					  showLoaderOnConfirm: true
					}, function () {

					    var success = swal({
							  title: "Order Placed Successfully!",
							  text: "",
							  type: "success",
							  confirmButtonClass: "btn-success"
							})

					    saveOrder(success);
			 
					});

			}
		}


		function saveDraft()
		{
			if(items.length > 0) {

				var data = JSON.stringify(items);

				$.post("{{ URL::action("OrderController@store") }}", {"items":data,"draft":1 , "type":"draft", "_token":"{{ csrf_token() }}"}, function (data) {

						 window.location.href = data;
				});
			}
		}

		function saveOrder(response)
		{
			var data = JSON.stringify(items);

			$.post("{{ URL::action("OrderController@store") }}", {"items":data,"order_id": "{{ @$Order->id }}" , "draft":0 ,"type":"save" , "_token":"{{ csrf_token() }}"}, function (data) {

					response

					setTimeout(function () { window.location.href = data},2000);

			});
		}
	</script>


@endpush