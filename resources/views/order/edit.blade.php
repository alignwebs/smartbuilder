@extends('master')

@section('title','Draft Order: '.$Order->code)

@section('pagecontent')

<style>
	#orderitems input { width: 80px; text-align: right }
	.dataTables_length { display: none }
	.select2-container { width: 300px !important;  z-index: 99; position: absolute;  }
	#itemTarget { clear: both; min-height: 30px  }
</style>

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Drafted Order: {{ $Order->code }} <small>Last Update: {{ $Order->updated_at }}</small></h3>
                <div class="pull-right">
                <button type="button" onclick="saveDraft()" class="btn btn-default ">Save Draft</button>
                <button type="button" onclick="placeOrder()" class="btn btn-success">Place Order</button>
                </div>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						@include('common.pagetop')

						{{ Form::open(['action' => 'OrderController@store', 'id'=>'add_frm']) }}
						
						<div class="row">
							<div class="col-lg-12">
						
						<!-- Order NAME BLOCK -->
							<div class="card">
		                        <div class="card-block">
		                        	<h5 class="card-title">Supplier Items</h5>
		                        	{{ Form::select('supplier', $Suppliers, null, ['placeholder'=>'Select Supplier','class'=>'form-control pull-right supplier', 'onChange'=>'getSupplierItems(this)']) }}
		                        	<div id="itemTarget"></div>
		                        </div>
		                    </div>

		                <!-- END Order NAME BLOCK -->

							
			            </div>
			            </div>

			            <div class="row">
	
						
			            <div class="col-lg-12">
			            	
			            	<div class="card">
		                        <div class="card-block">
		                        	<h5 class="card-title">Order Items</h5>
		                        	<table class="table table-condensed table-bordered table-hover" id="orderitems">
                                    <thead>
	                                    <tr>
	                                        <th>Item Code</th>
	                                        <th>Item Name</th>
	                                        <th>Supplier Code</th>
	                                        <th>Supplier Name</th>
	                                        <th>Qty</th>
	                                        <th>Units</th>
	                                        <th>Unit Price</th>
	                                        <th>Discount %</th>
	                                        <th>Tax %</th>
	                                        <th>Net Unit Price</th>
	                                        <th>Invoice Value</th>
	                                        <th>Delete</th>
	                                    </tr>
                                    </thead>
                                    <tbody>
                                    	
                                    </tbody>
                                </table>

                                <div class="text-right">
                                	<h3>Grand Total: <span id="grand-total"></span></h3>
                                </div>
		                        </div>
		                    </div>

			            </div>

			            
						</div>
						{{ Form::close() }}
					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection

@include('order.common')

@push('scripts')
	
	<script>
	var grnitems = [ @foreach($Order->draftItems as $item)
						{!! $item !!},
					 @endforeach ]



		$(document).ready(function () {
			console.log(grnitems)
			items  = grnitems;
			renderTable()
		})
	</script>

@endpush