@extends('master')

@section('title', $Order->code)

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">
				<h3>Order Details</h3>
				<div class="pull-right">
				<button class="btn btn-primary" onclick="PrepareGRN()">Prepare GRN</button> &nbsp;
	           	<a href="{{ URL::Action('OrderController@create') }}"  class="btn btn-success">New Order</a>
	           	</div>
	        </section>
        </div>

		<div class="ks-content">
            <div class="ks-body ks-tabs-page-container">
 <div class="ks-tabs-container-description">
                    <h3>{{ $Order->code }}</h3>
                    <p>Created By: {{ $Order->user->name }} | Created On: {{ $Order->created_at }}</p>
                </div>
     
			<ul class="nav ks-nav-tabs ks-tabs-page-default">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-toggle="tab" data-target="#items">
                            Order Items
                            <span class="badge badge-info badge-pill">{{ $Order->items->count() }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#grn">
                            Good Received Notes
                        </a>
                    </li>
                    
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active ks-column-section" id="items" role="tabpanel">
                    	@if($Order->items->count() > 0)
							<table class="table table-bordered table-hover">
								<thead>
									<th>Item Name</th>
									<th>Item Code</th>
									<th>Supplier Name</th>
									<th>Supplier Code</th>
									<th>Qty</th>
									<th>Unit</th>
									<th>Unit Price (Rs.)</th>
									<th>Discount</th>
									<th>Tax Type</th>
									<th>Net Unit Price (Rs.)</th>
									<th>Total Price (Rs.)</th>
									
								</thead>

								<tbody>
									@foreach($Order->items as $item)
										<tr>
											<td>{{ $item->item_name }}</td>
											<td>{{ $item->item_code }}</td>
											<td>{{ $item->supplier_name }}</td>
											<td>{{ $item->supplier_code }}</td>
											<td>{{ $item->qty }}</td>
											<td>{{ $item->unit }}</td>
											<td>{{ $item->unit_price }}</td>
											<td>{{ $item->discount }}%</td>
											<td>{{ $item->tax_name }} - {{ $item->tax }}%</td>
											<td>{{ $item->net_unit_price }}</td>
											<td style="font-weight: bold;">{{ $item->total_price }}</td>
											
										</tr>
									@endforeach
									<tr><td colspan="10" class="text-right">Grand Total:</td><td  style="font-weight: bold;">{{ $Order->invoice_total }}</td></tr>
								</tbody>
							</table>
						@else
							No items inside this order.
                    	@endif
                    </div>


                    <div class="tab-pane" id="grn" role="tabpanel">
                    	@if($Order->grn->count() > 0)
							
							<table class="table table-bordered table-hover datatable">

								<thead>
									<th>Supplier</th>
									<th>Item Name</th>
									<th>Item Code</th>
									<th>R. Qty</th>
									<th>O. Qty</th>
									<th>Amount</th>
									<th>Creation Date</th>
									<th>Status</th>
								</thead>

								@foreach($Order->grn->sortByDesc('id') as $grn)
									@php
										if(!empty($grn->payment_id))
										{
											$badge_class = "badge-success";
											$badge_text  = "Paid";
											$url = route('payments-transaction')."/".$grn->payment_id;
										}
										else
										{
											$badge_class = "badge-default";
											$badge_text  = "Unpaid";
											$url = "";
										}
									@endphp
									<tr>
										<td>{{ $grn->item->supplier_code." - ".$grn->item->supplier_name }}</td>
										<td>{{ $grn->item->item_name }}</td>
										<td>{{ $grn->item->item_code }}</td>
										<td>{{ $grn->qty." ".$grn->item->unit }}</td>
										<td>{{ $grn->item->qty." ".$grn->item->unit }}</td>
										<td>{{ $grn->total_price }}</td>
										<td>{{ $grn->created_at }}</td>
										<td><a href="{{ $url }}" target="_blank"><span class="badge {{ $badge_class }}">{{ $badge_text }}</span></a></td>
									</tr>

								@endforeach
								
							</table>

                    	@else
							No GRN Found!
                    	@endif



                    </div>


                </div>
		

   </div>
	</div>
				</div>


<!-- GRN MODAL -->
<div class="modal" tabindex="-1" role="dialog" id="grnModalAll">
    <div class="modal-dialog modal-lg" style="max-width: 98% !Important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">GRN Prepare</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="fa fa-close"></span>
                </button>
            </div>
            	
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" onclick="$('form#grn').submit()" class="btn btn-primary">Submit</button>
            </div>
            
        </div>
    </div>
</div>

@endsection



@push('scripts')
<script>
	function PrepareGRN()
	{
		$('#grnModalAll .modal-body').html('<h4>Preparing GRN. Please wait...</h4>');
		$('#grnModalAll').modal({backdrop:'static'},'show')
		$.get('{{ route('prepare-grn', $Order->id) }}', function (data) {
			$('#grnModalAll .modal-body').html(data);
		})
		
	}
</script>



@endpush