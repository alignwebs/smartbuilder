@if($data->count() > 0)
	
	{{ Form::open(['action'=>'OrderGrnController@store', 'id'=>'grn']) }}
	<table class="datatable table">
		<thead>
			<th>Item Name</th>
			<th>Item Code</th>
			<th>Supplier Name</th>
			<th>Supplier Code</th>
			
			<th>Unit</th>
			<th>Unit Price (Rs.)</th>
			<th>Discount</th>
			<th>Tax Type</th>
			<th>Net Unit Price (Rs.)</th>
			<th>Total Price (Rs.)</th>
			<th>Qty</th>
			<th>Avail Qty</th>
			<th>GRN Qty</th>
			<th>GRN Total (Rs.)</th>
		</thead>
		<tbody>
			@foreach($data as $item)
				@if($item->qty_left > 0)
				<tr data-rowid="{{ $item->id }}">
					<td>{{ $item->item_name }}</td>
					<td>{{ $item->item_code }}</td>
					<td>{{ $item->supplier_name }}</td>
					<td>{{ $item->supplier_code }}</td>
					<td>{{ $item->unit }}</td>
					<td>{{ $item->unit_price }}</td>
					<td>{{ $item->discount }}%</td>
					<td>{{ $item->tax_name }} - {{ $item->tax }}%</td>
					<td>{{ $item->net_unit_price }}</td>
					<td>{{ $item->total_price }}</td>
					<td>{{ $item->qty }}</td>
					<td>{{ $item->qty_avail }}</td>
					<td>{{ Form::number('data['.$item->id.'][raisegrnqty]', $item->qty_left, ['class'=>'form-control qty', 'required'=>'', 'max'=>$item->qty_left, 'onkeyup'=>'changeTotal('.$item->id.','.$item->net_unit_price.')' ]) }}</td>
					<td>{{ Form::number('data['.$item->id.'][total]', ($item->qty_left*$item->net_unit_price), ['class'=>'form-control total', 'required'=>'', 'readonly'=>'readonly']) }}</td>
					{{ Form::hidden('data['.$item->id.'][orderid]',$item->order_id) }}
					{{ Form::hidden('data['.$item->id.'][itemid]', $item->item_id) }}
				</tr>
				@endif
			@endforeach
			
		</tbody>
	</table>
	
	{{ Form::close() }}
@endif
<!-- <script src="http://localhost:8000/libs/jquery/jquery.min.js"></script> -->

<script>
	function changeTotal(key,net_unit_price)
	{
		var qty = $('tr[data-rowid="'+key+'"] input.qty').val();
		var total = parseFloat(qty * net_unit_price).toFixed(2);

		$('tr[data-rowid="'+key+'"] input.total').val(total);

	}

	$('form#grn').submit(function () {

		var fields = $(this).serialize()
		var action = $(this).attr('action')

		$(this).find('input').each(function () {
			$(this).attr('disabled','disabled')
		})

		$.post(action, fields, function (data){
			console.log(data)
			
			if($.trim(data) == '')
				window.location.reload();
			
		})
	
			
		return false;

	})


</script>