@extends('master')

@section('title','Project Orders')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Project Orders: <small>{{ getProject('proj_name') }}</small></h3>
              <a href="{{ URL::Action('OrderController@create') }}"  class="btn btn-success pull-right">Add New</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Orders->count() }} Orders added to this project</h5>
									
									@if($Orders->count() > 0)

										<table class="table datatable table-condensed table=bordered table-hover">
											<thead>
												<tr>
													
													<th>Order Code</th>
													<th>Invoice Total</th>
													<th>Created On</th>
													<th>Manage</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Orders as $Order)
													<tr>
														<td><a href="{{ URL::action('OrderController@show',$Order->code) }}" class=""><strong>{{ $Order->code }}</strong></a>
														@if($Order->draft > 0)
															&nbsp;&nbsp;<span class="badge badge-default">Draft</span>
														@endif
														</td>
														<td>{{ $Order->invoice_total }}</td>
														<td>{{ $Order->created_at }}</td>
														<td>
															@if($Order->draft > 0)
																<a href="{{ route('order.destroy',$Order->id) }}" data-method="delete" class="btn btn-default btn-sm pull-right delete-ajax">Delete</a>
															@endif
														</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										No orders yet!
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection