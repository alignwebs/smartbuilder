@extends('master')

@section('title', $Order->code)

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">
				<h3>Order Details</h3>
				<div class="pull-right">
				<button class="btn btn-primary" onclick="PrepareGRN()">Prepare GRN</button> &nbsp;
	           	<a href="{{ URL::Action('OrderController@create') }}"  class="btn btn-success">New Order</a>
	           	</div>
	        </section>
        </div>

		<div class="ks-content">
            <div class="ks-body ks-tabs-page-container">
 <div class="ks-tabs-container-description">
                    <h3>{{ $Order->code }}</h3>
                    <p>Created By: {{ $Order->user->name }} | Created On: {{ $Order->created_at }}</p>
                </div>
     
			<ul class="nav ks-nav-tabs ks-tabs-page-default">
                    <li class="nav-item">
                        <a class="nav-link active" href="#" data-toggle="tab" data-target="#items">
                            Order Items
                            <span class="badge badge-info badge-pill">{{ $Order->items->count() }}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" data-toggle="tab" data-target="#grn">
                            Good Received Notes
                        </a>
                    </li>
                    
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active ks-column-section" id="items" role="tabpanel">
                    	@if($Order->items->count() > 0)
							<table class="table table-bordered table-hover">
								<thead>
									<th>Item Name</th>
									<th>Item Code</th>
									<th>Supplier Name</th>
									<th>Supplier Code</th>
									<th>Qty</th>
									<th>GRN Qty</th>
									<th>Unit</th>
									<th>Unit Price (Rs.)</th>
									<th>Discount</th>
									<th>Tax Type</th>
									<th>Net Unit Price (Rs.)</th>
									<th>Total Price (Rs.)</th>
									<th>Option</th>
								</thead>

								<tbody>
									@foreach($Order->items as $item)
										<tr>
											<td>{{ $item->item_name }}</td>
											<td>{{ $item->item_code }}</td>
											<td>{{ $item->supplier_name }}</td>
											<td>{{ $item->supplier_code }}</td>
											<td>{{ $item->qty }}</td>
											<td>{{ $Order->grnItemQty($item->item_id) }}</td>
											<td>{{ $item->unit }}</td>
											<td>{{ $item->unit_price }}</td>
											<td>{{ $item->discount }}%</td>
											<td>{{ $item->tax_name }} - {{ $item->tax }}%</td>
											<td>{{ $item->net_unit_price }}</td>
											<td style="font-weight: bold;">{{ $item->total_price }}</td>
											<td><a href="javascript:" onclick="addGrn('{{$item->qty}}','{{$Order->grnItemQty($item->item_id)}}','{{$item->unit}}','{{ $item->unit_price }}','{{ $item->item_id }}','{{ $item->order_id }}','{{ $item->item_name }}')">Add GRN</a></td>
										</tr>
									@endforeach
									<tr><td colspan="10" class="text-right">Grand Total:</td><td  style="font-weight: bold;">{{ $Order->invoice_total }}</td></tr>
								</tbody>
							</table>
						@else
							No items inside this order.
                    	@endif
                    </div>


                    <div class="tab-pane" id="grn" role="tabpanel">
                    	@if($Order->grn->count() > 0)
							
							<table class="table table-bordered table-hover datatable">

								<thead>
									<th>Item Name</th>
									<th>Item Code</th>
									<th>Raise Quantity</th>
									<th>Prev Quantity</th>
									<th>Total Value</th>
									<th>Date</th>
									<th>Creation Date</th>
								</thead>

								@foreach($Order->grn->sortByDesc('id') as $grn)

									<tr>
										<td>{{ $grn->item->name }}</td>
										<td>{{ $grn->item->code }}</td>
										<td>{{ $grn->qty }}</td>
										<td>{{ $grn->total_grn_qty }} out of {{ $Order->items->where('item_id',$grn->item_id)->pluck('qty') }}</td>
										<td>{{ $grn->total_price }}</td>
										<td>{{ $grn->date }}</td>
										<td>{{ $grn->created_at }}</td>
									</tr>

								@endforeach
								
							</table>

                    	@else
							No GRN Found!
                    	@endif



                    </div>


                </div>
		

   </div>
	</div>
				</div>

    <div class="modal fade" tabindex="-1" role="dialog" id="grnModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add GRN Quantity - <span id="itemname"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="fa fa-close"></span>
                </button>
            </div>
            	{{ Form::open(['action'=>'OrderGrnController@store']) }}
            <div class="modal-body">
            	
            
					
						<div class="form-group row">
                            <label for="code" class="col-sm-4 form-control-label">Ordered Quantity:</label>
                            <div class="col-sm-8">
								<p class="form-control-static" id="qty"></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="code" class="col-sm-4 form-control-label">GRN Quantity:</label>
                            <div class="col-sm-8">
								<p class="form-control-static" id="grnqty"></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="code" class="col-sm-4 form-control-label">Raise GRN By:</label>
                            <div class="col-sm-8">
								  	{{ Form::number('raisegrnqty', null, ['class'=>'form-control input-lg', 'required'=>'']) }}
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="code" class="col-sm-4 form-control-label">Date:</label>
                            <div class="col-sm-8">
								  	{{ Form::text('date', '', ['class'=>'form-control input-lg', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=>date('Y-m-d') , 'required'=>'']) }}
                            </div>
                        </div>
				<input type="hidden" name="itemid">
				<input type="hidden" name="itemunitprice">
				<input type="hidden" name="orderid">

   

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>


<!-- GRN MODAL -->
<div class="modal" tabindex="-1" role="dialog" id="grnModalAll">
    <div class="modal-dialog modal-lg" style="max-width: 98% !Important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">GRN Prepare</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="fa fa-close"></span>
                </button>
            </div>
            	
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" onclick="$('form#grn').submit()" class="btn btn-primary">Submit</button>
            </div>
            
        </div>
    </div>
</div>

@endsection



@push('scripts')
<script>

function addGrn(qty,grnqty,unit,unitprice,itemid,orderid,itemname)
{
	var qty_left = qty - grnqty;

	$('#itemname').html(itemname);

	$('#grnModal #qty').html(qty+"&nbsp;"+unit)

	$('#grnModal #grnqty').html(grnqty+"&nbsp;"+unit)

	$('#grnModal input[name="raisegrnqty"]')
			.attr('max',qty_left)
			.val(qty_left)

	$('#grnModal input[name="itemid"]').val(itemid)
	$('#grnModal input[name="orderid"]').val(orderid)
	$('#grnModal input[name="itemunitprice"]').val(unitprice)


	$('#grnModal input[name="date"]').flatpickr()

	$('#grnModal').modal('show')

}

</script>

<script>
	function PrepareGRN()
	{
		$('#grnModalAll .modal-body').html('<h3>Preparing GRN. Please wait...</h3>');
		$('#grnModalAll').modal({backdrop:'static'},'show')
		$.get('{{ route('prepare-grn', $Order->id) }}', function (data) {
			$('#grnModalAll .modal-body').html(data);
		})
		
	}
</script>



@endpush