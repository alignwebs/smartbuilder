@if($items->count() > 0)
	
	<table class="table datatable">
		<thead>
			<th>Item Name</th>
			<th>Item Code</th>
			<th>Supplier Name</th>
			<th>Supplier Code</th>
			<th>Avail Qty</th>
			<th>Select</th>
		</thead>
		<tbody>
			@foreach($items as $item)
				
				<tr data-rowid="{{ $item->item_id }}">
					<td>{{ $item->item->item_name }}</td>
					<td>{{ $item->item->item_code }}</td>
					<td>{{ $item->item->supplier_name }}</td>
					<td>{{ $item->item->supplier_code }}</td>
					<td>{{ $item->avail_qty." ".$item->item->unit }}</td>
					<td>
					 @if($item->avail_qty > 0)
						<a href="javascript:" class="link" onclick="addItem(this)" 
						
						data-supplierid="{{ $item->item->supplier_id }}" 
						data-suppliercode="{{ $item->item->supplier_code }}" 
						data-suppliername="{{ $item->item->supplier_name }}" 

						data-itemid="{{ $item->item_id }}" 
						data-itemcode="{{ $item->item->item_code }}" 
						data-itemname="{{ $item->item->item_name }}" 
						data-itemunit="{{ $item->item->unit }}" 
						
						data-qtyavail="{{ $item->avail_qty }}" 


						><span class="fa fa-plus" aria-hidden="true"></span> Select</a>
					@else
						<span class="badge badge-default">out of stock</span>
					@endif

					</td>
				</tr>

			@endforeach
			
		</tbody>

	</table>

@else
	
	No Products found in inventory for this department.

@endif

