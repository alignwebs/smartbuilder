@extends('master')

@section('title','Inventory')

@section('pagecontent')

  <div class="ks-column ks-page">
        <div class="ks-header">

            <section class="ks-title">

                <h3>Project Inventory: <small>{{ getProject('proj_name') }}</small></h3>
              <a href="{{ route('material-transfer.index') }}"  class="btn btn-primary pull-right">Material Transfer</a>
            </section>
        </div>

		<div class="ks-content">
            <div class="ks-body">
				
				<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						
						<div class="card">
		                        <div class="card-block">
		                            <h5 class="card-title">You have {{ $Items->count() }} items added to this project</h5>
									
									@if($Items->count() > 0)

										<table class="table datatable">
											<thead>
												<tr>
													<th>Item Code</th>
													<th>Item Name</th>
													<th>Used Qty</th>
													<th>Avail Qty</th>
													<th>Status</th>
												</tr>
											</thead>

											<tbody>
												@foreach($Items as $Item)
													<tr>
														<td><strong>{{ $Item->item->item_code }}</strong></td>
														<td><strong>{{ $Item->item->item_name }}</strong></td>
														<td>{{ $Item->used_qty." ".$Item->item->unit }}</td>
														<td>{{ $Item->avail_qty." ".$Item->item->unit }}</td>
														<td>
														@if($Item->avail_qty > 0)
															<span class="badge badge-success">In Stock</span>
														@else
															<span class="badge badge-danger">Out of Stock</span>
														@endif
														</td>
													</tr>
												@endforeach
											</tbody>

										</table>

									@else
										Please add atleast One item to project
									@endif
		                        </div>
		                </div>

					</div>
						
			     </div>

			            
		        </div>

   </div>
	</div>
				</div>
@endsection