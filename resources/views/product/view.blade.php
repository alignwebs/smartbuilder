@extends('master')



@section('title', $Product['product_name'])



@section('pagecontent')



<style>

  #modal .select2  { width:100% !important; }

  .timeline p { font-size:  15px}

</style>

<div class="ks-column ks-page">

  <div class="ks-header">

    <section class="ks-title">



      <h3>Product Details</h3>



      <div class="">

        

        <button class="btn btn-default" type="button" onclick="$('#change-request').modal('show')">Add Change Request</button>

        <button class="btn btn-default" type="button" onclick="$('#sale-price').modal('show')">Set Sale Price</button>

        <button class="btn btn-default" type="button" onclick="$('#modal').modal('show')">Add Progress Stage</button>



      </div>







    </section>

  </div>



  <div class="ks-content">

    <div class="ks-body ks-tabs-page-container">

     <div class="ks-tabs-container-description">

      <h3>{{ $Product['product_name'] }} - {{ $Product['product_details'] }}</h3>

      <p><span class="badge badge-default">Product Category: {{  @$Product->productcategory->product_category_name }}</span> <span class="badge badge-default">Product Sale Price: {{  currency($Product->sale_price) }}</span></p>



    </div>



    <ul class="nav ks-nav-tabs ks-tabs-page-default">

      <li class="nav-item">

        <a class="nav-link active" href="#stages" data-toggle="tab" data-target="#tab1">

         Progress Stages



       </a>

     </li>

     <li class="nav-item">

      <a class="nav-link" href="#material" data-toggle="tab" data-target="#tab2">

        Material Transferred

      </a>

    </li>

    <li class="nav-item">

      <a class="nav-link" href="#customer" data-toggle="tab" data-target="#tab3">

        Customer Details & Payments



      </a>

    </li>

       <li class="nav-item">

      <a class="nav-link" href="#customer" data-toggle="tab" data-target="#tab4">

        Change Requests

      </a>

    </li>

  </ul>



  <div class="tab-content">

    <div class="tab-pane active ks-column-section" id="tab1" role="tabpanel">      





      @if($Product->stages->count() > 0)



      <table class="table datatable">

        <thead>

          <th>Stage</th>

          <th>Contractor</th>

          <th>Date</th>

          <th>Remark</th>

          <th></th>

        </thead>





        @foreach($Product->stages as $Stage)



        <tr>

          <td>{{ $Stage->stage->code }} - {{ $Stage->stage->name }}</td>

          <td>{{ $Stage->contractor->name }}</td>

          <td>{{ $Stage->date }}</td>

          <td>{{ $Stage->remark }}</td>

          <td>{!! deleteEditModelBtn("product-stages",$Stage->id, $Stage->stage->name) !!}</td>

        </tr>



        @endforeach

      </table>







      @else

      No Stages Completed!

      @endif



    </div>





    <div class="tab-pane" id="tab2" role="tabpanel">



     <h5 class="card-title">List of Materials Transferred to this product</h5>



     @if($Product->items->count() > 0)



     <table class="table datatable">

      <thead>

        <th>Department</th>

        <th>Item</th>

        <th>Quanity</th>

        <th>Transferred By</th>

        <th>Date</th>

      </thead>





      @foreach($Product->items as $items)



      <tr>

        <td>{{ $items->department->code." - ".$items->department->name }}</td>

        <td>{{ $items->item->item_code." - ".$items->item->item_name }}</td>

        <td>{{ $items->qty." ".$items->item->unit }}</td>

        <td>{{ $items->user->name }}</td>

        <td>{{ $items->created_at }}</td>

      </tr>



      @endforeach

    </table>



    @else

    No transfers yet!

    @endif





  </div>



  <div class="tab-pane" id="tab3" role="tabpanel">



    

    <div class="row">

      

        <div class="col-md-4">

           <h4>Customer Info:</h4>



               @if($Product->customers->count() > 0)



                 @foreach($Product->customers as $customer)



               

                      <table class="table">

                        <tbody>



                         <tr>

                          <td>Customer Name</td>

                          <td>{{ $customer->customer->name }}</td>

                        </tr>



                        <tr>

                          <td>Customer Phone</td>

                          <td>{{ $customer->customer->phone }}</td>

                        </tr>



                        <tr>

                          <td>Customer Email</td>

                          <td>{{ $customer->customer->email }}</td>

                        </tr>



                        <tr>

                          <td>Customer City</td>

                          <td>{{ $customer->customer->city }}</td>

                        </tr>



                        <tr>

                          <td>Customer State</td>

                          <td>{{ $customer->customer->state }}</td>

                        </tr>



                        <tr>

                          <td>Sec. Name</td>

                          <td>{{ $customer->customer->name_sec }}</td>

                        </tr>



                        <tr>

                          <td>Sec. Phone</td>

                          <td>{{ $customer->customer->phone_sec }}</td>

                        </tr>



                        <tr>

                          <td>Sec. Email</td>

                          <td>{{ $customer->customer->email_sec }}</td>

                        </tr>





                      </tbody>

                    </table>



                    @endforeach



                @else

                No Customer Assigned.

                @endif

            </div>





            <div class="col-md-8">

             <h4>Customer Payments:</h4>



               @if($Product->customerpayments->count() > 0)



                



               

                  <table class="table datatable">

                    

                    <thead>

                      <tr>

                        <th>Type</th>

                        <th>Amount</th>

                        <th>Receipt</th>

                      </tr>

                    </thead>



                    <tbody>

                       @foreach($Product->customerpayments as $payment)



                        <tr>

                          <td>{{ ucwords($payment->type) }} {{ ($payment->type == 'upgrade' ? " - ".$payment->changerequest->title : "") }}</td>

                          <td>{{ currency($payment->amount) }}</td>

                          <td><a href="{{ route('voucher', $payment->payment_id) }}" target="_blank">View</a></td>

                        </tr>



                       @endforeach

                    </tbody>



                  </table>



                    



                @else

                No Payment record found!

                @endif

            </div>







    </div>















</div>





<div class="tab-pane" id="tab4" role="tabpanel">



    @if($Product->changerequest->count() > 0)



  <div class="timeline">



    @foreach($Product->changerequest->sortBy('date') as $ChangeRequest)

        

        @php



          $date = Carbon\Carbon::createFromFormat('Y-m-d',$ChangeRequest->date);

  

        @endphp

        <div class="entry">

          <div class="title">

            <h3>{{ $ChangeRequest->title }} <small>{{ $date->format('l jS F Y') }}</small> <span class="badge badge-default">{{ currency($ChangeRequest->charge) }}</span> <span class="badge badge-default">{{ $ChangeRequest->status }}</span></h3>

            </div>

          <div class="body">



            <div class="row">

              <div class="col-sm-7">

                <p>{!! nl2br($ChangeRequest->instruction) !!}</p>    

              </div>



              <div class="col-sm-5">



              @hasanyrole('admin|owner')



                <div>



                   <b>Options:</b>

                    <ul class="list-unstyled">

                     

                        <li><a href="javascript:void(0)" class="link-icon" onclick="setCRcharge({{ $ChangeRequest->id }},'{{ $ChangeRequest->charge }}','{{ $ChangeRequest->status }}')">Set Upgrade</a></li>

                     

                      

                    </ul>

                </div>



                @else





                @endhasanyrole

                

                  @if($ChangeRequest->files->count() > 0)

                  <b>Files:</b>

                    <ul class="list-unstyled">

                      @foreach($ChangeRequest->files as $file)

                        <li><a target="_blank" href="{{ asset('storage/'.$file->file) }}" class="link-icon">Download / View File</a></li>

                      @endforeach

                      

                    </ul>

                  @endif

              </div>

            </div>

            

            

          </div>

        </div>



    @endforeach

</div>



    @else

      No Requests Avaialble.

    @endif











</div>



</div>





</div>

</div>

</div>





<!-- STAGE MODAL -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal">

  <div class="modal-dialog modal-sm ">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title">Add New Progress Stage</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true" class="fa fa-close"></span>

        </button>

      </div>

      {{ Form::open(['action'=>'ProductStageController@store']) }}

      <div class="modal-body">



        <div class="row">

          <div class="col-sm-12">



            <div class="form-group">

              <label for="code" class="form-control-label">Department</label>

              {{ Form::select('department', $Departments,null,['class'=>'form-control' , 'required'=>'', 'placeholder'=>'Select Department']) }}

            </div>



            <div class="form-group">

              <label for="code" class="form-control-label">Contractor</label>

              <div id="contractor">

                {{ Form::select('contractor', [],null,['class'=>'form-control' , 'required'=>'']) }}

              </div>

            </div>



            <div class="form-group">

              <label for="code" class="form-control-label">Stage</label>

              <div id="stage">

               {{ Form::select('stage', [],null,['class'=>'form-control' , 'required'=>'']) }}

             </div>

           </div>



           <div class="form-group">

            <label for="code" class="form-control-label">Standard Rate</label>

            <div id="stage">

             {{ Form::number('stage_rate', 0,['class'=>'form-control', 'required'=>'']) }}

           </div>

         </div>





         <div class="form-group">

          <label for="code" class="form-control-label">Date</label>

          {{ Form::text('date', date('Y-m-d'), ['class'=>'form-control', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=> date('Y-m-d') , 'required'=>'']) }}

        </div>



        <div class="form-group">

          <label for="code" class="form-control-label">Remark</label>

          {{ Form::text('remark', null, ['class'=>'form-control']) }}

        </div>



        {{ Form::hidden('productid', $Product->id, ['class'=>'form-control']) }}



      </div>



    </div>

  </div>

  <div class="modal-footer">

    <button type="submit" class="btn btn-primary">Submit</button>

  </div>

  {{ Form::close() }}

</div>

</div>

</div>





<!-- SALE PRICE MODAL -->



<div class="modal fade" tabindex="-1" role="dialog" id="sale-price">

  <div class="modal-dialog modal-sm ">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title">Set Product Sale Price</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true" class="fa fa-close"></span>

        </button>

      </div>

      {{ Form::open(['action'=>'ProductController@action']) }}

      <div class="modal-body">



        <div class="row">

          <div class="col-sm-12">

            <div class="form-group">

              <label for="code" class="form-control-label">Sale Price</label>

              {{ Form::text('sale_price', $Product->sale_price, ['class'=>'form-control']) }}

            </div>



            {{ Form::hidden('productid', $Product->id) }}

            {{ Form::hidden('action', 'setSalePrice') }}



          </div>



        </div>

      </div>

      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Submit</button>

      </div>

      {{ Form::close() }}

    </div>

  </div>

</div>



<!-- SALE PRICE MODAL -->



<div class="modal fade" tabindex="-1" role="dialog" id="upgrade-cr">

  <div class="modal-dialog modal-sm ">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title">Change Request Option</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true" class="fa fa-close"></span>

        </button>

      </div>

      {{ Form::open(['route'=>'update-cr']) }}

      <div class="modal-body">



        <div class="row">

          <div class="col-sm-12">



            <div class="form-group">

              <label for="code" class="form-control-label">Upgrade Charge</label>

              {{ Form::number('charge', null, ['class'=>'form-control']) }}

            </div>





            <div class="form-group">

              <label for="code" class="form-control-label">Status</label>

              {{ Form::select('status', ["pending"=>"pending","completed"=>"completed"], null , ['class'=>'form-control']) }}

            </div>



          </div>

       {{ Form::hidden('id', null) }}

        </div>

      </div>

      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Submit</button>

      </div>

      {{ Form::close() }}

    </div>

  </div>

</div>





<!-- CHANGE REQUEST MODAL -->



<div class="modal fade" tabindex="-1" role="dialog" id="change-request">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h5 class="modal-title">Add Change Request</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true" class="fa fa-close"></span>

        </button>

      </div>

      {{ Form::open(['action'=>'ChangeRequestController@store', 'files'=>'true']) }}

      <div class="modal-body">



        <div class="row">

          <div class="col-sm-12">

           <div class="form-group">

              <label for="code" class="form-control-label">Title:</label>

              {{ Form::text('title', '', ['class'=>'form-control', 'required'=>'required']) }}

            </div>

            <div class="form-group">

              <label for="code" class="form-control-label">Instructions:</label>

              {{ Form::textarea('instructions', '', ['class'=>'form-control', 'required'=>'required']) }}

            </div>

            <div class="form-group">

              <label for="code" class="form-control-label">Upload Files:</label>

              {{ Form::file('files[]', ['multiple'=>'true']) }}

            </div>



              <div class="form-group">

                <label for="code" class="form-control-label">Requested On:</label>

                {{ Form::text('date', date('Y-m-d'), ['class'=>'form-control', 'data-max-date'=>'today', 'data-date-format'=>'Y-m-d', 'data-default-date'=> date('Y-m-d') , 'required'=>'']) }}

              </div>



            {{ Form::hidden('productid', $Product->id) }}

            {{ Form::hidden('customerid', (!empty($Product->customers->first()->customer->id) ? $Product->customers->first()->customer->id : 0)) }}

            {{ Form::hidden('action', 'change-request') }}



          </div>



        </div>

      </div>

      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Submit</button>

      </div>

      {{ Form::close() }}

    </div>

  </div>

</div>



@endsection





@push('scripts')



<script>





  $('select[name="department"]').change(function () {



    var department_id = $(this).val();



    var product_category_id = '{{  @$Product->productcategory->id }}';



    getContractor(department_id)

    getStages(department_id, product_category_id)



  })









  function getContractor(department_id)

  {

    var $this = $('#contractor');



    $this.html('Loading...')



    $.get('{{ route('ContractorByDepartment') }}', {'department_id':department_id}, function (data) {



      $this.html(data).find('select').select2()



    });  

  }



  function getStages(department_id, product_category_id)

  {

    var $this = $('#stage');



    $this.html('Loading...')



    $.get('{{ route('StageByDepartment') }}', {'department_id':department_id, 'product_category_id':product_category_id}, function (data) {



      $this.html(data).find('select').select2()



    });  

  }



  function getStagerate(stage_id)

  {

    if(stage_id > 0){

      var $this = $('input[name="stage_rate"]');



      $this.attr('disabled','disabled')

      $.get('{{ route('StageStandardRate') }}', {'stage_id':stage_id}, function (data) {



        $('input[name="stage_rate"]').val(data)



        $this.removeAttr('disabled')



      });  

    }

  }





  function setCRcharge(change_request_id, charge, status)

  {

      $('#upgrade-cr input[name=charge]').val(charge)

      $('#upgrade-cr input[name=status]').val(status)

      $('#upgrade-cr input[name=id]').val(change_request_id)



      $('#upgrade-cr').modal('show');

  }







</script>



@endpush