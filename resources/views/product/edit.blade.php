@extends('master')

@section('title',$Product->product_name.': Edit Product')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Products</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            
                            <div class="col-lg-4 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                     Edit Product: {{ $Product->product_name }}
                                    </h5>
                                    <div class="card-block">
                                    
                                    {{ Form::open(['method'=>'PUT','action'=>['ProductController@update',$Product->id] ]) }}    

                                        <div class="form-group">
                                            {{ Form::text('product_name', $Product->product_name,['class'=>'form-control', 'placeholder'=>"Enter Product Name", 'required'=>'']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::text('product_details', $Product->product_details,['class'=>'form-control', 'placeholder'=>"Enter Product Details"]) }}
                                        </div>

                                        <br>

                                        <button type="submit" class="btn btn-primary btn-block">Update</button>
                                        
                                            
                                    {{ Form::close() }}
                                    </div>
                                </div>

                                

                            
                            
                        </div>

                         
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@push('scripts')

@endpush