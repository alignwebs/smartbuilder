@extends('master')

@section('title','Product Properties')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Product Properties</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-7 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                      Product Properties
                                    </h5>
                                    <div class="card-block">
                                    <!-- PRODUCT PROPERTY HEADER AS PPH -->
                                        <div id="product_property_header">
                                    <div class="row">
                                        <div class="col-md-4 text-right">Product Property Header:</div>

                                        <div class="col-md-6">{{ Form::select('pph', $pph->pluck('name','id'), null, ['class'=>'form-control', 'onchange'=>'getPPHP()', 'placeholder'=>'Select Property Header']) }}</div>
                                    </div>
                                 
                                        </div>
                                        <br>
                                     <!-- PRODUCT PROPERTY HEADER ENDS-->
                                    
                                <table class="table table-condensed table-bordered" id="pphp">
                                    <thead>
                                    <tr>
                                        <th>Delete</th>
                                        <th width="300">Property Name</th>
                                        <th width="300">Property Type</th>
                                        <th>Quantity</th>
                                        <th>Unit</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                <div class="text-center">
                                 <button type="button" class="btn btn-success" onclick="savePPHP()">Save</button>
                                </div>
                                
                                    </div>
                                </div>

                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                        Add Property
                                    </h5>
                                    <div class="card-block">
                                         
                                            <div id="addpphproperty">
                                                <div class="row">
                                                    <div class="col-md-4">{{ Form::text('pphp_name', '',['class'=>'form-control', 'placeholder'=>"Property Name"]) }}</div>
                                                     <div class="col-md-4">{{ Form::text('pphp_type', '',['class'=>'form-control', 'placeholder'=>"Property Type"]) }}</div>
                                                      <div class="col-md-2">{{ Form::text('pphp_qty', '',['class'=>'form-control', 'placeholder'=>"Quantity"]) }}</div>
                                                       <div class="col-md-2">{{ Form::text('pphp_unit', '',['class'=>'form-control', 'placeholder'=>"Unit"]) }}</div>
                                                </div>
                                                <br>
                                                <div class="text-center">
                                                <button type="button" class="btn btn-primary" onclick="addToPPHP()">Add Property</button>
                                                </div>
                                            </div>

                                    </div>
                                
                            </div>
                            </div>
                            

                         <div class="col-lg-5 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                      Default Product Properties
                                    </h5>
                                    <div class="card-block">
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@push('scripts')

<script>
    
    var pph_properties = [];


    $(document).ready(function () { 
    
     
    })

    function addPPHproperty(name,type,qty,unit)
    {
        var pphProperty = {'name':name, 'type':type, 'qty':qty, 'unit': unit};
       
        pph_properties.push(pphProperty);

        renderPPHP()
    }

    function addToPPHP()
    {
        if($('select[name=pph]').val() != "")
        {
            var name = $.trim($('input[name=pphp_name]').val());
            var type = $.trim($('input[name=pphp_type]').val());
            var qty = $.trim($('input[name=pphp_qty]').val());
            var unit = $.trim($('input[name=pphp_unit]').val());

            if(name != "")
                addPPHproperty(name,type,qty,unit);
        }
        else
            alert("Please select a Property Header")
    }

    function updatePPHP(index,name,value)
    {
        pph_properties[index][name] = value;
    }

    function deletePPHP(index)
    {
        pph_properties[index] = "";
        renderPPHP()
    }

    function renderPPHP()
    {
        if(pph_properties.length > 0)
        {
            $('#pphp tbody').html('');

            var html = "";

            $.each(pph_properties, function (index,value) {

               if($(value).size() > 0)
               {
                
                html = '<tr><td><button type="button" class="btn btn-sm btn-danger" onclick="deletePPHP('+index+')"><span class="fa fa-trash" aria-hidden="true"></span></button></td><td>'+value.name+'</td><td>'+value.type+'</td><td><input type="text" data-type="qty" data-id="'+index+'" name="pphpqty'+index+'" value="'+value.qty+'""></td><td><input type="text" data-type="unit" data-id="'+index+'" name="pphpunit'+index+'" value="'+value.unit+'"></td></tr>';

                $(html).appendTo('#pphp tbody');

               }


            })

            $('#addpphproperty input').val('')
        }
        else
            $('#pphp tbody').html("No Property Added Yet!")
    }

    function savePPHP()
    {

        if(pph_properties.length > 0)
        {
            /*SELECT PRODUCT PROPERTY HEADER*/
            var pph = $('select[name=pph]').val(); 

            /*SERIALIZE PPHP ARRAY */
            var pphp = JSON.stringify(pph_properties);

            $.post("{{ URL::action('ProductPropertyController@store') }}", {'pph':pph, 'pphp':pphp, '_token':'{{ csrf_token() }}'}, function (data) {

                    if(data == 'ok')
                           $.notify({
                                    title: "Success:",
                                    message: "Product Properties saved successfully!"
                                }, {
                                    offset: {
                                        x: 30,
                                        y: 70
                                    }
                                });


            })

        }
        else
            alert("Please add a Product Property to Property Header")

     
    }

    function getPPHP()
    {
        var pph = $('select[name=pph]').val();

        if(pph > 0)
        {
            pph_properties = []
                
            $.getJSON('{{ URL::route('ProjectPropertyList') }}', {'pph':pph}, function (json) {

                    $.each(json, function (index, value) {
                                
                             addPPHproperty(value.property_name,value.property_type,value.qty,value.unit)

                    })

            })

            renderPPHP()
        }
    }


    /*UPDATE CHANGED VALUES IN PPHP TABLE FILEDS*/

        $('body').on('blur', '#pphp input' ,function () { 
                
                var index = $(this).attr('data-id');
                var key = $(this).attr('data-type');
                var value = $(this).val()

                updatePPHP(index,key,value);

        })

</script>


@endpush