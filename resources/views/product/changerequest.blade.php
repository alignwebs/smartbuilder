@if($data->count() > 0)
	
	<p>{{ $data->count() }} unpaid upgrades found.</p>
	<table class="table table-bordered datatable table-hover">
		<thead>
			<tr class="active">
				<th></th>
				<th>Title</th>
				<th>Amount</th>
				<th>Date</th>
			</tr>
		</thead>

		<tbody>
			@foreach($data as $task)
				<tr>
					<td>{!! Form::checkbox('tasks[]', $task->id, null, ['class' => 'item', 'data-taskid' => $task->id ,'data-price' => $task->charge,'onchange' => 'updateItems()']) !!}</td>
					<td><b>{{ $task->title }}</b></td>
					<td>{{ currency($task->charge) }}</td>
						<td>{{ $task->date }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	

@else
	
	No unpaid orders!

@endif