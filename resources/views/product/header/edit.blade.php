@extends('master')

@section('title','Edit Product Property Header')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Edit Product Property Header</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-8 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                        Edit Product Property Header
                                    </h5>
                                    <div class="card-block">
                                         
                                            {{ Form::open(['action'=>['ProductPropertyHeaderController@update',$pph->id],'method'=>'PUT']) }}
                                            <div id="add_pph" class="row">
                                                <div class="col-md-5">{{ Form::text('pph_name', $pph->name,['class'=>'form-control', 'placeholder'=>"Product Property Header Name", 'required'=>'']) }}</div>
                                                <div class="col-md-5">{{ Form::text('pph_desc', $pph->description,['class'=>'form-control', 'placeholder'=>"Description"]) }}</div>
                                                <div class="col-md-2"><button type="submit" class="btn btn-primary btn-block">Update</button></div>
                                            </div>
                                        {{ Form::close() }}

                                    </div>
                                
                            </div>
                            
                        </div>
                            </div>


                        

                </div>
            </div>
        </div>
    </div>



@endsection