@extends('master')

@section('title','Product Property Header')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Product Property Header</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-7 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                        Add Product Property Header
                                    </h5>
                                    <div class="card-block">
                                         
                                            {{ Form::open(['action'=>'ProductPropertyHeaderController@store']) }}
                                            <div id="add_pph" class="row">
                                                <div class="col-md-5">{{ Form::text('pph_name', '',['class'=>'form-control', 'placeholder'=>"Product Property Header Name", 'required'=>'']) }}</div>
                                                <div class="col-md-5">{{ Form::text('pph_desc', '',['class'=>'form-control', 'placeholder'=>"Description"]) }}</div>
                                                <div class="col-md-2"><button type="submit" class="btn btn-primary btn-block">Add</button></div>
                                            </div>
                                        {{ Form::close() }}

                                    </div>
                                
                            </div>
                            
                        </div>
                            </div>


                        <div class="row">

                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                        Product Property Headers
                                    </h5>
                                    <div class="card-block">
                                         
                                            <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Product Property Header Name</th>
                                                    <th>Description</th>
                                                    <th>Option</th>
                                                </tr>
                                            </thead>

                                            @if($pph->count() > 0)
                                                
                                                @foreach($pph as $pphEntry)

                                                    <tr><td>{{ $pphEntry->name }}</td><td>{{ $pphEntry->description }}</td><td>{!! deleteEditModelBtn("header",$pphEntry->id, $pphEntry->name) !!}</td></tr>

                                                @endforeach

                                            @endif
                                        </table>

                                    </div>
                                
                            </div>
                            
                        </div>
                            
                           
                        </div>

                </div>
            </div>
        </div>
    </div>



@endsection