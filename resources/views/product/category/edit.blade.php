@extends('master')

@section('title','Edit Product Category')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Edit Product Category</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                          <div class="col-lg-5 ks-panels-column-section">
                             <div class="card ks-panel">
                                    <h5 class="card-header">
                                      Product Category Details
                                    </h5>
                                    <div class="card-block">
                                         {{ Form::open(['method'=>'PUT','action'=>['ProductCategoryController@update', $fetch->id]]) }}
                                                <div class="input-group">
                                                     {{ Form::text('product_category_name', $fetch->product_category_name ,['class'=>'form-control', 'placeholder'=>'Enter Product Category Name...',''=>'']) }}
                                                <span class="input-group-btn">
                                                    <button class="btn btn-success" type="submit">Submit</button>
                                                </span>
                                                </div>
                                        {{ Form::close() }}


                                    @if($errors->any())
                                        <br>
                                        <div class="alert alert-danger">
                                        @foreach ($errors->all() as $message) 
                                            <li>{{ $message }}</li>
                                        @endforeach
                                        </div>
                                    @endif

                                    </div>
                                </div>


                                

                            
                            
                        </div>

                         
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
