@extends('master')

@section('title', 'Manage Product Category')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Manage Product Category: {{ $ProductCategory->product_category_name }}</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                     Product Property Details - #{{ $ProductCategory->propertyheader->name }}
                                    </h5>
                                    <div class="card-block">
                                        
                                        @if($ProductProperties->count() > 0)
                                            
                                            <table class="table table-bordered table-condensed">
                                            <thead>
                                                <tr>
                                                    @foreach($ProductProperties as $ProductProperty)
                                                        
                                                        <th>{{ $ProductProperty->property_name }}</th>

                                                    @endforeach
                                                </tr>
                                            </thead>

                                             <tbody>
                                                <tr>
                                                    @foreach($ProductProperties as $ProductProperty)
                                                        
                                                        <th>{{ $ProductProperty->qty }} {{ $ProductProperty->unit }}</th>

                                                    @endforeach
                                                </tr>
                                            </tbody>


                                            </table>

                                        @endif

                                    </div>
                                </div>

                                

                            
                            
                        </div>
                      

                         
                    </div>
                    <div class="row">
                         <div class="col-lg-6 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                        Available Products
                                    </h5>
                                    <div class="card-block">
                                    
                                            
                                            <table class="table table-bordered table-condensed table-hover" id="availableProds">
                                            

                                             <tbody>
                                               @if($ProductsAvailable->count() > 0)
                                                    @foreach($ProductsAvailable as $Product)
                                                        
                                                         <tr data-prodid="{{ $Product->id }}"><td>{{ $Product->product_name." : ".$Product->product_details }}</td><td width="50"><a href="javascript:void(0)" onclick="AddProdToCategory('{{ $Product->id }}','{{ $ProductCategory->id }}', '{{ $Product->product_name." : ".$Product->product_details }}')">Add</a></td></tr>

                                                    @endforeach
                                                @endif
                                            </tbody>


                                            </table>

                                   
                                    </div>
                                </div>
                        </div>
                         <div class="col-lg-6 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                        Assigned Products
                                    </h5>
                                    <div class="card-block">
                                            <table class="table table-bordered table-condensed table-hover" id="assignedProds">
                                                 <tbody>
                                                     
                                             @if($ProductsAssigned->count() > 0)
                                  
                                                    @foreach($ProductsAssigned as $Product)
                                                           
                                                         <tr data-prodid="{{ $Product->id }}"><td>{{ $Product->product_name." : ".$Product->product_details }}</td><td width="50"><a href="javascript:void(0)" onclick="DeleteProdFromCategory('{{ $Product->id }}','{{ $ProductCategory->id }}', '{{ $Product->product_name." : ".$Product->product_details }}')">Remove</a></td></tr>

                                                    @endforeach

                                                 @endif

                                                 </tbody>
                                             </table>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@push('scripts')

<script>

$(document).ready(function () {
    
})

function AddProdToCategory(prod_id, cat_id, name)
{
    $.post("{{ URL::route('AddProdToCategory') }}", {"prod_id":prod_id, "cat_id": cat_id, "_token":'{{ csrf_token() }}'}, function (data) {

            removeAvailableProd(prod_id)
            addAssignedProd(prod_id,cat_id,name)
                $.notify({
                        title: "Success:",
                        message: name+" added successfully!"
                    }, {
                        offset: {
                            x: 30,
                            y: 70
                        }
                    });

    })
}

function DeleteProdFromCategory(prod_id, cat_id, name)
{
    $.post("{{ URL::route('DeleteProdFromCategory') }}", {"prod_id":prod_id, "cat_id": cat_id, "_token":'{{ csrf_token() }}'}, function (data) {

    
                removeAssignedProd(prod_id)
                addAvailableProd(prod_id,cat_id, name)

                $.notify({
                        title: "Success:",
                        message: name+" removed successfully!"
                    }, {
                        offset: {
                            x: 30,
                            y: 70
                        }
                    });


    })
}

function removeAvailableProd(prod_id)
{
    $('#availableProds tr[data-prodid='+prod_id+']').remove();
}

function addAssignedProd(prod_id,cat_id,name)
{
    $('<tr data-prodid="'+prod_id+'"><td>'+name+'</td><td width="50"><a href="javascript:void(0)" onclick="DeleteProdFromCategory('+prod_id+','+cat_id+',\''+name+'\')">Remove</a></td></tr>').appendTo('#assignedProds tbody');
}

function removeAssignedProd(prod_id)
{
    $('#assignedProds tr[data-prodid='+prod_id+']').remove();
}

function addAvailableProd(prod_id,cat_id,name)
{
    $('<tr data-prodid="'+prod_id+'"><td>'+name+'</td><td width="50"><a href="javascript:void(0)" onclick="AddProdToCategory('+prod_id+','+cat_id+',\''+name+'\')">Add</a></td></tr>').appendTo('#availableProds tbody');
}

</script>


@endpush