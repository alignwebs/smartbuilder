@extends('master')

@section('title','Product Categories')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Product Categories</h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-7 ks-panels-column-section">
                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                      List of Product Categories
                                    </h5>
                                    <div class="card-block">
                                    <div id="pc_list">
                                        
                                    </div>
                                    </div>
                                </div>

                                

                            
                            
                        </div>
                         <div class="col-lg-5 ks-panels-column-section">
                             <div class="card ks-panel">
                                    <h5 class="card-header">
                                      Add New Product Category
                                    </h5>
                                    <div class="card-block">
                                         {{ Form::open(['action'=>'ProductCategoryController@store']) }}
                                                <div class="input-group">
                                                     {{ Form::text('product_category_name','',['class'=>'form-control', 'placeholder'=>'Enter Product Category Name...',''=>'']) }}
                                                <span class="input-group-btn">
                                                    <button class="btn btn-success" type="submit">Add New</button>
                                                </span>
                                                </div>
                                        {{ Form::close() }}


                                    @if($errors->any())
                                        <br>
                                        <div class="alert alert-danger">
                                        @foreach ($errors->all() as $message) 
                                            <li>{{ $message }}</li>
                                        @endforeach
                                        </div>
                                    @endif

                                    </div>
                                </div>

                                <div class="card ks-panel">
                                    <h5 class="card-header">
                                      Assign Property Template to Product Category
                                    </h5>
                                    <div class="card-block">

                                    <div class="form-group">
                                            {{ Form::select('assign_product_cat', $product_caregories->pluck('product_category_name','id'), null, ['placeholder' => 'Select a Product Category','class'=>'form-control']) }}
                                    </div>
                                    <div class="form-group">
                                            {{ Form::select('assign_product_header', $ppheaders->pluck('name','id'), null, ['placeholder' => 'Select a Product Property Header Template','class'=>'form-control']) }}
                                    </div>    

                                     <div class="form-group">
                                        <button onclick="assignPC()" type="button" class="btn btn-primary btn-block">Assign</button>
                                     </div>
                                    
                                    </div>
                                </div>

                                

                            
                            
                        </div>

                         
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@push('scripts')

<script>

$(document).ready(function () {
    getProductCategories()
})

    function getProductCategories()
    {
        $('#pc_list').html("Loading...");

        $.get('{{ URL::route('ProductCategoriesviaAjax') }}', {'project_id':'{{ $project_id }}' }, function (data) {

             $('#pc_list').html(data);
         
        })
    }


    function assignPC()
    {
        var pc = $('select[name=assign_product_cat]').val();
        var tpl = $('select[name=assign_product_header]').val();

        if(pc == "")
            alert("Please select Project Category");
        else if(tpl == "")
            alert("Please select Property Header Template");
        else
        {
            $.post('{{ URL::route('assignPropertyHeader') }}', {'pc':pc,'tpl':tpl,'_token':'{{ csrf_token() }}'}, function (data) {
                 $.notify({
                                    title: "Success:",
                                    message: "Assigned successfully!"
                                }, {
                                    offset: {
                                        x: 30,
                                        y: 70
                                    }
                                });
            })

             getProductCategories()
        }

    }
</script>


@endpush