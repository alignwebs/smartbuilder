@if($rows->count() > 0)

 <table class="table table-condensed table-bordered table-hover">
                                           <thead>
                                                <tr>
                                                   <th>Product Category</th>
                                                   <th>Product Properties Template</th>
                                                   <th>Edit</th>
                                                </tr>
                                           </thead>

                                           <tbody>
                                           		
                                           		@foreach($rows as $row)

                                           			<tr>
                                           				<td><a href="{{ route('ShowProductCategory',$row->id) }}">{{ $row->product_category_name }}</a></td>
                                           				<td>{{ $row->property_header_title }}</td>
                                           				<td>{!! deleteEditModelBtn('product-category', $row->id, $row->product_category_name) !!}</td>
                                           			</tr>
	
                                           		@endforeach

                                           </tbody>
                                       </table>

@else

	No Product Category Found

@endif