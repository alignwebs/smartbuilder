@extends('master')

@section('title','Project Products')

@section('pagecontent')
    
    <style>
        #pphp tbody input {width: 60px;}
    </style>
    <div class="ks-column ks-page">
        <div class="ks-header">
            <section class="ks-title">
                <h3>Project Products: <small>{{ getProject('proj_name') }}</small></h3>
            </section>
        </div>

        <div class="ks-content">
            <div class="ks-body ks-projects-dashboard-page">
                
                <div class="ks-body">
                    <div class="container-fluid">

                        {{ Form::open(['action'=>'ProductController@store']) }}    
                            
                            <div class="row">
                                
                            <div class="col-lg-12">

                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">Add New Product <small>Enter new product details</small></h5>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                 {{ Form::text('product_name', '',['class'=>'form-control', 'placeholder'=>"Enter Product Name", 'required'=>'']) }}
                                            </div>
                                            <div class="col-sm-3">
                                                  {{ Form::text('product_details', '',['class'=>'form-control', 'placeholder'=>"Enter Product Details"]) }}
                                            </div>
                                            <div class="col-sm-2">
                                                  <button type="submit" class="btn btn-primary btn-block">Add Product</button>
                                            </div>
                                        </div>
                                   
                                </div>
                            </div>

                            <!-- END Item NAME BLOCK -->

                            
                        </div>

                        
                            </div>




                        {{ Form::close() }}



                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">

                                <div class="card">
                                    
                                    <div class="card-block">
                                        <h5 class="card-title">Products <small>List of products added</small></h5>
                                        @if($products->count() > 0)
                                            
                                            <table class="table datatable">
                                            <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Product Details</th>
                                                    <th>Product Category</th>
                                                    <th>Progress</th>
                                                    <th >Status</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            @foreach($products as $product)
                                                
                                                @php
                                                    $productStages = 0;
                                                    $productCategoryStages = 0;

                                                    if(isset($product->stages))
                                                        $productStages = $product->stages->count();
                                                
                                                    if(isset($product->productcategory->stages))
                                                        $productCategoryStages = $product->productcategory->stages->count();

                                                    if($product->customers->count() > 0)
                                                    {
                                                        $status = "Sold";
                                                        $status_class = "badge badge-success";
                                                    }
                                                    else
                                                    {
                                                        $status = "Available";
                                                        $status_class = "badge badge-default";
                                                    }
                                                @endphp



                                                <tr data-id="{{ $product->id }}"><td><a href="{{ URL::route('product.show',$product->id) }}">{{ $product->product_name }}</a></td>
                                                <td>{{ $product->product_details }}</td>
                                                <td>{{ (!empty($product->productcategory) ? $product->productcategory->product_category_name : '') }}</td>
                                                <td><div class="progress ks-rounded">
                                            <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: {{ @($productStages / $productCategoryStages)*100 }}%" aria-valuenow="{{ $productStages }}" aria-valuemin="0"></div>
                                        </div></td>
                                        <td ><span class="{{ $status_class }}">{{ $status }}</span></td>
                                                <td>{!! deleteEditModelBtn("product",$product->id, $product->product_name) !!} </td></tr>

                                            @endforeach
                                            </table>

                                        @else

                                            No products

                                        @endif
                                    
                                    </div>
                                </div>

                                

                            
                            
                        
                            </div>

                            <div class="col-lg-4 ks-panels-column-section"></div>

                         
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@push('scripts')


@endpush