<table class="table">
	<tr>
		<td width="50%">
					<p><span>Supplier Name:</span> {{ $payment->supplier->name }}</p>
					<p><span>Supplier Contact:</span> {{ $payment->supplier->phone }}</p>
					<p><span>Supplier Email:</span> {{ $payment->supplier->email }}</p>

					<p><span>GSTIN:</span> {{ $payment->supplier->meta_tin }}</p>
		</td>

		<td width="50%">
			<p><span>Supplier Address:</span> {{ $payment->supplier->address_one }}</p>
			<p>{{ $payment->supplier->address_two }}</p>
			<p>{{ $payment->supplier->city }} {{ $payment->supplier->district }} {{ $payment->supplier->state }} {{ $payment->supplier->pincode }} </p>
		</td>
	</tr>
</table>


<div class="items">
		@if($payment->materialpayment_grns->count() > 0)
			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Item Name</th>
						<th>Quantity</th>
						<th>Unit Price</th>
						<th>Discount</th>
						<th>Tax</th>
						<th>Net Unit</th>
						<th>Amount</th>
					</tr>
				</thead>

				<tbody>

					@foreach($payment->materialpayment_grns as $grn)
						
						<tr>
							<td>{{ $grn->item->item_name }} - {{ $grn->item->item_code }}</td>
							<td>{{ $grn->item->qty }} {{ $grn->item->unit }}</td>
							<td>{{ currency($grn->item->unit_price) }}</td>
							<td>{{ $grn->item->discount }}%</td>
							<td>{{ $grn->item->tax }}%</td>
							<td>{{ currency($grn->item->net_unit_price) }}</td>
							<td>{{ currency($grn->item->total_price) }}</td>
						</tr>

					@endforeach
					
					@foreach($payment->supplier_payment->where('type','advance') as $advance)

						<tr>
							<td  colspan="5"></td>
							<td>Advance: </td>
							<td>Rs. {{ $advance->amount }}</td>
						</tr>

					@endforeach

					<tr>
						<td  colspan="5"></td>
						<td>Grand Total: </td>
						<td>{{ currency($payment->amount) }}</td>
					</tr>
					
				</tbody>

			</table>

		@endif
</div>