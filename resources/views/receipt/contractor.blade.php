<table class="table">
	<tr>
		<td width="50%">
					<p><span>Contractor Name:</span> {{ $payment->contractor->name }} - {{ $payment->contractor->code }}</p>
					<p><span>Contractor Contact:</span> {{ $payment->contractor->phone }}</p>
					<p><span>Contractor Email:</span> {{ $payment->contractor->email }}</p>
		</td>

		<td width="50%">
			<p><span>Contractor Address:</span> {{ $payment->contractor->address }}</p>
		</td>
	</tr>
</table>


<div class="items">
		@if($payment->contractor_payment->count() > 0)
			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Product</th>
						<th>Stage </th>
						<th>Amount</th>
					</tr>
				</thead>

				<tbody>

					@foreach($payment->contractor_product_stages as $stage)
						
						<tr>
							<td>{{ $stage->product->product_name }}</td>
							<td>{{ $stage->stage->name }}</td>
							<td>{{ currency($stage->rate) }}</td>
						</tr>

					@endforeach
					
					@foreach($payment->contractor_payment->where('type','advance') as $advance)

						<tr>
							<td  colspan="1"></td>
							<td>Advance: </td>
							<td>Rs. {{ $advance->amount }}</td>
						</tr>

					@endforeach

					<tr>
						<td  colspan="1"></td>
						<td>Grand Total: </td>
						<td>{{ currency($payment->amount) }}</td>
					</tr>
					
				</tbody>

			</table>

		@endif
</div>