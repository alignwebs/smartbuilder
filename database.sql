-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 16, 2017 at 05:34 PM
-- Server version: 5.6.26-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tasueart_smartproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank_accounts`
--

CREATE TABLE `bank_accounts` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `balance` double(10,2) DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_accounts`
--

INSERT INTO `bank_accounts` (`id`, `org_id`, `project_id`, `name`, `bank_name`, `account_number`, `ifsc`, `balance`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Rambabu Bank', 'Personal', 'Rambabu_001', 'RAMBABU001', 321.55, '2017-09-09 02:23:33', '2017-09-09 07:17:52', NULL),
(2, 1, 1, 'SATHISH BANK', 'SATHISH BANK', 'SATHISHPersonal', 'SathishPersonal', 13000.00, '2017-09-09 02:25:21', '2017-09-09 06:51:38', NULL),
(3, 1, 1, 'ICICI-Mokila', 'ICICI', '001001001', 'ICIC0003999', 50000.00, '2017-09-09 02:46:06', '2017-09-09 02:46:06', NULL),
(4, 1, 2, 'rambabu', 'rambabu', 'XXXXXX', 'XXXXXX', 2450333.00, '2017-09-10 13:19:36', '2017-09-11 08:51:42', NULL),
(5, 1, 2, 'Subishi Infra', 'ICICI', 'XXXXXXX', 'XXXXXXXX', 1856260.00, '2017-09-10 13:20:29', '2017-09-11 07:05:13', NULL),
(6, 1, 2, 'SATHISH BANK', 'SATHISH BANK', '8877665554', 'ojcjlm8888', 1968000.00, '2017-09-11 02:27:00', '2017-09-11 07:14:46', NULL),
(7, 2, 3, 'ICICI_MOKILA', 'ICICI', '628777799977', 'ICIC0007654', 4000000.00, '2017-09-12 01:53:17', '2017-09-18 02:27:59', NULL),
(8, 2, 3, 'ICICI_MADHAPUR', 'ICICI', '76578997767', 'ICIC00876', 2000000.00, '2017-09-12 01:54:53', '2017-09-12 01:54:53', NULL),
(9, 2, 3, 'ICICI_GACHIBOWLI', 'ICICI', '765544346767', 'ICIC000768', 3000000.00, '2017-09-12 01:57:28', '2017-09-12 01:57:28', NULL),
(10, 2, 3, 'HDFC_MOKILA', 'HDFC', '77999976566', 'HDFC000888', 1301972.00, '2017-09-12 01:58:31', '2017-09-15 02:46:00', NULL),
(11, 2, 3, 'CORP_BHANUR', 'CORPORATION', '8876655777765', 'CORP0001237', 2500000.00, '2017-09-12 02:00:10', '2017-09-12 02:00:10', NULL),
(12, 2, 3, 'CORP_GACHIBOWLI', 'CORPORATION', '77888877666', 'CORP0008768', 2800000.00, '2017-09-12 02:01:17', '2017-09-12 02:01:17', NULL),
(13, 2, 3, 'SATHISH_BANK', 'SATHISH', '888777656676', 'BANK0001234', 3014000.00, '2017-09-12 02:02:16', '2017-09-18 02:39:14', NULL),
(14, 2, 3, 'RAMBABU_BANK', 'RAMBABU', '77886555456', 'BANK0001234', 2488800.00, '2017-09-12 02:03:05', '2017-09-13 02:19:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `change_requests`
--

CREATE TABLE `change_requests` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `product_id` int(12) NOT NULL,
  `customer_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `title` varchar(255) NOT NULL,
  `instruction` text NOT NULL,
  `date` varchar(10) NOT NULL,
  `status` varchar(255) DEFAULT 'pending',
  `charge` decimal(10,2) DEFAULT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `change_requests`
--

INSERT INTO `change_requests` (`id`, `org_id`, `project_id`, `product_id`, `customer_id`, `user_id`, `title`, `instruction`, `date`, `status`, `charge`, `payment_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 3, 10, 7, 1, 'Servent room', 'Extra window', '2017-09-18', 'pending', '5000.00', NULL, '2017-09-18 02:21:03', '2017-09-18 02:21:42', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `change_request_files`
--

CREATE TABLE `change_request_files` (
  `id` int(12) NOT NULL,
  `change_request_id` int(12) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contractors`
--

CREATE TABLE `contractors` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `department_id` int(12) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contractors`
--

INSERT INTO `contractors` (`id`, `org_id`, `department_id`, `code`, `name`, `phone`, `email`, `address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 3, 'madhu123', 'K.Madhu', 'XXXXXXXXXXX', 'XXXX', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', '2017-09-10 12:33:56', '2017-09-10 12:33:56', NULL),
(2, 1, 2, 'Ele000', 'M.Surya Prakash', 'XXXXXXXXXXX', 'XXXXX', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', '2017-09-10 12:34:46', '2017-09-10 12:34:46', NULL),
(3, 1, 4, 'Gaya123', 'Sri Gayatri Creations', 'XXXXXXXXXXX', 'XXXX', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', '2017-09-10 12:35:37', '2017-09-10 12:35:37', NULL),
(4, 1, 1, 'Mason123', 'M.Bikshalu', NULL, NULL, NULL, '2017-09-10 13:41:49', '2017-09-10 13:41:49', NULL),
(5, 1, 3, 'TL_Shiva', 'Shiva Electrician', '2131231232', 'shivaelectrician@gmail.com', 'Mokila, Ranga Reddy', '2017-09-11 06:07:49', '2017-09-11 06:07:49', NULL),
(6, 2, 7, 'Ele_001', 'Prakash', '967555578876', 'prakash@gmail.com', 'Pilligundla,Shankarpally', '2017-09-12 02:12:56', '2017-09-12 02:12:56', NULL),
(7, 2, 7, 'Ele_002', 'Shiva', '9866667444', 'shiva@gmail.com', 'Hyderabad', '2017-09-12 02:13:36', '2017-09-12 02:13:53', NULL),
(8, 2, 8, 'Msn_001', 'Bikshalu', '76889976655', 'bikshalu@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:14:51', '2017-09-12 02:14:51', NULL),
(9, 2, 8, 'Msn_002', 'Srinu', '8866654466', 'srinu@gmail.com', 'Pilligundla,Shankarpally', '2017-09-12 02:15:46', '2017-09-12 02:15:46', NULL),
(10, 2, 8, 'Msn_003', 'Kondal Rao', '8887667677', 'kondal@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:16:42', '2017-09-12 02:16:42', NULL),
(11, 2, 8, 'Msn_004', 'Anjaiah', '888765655', 'anjaiah@gmail.com', 'Pilligundla,Shankarpally', '2017-09-12 02:17:36', '2017-09-12 02:17:36', NULL),
(12, 2, 7, 'Msn_005', 'Babu', '776554477764', 'babu@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:18:22', '2017-09-12 02:18:22', NULL),
(13, 2, 9, 'CRP_001', 'M Venkataiah', '806666886', 'venkat@gmail.com', 'Borabanda,Hyderabad', '2017-09-12 02:20:14', '2017-09-12 02:20:14', NULL),
(14, 2, 10, 'PLM_001', 'Rohith kumar', '887996668', 'rohith@gmail.com', 'Sainikpuri', '2017-09-12 02:21:26', '2017-09-12 02:21:26', NULL),
(15, 2, 11, 'FLC_001', 'Rajesh Prathap', '9988866666', 'rajeshprathap@gmail.com', 'Hayath nagar, Raniganz', '2017-09-12 02:22:53', '2017-09-12 02:22:53', NULL),
(16, 2, 12, 'TLS_001', 'K Madhu', '8877656544', 'madhu@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:23:37', '2017-09-12 02:23:37', NULL),
(17, 2, 12, 'TLS_002', 'Narender', '8866668886', 'naren@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:24:16', '2017-09-12 02:24:16', NULL),
(18, 2, 13, 'CNT_001', 'Subbarao', '8866654666', 'subbarao@gmail.com', 'Kukat pally', '2017-09-12 02:25:49', '2017-09-12 02:25:49', NULL),
(19, 2, 14, 'PNT_001', 'Rajesh Prajapathi', '887776555', 'prajapathi@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:27:32', '2017-09-12 02:27:32', NULL),
(20, 2, 14, 'PNT_002', 'Mahipal', '67878989989', 'mahipal@gmail.com', 'Kollur,Shankarpally', '2017-09-12 02:28:49', '2017-09-12 02:28:49', NULL),
(21, 2, 14, 'PLM_003', 'Suresh', '99787655677', 'suresh@gmail.com', 'Kondakal,Shankarpally', '2017-09-12 02:29:54', '2017-09-12 02:29:54', NULL),
(22, 2, 15, 'LBR_001', 'Surender', '678899777556', 'surender@gmail.com', 'Shankar pally', '2017-09-12 02:30:58', '2017-09-12 02:30:58', NULL),
(23, 2, 15, 'LBR_002', 'Prabhudas', '87775544666', 'prabhudas@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:32:27', '2017-09-12 02:32:27', NULL),
(24, 2, 16, 'LWN_001', 'Prabhudas', '876655454356', 'prabhudas@gmail.com', 'Mokila, Shankar Pally', '2017-09-12 02:33:16', '2017-09-12 02:33:16', NULL),
(25, 2, 17, 'GLS_001', 'Eeshwar', '8876655444', 'eshwar@gmail.com', 'Hyderabad', '2017-09-12 02:34:36', '2017-09-12 02:34:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contractor_payments`
--

CREATE TABLE `contractor_payments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `contractor_id` int(12) NOT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `type` enum('advance','task') NOT NULL,
  `product_stage_id` int(12) DEFAULT NULL,
  `amount` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contractor_payments`
--

INSERT INTO `contractor_payments` (`id`, `org_id`, `project_id`, `contractor_id`, `payment_id`, `type`, `product_stage_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 23, 'task', NULL, 51000.00, '2017-09-11 09:13:41', '2017-09-11 09:13:41'),
(2, 1, 2, 1, 23, 'advance', NULL, 1000.00, '2017-09-11 09:13:41', '2017-09-11 09:13:41'),
(3, 1, 2, 1, 24, 'task', NULL, 49000.00, '2017-09-11 09:14:20', '2017-09-11 09:14:20'),
(4, 1, 2, 2, 25, 'task', NULL, 45000.00, '2017-09-11 09:22:01', '2017-09-11 09:22:01'),
(5, 2, 3, 6, 42, 'task', NULL, 5000.00, '2017-09-13 04:11:12', '2017-09-13 04:11:12'),
(6, 2, 3, 6, 44, 'task', NULL, 12000.00, '2017-09-13 04:49:15', '2017-09-13 04:49:15'),
(7, 2, 3, 16, 45, 'task', NULL, 100000.00, '2017-09-14 09:51:14', '2017-09-14 09:51:14'),
(8, 2, 3, 16, 45, 'advance', NULL, 37000.00, '2017-09-14 09:51:14', '2017-09-14 09:51:14'),
(9, 2, 3, 16, 46, 'task', NULL, 50000.00, '2017-09-14 09:57:28', '2017-09-14 09:57:28'),
(10, 2, 3, 16, 46, 'advance', NULL, 21000.00, '2017-09-14 09:57:28', '2017-09-14 09:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `name_sec` varchar(255) DEFAULT NULL,
  `phone_sec` varchar(255) DEFAULT NULL,
  `email_sec` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `org_id`, `project_id`, `user_id`, `name`, `phone`, `email`, `address`, `city`, `state`, `name_sec`, `phone_sec`, `email_sec`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 2, 'Jagan', '87766567778', 'jagan@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 'Jagan', '9999977666', NULL, '2017-09-11 01:58:01', '2017-09-11 01:58:01', NULL),
(2, 1, 2, 3, 'Sathish', '9849701105', 'sathishgoud82@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 'Sathish', '9849701105', 'sathishgoud82@gmail.com', '2017-09-11 01:58:39', '2017-09-11 01:58:39', NULL),
(3, 2, 3, 4, 'Sathish', '9849701105', 'alignwebs@gmail.com', NULL, 'Ranga Reddy', 'Telangana', NULL, NULL, NULL, '2017-09-12 02:04:25', '2017-09-12 02:04:25', NULL),
(4, 2, 3, 5, 'Rambabu', '9704644597', 'ramsmoola@gmail.com', NULL, 'Ranga Reddy', 'Telangana', NULL, NULL, NULL, '2017-09-12 02:07:11', '2017-09-12 02:07:11', NULL),
(5, 2, 3, 6, 'Narender', '98666888666', 'naren@gmail.com', NULL, 'Ranga Reddy', 'Telangana', NULL, NULL, NULL, '2017-09-12 02:09:10', '2017-09-12 02:09:10', NULL),
(6, 2, 3, 7, 'Madhu', '888666788', 'madhu@gmail.com', NULL, 'Ranga Reddy', 'Telangana', NULL, NULL, NULL, '2017-09-12 02:09:54', '2017-09-12 02:09:54', NULL),
(7, 2, 3, 8, 'Bhaskar', '888766554444', 'baskar@gmail.com', NULL, 'Ranga Reddy', 'Telangana', NULL, NULL, NULL, '2017-09-12 02:11:02', '2017-09-12 02:11:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_payments`
--

CREATE TABLE `customer_payments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `customer_id` int(12) NOT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `type` enum('product','upgrade') NOT NULL,
  `product_id` int(12) DEFAULT NULL,
  `change_request_id` int(12) DEFAULT NULL,
  `amount` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_payments`
--

INSERT INTO `customer_payments` (`id`, `org_id`, `project_id`, `customer_id`, `payment_id`, `type`, `product_id`, `change_request_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 32, 'product', 3, NULL, 2000000.00, '2017-09-11 08:45:53', '2017-09-11 08:45:53'),
(2, 1, 2, 1, 33, 'product', 7, NULL, 222.00, '2017-09-11 08:51:42', '2017-09-11 08:51:42'),
(3, 2, 3, 7, 49, 'product', 10, NULL, 2000000.00, '2017-09-18 02:27:59', '2017-09-18 02:27:59'),
(4, 2, 3, 6, 50, 'product', 11, NULL, 20000.00, '2017-09-18 02:39:14', '2017-09-18 02:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `org_id`, `project_id`, `code`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '0012', 'MASON', '2017-09-08 08:06:20', '2017-09-08 08:06:41', NULL),
(2, 1, 1, '0003', 'Electrical Dept', '2017-09-10 01:35:04', '2017-09-10 01:35:32', NULL),
(3, 1, 2, 'TILE123', 'Tiles', '2017-09-10 12:24:05', '2017-09-10 12:24:05', NULL),
(4, 1, 2, 'PAINT0', 'Painting', '2017-09-10 12:32:45', '2017-09-10 12:32:45', NULL),
(5, 1, 2, 'CENTRING00', 'Centring', '2017-09-10 13:04:01', '2017-09-10 13:04:01', NULL),
(6, 1, 2, 'ALUMN', 'Aluminium', '2017-09-11 02:38:32', '2017-09-11 02:38:32', NULL),
(7, 2, 3, 'ELC', 'Electrical_Dept', '2017-09-11 09:39:25', '2017-09-11 09:39:25', NULL),
(8, 2, 3, 'MSN', 'Mason_Dept', '2017-09-11 09:40:25', '2017-09-11 09:40:25', NULL),
(9, 2, 3, 'CRP', 'Carpentry_Dept', '2017-09-11 09:41:12', '2017-09-11 09:41:12', NULL),
(10, 2, 3, 'PLM', 'Plumbing_Dept', '2017-09-11 09:42:25', '2017-09-11 09:42:25', NULL),
(11, 2, 3, 'FLC', 'FalseCeiling_Dept', '2017-09-11 09:43:40', '2017-09-11 09:43:40', NULL),
(12, 2, 3, 'TLS', 'Tiles_Dept', '2017-09-11 09:44:38', '2017-09-11 09:44:38', NULL),
(13, 2, 3, 'CNT', 'Centring_Dept', '2017-09-11 09:45:40', '2017-09-11 09:45:40', NULL),
(14, 2, 3, 'PNT', 'Painting_Dept', '2017-09-11 09:46:33', '2017-09-11 09:46:33', NULL),
(15, 2, 3, 'LBR', 'Labour_Dept', '2017-09-11 09:48:09', '2017-09-11 09:48:09', NULL),
(16, 2, 3, 'LWN', 'Lawn_Dept', '2017-09-11 09:49:07', '2017-09-11 09:49:07', NULL),
(17, 2, 3, 'GLS', 'Glass_Dept', '2017-09-11 09:50:04', '2017-09-11 09:50:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department_contacts`
--

CREATE TABLE `department_contacts` (
  `id` int(12) NOT NULL,
  `department_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_contacts`
--

INSERT INTO `department_contacts` (`id`, `department_id`, `name`, `phone`, `email`, `address`, `state`, `created_at`, `updated_at`) VALUES
(1, 1, 'BIKSHALU', '9886688', 'BIKSHALU@GMAIL.COM', 'Mokila Village, Shankar pally', 'Telangana', '2017-09-08 08:06:20', '2017-09-08 08:06:20'),
(2, 2, 'Prakash', '1231231231', 'prakashsurya@gmail.com', 'Mokila, Ranga Reddy', 'Telangana', '2017-09-10 01:35:04', '2017-09-10 01:35:04'),
(3, 3, 'K.Madhu', 'XXXXXXXXX', 'Madh@Gmail.com', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-10 12:24:05', '2017-09-10 12:24:05'),
(4, 4, 'Gayatri creations', 'XXXXXXXXX', 'Gaya@gmail.com', 'XXXXXXXXXXX', 'Telangana', '2017-09-10 12:32:45', '2017-09-10 12:32:45'),
(5, 5, 'Subba  rao', 'XXXXXXXXX', 'XXXXXXXXXXX', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-10 13:04:01', '2017-09-10 13:04:01'),
(6, 6, 'Ashok', '988666656', 'ashok@gmail.com', 'HYDERABAD', 'Telangana', '2017-09-11 02:38:32', '2017-09-11 02:38:32'),
(7, 7, 'Prakash', '1231231231', 'prakashsubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:39:25', '2017-09-11 09:39:25'),
(8, 8, 'Bikshalu', '1231231231', 'Bikshalusubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:40:25', '2017-09-11 09:40:25'),
(9, 9, 'Venkatayya', '1231231231', 'venkatayyaSubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:41:12', '2017-09-11 09:41:12'),
(10, 10, 'Rohit', '1231231231', 'rohitSubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:42:25', '2017-09-11 09:42:25'),
(11, 11, 'Rajesh Pratap', '1231231231', 'rajeshsubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:43:40', '2017-09-11 09:43:40'),
(12, 12, 'Madhu', '1231231231', 'MadhuSubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:44:38', '2017-09-11 09:44:38'),
(13, 13, 'Subba Rao', '1231231231', 'subbaraoSubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:45:40', '2017-09-11 09:45:40'),
(14, 14, 'Rajesh Prajapathi', '1231231231', 'rajeshprajasubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:46:33', '2017-09-11 09:46:33'),
(15, 15, 'Surendar', '1231231231', 'surendarsubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:48:09', '2017-09-11 09:48:09'),
(16, 16, 'Prabhu Das', '1231231231', 'prabhudassubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:49:07', '2017-09-11 09:49:07'),
(17, 17, 'Eeshwar', '1231231231', 'eeshwarsubishi@gmail.com', 'Mokila, Shankar Pally', 'Telangana', '2017-09-11 09:50:04', '2017-09-11 09:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `external_payments`
--

CREATE TABLE `external_payments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `external_payments`
--

INSERT INTO `external_payments` (`id`, `org_id`, `project_id`, `payment_id`, `type`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 'Diesel for Car 2727', 1000.00, '2017-09-09 02:27:30', '2017-09-09 02:27:30'),
(2, 1, 1, 4, 'Petrol - Rambabu Bike', 500.00, '2017-09-09 02:34:38', '2017-09-09 02:34:38'),
(3, 1, 1, 5, 'Bike petrol for Sathish', 200.00, '2017-09-09 02:43:26', '2017-09-09 02:43:26'),
(4, 1, 1, 7, 'Purpose Testing', 7878.45, '2017-09-09 03:03:42', '2017-09-09 03:03:42'),
(5, 1, 1, 8, 'IT Support', 5000.00, '2017-09-09 06:47:31', '2017-09-09 06:47:31'),
(6, 1, 1, 9, 'Office Transportaion', 2000.00, '2017-09-09 06:51:38', '2017-09-09 06:51:38'),
(7, 1, 1, 10, 'Testing Purpose', 100.00, '2017-09-09 07:17:52', '2017-09-09 07:17:52'),
(8, 1, 2, 13, 'Diesel', 100.00, '2017-09-11 00:44:52', '2017-09-11 00:44:52'),
(9, 1, 2, 14, 'Material Paytmen', 3200.00, '2017-09-11 02:20:07', '2017-09-11 02:20:07');

-- --------------------------------------------------------

--
-- Table structure for table `india_states`
--

CREATE TABLE `india_states` (
  `id` int(11) NOT NULL,
  `statename` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `india_states`
--

INSERT INTO `india_states` (`id`, `statename`) VALUES
(1, 'Andhra Pradesh'),
(2, 'Assam'),
(3, 'Arunachal Pradesh'),
(4, 'Gujrat'),
(5, 'Bihar'),
(6, 'Haryana'),
(7, 'Himachal Pradesh'),
(8, 'Jammu & Kashmir'),
(9, 'Karnataka'),
(10, 'Kerala'),
(11, 'Madhya Pradesh'),
(12, 'Maharashtra'),
(13, 'Manipur'),
(14, 'Meghalaya'),
(15, 'Mizoram'),
(16, 'Nagaland'),
(17, 'Orissa'),
(18, 'Punjab'),
(19, 'Rajasthan'),
(20, 'Sikkim'),
(21, 'Tamil Nadu'),
(22, 'Tripura'),
(23, 'Uttar Pradesh'),
(24, 'West Bengal'),
(25, 'Goa'),
(26, 'Pondichery'),
(27, 'Lakshdweep'),
(28, 'Daman & Diu'),
(29, 'Dadra & Nagar'),
(30, 'Chandigarh'),
(31, 'Andaman & Nicobar'),
(32, 'Uttaranchal'),
(33, 'Jharkhand'),
(34, 'Chattisgarh'),
(35, 'Assam'),
(36, 'Telangana');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `total_qty` int(12) NOT NULL,
  `used_qty` int(12) NOT NULL,
  `avail_qty` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `org_id`, `project_id`, `item_id`, `total_qty`, `used_qty`, `avail_qty`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 400, 355, 45, '2017-09-10 16:05:32', '2017-09-11 09:51:15'),
(2, 1, 2, 2, 200, 119, 81, '2017-09-10 16:05:32', '2017-09-11 09:51:15'),
(3, 1, 2, 4, 1500, 1500, 0, '2017-09-10 16:09:16', '2017-09-11 03:53:14'),
(4, 1, 2, 5, 500, 10, 490, '2017-09-11 04:43:10', '2017-09-11 05:01:14'),
(5, 2, 3, 8, 1300, 0, 1300, '2017-09-13 04:36:12', '2017-09-18 05:06:13'),
(6, 2, 3, 9, 30, 5, 25, '2017-09-13 04:36:12', '2017-09-15 05:03:27'),
(7, 2, 3, 10, 200, 0, 200, '2017-09-13 04:36:12', '2017-09-15 05:13:32'),
(8, 2, 3, 11, 38, 0, 38, '2017-09-13 04:36:12', '2017-09-18 05:06:13'),
(9, 2, 3, 12, 10, 0, 10, '2017-09-13 04:37:53', '2017-09-13 04:37:53'),
(10, 2, 3, 15, 215, 20, 195, '2017-09-13 05:24:58', '2017-09-18 05:06:13'),
(11, 2, 3, 20, 150, 5, 145, '2017-09-14 09:49:05', '2017-09-15 05:03:27'),
(12, 2, 3, 17, 20, 20, 0, '2017-09-15 05:13:32', '2017-09-18 04:52:37'),
(13, 2, 3, 19, 250, 25, 225, '2017-09-15 05:13:32', '2017-09-18 04:52:37'),
(14, 2, 3, 16, 50, 0, 50, '2017-09-18 05:06:13', '2017-09-18 05:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `tax_id` int(12) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `discount` decimal(10,2) DEFAULT '0.00',
  `roq` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `org_id`, `project_id`, `tax_id`, `code`, `name`, `description`, `unit`, `unit_price`, `discount`, `roq`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 2, 'MESH123', 'Mesh', NULL, 'Nos', '70.00', '0.00', 50, '2017-09-10 13:25:32', '2017-09-10 13:25:32', NULL),
(2, 1, 2, 1, 'BROOMS1', 'Brooms', 'Brooms', 'Nos', '25.00', '0.00', 100, '2017-09-10 13:28:23', '2017-09-10 13:28:23', NULL),
(3, 1, 2, 1, 'FLOORING TILES', 'Bath Room Tiles', '600X600 Flooring Tiles', 'Boxes', '675.00', '0.00', 50, '2017-09-10 13:30:26', '2017-09-10 13:30:26', NULL),
(4, 1, 2, 1, 'CEMENT00', 'Cement', 'Cement', 'Bags', '250.00', '0.00', 500, '2017-09-10 13:32:16', '2017-09-10 13:32:16', NULL),
(5, 1, 2, 1, 'NRV', 'NRV', 'NON RETURN VALVE', 'Nos', '300.00', '0.00', 500, '2017-09-11 01:37:43', '2017-09-11 07:19:54', NULL),
(6, 1, 2, 1, 'PUTTY123', 'Putty', NULL, 'Bags', '750.00', '0.00', 25, '2017-09-11 07:24:04', '2017-09-11 07:24:04', NULL),
(7, 1, 2, 2, '8MM', '8MM Randam', NULL, 'Tons', '39000.00', '0.00', 2, '2017-09-11 07:25:12', '2017-09-11 07:25:12', NULL),
(8, 2, 3, 8, 'SPNG_0', 'SPONGES', NULL, 'Nos', '11.00', '0.00', 200, '2017-09-12 07:30:59', '2017-09-12 07:30:59', NULL),
(9, 2, 3, 8, 'PARA_1', 'GI Para', NULL, 'Nos', '274.00', '0.00', 10, '2017-09-12 07:33:34', '2017-09-12 07:33:34', NULL),
(10, 2, 3, 12, 'PVC GAMPA_02', 'PVC Gampa', NULL, 'Nos', '110.00', '0.00', 15, '2017-09-12 07:36:03', '2017-09-15 02:37:28', NULL),
(11, 2, 3, 8, 'SABBAL', 'Sabbal', NULL, 'Nos', '250.00', '0.00', 3, '2017-09-12 07:39:56', '2017-09-12 07:39:56', NULL),
(12, 2, 3, 8, '8MMSTL', '8MM Steel Randam', NULL, 'Tons', '39000.00', '0.00', 2, '2017-09-12 07:41:50', '2017-09-12 07:41:50', NULL),
(13, 2, 3, 8, 'PPC_CMNT', 'PPC Cemaent', NULL, 'Bags', '240.00', '0.00', 500, '2017-09-12 07:45:53', '2017-09-12 07:45:53', NULL),
(14, 2, 3, 8, 'OPC_CMNT', 'OPC Cement', NULL, 'Bags', '250.00', '0.00', 500, '2017-09-12 07:47:50', '2017-09-12 07:47:50', NULL),
(15, 2, 3, 12, 'MSBOXES', '6 MS Box', 'Electricalo', 'Nos', '25.00', '0.00', 15, '2017-09-13 02:40:18', '2017-09-13 02:40:18', NULL),
(16, 2, 3, 12, 'MSBOXES', '8 MS Box', 'Electrical', 'Nos', '35.00', '0.00', 25, '2017-09-13 02:41:32', '2017-09-13 02:41:32', NULL),
(17, 2, 3, 12, 'MSBOXES', '4 MS Box', 'Electrical', 'Nos', '30.00', '0.00', 20, '2017-09-13 02:42:49', '2017-09-13 02:47:44', NULL),
(18, 2, 3, 12, 'MSBOXES', '3 MS Box', 'Electrical', 'Nos', '28.00', '0.00', 20, '2017-09-13 02:43:53', '2017-09-13 02:43:53', NULL),
(19, 2, 3, 12, 'MSBOXES', '2 MS Box', 'Electrical', 'Nos', '18.00', '0.00', 25, '2017-09-13 02:45:22', '2017-09-13 02:45:22', NULL),
(20, 2, 3, 12, 'CRPIPES', 'Curing Pipes', NULL, 'Kgs', '121.00', '0.00', 2, '2017-09-14 07:18:00', '2017-09-14 07:18:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_departments`
--

CREATE TABLE `item_departments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `department_id` int(12) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_departments`
--

INSERT INTO `item_departments` (`id`, `org_id`, `project_id`, `item_id`, `department_id`) VALUES
(1, 1, 2, 1, 1),
(2, 1, 2, 2, 1),
(3, 1, 2, 2, 3),
(4, 1, 2, 3, 3),
(5, 1, 2, 4, 1),
(6, 1, 2, 4, 3),
(15, 2, 3, 8, 8),
(14, 1, 2, 7, 5),
(13, 1, 2, 6, 4),
(12, 1, 2, 5, 4),
(16, 2, 3, 8, 10),
(17, 2, 3, 8, 12),
(18, 2, 3, 9, 8),
(19, 2, 3, 9, 10),
(20, 2, 3, 9, 12),
(44, 2, 3, 10, 12),
(43, 2, 3, 10, 8),
(24, 2, 3, 11, 8),
(25, 2, 3, 11, 10),
(26, 2, 3, 11, 12),
(32, 2, 3, 12, 13),
(28, 2, 3, 13, 12),
(29, 2, 3, 14, 8),
(30, 2, 3, 14, 10),
(31, 2, 3, 14, 12),
(33, 2, 3, 15, 7),
(34, 2, 3, 16, 7),
(38, 2, 3, 17, 7),
(36, 2, 3, 18, 7),
(37, 2, 3, 19, 7),
(39, 2, 3, 20, 8),
(40, 2, 3, 20, 12);

-- --------------------------------------------------------

--
-- Table structure for table `item_staffs`
--

CREATE TABLE `item_staffs` (
  `id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `staff_id` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_staffs`
--

INSERT INTO `item_staffs` (`id`, `item_id`, `staff_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2017-09-10 15:55:32', '2017-09-10 15:55:32'),
(2, 2, 2, '2017-09-10 15:58:23', '2017-09-10 15:58:23'),
(3, 3, 1, '2017-09-10 16:00:26', '2017-09-10 16:00:26'),
(4, 4, 1, '2017-09-10 16:02:16', '2017-09-10 16:02:16'),
(7, 6, 1, '2017-09-11 09:54:04', '2017-09-11 09:54:04'),
(8, 7, 1, '2017-09-11 09:55:12', '2017-09-11 09:55:12'),
(9, 8, 4, '2017-09-12 10:00:59', '2017-09-12 10:00:59'),
(10, 9, 4, '2017-09-12 10:03:34', '2017-09-12 10:03:34'),
(25, 10, 4, '2017-09-15 05:09:21', '2017-09-15 05:09:21'),
(12, 11, 4, '2017-09-12 10:09:56', '2017-09-12 10:09:56'),
(16, 12, 4, '2017-09-12 10:19:25', '2017-09-12 10:19:25'),
(14, 13, 4, '2017-09-12 10:15:53', '2017-09-12 10:15:53'),
(15, 14, 4, '2017-09-12 10:17:50', '2017-09-12 10:17:50'),
(17, 15, 4, '2017-09-13 05:10:18', '2017-09-13 05:10:18'),
(18, 16, 4, '2017-09-13 05:11:32', '2017-09-13 05:11:32'),
(22, 17, 4, '2017-09-13 05:17:44', '2017-09-13 05:17:44'),
(20, 18, 4, '2017-09-13 05:13:53', '2017-09-13 05:13:53'),
(21, 19, 4, '2017-09-13 05:15:22', '2017-09-13 05:15:22'),
(23, 20, 4, '2017-09-14 09:48:00', '2017-09-14 09:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `item_suppliers`
--

CREATE TABLE `item_suppliers` (
  `item_id` int(12) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_suppliers`
--

INSERT INTO `item_suppliers` (`item_id`, `supplier_id`, `created_at`, `updated_at`) VALUES
(1, 4, '2017-09-10 15:55:32', '2017-09-10 15:55:32'),
(2, 4, '2017-09-10 15:58:23', '2017-09-10 15:58:23'),
(3, 5, '2017-09-10 16:00:26', '2017-09-10 16:00:26'),
(4, 2, '2017-09-10 16:02:16', '2017-09-10 16:02:16'),
(5, 4, '2017-09-11 09:50:20', '2017-09-11 09:50:20'),
(6, 6, '2017-09-11 09:54:04', '2017-09-11 09:54:04'),
(7, 3, '2017-09-11 09:55:12', '2017-09-11 09:55:12'),
(8, 7, '2017-09-12 10:00:59', '2017-09-12 10:00:59'),
(9, 7, '2017-09-12 10:03:34', '2017-09-12 10:03:34'),
(10, 7, '2017-09-15 05:09:21', '2017-09-15 05:09:21'),
(11, 7, '2017-09-12 10:09:56', '2017-09-12 10:09:56'),
(15, 7, '2017-09-13 05:10:18', '2017-09-13 05:10:18'),
(12, 10, '2017-09-12 10:19:25', '2017-09-12 10:19:25'),
(13, 14, '2017-09-12 10:15:53', '2017-09-12 10:15:53'),
(14, 14, '2017-09-12 10:17:50', '2017-09-12 10:17:50'),
(16, 7, '2017-09-13 05:11:32', '2017-09-13 05:11:32'),
(17, 7, '2017-09-13 05:17:44', '2017-09-13 05:17:44'),
(18, 7, '2017-09-13 05:13:53', '2017-09-13 05:13:53'),
(19, 7, '2017-09-13 05:15:22', '2017-09-13 05:15:22'),
(20, 8, '2017-09-14 09:48:00', '2017-09-14 09:48:00');

-- --------------------------------------------------------

--
-- Table structure for table `material_transfers`
--

CREATE TABLE `material_transfers` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `department_id` int(12) NOT NULL,
  `product_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_transfers`
--

INSERT INTO `material_transfers` (`id`, `user_id`, `org_id`, `project_id`, `supplier_id`, `department_id`, `product_id`, `item_id`, `qty`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 4, 1, 3, 2, 100, '2017-09-11 03:53:14', '2017-09-11 03:53:14'),
(2, 1, 1, 2, 4, 1, 3, 1, 350, '2017-09-11 03:53:14', '2017-09-11 03:53:14'),
(3, 1, 1, 2, 2, 1, 3, 4, 1500, '2017-09-11 03:53:14', '2017-09-11 03:53:14'),
(4, 1, 1, 2, 4, 2, 4, 5, 10, '2017-09-11 05:01:14', '2017-09-11 05:01:14'),
(5, 1, 1, 2, 4, 1, 4, 2, 10, '2017-09-11 07:02:03', '2017-09-11 07:02:03'),
(6, 1, 1, 2, 4, 1, 7, 2, 9, '2017-09-11 09:51:15', '2017-09-11 09:51:15'),
(7, 1, 1, 2, 4, 1, 7, 1, 5, '2017-09-11 09:51:15', '2017-09-11 09:51:15'),
(8, 1, 2, 3, 8, 8, 11, 20, 5, '2017-09-15 05:03:27', '2017-09-15 05:03:27'),
(9, 1, 2, 3, 7, 8, 11, 9, 5, '2017-09-15 05:03:27', '2017-09-15 05:03:27'),
(10, 1, 2, 3, 7, 7, 10, 19, 25, '2017-09-18 04:52:37', '2017-09-18 04:52:37'),
(11, 1, 2, 3, 7, 7, 10, 17, 20, '2017-09-18 04:52:37', '2017-09-18 04:52:37'),
(12, 1, 2, 3, 7, 7, 10, 15, 20, '2017-09-18 04:52:37', '2017-09-18 04:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `invoice_total` decimal(10,2) DEFAULT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `draft` tinyint(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `org_id`, `project_id`, `code`, `invoice_total`, `payment_id`, `draft`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 2, 'SUBISHI-SIG4-0001', '32110.00', NULL, 0, '2017-09-10 13:35:07', '2017-09-10 13:35:07', NULL),
(2, 1, 1, 2, 'SUBISHI-SIG4-0002', '480000.00', NULL, 0, '2017-09-10 13:36:32', '2017-09-10 13:36:32', NULL),
(3, 1, 1, 2, 'SUBISHI-SIG4-0003', '199330.00', NULL, 0, '2017-09-11 02:12:59', '2017-09-11 02:12:59', NULL),
(4, 1, 2, 3, 'SI-SI MIST-0004', '26201.60', NULL, 0, '2017-09-13 02:04:40', '2017-09-13 02:04:40', NULL),
(5, 1, 2, 3, 'SI-SI MIST-0005', '499200.00', NULL, 0, '2017-09-13 02:07:39', '2017-09-13 02:07:39', NULL),
(6, 1, 2, 3, 'SI-SI MIST-0006', NULL, NULL, 0, '2017-09-13 02:50:45', '2017-09-13 02:50:45', NULL),
(7, 1, 2, 3, 'SI-SI MIST-0007', '5900.00', NULL, 0, '2017-09-13 02:54:39', '2017-09-13 02:54:39', NULL),
(8, 1, 2, 3, 'SI-SI MIST-0008', '21417.00', NULL, 0, '2017-09-14 07:18:49', '2017-09-14 07:18:49', NULL),
(9, 1, 2, 3, 'SI-SI MIST-0009', '35088.00', NULL, 0, '2017-09-15 02:43:19', '2017-09-15 02:43:19', NULL),
(10, 1, 2, 3, 'SI-SI MIST-0010', '13496.75', NULL, 0, '2017-09-18 02:35:50', '2017-09-18 02:35:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_grns`
--

CREATE TABLE `order_grns` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `order_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `item_row_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `payment_id` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_grns`
--

INSERT INTO `order_grns` (`id`, `org_id`, `project_id`, `order_id`, `item_id`, `item_row_id`, `qty`, `total_price`, `payment_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 2, 1, 100, '3200.00', '18', '2017-09-10 16:05:32', '2017-09-11 06:11:54'),
(2, 1, 2, 1, 1, 2, 350, '28910.00', '19', '2017-09-10 16:05:32', '2017-09-11 06:19:17'),
(3, 1, 2, 2, 4, 3, 1500, '480000.00', NULL, '2017-09-10 16:09:16', '2017-09-10 16:09:16'),
(4, 1, 2, 3, 2, 4, 100, '3200.00', '17', '2017-09-11 04:43:10', '2017-09-11 05:55:18'),
(5, 1, 2, 3, 1, 5, 50, '4130.00', '20', '2017-09-11 04:43:10', '2017-09-11 06:20:16'),
(6, 1, 2, 3, 5, 6, 500, '192000.00', '21', '2017-09-11 04:43:10', '2017-09-11 06:31:52'),
(7, 2, 3, 4, 9, 7, 30, '10521.60', NULL, '2017-09-13 04:36:12', '2017-09-13 04:36:12'),
(8, 2, 3, 4, 10, 8, 50, '7040.00', NULL, '2017-09-13 04:36:12', '2017-09-13 04:36:12'),
(9, 2, 3, 4, 11, 9, 5, '1600.00', NULL, '2017-09-13 04:36:12', '2017-09-13 04:36:12'),
(10, 2, 3, 4, 8, 10, 500, '7040.00', '47', '2017-09-13 04:36:12', '2017-09-15 02:46:00'),
(11, 2, 3, 5, 12, 11, 10, '499200.00', '43', '2017-09-13 04:37:53', '2017-09-13 02:09:58'),
(12, 2, 3, 7, 15, 12, 200, '5900.00', '47', '2017-09-13 05:24:58', '2017-09-15 02:46:00'),
(13, 2, 3, 8, 20, 13, 150, '21417.00', NULL, '2017-09-14 09:49:05', '2017-09-14 09:49:05'),
(14, 2, 3, 9, 19, 16, 250, '5310.00', '47', '2017-09-15 05:13:32', '2017-09-15 02:46:00'),
(15, 2, 3, 9, 17, 17, 20, '708.00', '47', '2017-09-15 05:13:32', '2017-09-15 02:46:00'),
(16, 2, 3, 9, 10, 14, 150, '19470.00', '47', '2017-09-15 05:13:32', '2017-09-15 02:46:00'),
(17, 2, 3, 9, 11, 15, 30, '9600.00', '47', '2017-09-15 05:13:32', '2017-09-15 02:46:00'),
(18, 2, 3, 10, 15, 18, 15, '408.75', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(19, 2, 3, 10, 16, 19, 25, '1032.50', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(20, 2, 3, 10, 16, 20, 25, '997.50', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(21, 2, 3, 10, 11, 21, 3, '960.00', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(22, 2, 3, 10, 8, 22, 200, '2464.00', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(23, 2, 3, 10, 8, 23, 200, '2310.00', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(24, 2, 3, 10, 8, 24, 200, '2816.00', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13'),
(25, 2, 3, 10, 8, 25, 200, '2508.00', NULL, '2017-09-18 05:06:13', '2017-09-18 05:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `order_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `supplier_code` varchar(255) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `qty` int(12) NOT NULL,
  `unit` varchar(255) NOT NULL,
  `unit_price` decimal(10,2) DEFAULT NULL,
  `discount` int(12) DEFAULT NULL,
  `tax_name` varchar(255) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `net_unit_price` decimal(10,2) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `org_id`, `project_id`, `order_id`, `item_id`, `item_code`, `item_name`, `supplier_id`, `supplier_code`, `supplier_name`, `qty`, `unit`, `unit_price`, `discount`, `tax_name`, `tax`, `net_unit_price`, `total_price`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 2, 'BROOMS1', 'Brooms', 4, 'MAHA123', 'Mahalaxmi Electricals', 100, 'Nos', '25.00', 0, 'GST', '28.00', '32.00', '3200.00', '2017-09-10 16:05:07', '2017-09-10 16:05:07'),
(2, 1, 2, 1, 1, 'MESH123', 'Mesh', 4, 'MAHA123', 'Mahalaxmi Electricals', 350, 'Nos', '70.00', 0, 'GST', '18.00', '82.60', '28910.00', '2017-09-10 16:05:07', '2017-09-10 16:05:07'),
(3, 1, 2, 2, 4, 'CEMENT00', 'Cement', 2, '002', 'NCC Cement', 1500, 'Bags', '250.00', 0, 'GST', '28.00', '320.00', '480000.00', '2017-09-10 16:06:32', '2017-09-10 16:06:32'),
(4, 1, 2, 3, 2, 'BROOMS1', 'Brooms', 4, 'MAHA123', 'Mahalaxmi Electricals', 100, 'Nos', '25.00', 0, 'GST', '28.00', '32.00', '3200.00', '2017-09-11 04:42:59', '2017-09-11 04:42:59'),
(5, 1, 2, 3, 1, 'MESH123', 'Mesh', 4, 'MAHA123', 'Mahalaxmi Electricals', 50, 'Nos', '70.00', 0, 'GST', '18.00', '82.60', '4130.00', '2017-09-11 04:42:59', '2017-09-11 04:42:59'),
(6, 1, 2, 3, 5, 'NRV', 'NRV', 4, 'MAHA123', 'Mahalaxmi Electricals', 500, '200', '300.00', 0, 'GST', '28.00', '384.00', '192000.00', '2017-09-11 04:42:59', '2017-09-11 04:42:59'),
(7, 2, 3, 4, 9, 'PARA_1', 'GI Para', 7, 'MLX_ML', 'Mahalaxmi Electricals', 30, 'Nos', '274.00', 0, 'GST', '28.00', '350.72', '10521.60', '2017-09-13 04:34:40', '2017-09-13 04:34:40'),
(8, 2, 3, 4, 10, 'PVC GAMPA_02', 'PVC Gampa', 7, 'MLX_ML', 'Mahalaxmi Electricals', 50, 'Nos', '110.00', 0, 'GST', '28.00', '140.80', '7040.00', '2017-09-13 04:34:40', '2017-09-13 04:34:40'),
(9, 2, 3, 4, 11, 'SABBAL', 'Sabbal', 7, 'MLX_ML', 'Mahalaxmi Electricals', 5, 'Nos', '250.00', 0, 'GST', '28.00', '320.00', '1600.00', '2017-09-13 04:34:40', '2017-09-13 04:34:40'),
(10, 2, 3, 4, 8, 'SPNG_0', 'SPONGES', 7, 'MLX_ML', 'Mahalaxmi Electricals', 500, 'Nos', '11.00', 0, 'GST', '28.00', '14.08', '7040.00', '2017-09-13 04:34:40', '2017-09-13 04:34:40'),
(11, 2, 3, 5, 12, '8MMSTL', '8MM Steel Randam', 10, 'SRNST_SRN', 'Srinivasa Steel Traders', 10, 'Tons', '39000.00', 0, 'GST', '28.00', '49920.00', '499200.00', '2017-09-13 04:37:39', '2017-09-13 04:37:39'),
(12, 2, 3, 7, 15, 'MSBOXES', '6 MS Box', 7, 'MLX_ML', 'Mahalaxmi Electricals', 200, 'Nos', '25.00', 0, 'GST', '18.00', '29.50', '5900.00', '2017-09-13 05:24:39', '2017-09-13 05:24:39'),
(13, 2, 3, 8, 20, 'CRPIPES', 'Curing Pipes', 8, 'SRGA_SRA', 'Sriganesh Agencies', 150, 'Kgs', '121.00', 0, 'GST', '18.00', '142.78', '21417.00', '2017-09-14 09:48:49', '2017-09-14 09:48:49'),
(14, 2, 3, 9, 10, 'PVC GAMPA_02', 'PVC Gampa', 7, 'MLX_ML', 'Mahalaxmi Electricals', 150, 'Nos', '110.00', 0, 'GST', '18.00', '129.80', '19470.00', '2017-09-15 05:13:19', '2017-09-15 05:13:19'),
(15, 2, 3, 9, 11, 'SABBAL', 'Sabbal', 7, 'MLX_ML', 'Mahalaxmi Electricals', 30, 'Nos', '250.00', 0, 'GST', '28.00', '320.00', '9600.00', '2017-09-15 05:13:19', '2017-09-15 05:13:19'),
(16, 2, 3, 9, 19, 'MSBOXES', '2 MS Box', 7, 'MLX_ML', 'Mahalaxmi Electricals', 250, 'Nos', '18.00', 0, 'GST', '18.00', '21.24', '5310.00', '2017-09-15 05:13:19', '2017-09-15 05:13:19'),
(17, 2, 3, 9, 17, 'MSBOXES', '4 MS Box', 7, 'MLX_ML', 'Mahalaxmi Electricals', 20, 'Nos', '30.00', 0, 'GST', '18.00', '35.40', '708.00', '2017-09-15 05:13:19', '2017-09-15 05:13:19'),
(18, 2, 3, 10, 15, 'MSBOXES', '6 MS Box', 7, 'MLX_ML', 'Mahalaxmi Electricals', 15, 'Nos', '25.00', 0, 'GST', '9.00', '27.25', '408.75', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(19, 2, 3, 10, 16, 'MSBOXES', '8 MS Box', 7, 'MLX_ML', 'Mahalaxmi Electricals', 25, 'Nos', '35.00', 0, 'GST', '18.00', '41.30', '1032.50', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(20, 2, 3, 10, 16, 'MSBOXES', '8 MS Box', 7, 'MLX_ML', 'Mahalaxmi Electricals', 25, 'Nos', '35.00', 0, 'GST', '14.00', '39.90', '997.50', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(21, 2, 3, 10, 11, 'SABBAL', 'Sabbal', 7, 'MLX_ML', 'Mahalaxmi Electricals', 3, 'Nos', '250.00', 0, 'GST', '28.00', '320.00', '960.00', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(22, 2, 3, 10, 8, 'SPNG_0', 'SPONGES', 7, 'MLX_ML', 'Mahalaxmi Electricals', 200, 'Nos', '11.00', 0, 'GST', '12.00', '12.32', '2464.00', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(23, 2, 3, 10, 8, 'SPNG_0', 'SPONGES', 7, 'MLX_ML', 'Mahalaxmi Electricals', 200, 'Nos', '11.00', 0, 'GST', '5.00', '11.55', '2310.00', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(24, 2, 3, 10, 8, 'SPNG_0', 'SPONGES', 7, 'MLX_ML', 'Mahalaxmi Electricals', 200, 'Nos', '11.00', 0, 'GST', '28.00', '14.08', '2816.00', '2017-09-18 05:05:50', '2017-09-18 05:05:50'),
(25, 2, 3, 10, 8, 'SPNG_0', 'SPONGES', 7, 'MLX_ML', 'Mahalaxmi Electricals', 200, 'Nos', '11.00', 0, 'GST', '14.00', '12.54', '2508.00', '2017-09-18 05:05:50', '2017-09-18 05:05:50');

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `org_code` varchar(50) NOT NULL,
  `org_name` varchar(255) NOT NULL,
  `org_address_one` varchar(255) DEFAULT NULL,
  `org_address_two` varchar(255) DEFAULT NULL,
  `org_city` varchar(255) DEFAULT NULL,
  `org_district` varchar(255) DEFAULT NULL,
  `org_state` varchar(255) DEFAULT NULL,
  `org_pincode` varchar(6) DEFAULT NULL,
  `org_tin` varchar(50) DEFAULT NULL,
  `org_billing_address` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `user_id`, `org_code`, `org_name`, `org_address_one`, `org_address_two`, `org_city`, `org_district`, `org_state`, `org_pincode`, `org_tin`, `org_billing_address`, `logo`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'SUBISHI', 'Subishi', 'Mokila Village', 'Shankar pally', 'Hyderabad', 'Ranga Reddy', 'Telangana', '501203', '8866899aads', 'Mokila', 'org-logos/nX4LLBF6HDlOaXS52AQBhpSXhWkr2arobEi98emp.png', '2017-09-09 08:31:09', '2017-09-09 06:01:09', NULL),
(2, 1, 'SI', 'Subishi Infra', 'Suite# 308, Subishi TownCenter', 'Mokila', 'Shankarpally', 'Ranga Reddy', 'Telangana', '501203', '123123123', 'Mokila, Shankar pally, Ranga Reddy', 'org-logos/NHuS4QWSRp8PRVm6qqllwTBE26In3ghXLEvVxCWR.png', '2017-09-11 11:31:05', '2017-09-11 09:01:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization_contacts`
--

CREATE TABLE `organization_contacts` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` int(6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization_contacts`
--

INSERT INTO `organization_contacts` (`id`, `org_id`, `name`, `phone`, `email`, `address`, `state`, `pincode`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sathish', '9849701105', 'sathishgoud82@gmail.com', NULL, 'Telangana', 501203, '2017-09-08 08:00:37', '2017-09-08 08:00:37'),
(2, 2, 'Sathish', '9849701105', 'sathishgoud82@gmail.com', NULL, 'Telangana', 501203, '2017-09-11 09:01:05', '2017-09-11 09:01:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(12) NOT NULL,
  `gen_payment_id` varchar(255) DEFAULT NULL,
  `user_id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `bank_id` int(12) NOT NULL,
  `payment_method_id` int(12) NOT NULL,
  `payment_method_ref` varchar(255) DEFAULT NULL,
  `payment_type` int(12) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `type` enum('debit','credit') NOT NULL,
  `paid_to` varchar(255) DEFAULT NULL,
  `paid_by` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `gen_payment_id`, `user_id`, `org_id`, `project_id`, `bank_id`, `payment_method_id`, `payment_method_ref`, `payment_type`, `amount`, `type`, `paid_to`, `paid_by`, `approved_by`, `ref_id`, `remark`, `date`, `checksum`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, 1, 1, 0, NULL, NULL, 10000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-09', NULL, '2017-09-09 02:23:33', '2017-09-09 02:23:33'),
(2, NULL, 1, 1, 1, 2, 0, NULL, NULL, 20000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-09', NULL, '2017-09-09 02:25:21', '2017-09-09 02:25:21'),
(3, 'MIST201709090003', 1, 1, 1, 1, 3, 'Diesel', 4, 1000.00, 'debit', 'Bhoopal Reddy', NULL, NULL, 0, 'Cash memo received', '2017-09-09', NULL, '2017-09-09 02:27:30', '2017-09-09 02:27:30'),
(4, 'MIST201709090004', 1, 1, 1, 1, 3, NULL, 4, 500.00, 'debit', 'Jaipal Approved', NULL, NULL, 0, 'Paid to Naveen and Approved by Jaipal', '2017-09-08', NULL, '2017-09-09 02:34:38', '2017-09-09 02:34:38'),
(5, 'MIST201709090005', 1, 1, 1, 1, 3, NULL, 4, 200.00, 'debit', 'Pasha Approved', NULL, NULL, 0, 'Testing', '2017-09-09', NULL, '2017-09-09 02:43:26', '2017-09-09 02:43:26'),
(6, NULL, 1, 1, 1, 3, 0, NULL, NULL, 50000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-09', NULL, '2017-09-09 02:46:06', '2017-09-09 02:46:06'),
(7, 'MIST201709090007', 1, 1, 1, 1, 3, NULL, 4, 7878.45, 'debit', 'Rambabu - Approved By', NULL, NULL, 0, 'Remarks Column', '2017-09-09', NULL, '2017-09-09 03:03:42', '2017-09-09 03:03:42'),
(8, 'MIST201709090008', 1, 1, 1, 2, 3, NULL, 4, 5000.00, 'debit', 'Sathish', NULL, NULL, 0, 'Bug Fixing', '2017-09-09', NULL, '2017-09-09 06:47:31', '2017-09-09 06:47:31'),
(9, 'MIST201709090009', 1, 1, 1, 2, 2, NULL, 4, 2000.00, 'debit', 'Ravish Kumar', NULL, 'Jaipal reddy', 0, NULL, '2017-09-09', NULL, '2017-09-09 06:51:38', '2017-09-09 06:51:38'),
(10, 'MIST201709090010', 1, 1, 1, 1, 3, NULL, 4, 100.00, 'debit', 'Testing Paid To', NULL, 'Testing Approved By', 0, 'Testing Remarks', '2017-09-09', NULL, '2017-09-09 07:17:52', '2017-09-09 07:17:52'),
(11, NULL, 1, 1, 2, 4, 0, NULL, NULL, 500000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-10', NULL, '2017-09-10 13:19:36', '2017-09-10 13:19:36'),
(12, NULL, 1, 1, 2, 5, 0, NULL, NULL, 2000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-10', NULL, '2017-09-10 13:20:29', '2017-09-10 13:20:29'),
(13, 'SIG4201709110013', 1, 1, 2, 5, 3, NULL, 4, 100.00, 'debit', 'Sathish', NULL, 'Bhoopal', 0, 'Bike Petrol', '2017-09-11', NULL, '2017-09-11 00:44:52', '2017-09-11 00:44:52'),
(14, 'SIG4201709110014', 1, 1, 2, 5, 1, '#01', 4, 3200.00, 'debit', 'Mahalaxmi Electrials', NULL, 'Bhoopal', 0, NULL, '2017-09-11', NULL, '2017-09-11 02:20:07', '2017-09-11 02:20:07'),
(15, 'SIG4201709110015', 1, 1, 2, 5, 1, '001', 3, 20000.00, 'debit', 'Bhoopal Reddy', NULL, NULL, 0, NULL, '2017-09-11', NULL, '2017-09-11 02:22:18', '2017-09-11 02:22:18'),
(16, NULL, 1, 1, 2, 6, 0, NULL, NULL, 2000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-11', NULL, '2017-09-11 02:27:00', '2017-09-11 02:27:00'),
(17, 'SIG4201709110017', 1, 1, 2, 5, 3, NULL, 1, 3200.00, 'debit', 'Gulzar Ahmed', NULL, '', 4, NULL, '2017-09-11', NULL, '2017-09-11 05:55:18', '2017-09-11 05:55:18'),
(18, 'SIG4201709110018', 1, 1, 2, 5, 1, NULL, 1, 3200.00, 'debit', 'mahalaxmi', NULL, '', 4, NULL, '2017-09-11', NULL, '2017-09-11 06:11:54', '2017-09-11 06:11:54'),
(19, 'SIG4201709110019', 1, 1, 2, 5, 2, NULL, 1, 28910.00, 'debit', 'mahalaxmi', NULL, '', 4, NULL, '2017-09-11', NULL, '2017-09-11 06:19:17', '2017-09-11 06:19:17'),
(20, 'SIG4201709110020', 1, 1, 2, 5, 1, NULL, 1, 4130.00, 'debit', 'Mahalaxmi', NULL, '', 4, NULL, '2017-09-11', NULL, '2017-09-11 06:20:16', '2017-09-11 06:20:16'),
(21, 'SIG4201709110021', 1, 1, 2, 5, 1, NULL, 1, 192000.00, 'debit', 'mahalaxmi electricals', NULL, '', 4, NULL, '2017-09-11', NULL, '2017-09-11 06:31:52', '2017-09-11 06:31:52'),
(22, 'SIG4201709110022', 1, 1, 2, 4, 3, NULL, 3, 50000.00, 'debit', 'Bhoopal Reddy', NULL, '', 0, '3Months Salary Advance', '2017-09-11', NULL, '2017-09-11 06:33:26', '2017-09-11 06:33:26'),
(23, 'SIG4201709110023', 1, 1, 2, 6, 2, '0087', 2, 51000.00, 'debit', 'Madhu', NULL, '', 1, NULL, '2017-09-11', NULL, '2017-09-11 06:43:41', '2017-09-11 06:43:41'),
(24, 'SIG4201709110024', 1, 1, 2, 5, 1, '#0111', 2, 49000.00, 'debit', NULL, NULL, '', 1, NULL, '2017-09-11', NULL, '2017-09-11 06:44:20', '2017-09-11 06:44:20'),
(25, 'SIG4201709110025', 1, 1, 2, 5, 2, NULL, 2, 45000.00, 'debit', NULL, NULL, NULL, 2, 'Chipping &Wiring', '2017-09-11', NULL, '2017-09-11 06:52:01', '2017-09-11 06:52:01'),
(26, 'SIG4201709110026', 1, 1, 2, 5, 3, NULL, 6, 5000.00, 'credit', NULL, 'Gulzar Ahmed', 'Sathish', 0, NULL, '2017-09-11', NULL, '2017-09-11 07:00:45', '2017-09-11 07:00:45'),
(27, 'SIG4201709110027', 1, 1, 2, 6, 3, '0089', 6, 30000.00, 'credit', NULL, 'Venugopal', 'Vaani reddy', 0, NULL, '2017-09-04', NULL, '2017-09-11 07:04:17', '2017-09-11 07:04:17'),
(28, 'SIG4201709110028', 1, 1, 2, 5, 1, NULL, 6, 200000.00, 'credit', NULL, 'rambabu', 'Bhoopal Reddy', 0, NULL, '2017-09-11', NULL, '2017-09-11 07:05:13', '2017-09-11 07:05:13'),
(29, 'SIG4201709110029', 1, 1, 2, 6, 2, '#9988', 3, 9000.00, 'debit', 'Bhoopal reddy', NULL, NULL, 0, '15 days@50 Labour work payment', '2017-09-11', NULL, '2017-09-11 07:12:40', '2017-09-11 07:12:40'),
(30, 'SIG4201709110030', 1, 1, 2, 6, 3, '778999', 3, 2000.00, 'debit', 'Bhoopal Reddy', NULL, NULL, 0, 'Mokila Ganesh chandha 2000', '2017-09-11', NULL, '2017-09-11 07:14:46', '2017-09-11 07:14:46'),
(31, 'SIG4201709110031', 1, 1, 2, 4, 3, NULL, 6, 111.00, 'credit', NULL, 'satish', 'jaipal', 0, 'hjgfjhfg', '2017-09-11', NULL, '2017-09-11 08:41:01', '2017-09-11 08:41:01'),
(32, 'SIG4201709110032', 1, 1, 2, 4, 1, NULL, 5, 2000000.00, 'credit', 'sathish', NULL, '', 0, NULL, '2017-09-11', NULL, '2017-09-11 08:45:53', '2017-09-11 08:45:53'),
(33, 'SIG4201709110033', 1, 1, 2, 4, 1, NULL, 5, 222.00, 'credit', 'sathish', NULL, '', 0, NULL, '2017-09-11', NULL, '2017-09-11 08:51:42', '2017-09-11 08:51:42'),
(34, NULL, 1, 2, 3, 7, 0, NULL, NULL, 2000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 01:53:17', '2017-09-12 01:53:17'),
(35, NULL, 1, 2, 3, 8, 0, NULL, NULL, 2000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 01:54:53', '2017-09-12 01:54:53'),
(36, NULL, 1, 2, 3, 9, 0, NULL, NULL, 3000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 01:57:28', '2017-09-12 01:57:28'),
(37, NULL, 1, 2, 3, 10, 0, NULL, NULL, 1500000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 01:58:31', '2017-09-12 01:58:31'),
(38, NULL, 1, 2, 3, 11, 0, NULL, NULL, 2500000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 02:00:10', '2017-09-12 02:00:10'),
(39, NULL, 1, 2, 3, 12, 0, NULL, NULL, 2800000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 02:01:17', '2017-09-12 02:01:17'),
(40, NULL, 1, 2, 3, 13, 0, NULL, NULL, 3000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 02:02:16', '2017-09-12 02:02:16'),
(41, NULL, 1, 2, 3, 14, 0, NULL, NULL, 3000000.00, 'credit', NULL, NULL, NULL, NULL, NULL, '2017-09-12', NULL, '2017-09-12 02:03:05', '2017-09-12 02:03:05'),
(42, 'SI MIST201709130042', 1, 2, 3, 13, 1, NULL, 2, 5000.00, 'debit', 'Prakash', NULL, NULL, 6, '5K paid', '2017-09-13', NULL, '2017-09-13 01:41:12', '2017-09-13 01:41:12'),
(43, 'SI MIST201709130043', 1, 2, 3, 14, 1, NULL, 1, 499200.00, 'debit', 'srinivasa steel', NULL, NULL, 10, NULL, '2017-09-13', NULL, '2017-09-13 02:09:58', '2017-09-13 02:09:58'),
(44, 'SI MIST201709130044', 1, 2, 3, 14, 1, NULL, 2, 12000.00, 'debit', 'prakash', NULL, NULL, 6, NULL, '2017-09-13', NULL, '2017-09-13 02:19:15', '2017-09-13 02:19:15'),
(45, 'SI MIST201709140045', 1, 2, 3, 10, 1, NULL, 2, 100000.00, 'debit', 'madhu', NULL, NULL, 16, 'advance 37K', '2017-09-14', NULL, '2017-09-14 07:21:14', '2017-09-14 07:21:14'),
(46, 'SI MIST201709140046', 1, 2, 3, 10, 1, NULL, 2, 50000.00, 'debit', 'madhu', NULL, NULL, 16, '21K Advance', '2017-09-14', NULL, '2017-09-14 07:27:28', '2017-09-14 07:27:28'),
(47, 'SI MIST201709150047', 1, 2, 3, 10, 1, NULL, 1, 48028.00, 'debit', NULL, NULL, NULL, 7, NULL, '2017-09-15', NULL, '2017-09-15 02:46:00', '2017-09-15 02:46:00'),
(48, 'SI MIST201709180048', 1, 2, 3, 13, 3, '009', 3, 1000.00, 'debit', 'Bhoopal Reddy', NULL, NULL, 0, '10 Labour@250', '2017-09-18', NULL, '2017-09-18 01:51:51', '2017-09-18 01:51:51'),
(49, 'SI MIST201709180049', 1, 2, 3, 7, 1, '9987', 5, 2000000.00, 'credit', 'Venugopal reddy', NULL, '', 0, 'Amount cleared for vila 1', '2017-09-18', NULL, '2017-09-18 02:27:59', '2017-09-18 02:27:59'),
(50, 'SI MIST201709180050', 1, 2, 3, 13, 2, '99888', 5, 20000.00, 'credit', 'Sathish', NULL, '', 0, '#2 payment', '2017-09-18', NULL, '2017-09-18 02:39:14', '2017-09-18 02:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(12) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cheque', '2017-08-12 10:06:37', '2017-08-12 10:06:37'),
(2, 'NEFT', '2017-08-12 10:06:37', '2017-08-12 10:06:37'),
(3, 'Cash', '2017-08-24 12:28:36', '2017-08-24 12:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_types`
--

INSERT INTO `payment_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Material Payment', '2017-08-12 09:53:44', '2017-08-12 09:53:44'),
(2, 'Contractor Payment', '2017-08-12 09:53:44', '2017-08-12 09:53:44'),
(3, 'Service Payment', '2017-08-12 09:53:44', '2017-08-12 09:53:44'),
(4, 'External Payment', '2017-08-12 09:53:44', '2017-08-12 09:53:44'),
(5, 'Customer Payment', '2017-08-27 09:18:56', '2017-08-27 09:18:56'),
(6, 'External Income', '2017-09-11 09:13:46', '2017-09-11 09:13:46');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'access_panel', '2017-06-06 12:31:27', '2017-06-06 12:31:27'),
(3, 'access_admin', '2017-06-13 05:08:52', '2017-06-13 05:08:52'),
(4, 'access_customer', '2017-07-01 10:43:36', '2017-07-01 10:43:36');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `product_category_id` int(12) DEFAULT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_details` varchar(255) DEFAULT NULL,
  `sale_price` double(10,2) DEFAULT '0.00',
  `payment_id` int(12) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `org_id`, `project_id`, `product_category_id`, `product_name`, `product_details`, `sale_price`, `payment_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, NULL, 'VILLA 011', 'EAST', 0.00, NULL, '2017-09-08 08:07:18', '2017-09-08 08:07:38', NULL),
(2, 1, 1, NULL, 'VILLA 012', 'WEST', 0.00, NULL, '2017-09-08 08:08:08', '2017-09-08 08:08:08', NULL),
(3, 1, 2, 8, '#01', 'East', 2000000.00, 32, '2017-09-10 12:47:19', '2017-09-11 08:45:53', NULL),
(4, 1, 2, 8, '#02', 'East', 0.00, NULL, '2017-09-10 12:47:35', '2017-09-11 04:41:59', NULL),
(5, 1, 2, 8, '#03', 'East', 5000000.00, NULL, '2017-09-10 12:47:49', '2017-09-11 04:41:48', NULL),
(6, 1, 2, NULL, '#01', 'GF Brick Work', 0.00, NULL, '2017-09-10 13:58:46', '2017-09-10 13:59:01', '2017-09-10 13:59:01'),
(7, 1, 2, 9, '#04', 'WEST', 2000000.00, 33, '2017-09-11 00:49:22', '2017-09-11 08:51:42', NULL),
(8, 1, 2, 9, '#05', 'WEST', 0.00, NULL, '2017-09-11 00:49:40', '2017-09-11 04:42:32', NULL),
(9, 1, 2, 9, '#06', 'WEST', 0.00, NULL, '2017-09-11 00:49:57', '2017-09-11 04:42:29', NULL),
(10, 2, 3, 10, '#01', 'Villa #01', 2000000.00, 49, '2017-09-11 09:03:53', '2017-09-18 02:27:59', NULL),
(11, 2, 3, 10, '#02', 'Villa #02', 20000.00, 50, '2017-09-11 09:04:06', '2017-09-18 02:39:14', NULL),
(12, 2, 3, 10, '#03', 'Villa #03', 0.00, NULL, '2017-09-11 09:04:18', '2017-09-11 09:35:29', NULL),
(13, 2, 3, 10, '#04', 'Villa #04', 0.00, NULL, '2017-09-11 09:04:28', '2017-09-11 09:35:32', NULL),
(14, 2, 3, 11, '#05', 'Villa #05', 0.00, NULL, '2017-09-11 09:04:38', '2017-09-11 09:35:51', NULL),
(15, 2, 3, 11, '#06', 'Villa #06', 0.00, NULL, '2017-09-11 09:04:52', '2017-09-11 09:35:53', NULL),
(16, 2, 3, 11, '#07', 'Villa #07', 0.00, NULL, '2017-09-11 09:05:08', '2017-09-11 09:35:56', NULL),
(17, 2, 3, 12, '#08', 'Villa #08', 0.00, NULL, '2017-09-11 09:05:23', '2017-09-11 09:34:43', NULL),
(18, 2, 3, 12, '#09', 'Villa #09', 0.00, NULL, '2017-09-11 09:05:36', '2017-09-11 09:34:46', NULL),
(19, 2, 3, 12, '#10', 'Villa #10', 0.00, NULL, '2017-09-11 09:05:49', '2017-09-11 09:34:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `product_properties_header_id` int(12) DEFAULT NULL,
  `product_category_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `project_id`, `product_properties_header_id`, `product_category_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, NULL, '300Yards2500SqFt', '2017-09-10 01:48:15', '2017-09-10 01:48:15', NULL),
(2, 1, NULL, '500Yards3500SqFt', '2017-09-10 01:49:01', '2017-09-10 01:49:01', NULL),
(3, 1, NULL, '800Yards4500SqFt', '2017-09-10 01:49:17', '2017-09-10 01:49:17', NULL),
(8, 2, 6, 'EAST', '2017-09-11 02:42:15', '2017-09-11 02:42:56', NULL),
(10, 3, 8, 'PC_900_Yards', '2017-09-11 09:07:19', '2017-09-11 09:33:43', NULL),
(9, 2, 7, 'WEST', '2017-09-11 02:42:23', '2017-09-11 02:43:02', NULL),
(11, 3, 9, 'PC_450_Yards', '2017-09-11 09:09:32', '2017-09-11 09:33:52', NULL),
(12, 3, 10, 'PC_1200_Yards', '2017-09-11 09:23:31', '2017-09-11 09:33:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_customers`
--

CREATE TABLE `product_customers` (
  `id` int(12) NOT NULL,
  `product_id` int(12) NOT NULL,
  `customer_id` int(12) NOT NULL,
  `date` varchar(10) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_customers`
--

INSERT INTO `product_customers` (`id`, `product_id`, `customer_id`, `date`, `remark`, `payment_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 1, '2017-09-11', NULL, NULL, '2017-09-11 01:59:27', '2017-09-11 01:59:27', NULL),
(2, 7, 1, '2017-09-11', NULL, NULL, '2017-09-11 01:59:39', '2017-09-11 01:59:39', NULL),
(3, 5, 2, '2017-09-11', NULL, NULL, '2017-09-11 02:00:29', '2017-09-11 02:00:29', NULL),
(4, 10, 7, '2017-09-12', NULL, NULL, '2017-09-12 03:18:36', '2017-09-12 03:18:36', NULL),
(5, 11, 6, '2017-09-12', NULL, NULL, '2017-09-12 03:19:00', '2017-09-12 03:19:00', NULL),
(6, 12, 6, '2017-09-12', NULL, NULL, '2017-09-12 03:19:37', '2017-09-12 03:19:37', NULL),
(7, 13, 5, '2017-09-12', NULL, NULL, '2017-09-12 03:20:05', '2017-09-12 03:20:05', NULL),
(8, 14, 5, '2017-09-12', NULL, NULL, '2017-09-12 03:20:10', '2017-09-12 03:20:10', NULL),
(9, 15, 4, '2017-09-12', NULL, NULL, '2017-09-12 03:20:25', '2017-09-12 03:20:25', NULL),
(10, 16, 4, '2017-09-12', NULL, NULL, '2017-09-12 03:20:34', '2017-09-12 03:20:34', NULL),
(11, 17, 3, '2017-09-12', NULL, NULL, '2017-09-12 03:21:04', '2017-09-12 03:21:04', NULL),
(12, 18, 3, '2017-09-12', NULL, NULL, '2017-09-12 03:21:15', '2017-09-12 03:21:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_items`
--

CREATE TABLE `product_items` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `department_id` int(12) NOT NULL,
  `product_id` int(12) NOT NULL,
  `item_id` int(12) NOT NULL,
  `qty` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_items`
--

INSERT INTO `product_items` (`id`, `user_id`, `org_id`, `project_id`, `supplier_id`, `department_id`, `product_id`, `item_id`, `qty`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 4, 1, 3, 2, 100, '2017-09-11 03:53:14', '2017-09-11 03:53:14'),
(2, 1, 1, 2, 4, 1, 3, 1, 350, '2017-09-11 03:53:14', '2017-09-11 03:53:14'),
(3, 1, 1, 2, 2, 1, 3, 4, 1500, '2017-09-11 03:53:14', '2017-09-11 03:53:14'),
(4, 1, 1, 2, 4, 2, 4, 5, 10, '2017-09-11 05:01:14', '2017-09-11 05:01:14'),
(5, 1, 1, 2, 4, 1, 4, 2, 10, '2017-09-11 07:02:03', '2017-09-11 07:02:03'),
(6, 1, 1, 2, 4, 1, 7, 2, 9, '2017-09-11 09:51:15', '2017-09-11 09:51:15'),
(7, 1, 1, 2, 4, 1, 7, 1, 5, '2017-09-11 09:51:15', '2017-09-11 09:51:15'),
(8, 1, 2, 3, 8, 8, 11, 20, 5, '2017-09-15 05:03:27', '2017-09-15 05:03:27'),
(9, 1, 2, 3, 7, 8, 11, 9, 5, '2017-09-15 05:03:27', '2017-09-15 05:03:27'),
(10, 1, 2, 3, 7, 7, 10, 19, 25, '2017-09-18 04:52:37', '2017-09-18 04:52:37'),
(11, 1, 2, 3, 7, 7, 10, 17, 20, '2017-09-18 04:52:37', '2017-09-18 04:52:37'),
(12, 1, 2, 3, 7, 7, 10, 15, 20, '2017-09-18 04:52:37', '2017-09-18 04:52:37');

-- --------------------------------------------------------

--
-- Table structure for table `product_properties`
--

CREATE TABLE `product_properties` (
  `id` int(12) NOT NULL,
  `property_header_id` int(12) NOT NULL,
  `property_name` varchar(255) NOT NULL,
  `property_type` varchar(255) NOT NULL,
  `qty` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_properties`
--

INSERT INTO `product_properties` (`id`, `property_header_id`, `property_name`, `property_type`, `qty`, `unit`, `created_at`, `updated_at`) VALUES
(1, 2, 'SANITORY', 'GROHE', '6', 'NUMBER', '2017-09-08 10:49:45', '2017-09-08 10:49:45'),
(2, 2, 'CP FITTING', 'VIKING', '5', 'NUMBER', '2017-09-08 10:49:45', '2017-09-08 10:49:45'),
(3, 1, 'MAIN DOOR', 'TEAK', '5', 'NUMBER', '2017-09-08 10:50:33', '2017-09-08 10:50:33'),
(4, 1, 'WIRES', 'GM', '15', 'NUMBER', '2017-09-08 10:50:33', '2017-09-08 10:50:33'),
(6, 6, 'BED Rooms', 'Bed Rooms', '6', 'Nos', '2017-09-10 16:38:37', '2017-09-10 16:38:37'),
(7, 6, 'Bat Rooms', 'Bath Rooms', '7', 'Nos', '2017-09-10 16:38:37', '2017-09-10 16:38:37'),
(8, 6, 'Kitchen', 'Kitchen', '1', 'No', '2017-09-10 16:38:37', '2017-09-10 16:38:37'),
(9, 7, 'Main Door', 'Teak', '5', 'Number', '2017-09-11 03:25:17', '2017-09-11 03:25:17'),
(10, 7, 'Bed Room', 'Teak', '6', 'Number', '2017-09-11 03:25:17', '2017-09-11 03:25:17'),
(11, 7, 'Windows', 'Teak', '6', 'Number', '2017-09-11 03:25:17', '2017-09-11 03:25:17'),
(17, 8, 'Pty_BuildUpArea_5400', 'Area', '5400', 'SFT', '2017-09-11 12:00:50', '2017-09-11 12:00:50'),
(14, 9, 'Pty_BuildUpArea_4400', 'Area', '4400', 'SFT', '2017-09-11 11:58:52', '2017-09-11 11:58:52'),
(15, 9, 'Pty_FalseCeilingArea_3500', 'Area', '3500', 'SFT', '2017-09-11 11:58:52', '2017-09-11 11:58:52'),
(16, 9, 'Pty_TilesArea_4500', 'Area', '4500', 'SFT', '2017-09-11 11:58:52', '2017-09-11 11:58:52'),
(18, 8, 'Pty_TilesArea', 'Area', '5600', 'SFT', '2017-09-11 12:00:50', '2017-09-11 12:00:50'),
(19, 8, 'Pty_FalseCeilingArea_4800', 'Area', '4800', 'SFT', '2017-09-11 12:00:50', '2017-09-11 12:00:50'),
(20, 10, 'Pty_BuildUpArea_5600', 'Area', '5400', 'SFT', '2017-09-11 12:02:47', '2017-09-11 12:02:47'),
(21, 10, 'Pty_TilesArea_5800', 'Area', '5800', 'SFT', '2017-09-11 12:02:47', '2017-09-11 12:02:47'),
(22, 10, 'Pty_FalseCeilingArea_5200', 'Area', '5200', 'SFT', '2017-09-11 12:02:47', '2017-09-11 12:02:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_property_headers`
--

CREATE TABLE `product_property_headers` (
  `id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_property_headers`
--

INSERT INTO `product_property_headers` (`id`, `project_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'EAST P HEADER', 'EAST FACE VILLA', '2017-09-08 08:15:49', '2017-09-08 08:16:05', NULL),
(2, 1, 'WEST P HEADER', 'WEST FACING VILLA', '2017-09-08 08:16:58', '2017-09-08 08:16:58', NULL),
(3, 1, 'PH_300Yards2500SFT', '300 Yards 2500 SFT PH', '2017-09-10 01:50:49', '2017-09-10 01:50:49', NULL),
(4, 1, 'PH_500Yards3500SFT', '500 Yards 3500 SFT PH', '2017-09-10 01:51:34', '2017-09-10 01:51:34', NULL),
(5, 1, 'PH_800Yards4500SFT', '800 Yards 4500 SFT PH', '2017-09-10 01:52:07', '2017-09-10 01:52:07', NULL),
(6, 2, '700 yards', 'East', '2017-09-10 12:53:54', '2017-09-10 12:53:54', NULL),
(7, 2, '600 Yards', 'West', '2017-09-11 00:51:31', '2017-09-11 00:51:31', NULL),
(8, 3, 'PH_900_Yards', '900 Yards Villas', '2017-09-11 09:10:19', '2017-09-11 09:10:19', NULL),
(9, 3, 'PH_450_Yards', '450 Yards Villas', '2017-09-11 09:10:46', '2017-09-11 09:10:46', NULL),
(10, 3, 'PH_1200_Yards', '1200 Yards Villas', '2017-09-11 09:24:17', '2017-09-11 09:24:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_stages`
--

CREATE TABLE `product_stages` (
  `id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `stage_id` int(11) NOT NULL,
  `contractor_id` int(11) NOT NULL,
  `department_id` int(12) NOT NULL,
  `rate` decimal(10,2) DEFAULT NULL,
  `date` varchar(10) NOT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `payment_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stages`
--

INSERT INTO `product_stages` (`id`, `org_id`, `project_id`, `product_id`, `stage_id`, `contractor_id`, `department_id`, `rate`, `date`, `remark`, `payment_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 3, 6, 4, 1, '20000.00', '2017-09-11', NULL, NULL, '2017-09-11 06:41:52', '2017-09-11 06:41:52'),
(2, 1, 2, 3, 4, 1, 3, '50000.00', '2017-09-11', NULL, 23, '2017-09-11 06:42:29', '2017-09-11 06:43:41'),
(3, 1, 2, 3, 1, 4, 1, '40000.00', '2017-09-11', NULL, NULL, '2017-09-11 06:42:47', '2017-09-11 06:42:47'),
(4, 1, 2, 3, 4, 1, 3, '50000.00', '2017-09-11', NULL, 24, '2017-09-11 06:43:12', '2017-09-11 06:44:20'),
(5, 1, 2, 5, 9, 2, 2, '20000.00', '2017-09-11', NULL, 25, '2017-09-11 06:48:15', '2017-09-11 06:52:01'),
(6, 1, 2, 5, 10, 2, 2, '25000.00', '2017-09-11', NULL, 25, '2017-09-11 06:50:47', '2017-09-11 06:52:01'),
(7, 2, 3, 10, 11, 6, 7, '12000.00', '2017-09-13', NULL, 42, '2017-09-13 01:38:28', '2017-09-13 01:41:12'),
(8, 2, 3, 10, 11, 6, 7, '12000.00', '2017-09-13', NULL, 44, '2017-09-13 02:17:49', '2017-09-13 02:19:15'),
(9, 2, 3, 11, 32, 16, 12, '30000.00', '2017-09-14', NULL, 45, '2017-09-14 07:05:27', '2017-09-14 07:21:14'),
(10, 2, 3, 12, 33, 16, 12, '33000.00', '2017-09-14', NULL, 45, '2017-09-14 07:06:25', '2017-09-14 07:21:14'),
(11, 2, 3, 12, 23, 8, 8, '50000.00', '2017-09-14', NULL, NULL, '2017-09-14 07:06:55', '2017-09-14 07:06:55'),
(12, 2, 3, 13, 33, 16, 12, '33000.00', '2017-09-14', NULL, 46, '2017-09-14 07:23:13', '2017-09-14 07:27:28'),
(13, 2, 3, 13, 33, 16, 12, '33000.00', '2017-09-14', NULL, 46, '2017-09-14 07:23:35', '2017-09-14 07:27:28'),
(14, 2, 3, 13, 32, 16, 12, '30000.00', '2017-09-14', NULL, NULL, '2017-09-14 07:24:13', '2017-09-14 07:24:13'),
(15, 2, 3, 10, 11, 6, 7, '12000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:23:24', '2017-09-18 02:23:24'),
(16, 2, 3, 10, 23, 8, 8, '50000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:23:51', '2017-09-18 02:23:51'),
(17, 2, 3, 10, 12, 6, 7, '10000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:24:32', '2017-09-18 02:24:32'),
(18, 2, 3, 10, 13, 6, 7, '12000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:24:49', '2017-09-18 02:24:49'),
(19, 2, 3, 10, 14, 6, 7, '20000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:25:17', '2017-09-18 02:25:17'),
(20, 2, 3, 10, 24, 6, 7, '60000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:25:45', '2017-09-18 02:25:45'),
(21, 2, 3, 10, 29, 15, 11, '200000.00', '2017-09-18', NULL, NULL, '2017-09-18 02:31:26', '2017-09-18 02:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `proj_code` varchar(255) NOT NULL,
  `proj_name` varchar(255) NOT NULL,
  `address_one` varchar(255) DEFAULT NULL,
  `address_two` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` int(6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `user_id`, `org_id`, `proj_code`, `proj_name`, `address_one`, `address_two`, `city`, `district`, `state`, `pincode`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'MIST', 'Mist', 'Mokila Village', 'Shankar pally,mist', 'Ranga Reddy', 'Ranga Reddy', 'Telangana', 501203, '2017-09-08 08:02:01', '2017-09-08 08:03:36', NULL),
(2, 1, 1, 'SIG4', 'Signature Homes', 'Mokila,Shankarpally (M), RR Dist', 'Mokila,Shankarpally Mandal RR Dist', 'Hyderabad', 'Rangareddy', 'Telangana', 501203, '2017-09-09 23:39:04', '2017-09-12 03:08:12', NULL),
(3, 1, 2, 'SI MIST', 'Subishi\'s MIST Luxury Homes', 'Mokila', 'Shankar Pally', 'Shankarpally', 'Ranga Reddy', 'Telangana', 501203, '2017-09-11 09:03:06', '2017-09-11 09:03:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_managers`
--

CREATE TABLE `project_managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_types`
--

CREATE TABLE `report_types` (
  `id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_types`
--

INSERT INTO `report_types` (`id`, `name`) VALUES
(1, 'Customer Details');

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE `revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `revisions`
--

INSERT INTO `revisions` (`id`, `revisionable_type`, `revisionable_id`, `user_id`, `key`, `old_value`, `new_value`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', 1, NULL, 'created_at', NULL, '2017-09-08 15:10:48', '2017-09-08 07:10:48', '2017-09-08 07:10:48'),
(2, 'App\\User', 1, 1, 'remember_token', NULL, 'GblMauo8H0EjpxVo4LvhqwV3qi6zXjaO0ocDvxemI9wKUArgRcuKxQBf5qlf', '2017-09-08 07:15:15', '2017-09-08 07:15:15'),
(3, 'App\\Organization', 1, 1, 'created_at', NULL, '2017-09-08 16:00:37', '2017-09-08 08:00:37', '2017-09-08 08:00:37'),
(4, 'App\\Organization', 1, 1, 'logo', NULL, 'org-logos/EyoLzakQS4fCnq5kdjUXxLb9N9zoKZc2UO4wyly1.jpeg', '2017-09-08 08:00:37', '2017-09-08 08:00:37'),
(5, 'App\\Project', 1, 1, 'created_at', NULL, '2017-09-08 16:02:01', '2017-09-08 08:02:01', '2017-09-08 08:02:01'),
(6, 'App\\Project', 1, 1, 'address_two', 'Shankar pally', 'Shankar pally,mist', '2017-09-08 08:03:36', '2017-09-08 08:03:36'),
(7, 'App\\Department', 1, 1, 'created_at', NULL, '2017-09-08 16:06:20', '2017-09-08 08:06:20', '2017-09-08 08:06:20'),
(8, 'App\\DepartmentContact', 1, 1, 'created_at', NULL, '2017-09-08 16:06:20', '2017-09-08 08:06:20', '2017-09-08 08:06:20'),
(9, 'App\\Department', 1, 1, 'code', '001', '0012', '2017-09-08 08:06:41', '2017-09-08 08:06:41'),
(10, 'App\\Product', 1, 1, 'created_at', NULL, '2017-09-08 16:07:18', '2017-09-08 08:07:18', '2017-09-08 08:07:18'),
(11, 'App\\Product', 1, 1, 'product_name', 'VILLA 01', 'VILLA 011', '2017-09-08 08:07:38', '2017-09-08 08:07:38'),
(12, 'App\\Product', 2, 1, 'created_at', NULL, '2017-09-08 16:08:08', '2017-09-08 08:08:08', '2017-09-08 08:08:08'),
(13, 'App\\Supplier', 1, 1, 'created_at', NULL, '2017-09-08 16:11:35', '2017-09-08 08:11:35', '2017-09-08 08:11:35'),
(14, 'App\\Supplier', 1, 1, 'meta_bank_acc', '7900976555', '7900976888', '2017-09-08 08:12:02', '2017-09-08 08:12:02'),
(15, 'App\\Supplier', 1, 1, 'phone', '977666767', '97766676789', '2017-09-08 08:12:28', '2017-09-08 08:12:28'),
(16, 'App\\ProductPropertyHeader', 1, 1, 'created_at', NULL, '2017-09-08 16:15:49', '2017-09-08 08:15:49', '2017-09-08 08:15:49'),
(17, 'App\\ProductPropertyHeader', 1, 1, 'description', 'EAST FACE', 'EAST FACE VILLA', '2017-09-08 08:16:05', '2017-09-08 08:16:05'),
(18, 'App\\ProductPropertyHeader', 2, 1, 'created_at', NULL, '2017-09-08 16:16:58', '2017-09-08 08:16:58', '2017-09-08 08:16:58'),
(19, 'App\\User', 1, 1, 'remember_token', 'GblMauo8H0EjpxVo4LvhqwV3qi6zXjaO0ocDvxemI9wKUArgRcuKxQBf5qlf', 'd41lM79jQKye9rep8HHGWgxAIMnrbVhUetubHM1fJRrvNgr1srSrFfdcok88', '2017-09-08 09:07:25', '2017-09-08 09:07:25'),
(20, 'App\\Organization', 1, 1, 'logo', 'org-logos/EyoLzakQS4fCnq5kdjUXxLb9N9zoKZc2UO4wyly1.jpeg', 'org-logos/hlWRNkkfVFfuJ8xjNxiDnR8lcR5tUVziCZp5ZnN8.png', '2017-09-09 05:40:22', '2017-09-09 05:40:22'),
(21, 'App\\Organization', 1, 1, 'logo', 'org-logos/hlWRNkkfVFfuJ8xjNxiDnR8lcR5tUVziCZp5ZnN8.png', 'org-logos/bqThSGkJ9SEnkUStuGbgDF8BQf8BuhfcJTCi55Td.png', '2017-09-09 05:42:55', '2017-09-09 05:42:55'),
(22, 'App\\Organization', 1, 1, 'logo', 'org-logos/bqThSGkJ9SEnkUStuGbgDF8BQf8BuhfcJTCi55Td.png', 'org-logos/nX4LLBF6HDlOaXS52AQBhpSXhWkr2arobEi98emp.png', '2017-09-09 06:01:09', '2017-09-09 06:01:09'),
(23, 'App\\User', 1, 1, 'remember_token', 'd41lM79jQKye9rep8HHGWgxAIMnrbVhUetubHM1fJRrvNgr1srSrFfdcok88', '6mAEsoG6aa1Ep7yUKago1KBg95ywHkoA0CBTN8L7IvPoaGXKVfuRUb9D60mv', '2017-09-09 07:54:51', '2017-09-09 07:54:51'),
(24, 'App\\Project', 2, 1, 'created_at', NULL, '2017-09-10 07:39:04', '2017-09-09 23:39:04', '2017-09-09 23:39:04'),
(25, 'App\\Department', 2, 1, 'created_at', NULL, '2017-09-10 09:35:04', '2017-09-10 01:35:04', '2017-09-10 01:35:04'),
(26, 'App\\DepartmentContact', 2, 1, 'created_at', NULL, '2017-09-10 09:35:04', '2017-09-10 01:35:04', '2017-09-10 01:35:04'),
(27, 'App\\Department', 2, 1, 'code', 'ELECTRICALDEPT', '0003', '2017-09-10 01:35:32', '2017-09-10 01:35:32'),
(28, 'App\\Supplier', 2, 1, 'created_at', NULL, '2017-09-10 09:44:40', '2017-09-10 01:44:40', '2017-09-10 01:44:40'),
(29, 'App\\Supplier', 3, 1, 'created_at', NULL, '2017-09-10 09:47:13', '2017-09-10 01:47:13', '2017-09-10 01:47:13'),
(30, 'App\\ProductCategory', 1, 1, 'created_at', NULL, '2017-09-10 09:48:15', '2017-09-10 01:48:15', '2017-09-10 01:48:15'),
(31, 'App\\ProductCategory', 2, 1, 'created_at', NULL, '2017-09-10 09:49:01', '2017-09-10 01:49:01', '2017-09-10 01:49:01'),
(32, 'App\\ProductCategory', 3, 1, 'created_at', NULL, '2017-09-10 09:49:17', '2017-09-10 01:49:17', '2017-09-10 01:49:17'),
(33, 'App\\ProductPropertyHeader', 3, 1, 'created_at', NULL, '2017-09-10 09:50:49', '2017-09-10 01:50:49', '2017-09-10 01:50:49'),
(34, 'App\\ProductPropertyHeader', 4, 1, 'created_at', NULL, '2017-09-10 09:51:34', '2017-09-10 01:51:34', '2017-09-10 01:51:34'),
(35, 'App\\ProductPropertyHeader', 5, 1, 'created_at', NULL, '2017-09-10 09:52:07', '2017-09-10 01:52:07', '2017-09-10 01:52:07'),
(36, 'App\\Supplier', 4, 1, 'created_at', NULL, '2017-09-10 20:20:45', '2017-09-10 12:20:45', '2017-09-10 12:20:45'),
(37, 'App\\Supplier', 5, 1, 'created_at', NULL, '2017-09-10 20:22:08', '2017-09-10 12:22:08', '2017-09-10 12:22:08'),
(38, 'App\\Department', 3, 1, 'created_at', NULL, '2017-09-10 20:24:05', '2017-09-10 12:24:05', '2017-09-10 12:24:05'),
(39, 'App\\DepartmentContact', 3, 1, 'created_at', NULL, '2017-09-10 20:24:05', '2017-09-10 12:24:05', '2017-09-10 12:24:05'),
(40, 'App\\Department', 4, 1, 'created_at', NULL, '2017-09-10 20:32:45', '2017-09-10 12:32:45', '2017-09-10 12:32:45'),
(41, 'App\\DepartmentContact', 4, 1, 'created_at', NULL, '2017-09-10 20:32:45', '2017-09-10 12:32:45', '2017-09-10 12:32:45'),
(42, 'App\\Contractor', 1, 1, 'created_at', NULL, '2017-09-10 20:33:56', '2017-09-10 12:33:56', '2017-09-10 12:33:56'),
(43, 'App\\Contractor', 2, 1, 'created_at', NULL, '2017-09-10 20:34:46', '2017-09-10 12:34:46', '2017-09-10 12:34:46'),
(44, 'App\\Contractor', 3, 1, 'created_at', NULL, '2017-09-10 20:35:37', '2017-09-10 12:35:37', '2017-09-10 12:35:37'),
(45, 'App\\Staff', 1, 1, 'created_at', NULL, '2017-09-10 20:39:00', '2017-09-10 12:39:00', '2017-09-10 12:39:00'),
(46, 'App\\Staff', 1, 1, 'code', NULL, 'SUBISHI-S1', '2017-09-10 12:39:00', '2017-09-10 12:39:00'),
(47, 'App\\Staff', 2, 1, 'created_at', NULL, '2017-09-10 20:39:00', '2017-09-10 12:39:00', '2017-09-10 12:39:00'),
(48, 'App\\Staff', 2, 1, 'code', NULL, 'SUBISHI-S2', '2017-09-10 12:39:00', '2017-09-10 12:39:00'),
(49, 'App\\Staff', 3, 1, 'created_at', NULL, '2017-09-10 20:39:41', '2017-09-10 12:39:41', '2017-09-10 12:39:41'),
(50, 'App\\Staff', 3, 1, 'code', NULL, 'SUBISHI-S3', '2017-09-10 12:39:41', '2017-09-10 12:39:41'),
(51, 'App\\Product', 3, 1, 'created_at', NULL, '2017-09-10 20:47:19', '2017-09-10 12:47:19', '2017-09-10 12:47:19'),
(52, 'App\\Product', 4, 1, 'created_at', NULL, '2017-09-10 20:47:35', '2017-09-10 12:47:35', '2017-09-10 12:47:35'),
(53, 'App\\Product', 5, 1, 'created_at', NULL, '2017-09-10 20:47:49', '2017-09-10 12:47:49', '2017-09-10 12:47:49'),
(54, 'App\\ProductCategory', 4, 1, 'created_at', NULL, '2017-09-10 20:50:07', '2017-09-10 12:50:07', '2017-09-10 12:50:07'),
(55, 'App\\ProductCategory', 5, 1, 'created_at', NULL, '2017-09-10 20:52:10', '2017-09-10 12:52:10', '2017-09-10 12:52:10'),
(56, 'App\\ProductPropertyHeader', 6, 1, 'created_at', NULL, '2017-09-10 20:53:54', '2017-09-10 12:53:54', '2017-09-10 12:53:54'),
(57, 'App\\ProductCategory', 6, 1, 'created_at', NULL, '2017-09-10 20:55:29', '2017-09-10 12:55:29', '2017-09-10 12:55:29'),
(58, 'App\\ProductCategory', 6, 1, 'product_category_name', '7', '700Yard', '2017-09-10 12:55:48', '2017-09-10 12:55:48'),
(59, 'App\\ProductCategory', 6, 1, 'product_properties_header_id', NULL, '6', '2017-09-10 12:56:58', '2017-09-10 12:56:58'),
(60, 'App\\Department', 5, 1, 'created_at', NULL, '2017-09-10 21:04:01', '2017-09-10 13:04:01', '2017-09-10 13:04:01'),
(61, 'App\\DepartmentContact', 5, 1, 'created_at', NULL, '2017-09-10 21:04:01', '2017-09-10 13:04:01', '2017-09-10 13:04:01'),
(62, 'App\\Item', 1, 1, 'created_at', NULL, '2017-09-10 21:25:32', '2017-09-10 13:25:32', '2017-09-10 13:25:32'),
(63, 'App\\Item', 2, 1, 'created_at', NULL, '2017-09-10 21:28:23', '2017-09-10 13:28:23', '2017-09-10 13:28:23'),
(64, 'App\\Item', 3, 1, 'created_at', NULL, '2017-09-10 21:30:26', '2017-09-10 13:30:26', '2017-09-10 13:30:26'),
(65, 'App\\Item', 4, 1, 'created_at', NULL, '2017-09-10 21:32:16', '2017-09-10 13:32:16', '2017-09-10 13:32:16'),
(66, 'App\\Order', 1, 1, 'created_at', NULL, '2017-09-10 21:35:07', '2017-09-10 13:35:07', '2017-09-10 13:35:07'),
(67, 'App\\Order', 1, 1, 'code', NULL, 'SUBISHI-SIG4-0001', '2017-09-10 13:35:07', '2017-09-10 13:35:07'),
(68, 'App\\Order', 1, 1, 'invoice_total', NULL, '32110', '2017-09-10 13:35:07', '2017-09-10 13:35:07'),
(69, 'App\\Order', 2, 1, 'created_at', NULL, '2017-09-10 21:36:32', '2017-09-10 13:36:32', '2017-09-10 13:36:32'),
(70, 'App\\Order', 2, 1, 'code', NULL, 'SUBISHI-SIG4-0002', '2017-09-10 13:36:32', '2017-09-10 13:36:32'),
(71, 'App\\Order', 2, 1, 'invoice_total', NULL, '480000', '2017-09-10 13:36:32', '2017-09-10 13:36:32'),
(72, 'App\\Contractor', 4, 1, 'created_at', NULL, '2017-09-10 21:41:49', '2017-09-10 13:41:49', '2017-09-10 13:41:49'),
(73, 'App\\Product', 6, 1, 'created_at', NULL, '2017-09-10 21:58:46', '2017-09-10 13:58:46', '2017-09-10 13:58:46'),
(74, 'App\\Product', 6, 1, 'deleted_at', NULL, '2017-09-10 21:59:01', '2017-09-10 13:59:01', '2017-09-10 13:59:01'),
(75, 'App\\ProductCategory', 7, 1, 'created_at', NULL, '2017-09-10 22:09:23', '2017-09-10 14:09:23', '2017-09-10 14:09:23'),
(76, 'App\\Product', 7, 1, 'created_at', NULL, '2017-09-11 08:49:22', '2017-09-11 00:49:22', '2017-09-11 00:49:22'),
(77, 'App\\Product', 8, 1, 'created_at', NULL, '2017-09-11 08:49:40', '2017-09-11 00:49:40', '2017-09-11 00:49:40'),
(78, 'App\\Product', 9, 1, 'created_at', NULL, '2017-09-11 08:49:57', '2017-09-11 00:49:57', '2017-09-11 00:49:57'),
(79, 'App\\ProductPropertyHeader', 7, 1, 'created_at', NULL, '2017-09-11 08:51:31', '2017-09-11 00:51:31', '2017-09-11 00:51:31'),
(80, 'App\\Item', 5, 1, 'created_at', NULL, '2017-09-11 09:37:43', '2017-09-11 01:37:43', '2017-09-11 01:37:43'),
(81, 'App\\User', 2, 1, 'created_at', NULL, '2017-09-11 09:58:01', '2017-09-11 01:58:01', '2017-09-11 01:58:01'),
(82, 'App\\Customer', 1, 1, 'created_at', NULL, '2017-09-11 09:58:01', '2017-09-11 01:58:01', '2017-09-11 01:58:01'),
(83, 'App\\User', 3, 1, 'created_at', NULL, '2017-09-11 09:58:39', '2017-09-11 01:58:39', '2017-09-11 01:58:39'),
(84, 'App\\Customer', 2, 1, 'created_at', NULL, '2017-09-11 09:58:39', '2017-09-11 01:58:39', '2017-09-11 01:58:39'),
(85, 'App\\ProductCustomer', 1, 1, 'created_at', NULL, '2017-09-11 09:59:27', '2017-09-11 01:59:27', '2017-09-11 01:59:27'),
(86, 'App\\ProductCustomer', 2, 1, 'created_at', NULL, '2017-09-11 09:59:39', '2017-09-11 01:59:39', '2017-09-11 01:59:39'),
(87, 'App\\ProductCustomer', 3, 1, 'created_at', NULL, '2017-09-11 10:00:29', '2017-09-11 02:00:29', '2017-09-11 02:00:29'),
(88, 'App\\Order', 3, 1, 'created_at', NULL, '2017-09-11 10:12:59', '2017-09-11 02:12:59', '2017-09-11 02:12:59'),
(89, 'App\\Order', 3, 1, 'code', NULL, 'SUBISHI-SIG4-0003', '2017-09-11 02:12:59', '2017-09-11 02:12:59'),
(90, 'App\\Order', 3, 1, 'invoice_total', NULL, '199330', '2017-09-11 02:12:59', '2017-09-11 02:12:59'),
(91, 'App\\Department', 6, 1, 'created_at', NULL, '2017-09-11 10:38:32', '2017-09-11 02:38:32', '2017-09-11 02:38:32'),
(92, 'App\\DepartmentContact', 6, 1, 'created_at', NULL, '2017-09-11 10:38:32', '2017-09-11 02:38:32', '2017-09-11 02:38:32'),
(93, 'App\\ProductCategory', 8, 1, 'created_at', NULL, '2017-09-11 10:42:15', '2017-09-11 02:42:15', '2017-09-11 02:42:15'),
(94, 'App\\ProductCategory', 9, 1, 'created_at', NULL, '2017-09-11 10:42:23', '2017-09-11 02:42:23', '2017-09-11 02:42:23'),
(95, 'App\\ProductCategory', 8, 1, 'product_properties_header_id', NULL, '7', '2017-09-11 02:42:37', '2017-09-11 02:42:37'),
(96, 'App\\ProductCategory', 8, 1, 'product_properties_header_id', '7', '6', '2017-09-11 02:42:56', '2017-09-11 02:42:56'),
(97, 'App\\ProductCategory', 9, 1, 'product_properties_header_id', NULL, '7', '2017-09-11 02:43:02', '2017-09-11 02:43:02'),
(98, 'App\\Product', 3, 1, 'product_category_id', NULL, '8', '2017-09-11 04:41:45', '2017-09-11 04:41:45'),
(99, 'App\\Product', 5, 1, 'product_category_id', NULL, '8', '2017-09-11 04:41:48', '2017-09-11 04:41:48'),
(100, 'App\\Product', 4, 1, 'product_category_id', NULL, '8', '2017-09-11 04:41:59', '2017-09-11 04:41:59'),
(101, 'App\\Product', 7, 1, 'product_category_id', NULL, '9', '2017-09-11 04:42:27', '2017-09-11 04:42:27'),
(102, 'App\\Product', 9, 1, 'product_category_id', NULL, '9', '2017-09-11 04:42:29', '2017-09-11 04:42:29'),
(103, 'App\\Product', 8, 1, 'product_category_id', NULL, '9', '2017-09-11 04:42:32', '2017-09-11 04:42:32'),
(104, 'App\\Supplier', 6, 1, 'created_at', NULL, '2017-09-11 12:57:30', '2017-09-11 04:57:30', '2017-09-11 04:57:30'),
(105, 'App\\Contractor', 5, 1, 'created_at', NULL, '2017-09-11 14:07:49', '2017-09-11 06:07:49', '2017-09-11 06:07:49'),
(106, 'App\\User', 1, 1, 'remember_token', '6mAEsoG6aa1Ep7yUKago1KBg95ywHkoA0CBTN8L7IvPoaGXKVfuRUb9D60mv', 'oVa6OLF2UKkYxAJv6FJLlxOZzoEeMkifJe3RgYSi0umCguxC05gndym7ZFbs', '2017-09-11 06:12:18', '2017-09-11 06:12:18'),
(107, 'App\\Item', 5, 1, 'unit', '200', 'Nos', '2017-09-11 07:19:54', '2017-09-11 07:19:54'),
(108, 'App\\Item', 6, 1, 'created_at', NULL, '2017-09-11 15:24:04', '2017-09-11 07:24:04', '2017-09-11 07:24:04'),
(109, 'App\\Item', 7, 1, 'created_at', NULL, '2017-09-11 15:25:12', '2017-09-11 07:25:12', '2017-09-11 07:25:12'),
(110, 'App\\Organization', 2, 1, 'created_at', NULL, '2017-09-11 17:01:05', '2017-09-11 09:01:05', '2017-09-11 09:01:05'),
(111, 'App\\Organization', 2, 1, 'logo', NULL, 'org-logos/NHuS4QWSRp8PRVm6qqllwTBE26In3ghXLEvVxCWR.png', '2017-09-11 09:01:05', '2017-09-11 09:01:05'),
(112, 'App\\Project', 3, 1, 'created_at', NULL, '2017-09-11 17:03:06', '2017-09-11 09:03:06', '2017-09-11 09:03:06'),
(113, 'App\\Product', 10, 1, 'created_at', NULL, '2017-09-11 17:03:53', '2017-09-11 09:03:53', '2017-09-11 09:03:53'),
(114, 'App\\Product', 11, 1, 'created_at', NULL, '2017-09-11 17:04:06', '2017-09-11 09:04:06', '2017-09-11 09:04:06'),
(115, 'App\\Product', 12, 1, 'created_at', NULL, '2017-09-11 17:04:18', '2017-09-11 09:04:18', '2017-09-11 09:04:18'),
(116, 'App\\Product', 13, 1, 'created_at', NULL, '2017-09-11 17:04:28', '2017-09-11 09:04:28', '2017-09-11 09:04:28'),
(117, 'App\\Product', 14, 1, 'created_at', NULL, '2017-09-11 17:04:38', '2017-09-11 09:04:38', '2017-09-11 09:04:38'),
(118, 'App\\Product', 15, 1, 'created_at', NULL, '2017-09-11 17:04:52', '2017-09-11 09:04:52', '2017-09-11 09:04:52'),
(119, 'App\\Product', 16, 1, 'created_at', NULL, '2017-09-11 17:05:08', '2017-09-11 09:05:08', '2017-09-11 09:05:08'),
(120, 'App\\Product', 17, 1, 'created_at', NULL, '2017-09-11 17:05:23', '2017-09-11 09:05:23', '2017-09-11 09:05:23'),
(121, 'App\\Product', 18, 1, 'created_at', NULL, '2017-09-11 17:05:36', '2017-09-11 09:05:36', '2017-09-11 09:05:36'),
(122, 'App\\Product', 19, 1, 'created_at', NULL, '2017-09-11 17:05:49', '2017-09-11 09:05:49', '2017-09-11 09:05:49'),
(123, 'App\\Product', 10, 1, 'product_details', 'Villa #01', 'Villa #001', '2017-09-11 09:06:09', '2017-09-11 09:06:09'),
(124, 'App\\Product', 10, 1, 'product_details', 'Villa #001', 'Villa #01', '2017-09-11 09:06:21', '2017-09-11 09:06:21'),
(125, 'App\\ProductCategory', 10, 1, 'created_at', NULL, '2017-09-11 17:07:19', '2017-09-11 09:07:19', '2017-09-11 09:07:19'),
(126, 'App\\ProductCategory', 11, 1, 'created_at', NULL, '2017-09-11 17:09:32', '2017-09-11 09:09:32', '2017-09-11 09:09:32'),
(127, 'App\\ProductPropertyHeader', 8, 1, 'created_at', NULL, '2017-09-11 17:10:19', '2017-09-11 09:10:19', '2017-09-11 09:10:19'),
(128, 'App\\ProductPropertyHeader', 9, 1, 'created_at', NULL, '2017-09-11 17:10:46', '2017-09-11 09:10:46', '2017-09-11 09:10:46'),
(129, 'App\\ProductCategory', 12, 1, 'created_at', NULL, '2017-09-11 17:23:31', '2017-09-11 09:23:31', '2017-09-11 09:23:31'),
(130, 'App\\ProductPropertyHeader', 10, 1, 'created_at', NULL, '2017-09-11 17:24:17', '2017-09-11 09:24:17', '2017-09-11 09:24:17'),
(131, 'App\\ProductCategory', 12, 1, 'product_properties_header_id', NULL, '10', '2017-09-11 09:33:31', '2017-09-11 09:33:31'),
(132, 'App\\ProductCategory', 10, 1, 'product_properties_header_id', NULL, '8', '2017-09-11 09:33:43', '2017-09-11 09:33:43'),
(133, 'App\\ProductCategory', 11, 1, 'product_properties_header_id', NULL, '9', '2017-09-11 09:33:52', '2017-09-11 09:33:52'),
(134, 'App\\Product', 17, 1, 'product_category_id', NULL, '12', '2017-09-11 09:34:43', '2017-09-11 09:34:43'),
(135, 'App\\Product', 18, 1, 'product_category_id', NULL, '12', '2017-09-11 09:34:46', '2017-09-11 09:34:46'),
(136, 'App\\Product', 19, 1, 'product_category_id', NULL, '12', '2017-09-11 09:34:48', '2017-09-11 09:34:48'),
(137, 'App\\Product', 10, 1, 'product_category_id', NULL, '10', '2017-09-11 09:35:23', '2017-09-11 09:35:23'),
(138, 'App\\Product', 11, 1, 'product_category_id', NULL, '10', '2017-09-11 09:35:27', '2017-09-11 09:35:27'),
(139, 'App\\Product', 12, 1, 'product_category_id', NULL, '10', '2017-09-11 09:35:29', '2017-09-11 09:35:29'),
(140, 'App\\Product', 13, 1, 'product_category_id', NULL, '10', '2017-09-11 09:35:32', '2017-09-11 09:35:32'),
(141, 'App\\Product', 14, 1, 'product_category_id', NULL, '11', '2017-09-11 09:35:51', '2017-09-11 09:35:51'),
(142, 'App\\Product', 15, 1, 'product_category_id', NULL, '11', '2017-09-11 09:35:53', '2017-09-11 09:35:53'),
(143, 'App\\Product', 16, 1, 'product_category_id', NULL, '11', '2017-09-11 09:35:56', '2017-09-11 09:35:56'),
(144, 'App\\Department', 7, 1, 'created_at', NULL, '2017-09-11 17:39:25', '2017-09-11 09:39:25', '2017-09-11 09:39:25'),
(145, 'App\\DepartmentContact', 7, 1, 'created_at', NULL, '2017-09-11 17:39:25', '2017-09-11 09:39:25', '2017-09-11 09:39:25'),
(146, 'App\\Department', 8, 1, 'created_at', NULL, '2017-09-11 17:40:25', '2017-09-11 09:40:25', '2017-09-11 09:40:25'),
(147, 'App\\DepartmentContact', 8, 1, 'created_at', NULL, '2017-09-11 17:40:25', '2017-09-11 09:40:25', '2017-09-11 09:40:25'),
(148, 'App\\Department', 9, 1, 'created_at', NULL, '2017-09-11 17:41:12', '2017-09-11 09:41:12', '2017-09-11 09:41:12'),
(149, 'App\\DepartmentContact', 9, 1, 'created_at', NULL, '2017-09-11 17:41:12', '2017-09-11 09:41:12', '2017-09-11 09:41:12'),
(150, 'App\\Department', 10, 1, 'created_at', NULL, '2017-09-11 17:42:25', '2017-09-11 09:42:25', '2017-09-11 09:42:25'),
(151, 'App\\DepartmentContact', 10, 1, 'created_at', NULL, '2017-09-11 17:42:25', '2017-09-11 09:42:25', '2017-09-11 09:42:25'),
(152, 'App\\User', 1, 1, 'remember_token', 'oVa6OLF2UKkYxAJv6FJLlxOZzoEeMkifJe3RgYSi0umCguxC05gndym7ZFbs', 'vzISkdOiTn2J2IHvwQgPnK3Kx6ZqftlJuaEJmI3W0lGdUxKHUhLJDiPFeLKY', '2017-09-11 09:43:03', '2017-09-11 09:43:03'),
(153, 'App\\Department', 11, 1, 'created_at', NULL, '2017-09-11 17:43:40', '2017-09-11 09:43:40', '2017-09-11 09:43:40'),
(154, 'App\\DepartmentContact', 11, 1, 'created_at', NULL, '2017-09-11 17:43:40', '2017-09-11 09:43:40', '2017-09-11 09:43:40'),
(155, 'App\\Department', 12, 1, 'created_at', NULL, '2017-09-11 17:44:38', '2017-09-11 09:44:38', '2017-09-11 09:44:38'),
(156, 'App\\DepartmentContact', 12, 1, 'created_at', NULL, '2017-09-11 17:44:38', '2017-09-11 09:44:38', '2017-09-11 09:44:38'),
(157, 'App\\Department', 13, 1, 'created_at', NULL, '2017-09-11 17:45:40', '2017-09-11 09:45:40', '2017-09-11 09:45:40'),
(158, 'App\\DepartmentContact', 13, 1, 'created_at', NULL, '2017-09-11 17:45:40', '2017-09-11 09:45:40', '2017-09-11 09:45:40'),
(159, 'App\\Department', 14, 1, 'created_at', NULL, '2017-09-11 17:46:33', '2017-09-11 09:46:33', '2017-09-11 09:46:33'),
(160, 'App\\DepartmentContact', 14, 1, 'created_at', NULL, '2017-09-11 17:46:33', '2017-09-11 09:46:33', '2017-09-11 09:46:33'),
(161, 'App\\Department', 15, 1, 'created_at', NULL, '2017-09-11 17:48:09', '2017-09-11 09:48:09', '2017-09-11 09:48:09'),
(162, 'App\\DepartmentContact', 15, 1, 'created_at', NULL, '2017-09-11 17:48:09', '2017-09-11 09:48:09', '2017-09-11 09:48:09'),
(163, 'App\\Department', 16, 1, 'created_at', NULL, '2017-09-11 17:49:07', '2017-09-11 09:49:07', '2017-09-11 09:49:07'),
(164, 'App\\DepartmentContact', 16, 1, 'created_at', NULL, '2017-09-11 17:49:07', '2017-09-11 09:49:07', '2017-09-11 09:49:07'),
(165, 'App\\Department', 17, 1, 'created_at', NULL, '2017-09-11 17:50:04', '2017-09-11 09:50:04', '2017-09-11 09:50:04'),
(166, 'App\\DepartmentContact', 17, 1, 'created_at', NULL, '2017-09-11 17:50:04', '2017-09-11 09:50:04', '2017-09-11 09:50:04'),
(167, 'App\\User', 1, 1, 'remember_token', 'vzISkdOiTn2J2IHvwQgPnK3Kx6ZqftlJuaEJmI3W0lGdUxKHUhLJDiPFeLKY', '3LkQEPfMacoU5ofpYQJuJ2bDv5YLWUxM967LhEd2AFRzhUcwY0LonFiiBT5V', '2017-09-11 10:00:35', '2017-09-11 10:00:35'),
(168, 'App\\Supplier', 7, 1, 'created_at', NULL, '2017-09-12 08:41:17', '2017-09-12 00:41:17', '2017-09-12 00:41:17'),
(169, 'App\\Supplier', 8, 1, 'created_at', NULL, '2017-09-12 08:46:27', '2017-09-12 00:46:27', '2017-09-12 00:46:27'),
(170, 'App\\Supplier', 8, 1, 'name', 'Kunal', 'Sriganesh Agencies', '2017-09-12 00:47:25', '2017-09-12 00:47:25'),
(171, 'App\\Supplier', 9, 1, 'created_at', NULL, '2017-09-12 09:00:04', '2017-09-12 01:00:04', '2017-09-12 01:00:04'),
(172, 'App\\Supplier', 10, 1, 'created_at', NULL, '2017-09-12 09:07:17', '2017-09-12 01:07:17', '2017-09-12 01:07:17'),
(173, 'App\\Supplier', 11, 1, 'created_at', NULL, '2017-09-12 09:20:08', '2017-09-12 01:20:08', '2017-09-12 01:20:08'),
(174, 'App\\Supplier', 12, 1, 'created_at', NULL, '2017-09-12 09:29:15', '2017-09-12 01:29:15', '2017-09-12 01:29:15'),
(175, 'App\\Supplier', 13, 1, 'created_at', NULL, '2017-09-12 09:36:39', '2017-09-12 01:36:39', '2017-09-12 01:36:39'),
(176, 'App\\Supplier', 14, 1, 'created_at', NULL, '2017-09-12 09:46:39', '2017-09-12 01:46:39', '2017-09-12 01:46:39'),
(177, 'App\\User', 4, 1, 'created_at', NULL, '2017-09-12 10:04:25', '2017-09-12 02:04:25', '2017-09-12 02:04:25'),
(178, 'App\\Customer', 3, 1, 'created_at', NULL, '2017-09-12 10:04:25', '2017-09-12 02:04:25', '2017-09-12 02:04:25'),
(179, 'App\\User', 5, 1, 'created_at', NULL, '2017-09-12 10:07:11', '2017-09-12 02:07:11', '2017-09-12 02:07:11'),
(180, 'App\\Customer', 4, 1, 'created_at', NULL, '2017-09-12 10:07:11', '2017-09-12 02:07:11', '2017-09-12 02:07:11'),
(181, 'App\\User', 6, 1, 'created_at', NULL, '2017-09-12 10:09:10', '2017-09-12 02:09:10', '2017-09-12 02:09:10'),
(182, 'App\\Customer', 5, 1, 'created_at', NULL, '2017-09-12 10:09:10', '2017-09-12 02:09:10', '2017-09-12 02:09:10'),
(183, 'App\\User', 7, 1, 'created_at', NULL, '2017-09-12 10:09:54', '2017-09-12 02:09:54', '2017-09-12 02:09:54'),
(184, 'App\\Customer', 6, 1, 'created_at', NULL, '2017-09-12 10:09:54', '2017-09-12 02:09:54', '2017-09-12 02:09:54'),
(185, 'App\\User', 8, 1, 'created_at', NULL, '2017-09-12 10:11:02', '2017-09-12 02:11:02', '2017-09-12 02:11:02'),
(186, 'App\\Customer', 7, 1, 'created_at', NULL, '2017-09-12 10:11:02', '2017-09-12 02:11:02', '2017-09-12 02:11:02'),
(187, 'App\\Contractor', 6, 1, 'created_at', NULL, '2017-09-12 10:12:56', '2017-09-12 02:12:56', '2017-09-12 02:12:56'),
(188, 'App\\Contractor', 7, 1, 'created_at', NULL, '2017-09-12 10:13:36', '2017-09-12 02:13:36', '2017-09-12 02:13:36'),
(189, 'App\\Contractor', 7, 1, 'code', 'Ele_02', 'Ele_002', '2017-09-12 02:13:53', '2017-09-12 02:13:53'),
(190, 'App\\Contractor', 8, 1, 'created_at', NULL, '2017-09-12 10:14:51', '2017-09-12 02:14:51', '2017-09-12 02:14:51'),
(191, 'App\\Contractor', 9, 1, 'created_at', NULL, '2017-09-12 10:15:46', '2017-09-12 02:15:46', '2017-09-12 02:15:46'),
(192, 'App\\Contractor', 10, 1, 'created_at', NULL, '2017-09-12 10:16:42', '2017-09-12 02:16:42', '2017-09-12 02:16:42'),
(193, 'App\\Contractor', 11, 1, 'created_at', NULL, '2017-09-12 10:17:36', '2017-09-12 02:17:36', '2017-09-12 02:17:36'),
(194, 'App\\Contractor', 12, 1, 'created_at', NULL, '2017-09-12 10:18:22', '2017-09-12 02:18:22', '2017-09-12 02:18:22'),
(195, 'App\\Contractor', 13, 1, 'created_at', NULL, '2017-09-12 10:20:14', '2017-09-12 02:20:14', '2017-09-12 02:20:14'),
(196, 'App\\Contractor', 14, 1, 'created_at', NULL, '2017-09-12 10:21:26', '2017-09-12 02:21:26', '2017-09-12 02:21:26'),
(197, 'App\\Contractor', 15, 1, 'created_at', NULL, '2017-09-12 10:22:53', '2017-09-12 02:22:53', '2017-09-12 02:22:53'),
(198, 'App\\Contractor', 16, 1, 'created_at', NULL, '2017-09-12 10:23:37', '2017-09-12 02:23:37', '2017-09-12 02:23:37'),
(199, 'App\\Contractor', 17, 1, 'created_at', NULL, '2017-09-12 10:24:16', '2017-09-12 02:24:16', '2017-09-12 02:24:16'),
(200, 'App\\Contractor', 18, 1, 'created_at', NULL, '2017-09-12 10:25:49', '2017-09-12 02:25:49', '2017-09-12 02:25:49'),
(201, 'App\\Contractor', 19, 1, 'created_at', NULL, '2017-09-12 10:27:32', '2017-09-12 02:27:32', '2017-09-12 02:27:32'),
(202, 'App\\Contractor', 20, 1, 'created_at', NULL, '2017-09-12 10:28:49', '2017-09-12 02:28:49', '2017-09-12 02:28:49'),
(203, 'App\\Contractor', 21, 1, 'created_at', NULL, '2017-09-12 10:29:54', '2017-09-12 02:29:54', '2017-09-12 02:29:54'),
(204, 'App\\Contractor', 22, 1, 'created_at', NULL, '2017-09-12 10:30:58', '2017-09-12 02:30:58', '2017-09-12 02:30:58'),
(205, 'App\\Contractor', 23, 1, 'created_at', NULL, '2017-09-12 10:32:27', '2017-09-12 02:32:27', '2017-09-12 02:32:27'),
(206, 'App\\Contractor', 24, 1, 'created_at', NULL, '2017-09-12 10:33:16', '2017-09-12 02:33:16', '2017-09-12 02:33:16'),
(207, 'App\\Contractor', 25, 1, 'created_at', NULL, '2017-09-12 10:34:36', '2017-09-12 02:34:36', '2017-09-12 02:34:36'),
(208, 'App\\User', 9, 1, 'created_at', NULL, '2017-09-12 10:40:51', '2017-09-12 02:40:51', '2017-09-12 02:40:51'),
(209, 'App\\UserHasProject', 1, 1, 'created_at', NULL, '2017-09-12 10:40:51', '2017-09-12 02:40:51', '2017-09-12 02:40:51'),
(210, 'App\\UserHasProject', 2, 1, 'created_at', NULL, '2017-09-12 10:42:46', '2017-09-12 02:42:46', '2017-09-12 02:42:46'),
(211, 'App\\User', 10, 1, 'created_at', NULL, '2017-09-12 10:44:04', '2017-09-12 02:44:04', '2017-09-12 02:44:04'),
(212, 'App\\UserHasProject', 3, 1, 'created_at', NULL, '2017-09-12 10:44:04', '2017-09-12 02:44:04', '2017-09-12 02:44:04'),
(213, 'App\\Project', 2, 1, 'address_one', 'Mokila,Shankarpally Mandal RR Dist', 'Mokila,Shankarpally (M), RR Dist', '2017-09-12 03:08:12', '2017-09-12 03:08:12'),
(214, 'App\\ProductCustomer', 4, 1, 'created_at', NULL, '2017-09-12 11:18:36', '2017-09-12 03:18:36', '2017-09-12 03:18:36'),
(215, 'App\\ProductCustomer', 5, 1, 'created_at', NULL, '2017-09-12 11:19:00', '2017-09-12 03:19:00', '2017-09-12 03:19:00'),
(216, 'App\\ProductCustomer', 6, 1, 'created_at', NULL, '2017-09-12 11:19:37', '2017-09-12 03:19:37', '2017-09-12 03:19:37'),
(217, 'App\\ProductCustomer', 7, 1, 'created_at', NULL, '2017-09-12 11:20:05', '2017-09-12 03:20:05', '2017-09-12 03:20:05'),
(218, 'App\\ProductCustomer', 8, 1, 'created_at', NULL, '2017-09-12 11:20:10', '2017-09-12 03:20:10', '2017-09-12 03:20:10'),
(219, 'App\\ProductCustomer', 9, 1, 'created_at', NULL, '2017-09-12 11:20:25', '2017-09-12 03:20:25', '2017-09-12 03:20:25'),
(220, 'App\\ProductCustomer', 10, 1, 'created_at', NULL, '2017-09-12 11:20:34', '2017-09-12 03:20:34', '2017-09-12 03:20:34'),
(221, 'App\\ProductCustomer', 11, 1, 'created_at', NULL, '2017-09-12 11:21:04', '2017-09-12 03:21:04', '2017-09-12 03:21:04'),
(222, 'App\\ProductCustomer', 12, 1, 'created_at', NULL, '2017-09-12 11:21:15', '2017-09-12 03:21:15', '2017-09-12 03:21:15'),
(223, 'App\\User', 1, 1, 'remember_token', '3LkQEPfMacoU5ofpYQJuJ2bDv5YLWUxM967LhEd2AFRzhUcwY0LonFiiBT5V', 'lt7SfXiQqSeXheJxaAfDI4m6aJcVNOgupzG8SpA4N1xh4Jrff14D3RzlJu0L', '2017-09-12 04:35:38', '2017-09-12 04:35:38'),
(224, 'App\\User', 1, 1, 'remember_token', 'lt7SfXiQqSeXheJxaAfDI4m6aJcVNOgupzG8SpA4N1xh4Jrff14D3RzlJu0L', 'alD7ZOvNkptjX02iGkA1aZrHGGXOeFPwYi2wxX2nBhe94uwqMBI3CTEgnzvE', '2017-09-12 04:50:29', '2017-09-12 04:50:29'),
(225, 'App\\Staff', 4, 1, 'created_at', NULL, '2017-09-12 15:13:33', '2017-09-12 07:13:33', '2017-09-12 07:13:33'),
(226, 'App\\Staff', 4, 1, 'code', NULL, 'SI-S4', '2017-09-12 07:13:33', '2017-09-12 07:13:33'),
(227, 'App\\Staff', 5, 1, 'created_at', NULL, '2017-09-12 15:14:48', '2017-09-12 07:14:48', '2017-09-12 07:14:48'),
(228, 'App\\Staff', 5, 1, 'code', NULL, 'SI-S5', '2017-09-12 07:14:48', '2017-09-12 07:14:48'),
(229, 'App\\Item', 8, 1, 'created_at', NULL, '2017-09-12 15:30:59', '2017-09-12 07:30:59', '2017-09-12 07:30:59'),
(230, 'App\\Item', 9, 1, 'created_at', NULL, '2017-09-12 15:33:34', '2017-09-12 07:33:34', '2017-09-12 07:33:34'),
(231, 'App\\Item', 10, 1, 'created_at', NULL, '2017-09-12 15:36:03', '2017-09-12 07:36:03', '2017-09-12 07:36:03'),
(232, 'App\\Item', 11, 1, 'created_at', NULL, '2017-09-12 15:39:56', '2017-09-12 07:39:56', '2017-09-12 07:39:56'),
(233, 'App\\Item', 12, 1, 'created_at', NULL, '2017-09-12 15:41:50', '2017-09-12 07:41:50', '2017-09-12 07:41:50'),
(234, 'App\\Item', 13, 1, 'created_at', NULL, '2017-09-12 15:45:53', '2017-09-12 07:45:53', '2017-09-12 07:45:53'),
(235, 'App\\Item', 14, 1, 'created_at', NULL, '2017-09-12 15:47:50', '2017-09-12 07:47:50', '2017-09-12 07:47:50'),
(236, 'App\\User', 1, 1, 'remember_token', 'alD7ZOvNkptjX02iGkA1aZrHGGXOeFPwYi2wxX2nBhe94uwqMBI3CTEgnzvE', 'm3Tgoe4TXDvOUTEWYoCONH1zg0AaKSZxJN2aSmAcOg8hq1CcAgapv9GpfLVC', '2017-09-12 08:26:58', '2017-09-12 08:26:58'),
(237, 'App\\Order', 4, 1, 'created_at', NULL, '2017-09-13 10:04:40', '2017-09-13 02:04:40', '2017-09-13 02:04:40'),
(238, 'App\\Order', 4, 1, 'code', NULL, 'SI-SI MIST-0004', '2017-09-13 02:04:40', '2017-09-13 02:04:40'),
(239, 'App\\Order', 4, 1, 'invoice_total', NULL, '26201.6', '2017-09-13 02:04:40', '2017-09-13 02:04:40'),
(240, 'App\\Order', 5, 1, 'created_at', NULL, '2017-09-13 10:07:39', '2017-09-13 02:07:39', '2017-09-13 02:07:39'),
(241, 'App\\Order', 5, 1, 'code', NULL, 'SI-SI MIST-0005', '2017-09-13 02:07:39', '2017-09-13 02:07:39'),
(242, 'App\\Order', 5, 1, 'invoice_total', NULL, '499200', '2017-09-13 02:07:39', '2017-09-13 02:07:39'),
(243, 'App\\Item', 15, 1, 'created_at', NULL, '2017-09-13 10:40:18', '2017-09-13 02:40:18', '2017-09-13 02:40:18'),
(244, 'App\\Item', 16, 1, 'created_at', NULL, '2017-09-13 10:41:32', '2017-09-13 02:41:32', '2017-09-13 02:41:32'),
(245, 'App\\Item', 17, 1, 'created_at', NULL, '2017-09-13 10:42:49', '2017-09-13 02:42:49', '2017-09-13 02:42:49'),
(246, 'App\\Item', 18, 1, 'created_at', NULL, '2017-09-13 10:43:53', '2017-09-13 02:43:53', '2017-09-13 02:43:53'),
(247, 'App\\Item', 19, 1, 'created_at', NULL, '2017-09-13 10:45:22', '2017-09-13 02:45:22', '2017-09-13 02:45:22'),
(248, 'App\\Item', 17, 1, 'unit_price', '28.00', '30.00', '2017-09-13 02:47:44', '2017-09-13 02:47:44'),
(249, 'App\\Order', 6, 1, 'created_at', NULL, '2017-09-13 10:50:45', '2017-09-13 02:50:45', '2017-09-13 02:50:45'),
(250, 'App\\Order', 6, 1, 'code', NULL, 'SI-SI MIST-0006', '2017-09-13 02:50:45', '2017-09-13 02:50:45'),
(251, 'App\\Order', 7, 1, 'created_at', NULL, '2017-09-13 10:54:39', '2017-09-13 02:54:39', '2017-09-13 02:54:39'),
(252, 'App\\Order', 7, 1, 'code', NULL, 'SI-SI MIST-0007', '2017-09-13 02:54:39', '2017-09-13 02:54:39'),
(253, 'App\\Order', 7, 1, 'invoice_total', NULL, '5900', '2017-09-13 02:54:39', '2017-09-13 02:54:39'),
(254, 'App\\User', 1, 1, 'remember_token', 'm3Tgoe4TXDvOUTEWYoCONH1zg0AaKSZxJN2aSmAcOg8hq1CcAgapv9GpfLVC', '9Vcjihjki6aLjOSd1IxrNXEbj0uN9O1t8G9V21HBrrBJBNtvQlNqj9MHlfYn', '2017-09-13 02:55:19', '2017-09-13 02:55:19'),
(255, 'App\\Item', 20, 1, 'created_at', NULL, '2017-09-14 15:18:00', '2017-09-14 07:18:00', '2017-09-14 07:18:00'),
(256, 'App\\Order', 8, 1, 'created_at', NULL, '2017-09-14 15:18:49', '2017-09-14 07:18:49', '2017-09-14 07:18:49'),
(257, 'App\\Order', 8, 1, 'code', NULL, 'SI-SI MIST-0008', '2017-09-14 07:18:49', '2017-09-14 07:18:49'),
(258, 'App\\Order', 8, 1, 'invoice_total', NULL, '21417', '2017-09-14 07:18:49', '2017-09-14 07:18:49'),
(259, 'App\\User', 1, 1, 'remember_token', '9Vcjihjki6aLjOSd1IxrNXEbj0uN9O1t8G9V21HBrrBJBNtvQlNqj9MHlfYn', 'Y5wVQi7uRKh4jHFp3ncUoMA7va3nAcjjtL1RyLLkK5FozdhLp07YvpUpMvhx', '2017-09-14 07:29:41', '2017-09-14 07:29:41'),
(260, 'App\\Item', 10, 1, 'tax_id', '8', '12', '2017-09-15 02:37:28', '2017-09-15 02:37:28'),
(261, 'App\\Order', 9, 1, 'created_at', NULL, '2017-09-15 10:43:19', '2017-09-15 02:43:19', '2017-09-15 02:43:19'),
(262, 'App\\Order', 9, 1, 'code', NULL, 'SI-SI MIST-0009', '2017-09-15 02:43:19', '2017-09-15 02:43:19'),
(263, 'App\\Order', 9, 1, 'invoice_total', NULL, '35088', '2017-09-15 02:43:19', '2017-09-15 02:43:19'),
(264, 'App\\Order', 10, 1, 'created_at', NULL, '2017-09-18 10:35:50', '2017-09-18 02:35:50', '2017-09-18 02:35:50'),
(265, 'App\\Order', 10, 1, 'code', NULL, 'SI-SI MIST-0010', '2017-09-18 02:35:50', '2017-09-18 02:35:50'),
(266, 'App\\Order', 10, 1, 'invoice_total', NULL, '13496.75', '2017-09-18 02:35:50', '2017-09-18 02:35:50'),
(267, 'App\\User', 1, 1, 'remember_token', 'Y5wVQi7uRKh4jHFp3ncUoMA7va3nAcjjtL1RyLLkK5FozdhLp07YvpUpMvhx', 'tjkPQZ4EWTp6XfeiSHFUJujZuJuPeAyEH4U80YA27gCgbpzng6XNKk8K7O5X', '2017-09-18 04:27:25', '2017-09-18 04:27:25'),
(268, 'App\\User', 1, 1, 'remember_token', 'tjkPQZ4EWTp6XfeiSHFUJujZuJuPeAyEH4U80YA27gCgbpzng6XNKk8K7O5X', 'ZeiN0vngwzzCwAua7hP3aZoh11ljQNgqbm9VBxPuy5vM87oVExP8r96pobzo', '2017-09-18 04:41:49', '2017-09-18 04:41:49'),
(269, 'App\\User', 1, 1, 'remember_token', 'ZeiN0vngwzzCwAua7hP3aZoh11ljQNgqbm9VBxPuy5vM87oVExP8r96pobzo', 'NioB7ocb6gCahGaB5YZ1SrLEfM5BjlMlPt02TBOmYimQU7G0eoXrqAF4umPU', '2017-09-18 04:49:56', '2017-09-18 04:49:56'),
(270, 'App\\User', 4, 4, 'remember_token', NULL, 'zTmUMmbBGSXGiGeiawJcQUS8xv2lDvDHH0kywAi1tE3dRAPw2rzD81EAPRpX', '2017-09-18 06:50:21', '2017-09-18 06:50:21'),
(271, 'App\\User', 1, 1, 'remember_token', 'NioB7ocb6gCahGaB5YZ1SrLEfM5BjlMlPt02TBOmYimQU7G0eoXrqAF4umPU', 'ccMu17PV1leiL3XTwdSteihZysbgAhof0VIfm8ZsGf8TvYUVbjKFy0OXe1jW', '2017-09-18 06:53:22', '2017-09-18 06:53:22'),
(272, 'App\\User', 1, 1, 'remember_token', 'ccMu17PV1leiL3XTwdSteihZysbgAhof0VIfm8ZsGf8TvYUVbjKFy0OXe1jW', 'glMMPKJpKuc0MZqMnhQfhKLuPNxT9T0rsK08pAypJbZoEUGPgCmz6F48xmx0', '2017-10-11 14:44:17', '2017-10-11 14:44:17');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', 'First Level User', '2017-06-06 12:24:53', '2017-06-06 12:24:53'),
(2, 'owner', 'Owner', 'Second Level User', '2017-06-06 12:39:07', '2017-06-06 12:39:07'),
(3, 'manager', 'Project Manager', 'Third Level User - Project Level', '2017-06-06 12:50:24', '2017-06-06 12:50:24'),
(4, 'customer', 'Customer', 'Fourth Level User', '2017-07-01 10:41:23', '2017-07-01 10:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(3, 1),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `service_payments`
--

CREATE TABLE `service_payments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_payments`
--

INSERT INTO `service_payments` (`id`, `org_id`, `project_id`, `payment_id`, `type`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 15, 'Security', 20000.00, '2017-09-11 02:22:18', '2017-09-11 02:22:18'),
(2, 1, 2, 22, 'Salaries', 50000.00, '2017-09-11 06:33:26', '2017-09-11 06:33:26'),
(3, 1, 2, 29, 'Labour', 9000.00, '2017-09-11 07:12:40', '2017-09-11 07:12:40'),
(4, 1, 2, 30, 'Misc', 2000.00, '2017-09-11 07:14:46', '2017-09-11 07:14:46'),
(5, 2, 3, 48, 'Labour', 1000.00, '2017-09-18 01:51:51', '2017-09-18 01:51:51');

-- --------------------------------------------------------

--
-- Table structure for table `service_payment_types`
--

CREATE TABLE `service_payment_types` (
  `id` int(12) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_payment_types`
--

INSERT INTO `service_payment_types` (`id`, `name`) VALUES
(1, 'Diesel'),
(2, 'Security'),
(3, 'Labour'),
(4, 'Salaries'),
(5, 'Misc');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `org_id`, `project_id`, `code`, `name`, `phone`, `email`, `address`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 'SUBISHI-S1', 'rambabu', 'XXXXXXXXXXX', 'Ram@gmail', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-10 12:39:00', '2017-09-10 12:39:00', NULL),
(2, 1, 2, 'SUBISHI-S2', 'rambabu', 'XXXXXXXXXXX', 'Ram@gmail', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-10 12:39:00', '2017-09-10 12:39:00', NULL),
(3, 1, 2, 'SUBISHI-S3', 'sathish', 'XXXXXXXXXXX', 'sathi@Gmail.com', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-10 12:39:41', '2017-09-10 12:39:41', NULL),
(4, 2, 3, 'SI-S4', 'M.Rambabu', '9704644597', 'Ram@gmail', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-12 07:13:33', '2017-09-12 07:13:33', NULL),
(5, 2, 3, 'SI-S5', 'B.Sathish', '9948585473', 'sathi@Gmail.com', 'Mokila,Shankarpally Mandal RR Dist, Mokila,Shankarpally Mandal RR Dist', 'Telangana', '2017-09-12 07:14:48', '2017-09-12 07:14:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `id` int(12) NOT NULL,
  `project_id` int(11) NOT NULL,
  `department_id` int(12) NOT NULL,
  `product_category_id` int(12) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `rate` decimal(10,2) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stages`
--

INSERT INTO `stages` (`id`, `project_id`, `department_id`, `product_category_id`, `code`, `name`, `description`, `rate`, `comment`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 8, 'BRICK GF', 'Brick WGF', 'Brick Work GF', '40000.00', NULL, '2017-09-10 12:58:51', '2017-09-11 06:08:48'),
(2, 2, 1, 9, 'BRICKWFF', 'Brick WFF', 'Brick Work FF', '50000.00', NULL, '2017-09-10 12:59:33', '2017-09-11 05:35:58'),
(3, 2, 3, 9, 'GF FLOORING', 'GF Flooring Tiles', 'GF Flooring Tiles', '40000.00', NULL, '2017-09-10 13:00:30', '2017-09-11 06:09:16'),
(4, 2, 3, 8, 'FF FLOORING TILES', 'FF Flooring Tiles', 'FF Flooring Tiles', '50000.00', NULL, '2017-09-10 13:01:15', '2017-09-11 06:13:37'),
(5, 2, 3, 8, 'BATH TILES', 'Bath Room Tiles', 'Tiles Work', '30000.00', NULL, '2017-09-10 13:11:18', '2017-09-11 06:14:51'),
(6, 2, 1, 8, 'WINDOW', 'Window Level', 'Window level', '20000.00', NULL, '2017-09-11 06:11:00', '2017-09-11 06:11:00'),
(7, 2, 1, 8, 'PLSTRNG', 'IS Plastring', 'Inside Plastring', '55000.00', NULL, '2017-09-11 06:36:26', '2017-09-11 06:36:26'),
(8, 2, 2, 9, 'CHP0', 'Chipping', 'Electrical', '25000.00', NULL, '2017-09-11 06:46:24', '2017-09-11 06:46:24'),
(9, 2, 2, 8, 'WIRE00', 'Wiring', 'Electrical', '20000.00', NULL, '2017-09-11 06:47:07', '2017-09-11 06:47:07'),
(10, 2, 2, 8, 'CHP0', 'Chipping', 'Electrical', '25000.00', NULL, '2017-09-11 06:49:58', '2017-09-11 06:49:58'),
(11, 3, 7, 10, 'CHP', 'CHIPPING', 'WALL CHIPPING', '12000.00', NULL, '2017-09-12 02:50:51', '2017-09-12 02:50:51'),
(12, 3, 7, 10, 'PING', 'PIPING', 'WALL PIPING', '10000.00', NULL, '2017-09-12 02:51:40', '2017-09-12 02:51:40'),
(13, 3, 7, 10, 'WIR', 'WIRING', 'WIRING', '12000.00', NULL, '2017-09-12 02:52:21', '2017-09-12 02:52:21'),
(14, 3, 7, 10, 'SWTH', 'SWITCHES', 'FINAL', '20000.00', NULL, '2017-09-12 02:53:37', '2017-09-12 02:53:37'),
(15, 3, 7, 11, 'CHP', 'CHIPPING', 'CHIPPING', '8000.00', NULL, '2017-09-12 02:55:08', '2017-09-12 02:55:08'),
(16, 3, 7, 11, 'PING', 'PIPING', 'WALL PIPING', '7000.00', NULL, '2017-09-12 02:56:53', '2017-09-12 02:56:53'),
(17, 3, 7, 11, 'WIR', 'WIRING', 'WIRING', '9000.00', NULL, '2017-09-12 02:58:09', '2017-09-12 02:58:09'),
(18, 3, 7, 11, 'SWTH', 'SWITCHES', 'FINAL', '9000.00', NULL, '2017-09-12 02:58:40', '2017-09-12 02:58:40'),
(19, 3, 7, 12, 'CHP_01', 'CHIPPING', 'WALL CHIPPING', '13000.00', NULL, '2017-09-12 03:23:51', '2017-09-12 03:23:51'),
(20, 3, 7, 12, 'PING_01', 'PIPING', 'WALL PIPING', '15000.00', NULL, '2017-09-12 03:24:36', '2017-09-12 03:24:36'),
(21, 3, 7, 12, 'SWTH_01', 'SWITCHES', 'FINAL', '17000.00', NULL, '2017-09-12 03:25:14', '2017-09-12 03:25:14'),
(22, 3, 7, 12, 'WIR_01', 'WIRING', 'WIRING', '17000.00', NULL, '2017-09-12 03:26:14', '2017-09-12 03:26:14'),
(23, 3, 8, 10, 'BRCKWK', 'GF Brick Work', 'mason Work', '50000.00', NULL, '2017-09-14 06:42:39', '2017-09-14 06:42:39'),
(24, 3, 7, 10, 'BRCKWK', 'Brick WFF', 'Mason Work', '60000.00', NULL, '2017-09-14 06:43:57', '2017-09-14 06:43:57'),
(25, 3, 8, 12, 'BRCKWK', 'Brick WGF', 'Mason Work', '60000.00', NULL, '2017-09-14 06:44:38', '2017-09-14 06:44:38'),
(26, 3, 8, 12, 'BRCKWK', 'Brick WFF', 'Mason Work', '70000.00', NULL, '2017-09-14 06:45:14', '2017-09-14 06:45:14'),
(27, 3, 8, 11, 'BRCKWK', 'Brick WGF', 'Mason Work', '30000.00', NULL, '2017-09-14 06:45:55', '2017-09-14 06:45:55'),
(28, 3, 8, 11, 'BRCKWK', 'Brick WFF', 'Mason Work', '40000.00', NULL, '2017-09-14 06:46:34', '2017-09-14 06:46:34'),
(29, 3, 11, 10, 'CHNL&FLG', 'Chanal&False Ceiling', 'False Ceiling Work', '200000.00', NULL, '2017-09-14 06:56:44', '2017-09-14 06:56:44'),
(30, 3, 11, 11, 'CHNL&FLG', 'Chanal&False Ceiling', 'False Ceiling Work', '150000.00', NULL, '2017-09-14 06:57:55', '2017-09-14 06:57:55'),
(31, 3, 11, 12, 'CHNL&FLG', 'Chanal&False Ceiling', 'False Ceiling Work', '220000.00', NULL, '2017-09-14 06:58:34', '2017-09-14 06:58:34'),
(32, 3, 12, 10, 'GFFT', 'GF Flooring Tiles', 'Tiles Work', '30000.00', NULL, '2017-09-14 06:59:52', '2017-09-14 06:59:52'),
(33, 3, 12, 10, 'FFFT', 'FF Flooring Tiles', 'Tiles Work', '33000.00', NULL, '2017-09-14 07:00:52', '2017-09-14 07:00:52'),
(34, 3, 12, 12, 'GFFT', 'GF Flooring Tiles', 'Tiles Work', '38000.00', NULL, '2017-09-14 07:01:36', '2017-09-14 07:01:36'),
(35, 3, 12, 12, 'FFFT', 'FF Flooring Tiles', 'Tiles Work', '400000.00', NULL, '2017-09-14 07:02:06', '2017-09-14 07:02:06'),
(36, 3, 12, 11, 'GFFT', 'GF Flooring Tiles', 'Tiles Work', '28000.00', NULL, '2017-09-14 07:02:39', '2017-09-14 07:02:39'),
(37, 3, 12, 11, 'FFFT', 'FF Flooring Tiles', 'Tiles Work', '30000.00', NULL, '2017-09-14 07:03:25', '2017-09-14 07:03:25');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address_one` varchar(255) DEFAULT NULL,
  `address_two` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` varchar(10) DEFAULT NULL,
  `meta_tin` varchar(255) DEFAULT NULL,
  `meta_bank_acc` varchar(255) DEFAULT NULL,
  `meta_bank_name` varchar(255) DEFAULT NULL,
  `meta_bank_branch` varchar(255) DEFAULT NULL,
  `meta_bank_acc_type` varchar(255) DEFAULT NULL,
  `meta_bank_ifsc` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `org_id`, `project_id`, `code`, `name`, `email`, `phone`, `address_one`, `address_two`, `city`, `district`, `state`, `pincode`, `meta_tin`, `meta_bank_acc`, `meta_bank_name`, `meta_bank_branch`, `meta_bank_acc_type`, `meta_bank_ifsc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '0031', 'MAHALAXMI', 'MAHA@GMAIL.COM', '97766676789', 'HAYATH NAGAR', 'HYDERABAD', 'Hyderabad', 'RANGA REDDY', 'Telangana', '501203', NULL, '7900976888', 'SBI', 'PARKAL', 'SAVING', 'SBIN0005879', '2017-09-08 08:11:35', '2017-09-08 08:12:28', NULL),
(2, 1, 1, '002', 'NCC Cement', 'ncccement@gmail.com', '1231231231', 'Gachibowli', NULL, NULL, 'Ranga Reddy', 'Telangana', '501203', NULL, 'asdfasdf', 'asdfasdf', 'asdfsadf', 'asdfasdf', 'asdfasdf', '2017-09-10 01:44:40', '2017-09-10 01:44:40', NULL),
(3, 1, 1, '0004', 'Suguna Steel', 'sugunasteel@gmail.com', '1232132111', 'Kondapur', 'Kondapur', 'Gachibowli', 'Ranga Reddy', 'Telangana', '501203', NULL, 'asdfasdf', 'asdfasdf', 'asdfasfd', 'asdfasdf', 'asdfasdf', '2017-09-10 01:47:13', '2017-09-10 01:47:13', NULL),
(4, 1, 2, 'MAHA123', 'Mahalaxmi Electricals', 'Maha@gmails', 'XXXXXXXX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-10 12:20:45', '2017-09-10 12:20:45', NULL),
(5, 1, 2, 'MARBLE123', 'Marble House', 'Marbal@Gmail.com', 'XXXXXXXX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-10 12:22:08', '2017-09-10 12:22:08', NULL),
(6, 1, 2, 'GANESH123', 'Ganesh Aencies', 'ganesh@gmail.com', 'XXXXXX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-11 04:57:30', '2017-09-11 04:57:30', NULL),
(7, 2, 3, 'MLX_ML', 'Mahalaxmi Electricals', 'Mahalaxmi@gmail.com', '9490475314', 'Rathan lal', 'Hayath Nagar,Raniganz', 'Hyderabad', 'Ranga Reddy', 'Telangana', '501203', NULL, '621100089796', 'HDFC', 'RANIGANZ', 'CURRENT', 'HDFC000997', '2017-09-12 00:41:17', '2017-09-12 00:41:17', NULL),
(8, 2, 3, 'SRGA_SRA', 'Sriganesh Agencies', 'kunalskp@gmail.com', '9849643216', 'Shankar pally', NULL, 'Hyderabad', 'Ranga Reddy', 'Telangana', '501203', NULL, '6288765798', 'CORPORATION', 'BHANUR', 'SB', 'CORP0001237', '2017-09-12 00:46:27', '2017-09-12 00:47:25', NULL),
(9, 2, 3, 'SGNST_SGN', 'Suguna Steel', 'sugunast@gmail.com', '9030717916', 'Hazmabad', NULL, 'Parigi', 'Ranga Reddy', 'Telangana', '501203', NULL, '78654323', 'AXIS BANK', 'Hazmabad', 'SB', 'AXIX000897', '2017-09-12 01:00:04', '2017-09-12 01:00:04', NULL),
(10, 2, 3, 'SRNST_SRN', 'Srinivasa Steel Traders', 'srinivasasteel@gmail.com', '9848366681', 'Kukat pally', NULL, 'Hyderabad', 'Ranga Reddy', 'Telangana', '557876', NULL, '72566789979', 'State Bank Of India', 'Kukat pally', 'Current', 'SBIN0008763', '2017-09-12 01:07:17', '2017-09-12 01:07:17', NULL),
(11, 2, 3, 'RSMKT_RSB', 'Rishab GM Marketing', 'rishabmarketing@gmail.com', '9160786353', 'Hindi nagar', 'Goshamahal North,', 'Nampally,Hyderabad', 'Ranga Reddy', 'Telangana', '500001', NULL, '4367890000655', 'INDUS IND', 'GOSHAMAHAL', 'CURRENT', 'INDSIND0001', '2017-09-12 01:20:08', '2017-09-12 01:20:08', NULL),
(12, 2, 3, 'PRNCHYD_PRNC', 'Prince Systems Hyderabad', 'mayankprince@yahoo.co.in', '9348826457', 'LB Nagar', 'Snehapuri Colony', 'Hyderabad', 'Ranga Reddy', 'Telangana', '500035', NULL, '6377787887', 'ICICI BANK', 'LB NAGAR', '875555777887', 'ICIC00080', '2017-09-12 01:29:15', '2017-09-12 01:29:15', NULL),
(13, 2, 3, 'BRTHEL_BRTH', 'Bharath Electricals', 'bharathelectricals@gmail.com', '9246373781', 'Hayath nagar', 'Raniganz', 'Hyderabad', 'Ranga Reddy', 'Telangana', '500001', NULL, '675444466775', 'HDFC', 'HAYATH NAGAR', 'CURRENT', 'HDFC000887', '2017-09-12 01:36:39', '2017-09-12 01:36:39', NULL),
(14, 2, 3, 'NCLIND_NCL', 'NCL Industries Ltd', 'nclindhyd@gmail.com', '9573221180', 'Patancheru', 'Hayath Nagar,Raniganz', 'Hyderabad', 'Medak', 'Telangana', '500786', NULL, '72555656777', 'ICICI BANK', 'PATANCHERU', 'CURRENT', 'ICIC00088', '2017-09-12 01:46:39', '2017-09-12 01:46:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_balance`
--

CREATE TABLE `supplier_balance` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `balance` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_contacts`
--

CREATE TABLE `supplier_contacts` (
  `id` int(12) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pincode` int(6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_contacts`
--

INSERT INTO `supplier_contacts` (`id`, `supplier_id`, `name`, `phone`, `email`, `address`, `district`, `state`, `pincode`, `created_at`, `updated_at`) VALUES
(1, 1, 'MAHI', '9849701105', 'MAHI@GMAIL.COM', NULL, 'RANGA REDDY', 'Telangana', 501203, '2017-09-08 08:11:35', '2017-09-08 08:11:35'),
(2, 2, 'NCC Manager', '1231231231', 'nccmanager@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 501203, '2017-09-10 01:44:40', '2017-09-10 01:44:40'),
(3, 3, 'Suguna Manager', '1231231231', 'sugunamanager@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 501203, '2017-09-10 01:47:13', '2017-09-10 01:47:13'),
(4, 4, 'Ratanlal', 'XXXXXXXXX', 'maha@gmail.com', NULL, NULL, 'Telangana', NULL, '2017-09-10 12:20:45', '2017-09-10 12:20:45'),
(5, 5, 'Nagesh', NULL, NULL, NULL, NULL, 'Telangana', NULL, '2017-09-10 12:22:08', '2017-09-10 12:22:08'),
(6, 7, 'Rathan lal', '9490475314', 'Mahalaxmimanage@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 501203, '2017-09-12 00:41:17', '2017-09-12 00:41:17'),
(7, 8, 'Kunal', '9849643216', 'kunalskp@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 501203, '2017-09-12 00:46:27', '2017-09-12 00:46:27'),
(8, 9, 'Pradeep', '9030717916', 'sugunast@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 501203, '2017-09-12 01:00:04', '2017-09-12 01:00:04'),
(9, 10, 'Ashok', '9848366681', 'srinivasasteel@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 557876, '2017-09-12 01:07:17', '2017-09-12 01:07:17'),
(10, 11, 'Amjad', '9160786353', 'rishabmarketing@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 500001, '2017-09-12 01:20:08', '2017-09-12 01:20:08'),
(11, 12, 'Murthy', '9348826457', 'mayankprince@yahoo.co.in', NULL, 'Ranga Reddy', 'Telangana', 500035, '2017-09-12 01:29:15', '2017-09-12 01:29:15'),
(12, 13, 'Kuparao', '9246373781', 'bharathelectricals@gmail.com', NULL, 'Ranga Reddy', 'Telangana', 500001, '2017-09-12 01:36:39', '2017-09-12 01:36:39'),
(13, 14, 'Raju', '9573221180', 'nclindhyd@gmail.com', NULL, 'Medak', 'Telangana', 500786, '2017-09-12 01:46:39', '2017-09-12 01:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_payments`
--

CREATE TABLE `supplier_payments` (
  `id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `supplier_id` int(12) NOT NULL,
  `payment_id` int(12) DEFAULT NULL,
  `type` enum('advance','order','grn') NOT NULL,
  `order_id` int(12) DEFAULT NULL,
  `grn_id` int(12) DEFAULT NULL,
  `amount` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_payments`
--

INSERT INTO `supplier_payments` (`id`, `org_id`, `project_id`, `supplier_id`, `payment_id`, `type`, `order_id`, `grn_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 4, 17, 'grn', NULL, NULL, 3200.00, '2017-09-11 08:25:18', '2017-09-11 08:25:18'),
(2, 1, 2, 4, 18, 'grn', NULL, NULL, 3200.00, '2017-09-11 08:41:54', '2017-09-11 08:41:54'),
(3, 1, 2, 4, 19, 'grn', NULL, NULL, 28910.00, '2017-09-11 08:49:17', '2017-09-11 08:49:17'),
(4, 1, 2, 4, 20, 'grn', NULL, NULL, 4130.00, '2017-09-11 08:50:16', '2017-09-11 08:50:16'),
(5, 1, 2, 4, 21, 'grn', NULL, NULL, 192000.00, '2017-09-11 09:01:52', '2017-09-11 09:01:52'),
(6, 2, 3, 10, 43, 'grn', NULL, NULL, 499200.00, '2017-09-13 04:39:58', '2017-09-13 04:39:58'),
(7, 2, 3, 7, 47, 'grn', NULL, NULL, 48028.00, '2017-09-15 05:16:00', '2017-09-15 05:16:00');

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `org_id` int(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `percent` decimal(10,2) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `project_id`, `org_id`, `name`, `percent`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'GST', '28.00', '28%', '2017-09-08 08:05:06', '2017-09-08 08:05:29'),
(2, 2, 1, 'GST', '18.00', 'GST Tax', '2017-09-10 12:45:18', '2017-09-10 12:45:18'),
(3, 2, 1, 'GST', '12.00', 'GST Tax', '2017-09-10 12:45:45', '2017-09-10 12:45:45'),
(4, 2, 1, 'Gst', '5.00', 'GST Tax', '2017-09-10 12:46:13', '2017-09-10 12:46:13'),
(5, 3, 2, 'GST', '5.00', '5%', '2017-09-12 02:44:58', '2017-09-12 02:44:58'),
(6, 3, 2, 'GST', '9.00', '9%', '2017-09-12 02:45:39', '2017-09-12 02:45:39'),
(7, 3, 2, 'GST', '12.00', '12%', '2017-09-12 02:46:03', '2017-09-12 02:46:03'),
(8, 3, 2, 'GST', '28.00', '28%', '2017-09-12 02:46:35', '2017-09-12 02:46:35'),
(9, 3, 2, 'CGST', '9.00', '9%', '2017-09-12 02:46:54', '2017-09-12 02:46:54'),
(10, 3, 2, 'CGST', '14.00', '14%', '2017-09-12 02:47:16', '2017-09-12 02:47:16'),
(11, 3, 2, 'GST', '14.00', '14%', '2017-09-12 02:47:41', '2017-09-12 02:47:41'),
(12, 3, 2, 'GST', '18.00', 'GST Tax', '2017-09-13 02:02:51', '2017-09-13 02:02:51');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrator', 'jaipalt@gmail.com', '$2y$10$TDjvSojuhoLsTRZGHnBdzeJBzFiszWB/Pilvou1LKcyGusv/NVZjq', 'glMMPKJpKuc0MZqMnhQfhKLuPNxT9T0rsK08pAypJbZoEUGPgCmz6F48xmx0', '2017-09-08 07:10:48', '2017-09-08 07:10:48', NULL),
(2, 'Jagan', 'jagan@gmail.com', '$2y$10$wUvgjflcoVd7.RNXFJ3j/.Xw6DZkFuDoYe8dy9hx1XXoxrOiV/ykm', NULL, '2017-09-11 01:58:01', '2017-09-11 01:58:01', NULL),
(3, 'Sathish', 'sathishgoud82@gmail.com', '$2y$10$SLQsFWki0x2DgXutqTh/..bda8cgSu307IlC0t1Zn74NCtoeMNeve', NULL, '2017-09-11 01:58:39', '2017-09-11 01:58:39', NULL),
(4, 'Sathish', 'alignwebs@gmail.com', '$2y$10$Te5gsK31MTyeKA2oIBOlaOc7bNNQGQELrVUf8x/tc4/GIfrpNF0PO', 'zTmUMmbBGSXGiGeiawJcQUS8xv2lDvDHH0kywAi1tE3dRAPw2rzD81EAPRpX', '2017-09-12 02:04:25', '2017-09-12 02:04:25', NULL),
(5, 'Rambabu', 'ramsmoola@gmail.com', '$2y$10$iDTzjG0Y1n0ylMGdoBufnOC6uWfZFxVPFCLZ0oSjUEnXsXTqhsb8O', NULL, '2017-09-12 02:07:11', '2017-09-12 02:07:11', NULL),
(6, 'Narender', 'naren@gmail.com', '$2y$10$qfKDXFuRxDYN27lrUd.EYekgPMn0bsBcnSQc1FvMXEwhwAwC3hBLC', NULL, '2017-09-12 02:09:10', '2017-09-12 02:09:10', NULL),
(7, 'Madhu', 'madhu@gmail.com', '$2y$10$RSZH8d/9WnqWK/J.TE17Ze485ucAyXykbIJCs0/gXEE/yfr5nScvK', NULL, '2017-09-12 02:09:54', '2017-09-12 02:09:54', NULL),
(8, 'Bhaskar', 'baskar@gmail.com', '$2y$10$CaF0tbHMQuksoNH21jSSPe3tqbu5NvViDhBNHm44AjHrN4xmB2h0K', NULL, '2017-09-12 02:11:02', '2017-09-12 02:11:02', NULL),
(9, 'Jaipal Reddy', 'jaipal@gmail.com', '$2y$10$AJ86BXUEW4rCSrtNQPTbeukfoX2o.hc/2n.F.RDwH6Qj1QrsNPqa6', NULL, '2017-09-12 02:40:51', '2017-09-12 02:40:51', NULL),
(10, 'Rambabu', 'rams@gmail.com', '$2y$10$13fAK.keH24wudkUlNfGLe7nkyFG99zuFVzrNrpHunp/8Zcuimkre', NULL, '2017-09-12 02:44:04', '2017-09-12 02:44:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `role_title` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `phone`, `address`, `district`, `state`, `role_title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, '7966677654', 'Mokila, Shankar Pally', 'Ranga Reddy', 'Telangana', 'Project Co-ordinator', '2017-09-12 02:40:51', '2017-09-12 02:40:51', NULL),
(2, 3, '9849701105', 'Suite# 308, Subishi TownCenter, Mokila', NULL, 'Telangana', 'Project Manager', '2017-09-12 02:42:46', '2017-09-12 02:42:46', NULL),
(3, 10, '9704644597', 'Mokila, Shankar Pally', 'Ranga Reddy', 'Telangana', 'Project Manager', '2017-09-12 02:44:04', '2017-09-12 02:44:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_permissions`
--

CREATE TABLE `user_has_permissions` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_has_projects`
--

CREATE TABLE `user_has_projects` (
  `id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `project_id` int(12) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_has_projects`
--

INSERT INTO `user_has_projects` (`id`, `user_id`, `project_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 3, '2017-09-12 02:40:51', '2017-09-12 02:40:51', NULL),
(2, 3, 3, '2017-09-12 02:42:46', '2017-09-12 02:42:46', NULL),
(3, 10, 3, '2017-09-12 02:44:04', '2017-09-12 02:44:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_has_roles`
--

CREATE TABLE `user_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_has_roles`
--

INSERT INTO `user_has_roles` (`role_id`, `user_id`) VALUES
(1, 1),
(3, 3),
(3, 9),
(3, 10),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8);

-- --------------------------------------------------------

--
-- Table structure for table `user_login_activities`
--

CREATE TABLE `user_login_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_login_activities`
--

INSERT INTO `user_login_activities` (`id`, `user_id`, `ip_address`, `event`, `remember`, `created_at`) VALUES
(1, 1, '112.133.246.105', 'login', 0, '2017-09-08 09:42:14'),
(2, 1, '112.133.246.105', 'logout', 0, '2017-09-08 09:45:15'),
(3, 1, '157.48.10.133', 'login', 0, '2017-09-08 10:26:20'),
(4, 1, '157.48.10.133', 'logout', 0, '2017-09-08 11:37:25'),
(5, 1, '112.133.246.105', 'login', 0, '2017-09-08 11:48:46'),
(6, 1, '112.133.246.105', 'login', 0, '2017-09-08 17:01:39'),
(7, 1, '157.48.11.14', 'login', 0, '2017-09-09 04:22:39'),
(8, 1, '112.133.246.117', 'login', 0, '2017-09-09 08:08:02'),
(9, 1, '157.48.13.246', 'login', 0, '2017-09-09 09:46:12'),
(10, 1, '157.48.11.36', 'login', 0, '2017-09-09 10:21:49'),
(11, 1, '157.48.11.36', 'logout', 0, '2017-09-09 10:24:51'),
(12, 1, '157.48.11.205', 'login', 0, '2017-09-10 01:58:06'),
(13, 1, '157.48.21.251', 'login', 0, '2017-09-10 02:06:23'),
(14, 1, '157.48.13.149', 'login', 0, '2017-09-10 14:48:37'),
(15, 1, '157.48.22.155', 'login', 0, '2017-09-11 03:09:36'),
(16, 1, '157.48.22.155', 'login', 0, '2017-09-11 03:11:57'),
(17, 1, '112.133.246.125', 'login', 0, '2017-09-11 07:52:23'),
(18, 1, '157.48.12.255', 'login', 0, '2017-09-11 08:22:05'),
(19, 1, '157.48.22.10', 'logout', 0, '2017-09-11 08:42:18'),
(20, 1, '157.48.22.10', 'login', 0, '2017-09-11 08:42:29'),
(21, 1, '157.48.22.10', 'login', 0, '2017-09-11 11:09:49'),
(22, 1, '157.48.22.10', 'logout', 0, '2017-09-11 12:13:03'),
(23, 1, '112.133.246.116', 'logout', 0, '2017-09-11 12:30:35'),
(24, 1, '157.48.13.186', 'login', 0, '2017-09-12 02:45:36'),
(25, 1, '157.48.10.74', 'login', 0, '2017-09-12 04:31:49'),
(26, 1, '157.48.13.186', 'login', 0, '2017-09-12 05:40:47'),
(27, 1, '112.133.246.126', 'login', 0, '2017-09-12 07:03:13'),
(28, 1, '112.133.246.126', 'logout', 0, '2017-09-12 07:05:38'),
(29, 1, '157.48.13.186', 'logout', 0, '2017-09-12 07:20:29'),
(30, 1, '157.48.13.186', 'login', 0, '2017-09-12 08:45:42'),
(31, 1, '157.48.12.208', 'logout', 0, '2017-09-12 10:56:58'),
(32, 1, '157.48.20.25', 'login', 0, '2017-09-13 03:41:16'),
(33, 1, '157.48.10.48', 'login', 0, '2017-09-13 04:01:58'),
(34, 1, '157.48.20.95', 'logout', 0, '2017-09-13 05:25:19'),
(35, 1, '157.48.13.63', 'login', 0, '2017-09-14 09:06:23'),
(36, 1, '157.48.13.63', 'logout', 0, '2017-09-14 09:59:41'),
(37, 1, '112.133.246.106', 'login', 0, '2017-09-14 11:08:57'),
(38, 1, '157.48.23.137', 'login', 0, '2017-09-15 04:59:10'),
(39, 1, '157.48.23.137', 'login', 0, '2017-09-15 05:00:14'),
(40, 1, '157.48.23.137', 'login', 0, '2017-09-15 05:00:32'),
(41, 1, '112.133.246.110', 'login', 0, '2017-09-15 11:11:01'),
(42, 1, '157.48.22.83', 'login', 0, '2017-09-15 12:36:49'),
(43, 1, '157.48.13.241', 'login', 0, '2017-09-18 04:07:27'),
(44, 1, '45.127.57.134', 'login', 0, '2017-09-18 06:39:11'),
(45, 1, '45.127.57.134', 'login', 0, '2017-09-18 06:46:29'),
(46, 1, '45.127.57.134', 'logout', 0, '2017-09-18 06:57:25'),
(47, 1, '45.127.57.134', 'login', 0, '2017-09-18 07:00:35'),
(48, 1, '45.127.57.134', 'logout', 0, '2017-09-18 07:11:49'),
(49, 1, '45.127.57.134', 'login', 0, '2017-09-18 07:15:05'),
(50, 1, '45.127.57.134', 'logout', 0, '2017-09-18 07:19:56'),
(51, 4, '157.48.12.124', 'login', 0, '2017-09-18 09:20:20'),
(52, 4, '157.48.12.124', 'logout', 0, '2017-09-18 09:20:21'),
(53, 1, '157.48.12.124', 'login', 0, '2017-09-18 09:20:33'),
(54, 1, '45.127.57.134', 'logout', 0, '2017-09-18 09:23:22'),
(55, 1, '112.133.246.111', 'login', 0, '2017-10-05 08:43:57'),
(56, 1, '45.127.57.134', 'login', 0, '2017-10-11 10:13:55'),
(57, 1, '112.133.246.122', 'login', 0, '2017-10-11 17:10:56'),
(58, 1, '112.133.246.122', 'logout', 0, '2017-10-11 17:14:17'),
(59, 1, '183.82.4.41', 'login', 0, '2017-10-16 08:44:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_requests`
--
ALTER TABLE `change_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_request_files`
--
ALTER TABLE `change_request_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contractors`
--
ALTER TABLE `contractors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contractor_payments`
--
ALTER TABLE `contractor_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_payments`
--
ALTER TABLE `customer_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_contacts`
--
ALTER TABLE `department_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `external_payments`
--
ALTER TABLE `external_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `india_states`
--
ALTER TABLE `india_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_departments`
--
ALTER TABLE `item_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_staffs`
--
ALTER TABLE `item_staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_transfers`
--
ALTER TABLE `material_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_grns`
--
ALTER TABLE `order_grns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_contacts`
--
ALTER TABLE `organization_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_customers`
--
ALTER TABLE `product_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_items`
--
ALTER TABLE `product_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_properties`
--
ALTER TABLE `product_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_property_headers`
--
ALTER TABLE `product_property_headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stages`
--
ALTER TABLE `product_stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_managers`
--
ALTER TABLE `project_managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_types`
--
ALTER TABLE `report_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `service_payments`
--
ALTER TABLE `service_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_payment_types`
--
ALTER TABLE `service_payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_balance`
--
ALTER TABLE `supplier_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_contacts`
--
ALTER TABLE `supplier_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_payments`
--
ALTER TABLE `supplier_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_has_permissions`
--
ALTER TABLE `user_has_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `user_has_permissions_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `user_has_projects`
--
ALTER TABLE `user_has_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_has_roles`
--
ALTER TABLE `user_has_roles`
  ADD PRIMARY KEY (`role_id`,`user_id`),
  ADD KEY `user_has_roles_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_login_activities`
--
ALTER TABLE `user_login_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_login_activities_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank_accounts`
--
ALTER TABLE `bank_accounts`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `change_requests`
--
ALTER TABLE `change_requests`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `change_request_files`
--
ALTER TABLE `change_request_files`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contractors`
--
ALTER TABLE `contractors`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `contractor_payments`
--
ALTER TABLE `contractor_payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `customer_payments`
--
ALTER TABLE `customer_payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `department_contacts`
--
ALTER TABLE `department_contacts`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `external_payments`
--
ALTER TABLE `external_payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `india_states`
--
ALTER TABLE `india_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `item_departments`
--
ALTER TABLE `item_departments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `item_staffs`
--
ALTER TABLE `item_staffs`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `material_transfers`
--
ALTER TABLE `material_transfers`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `order_grns`
--
ALTER TABLE `order_grns`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `organization_contacts`
--
ALTER TABLE `organization_contacts`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_customers`
--
ALTER TABLE `product_customers`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_items`
--
ALTER TABLE `product_items`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_properties`
--
ALTER TABLE `product_properties`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `product_property_headers`
--
ALTER TABLE `product_property_headers`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `product_stages`
--
ALTER TABLE `product_stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `project_managers`
--
ALTER TABLE `project_managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `report_types`
--
ALTER TABLE `report_types`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `service_payments`
--
ALTER TABLE `service_payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `service_payment_types`
--
ALTER TABLE `service_payment_types`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `supplier_balance`
--
ALTER TABLE `supplier_balance`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier_contacts`
--
ALTER TABLE `supplier_contacts`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `supplier_payments`
--
ALTER TABLE `supplier_payments`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_has_projects`
--
ALTER TABLE `user_has_projects`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_login_activities`
--
ALTER TABLE `user_login_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
