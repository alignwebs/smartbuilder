<?php

use Illuminate\Database\Seeder;

class ServicePaymentTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('service_payment_types')->delete();
        
        \DB::table('service_payment_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Diesel',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Security',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Labour',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Salaries',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Misc',
            ),
        ));
        
        
    }
}