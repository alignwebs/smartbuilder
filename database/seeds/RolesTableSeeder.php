<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'Administrator',
                'description' => 'First Level User',
                'created_at' => '2017-06-06 17:54:53',
                'updated_at' => '2017-06-06 17:54:53',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'owner',
                'display_name' => 'Owner',
                'description' => 'Second Level User',
                'created_at' => '2017-06-06 18:09:07',
                'updated_at' => '2017-06-06 18:09:07',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'manager',
                'display_name' => 'Project Manager',
                'description' => 'Third Level User - Project Level',
                'created_at' => '2017-06-06 18:20:24',
                'updated_at' => '2017-06-06 18:20:24',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'customer',
                'display_name' => 'Customer',
                'description' => 'Fourth Level User',
                'created_at' => '2017-07-01 16:11:23',
                'updated_at' => '2017-07-01 16:11:23',
            ),
        ));
        
        
    }
}