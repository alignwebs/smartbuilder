<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(IndiaStatesTableSeeder::class);
        $this->call(PaymentMethodsTableSeeder::class);
        $this->call(PaymentTypesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(ReportTypesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(ServicePaymentTypesTableSeeder::class);
        $this->call(UserHasRolesTableSeeder::class);
    }
}
