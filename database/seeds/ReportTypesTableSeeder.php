<?php

use Illuminate\Database\Seeder;

class ReportTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('report_types')->delete();
        
        \DB::table('report_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Customer Details',
            ),
        ));
        
        
    }
}