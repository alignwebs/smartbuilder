<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'access_panel',
                'created_at' => '2017-06-06 18:01:27',
                'updated_at' => '2017-06-06 18:01:27',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'access_admin',
                'created_at' => '2017-06-13 10:38:52',
                'updated_at' => '2017-06-13 10:38:52',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'access_customer',
                'created_at' => '2017-07-01 16:13:36',
                'updated_at' => '2017-07-01 16:13:36',
            ),
        ));
        
        
    }
}