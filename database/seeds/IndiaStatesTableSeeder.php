<?php

use Illuminate\Database\Seeder;

class IndiaStatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('india_states')->delete();
        
        \DB::table('india_states')->insert(array (
            0 => 
            array (
                'id' => 1,
                'statename' => 'Andhra Pradesh',
            ),
            1 => 
            array (
                'id' => 2,
                'statename' => 'Assam',
            ),
            2 => 
            array (
                'id' => 3,
                'statename' => 'Arunachal Pradesh',
            ),
            3 => 
            array (
                'id' => 4,
                'statename' => 'Gujrat',
            ),
            4 => 
            array (
                'id' => 5,
                'statename' => 'Bihar',
            ),
            5 => 
            array (
                'id' => 6,
                'statename' => 'Haryana',
            ),
            6 => 
            array (
                'id' => 7,
                'statename' => 'Himachal Pradesh',
            ),
            7 => 
            array (
                'id' => 8,
                'statename' => 'Jammu & Kashmir',
            ),
            8 => 
            array (
                'id' => 9,
                'statename' => 'Karnataka',
            ),
            9 => 
            array (
                'id' => 10,
                'statename' => 'Kerala',
            ),
            10 => 
            array (
                'id' => 11,
                'statename' => 'Madhya Pradesh',
            ),
            11 => 
            array (
                'id' => 12,
                'statename' => 'Maharashtra',
            ),
            12 => 
            array (
                'id' => 13,
                'statename' => 'Manipur',
            ),
            13 => 
            array (
                'id' => 14,
                'statename' => 'Meghalaya',
            ),
            14 => 
            array (
                'id' => 15,
                'statename' => 'Mizoram',
            ),
            15 => 
            array (
                'id' => 16,
                'statename' => 'Nagaland',
            ),
            16 => 
            array (
                'id' => 17,
                'statename' => 'Orissa',
            ),
            17 => 
            array (
                'id' => 18,
                'statename' => 'Punjab',
            ),
            18 => 
            array (
                'id' => 19,
                'statename' => 'Rajasthan',
            ),
            19 => 
            array (
                'id' => 20,
                'statename' => 'Sikkim',
            ),
            20 => 
            array (
                'id' => 21,
                'statename' => 'Tamil Nadu',
            ),
            21 => 
            array (
                'id' => 22,
                'statename' => 'Tripura',
            ),
            22 => 
            array (
                'id' => 23,
                'statename' => 'Uttar Pradesh',
            ),
            23 => 
            array (
                'id' => 24,
                'statename' => 'West Bengal',
            ),
            24 => 
            array (
                'id' => 25,
                'statename' => 'Goa',
            ),
            25 => 
            array (
                'id' => 26,
                'statename' => 'Pondichery',
            ),
            26 => 
            array (
                'id' => 27,
                'statename' => 'Lakshdweep',
            ),
            27 => 
            array (
                'id' => 28,
                'statename' => 'Daman & Diu',
            ),
            28 => 
            array (
                'id' => 29,
                'statename' => 'Dadra & Nagar',
            ),
            29 => 
            array (
                'id' => 30,
                'statename' => 'Chandigarh',
            ),
            30 => 
            array (
                'id' => 31,
                'statename' => 'Andaman & Nicobar',
            ),
            31 => 
            array (
                'id' => 32,
                'statename' => 'Uttaranchal',
            ),
            32 => 
            array (
                'id' => 33,
                'statename' => 'Jharkhand',
            ),
            33 => 
            array (
                'id' => 34,
                'statename' => 'Chattisgarh',
            ),
            34 => 
            array (
                'id' => 35,
                'statename' => 'Assam',
            ),
            35 => 
            array (
                'id' => 36,
                'statename' => 'Telangana',
            ),
        ));
        
        
    }
}