<?php

use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_methods')->delete();
        
        \DB::table('payment_methods')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Cheque',
                'created_at' => '2017-08-12 15:36:37',
                'updated_at' => '2017-08-12 15:36:37',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'NEFT',
                'created_at' => '2017-08-12 15:36:37',
                'updated_at' => '2017-08-12 15:36:37',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Cash',
                'created_at' => '2017-08-24 17:58:36',
                'updated_at' => '2017-08-24 17:58:36',
            ),
        ));
        
        
    }
}