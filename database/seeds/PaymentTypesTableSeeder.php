<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_types')->delete();
        
        \DB::table('payment_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Material Payment',
                'created_at' => '2017-08-12 15:23:44',
                'updated_at' => '2017-08-12 15:23:44',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Contractor Payment',
                'created_at' => '2017-08-12 15:23:44',
                'updated_at' => '2017-08-12 15:23:44',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Service Payment',
                'created_at' => '2017-08-12 15:23:44',
                'updated_at' => '2017-08-12 15:23:44',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'External Payment',
                'created_at' => '2017-08-12 15:23:44',
                'updated_at' => '2017-08-12 15:23:44',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Customer Payment',
                'created_at' => '2017-08-27 14:48:56',
                'updated_at' => '2017-08-27 14:48:56',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'External Income',
                'created_at' => '2017-09-11 14:43:46',
                'updated_at' => '2017-09-11 14:43:46',
            ),
        ));
        
        
    }
}