<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Administrator',
                'email' => 'alignwebs@gmail.com',
                'password' => '$2y$10$TDjvSojuhoLsTRZGHnBdzeJBzFiszWB/Pilvou1LKcyGusv/NVZjq',
                'remember_token' => 'glMMPKJpKuc0MZqMnhQfhKLuPNxT9T0rsK08pAypJbZoEUGPgCmz6F48xmx0',
                'created_at' => '2017-09-08 12:40:48',
                'updated_at' => '2017-09-08 12:40:48',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}