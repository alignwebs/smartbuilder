<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id');
			$table->integer('org_id');
			$table->string('proj_code');
			$table->string('proj_name');
			$table->string('address_one')->nullable();
			$table->string('address_two')->nullable();
			$table->string('city')->nullable();
			$table->string('district')->nullable();
			$table->string('state')->nullable();
			$table->integer('pincode')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
