<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->integer('project_id');
			$table->integer('order_id');
			$table->integer('item_id');
			$table->string('item_code');
			$table->string('item_name');
			$table->integer('supplier_id');
			$table->string('supplier_code');
			$table->string('supplier_name');
			$table->integer('qty');
			$table->string('unit');
			$table->decimal('unit_price', 10)->nullable();
			$table->integer('discount')->nullable();
			$table->string('tax_name')->nullable();
			$table->decimal('tax', 10)->nullable();
			$table->decimal('net_unit_price', 10)->nullable();
			$table->decimal('total_price', 10)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_items');
	}

}
