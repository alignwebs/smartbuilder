<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChangeRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('change_requests', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->integer('project_id');
			$table->integer('product_id');
			$table->integer('customer_id');
			$table->integer('user_id');
			$table->string('title');
			$table->text('instruction', 65535);
			$table->string('date', 10);
			$table->string('status')->nullable()->default('pending');
			$table->decimal('charge', 10)->nullable();
			$table->integer('payment_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('change_requests');
	}

}
