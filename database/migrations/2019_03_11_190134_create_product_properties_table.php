<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductPropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_properties', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_header_id');
			$table->string('property_name');
			$table->string('property_type');
			$table->string('qty')->nullable();
			$table->string('unit')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_properties');
	}

}
