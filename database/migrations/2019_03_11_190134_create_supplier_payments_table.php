<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSupplierPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supplier_payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->integer('project_id');
			$table->integer('supplier_id');
			$table->integer('payment_id')->nullable();
			$table->enum('type', array('advance','order','grn'));
			$table->integer('order_id')->nullable();
			$table->integer('grn_id')->nullable();
			$table->float('amount', 10);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('supplier_payments');
	}

}
