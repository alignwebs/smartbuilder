<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->integer('project_id');
			$table->string('code');
			$table->string('name');
			$table->string('email');
			$table->string('phone');
			$table->string('address_one')->nullable();
			$table->string('address_two')->nullable();
			$table->string('city')->nullable();
			$table->string('district')->nullable();
			$table->string('state')->nullable();
			$table->string('pincode', 10)->nullable();
			$table->string('meta_tin')->nullable();
			$table->string('meta_bank_acc')->nullable();
			$table->string('meta_bank_name')->nullable();
			$table->string('meta_bank_branch')->nullable();
			$table->string('meta_bank_acc_type')->nullable();
			$table->string('meta_bank_ifsc')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suppliers');
	}

}
