<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductStagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_stages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->integer('project_id');
			$table->integer('product_id');
			$table->integer('stage_id');
			$table->integer('contractor_id');
			$table->integer('department_id');
			$table->decimal('rate', 10)->nullable();
			$table->string('date', 10);
			$table->string('remark')->nullable();
			$table->integer('payment_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_stages');
	}

}
