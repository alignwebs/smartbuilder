<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->integer('project_id');
			$table->integer('tax_id')->nullable();
			$table->string('code');
			$table->string('name');
			$table->string('description')->nullable();
			$table->string('unit');
			$table->decimal('unit_price', 10);
			$table->decimal('discount', 10)->nullable()->default(0.00);
			$table->integer('roq')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}
