<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('gen_payment_id')->nullable();
			$table->integer('user_id');
			$table->integer('org_id');
			$table->integer('project_id');
			$table->integer('bank_id');
			$table->integer('payment_method_id');
			$table->string('payment_method_ref')->nullable();
			$table->integer('payment_type')->nullable();
			$table->float('amount', 10)->nullable();
			$table->enum('type', array('debit','credit'));
			$table->string('paid_to')->nullable();
			$table->string('paid_by')->nullable();
			$table->string('approved_by')->nullable();
			$table->integer('ref_id')->nullable();
			$table->string('remark')->nullable();
			$table->date('date');
			$table->string('checksum')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
