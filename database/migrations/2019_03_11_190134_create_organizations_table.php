<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organizations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id');
			$table->string('org_code', 50);
			$table->string('org_name');
			$table->string('org_address_one')->nullable();
			$table->string('org_address_two')->nullable();
			$table->string('org_city')->nullable();
			$table->string('org_district')->nullable();
			$table->string('org_state')->nullable();
			$table->string('org_pincode', 6)->nullable();
			$table->string('org_tin', 50)->nullable();
			$table->string('org_billing_address')->nullable();
			$table->string('logo')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organizations');
	}

}
