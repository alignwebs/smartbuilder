<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartmentContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('department_contacts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('department_id');
			$table->string('name');
			$table->string('phone', 50)->nullable();
			$table->string('email')->nullable();
			$table->string('address')->nullable();
			$table->string('state')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('department_contacts');
	}

}
