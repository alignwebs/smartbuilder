<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('organization_contacts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('org_id');
			$table->string('name');
			$table->string('phone', 50)->nullable();
			$table->string('email')->nullable();
			$table->string('address')->nullable();
			$table->string('state')->nullable();
			$table->integer('pincode')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('organization_contacts');
	}

}
