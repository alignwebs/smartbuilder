<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    static public function scopeofProjectCategory($query,$product_category_id)
    {
    	return $query->where('product_category_id',$product_category_id);
    }

    public function department()
    {
    	return $this->belongsTo('App\Department','department_id');
    }

    public function productcategory()
    {
    	return $this->belongsTo('App\ProductCategory','product_category_id');
    }

    public function setCodeAttribute($value)
    {
      $this->attributes['code'] = strtoupper($value);
    }

    public function setNameAttribute($value)
    {
      $this->attributes['name'] = ucwords($value);
    }

}
