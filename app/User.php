<?php

namespace App;

use App\UserHasProject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];
    
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function addProject($projectid)
    {
        return UserHasProject::firstOrCreate(['user_id'=>$this->id],['project_id'=>$projectid]);
    }

    public function detail()
    {
        return $this->hasOne('App\UserDetail','user_id','id');
    }


    public function isOwner()
    {
        return ($this->hasRole('owner') ? true : false);
    }

    public function isAdmin()
    {
        return ($this->hasRole('admin') ? true : false);
    }

    public function isManager()
    {
        return ($this->hasRole('manager') ? true : false);
    }
}
