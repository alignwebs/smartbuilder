<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

use Auth;

use App\UserHasProject;

class Organization extends Model
{
      use SoftDeletes;
      use \Venturecraft\Revisionable\RevisionableTrait;

      protected $dates = ['deleted_at'];
      protected $revisionCreationsEnabled = true;

      protected $appends = array('orgNameCode');



      protected static function boot()
      {
          parent::boot();

          /*GLOBAL SCOPE FOR PREVENTING USER FROM VIEWING OR EDITING OTHER USERS ORGANIZATION*/

          static::addGlobalScope('is_user', function (Builder $builder) {
          	
          	$user = User::find(Auth::id());

          	 if($user->isOwner() or $user->isAdmin())
      				{
      					$builder->where('user_id', '=', Auth::id());
      				}

      			if($user->isManager())
        			{
        				$UserProjectId = UserHasProject::where('user_id',Auth::id())->get()->pluck('project_id');

        				$Project = Project::find($UserProjectId)->pluck('org_id');
                
                $builder->where('id', $Project);
        			}
              
          });

          /*ENDS*/
      }


      public function getOrgNameCodeAttribute()
      {
      		return $this->org_name.": ".$this->org_code;
      }

      public function projects()
      {
        return $this->hasMany('App\Project','org_id');
      }

      public function contacts()
      {
        return $this->hasMany('App\OrganizationContact','org_id');
      }

      public function setOrgCodeAttribute($value)
      {
          $this->attributes['org_code'] = strtoupper($value);
      }

      public function setOrgNameAttribute($value)
      {
          $this->attributes['org_name'] = ucwords($value);
      }
}
