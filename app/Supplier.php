<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];

    public function contacts()
    {
    	return $this->hasMany('App\SupplierContact','supplier_id');
    }

    public function items()
    {
    	return $this->hasMany('App\ItemSupplier','supplier_id');
    }

    public function setCodeAttribute($value)
    {
      $this->attributes['code'] = strtoupper($value);
    }

    public function setNameAttribute($value)
    {
      $this->attributes['name'] = ucwords($value);
    }

    static public function scopeofOrg($query,$org_id="")
    {
      $org_id = (!empty($org_id) ? $org_id : getOrganization('org_id'));

      return $query->where('org_id',$org_id);
    }
}
