<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
   protected $appends = ['fullname'];

   public function getFullnameAttribute()
   {
   		return $this->name." - ".$this->percent."%";
   }

   static public function scopeofOrg($query,$org_id="")
   {
      $org_id = (!empty($org_id) ? $org_id : getOrganization('org_id'));

      return $query->where('org_id',$org_id);
   }
}
