<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStage extends Model
{
	static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage','stage_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product','product_id');
    }

    public function contractor()
    {
        return $this->belongsTo('App\Contractor','contractor_id');
    }


}
