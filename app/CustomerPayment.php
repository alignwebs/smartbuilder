<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerPayment extends Model
{
 	public function changerequest()
 	{
 		return $this->belongsTo('App\ChangeRequest', 'change_request_id');
 	}

 	public function customer()
 	{
 		return $this->belongsTo('App\Customer', 'customer_id');
 	}
}
