<?php 

if (! function_exists('getOrganization')) {
    function getOrganization($key)
    {
        return Session::get('selectedOrg')[$key];
    }
}

if (! function_exists('getProject')) {
    function getProject($key)
    {
        return Session::get('selectedProject')[$key];
    }
}

if (! function_exists('getIndiaStates')) {

 	function getIndiaStates()
    {
        $states = App\IndiaStates::orderby('statename','asc')->get()->pluck('statename','statename');

        return $states;
    }
}

if (! function_exists('deleteEditModelBtn')) {

    function deleteEditModelBtn($route,$id,$title="")
    {
        $html = '<div class="btn-group">';
        $html.= '<a class="btn btn-link btn-default btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-ellipsis-h"></span></a>';
        $html.= '<div class="dropdown-menu">';
        $html.= '<a href="'.URL::route($route.'.edit',$id).'" class="dropdown-item"><span class="fa fa-edit" aria-hidden="true"></span> Edit</a>';
        $html.= Form::open(['route' => [$route.'.destroy', $id], 'method' => 'delete', 'class' => 'deleteFrm']);
        $html.= '<button type="submit" class="dropdown-item btn-delete" data-title="'.$title.'"><span class="fa fa-trash" aria-hidden="true"></span> Delete</button>';
        $html.= Form::close();
     
        $html.= '</div>';
        $html.= '</div>';
        return $html;
    }
}

if (! function_exists('currency')) {

    function currency($num) {

        if(is_array($num)) {

            foreach ($num as $key => $val) {
                $arr[$key] = currency($val);
            }

            return $arr;
        }
            $fmt = new NumberFormatter( 'en_US', NumberFormatter::CURRENCY );
            
            $fmt->setPattern( str_replace('¤#','¤ #', $fmt->getPattern() ) );

            $thecash = $fmt->formatCurrency($num, "INR");

            $thecash = str_replace("₹", "Rs ", $thecash);
            
            return $thecash; // writes the final format where $currency is the currency symbol.
        }

}

if (! function_exists('currency_to_word')) {

    function currency_to_word($num) {
 
        $number = $num;
           $no = round($number);
           $point = round($number - $no, 2) * 100;
           $hundred = null;
           $digits_1 = strlen($no);
           $i = 0;
           $str = array();
           $words = array('0' => '', '1' => 'one', '2' => 'two',
            '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
            '7' => 'seven', '8' => 'eight', '9' => 'nine',
            '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
            '13' => 'thirteen', '14' => 'fourteen',
            '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
            '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
            '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
            '60' => 'sixty', '70' => 'seventy',
            '80' => 'eighty', '90' => 'ninety');
           $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
           while ($i < $digits_1) {
             $divider = ($i == 2) ? 10 : 100;
             $number = floor($no % $divider);
             $no = floor($no / $divider);
             $i += ($divider == 10) ? 1 : 2;
             if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number] .
                    " " . $digits[$counter] . $plural . " " . $hundred
                    :
                    $words[floor($number / 10) * 10]
                    . " " . $words[$number % 10] . " "
                    . $digits[$counter] . $plural . " " . $hundred;
             } else $str[] = null;
          }
          $str = array_reverse($str);
          $result = implode('', $str);
          $points = ($point) ?
            "." . $words[$point / 10] . " " . 
                  $words[$point = $point % 10] : '';
          return ucwords($result) . "Rupees";

    }
}

 ?>
