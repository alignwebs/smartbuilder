<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
	use \Venturecraft\Revisionable\RevisionableTrait;
    use SoftDeletes;
    
    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];

    public function suppliers()
    {
    	return $this->hasMany('App\ItemSupplier','item_id');
    }

    public function departments()
    {
    	return $this->hasMany('App\ItemDepartment','item_id','id');
    }

    public function staff()
    {
        return $this->hasMany('App\ItemStaff','item_id');
    }
    
    public function tax()
    {
        return $this->belongsTo('App\Tax','tax_id');
    }

    public function grn()
    {
        return $this->hasMany("App\OrderGrn","item_id");
    }

    public function setCodeAttribute($value)
    {
      $this->attributes['code'] = strtoupper($value);
    }

    public function setNameAttribute($value)
    {
      $this->attributes['name'] = ucwords($value);
    }
    static public function scopeofProject($query,$project_id="")
    {
        $project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

        return $query->where('project_id',$project_id);
    }

    static public function scopeofOrg($query,$org_id="")
    {
      $org_id = (!empty($org_id) ? $org_id : getOrganization('org_id'));

      return $query->where('org_id',$org_id);
    }
}
