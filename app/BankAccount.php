<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{

   static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));
 
    	return $query->where('project_id',$project_id);
    }

  	static public function balance($bank_account_id)
  	{
  		 $fetch = Self::select('balance')->find($bank_account_id);

  		 return $fetch->balance;

  	}

  	static public function updateBankBalance($bank_account_id,$balance)
    {
    	$bank_account = Self::find($bank_account_id);
    	$bank_account->balance = $balance;
    	$bank_account->save();
    }

}
