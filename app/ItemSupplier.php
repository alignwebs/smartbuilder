<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemSupplier extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    

    public function item()
    {
    	return $this->belongsTo('App\Item','item_id','id');
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier','supplier_id');
    }

    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }
}
