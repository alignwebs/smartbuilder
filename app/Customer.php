<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    
    use SoftDeletes;

    public function getNameAttribute($value)
    {
    	return ucwords($value);
    }

   static public function scopeofProject($query,$project_id="")
   {
      $project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

      return $query->where('project_id',$project_id);
   }

    static public function scopeofOrg($query,$org_id="")
    {
      $org_id = (!empty($org_id) ? $org_id : getOrganization('org_id'));

      return $query->where('org_id',$org_id);
    }

   public function products()
    {
    	return $this->hasMany('App\ProductCustomer','customer_id');
    }
}
