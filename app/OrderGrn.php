<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\OrderItem;

class OrderGrn extends Model
{
    public function item()
    {
    	return $this->belongsTo('App\OrderItem','item_row_id','id');
    }

    public function order()
    {
      return $this->belongsTo('App\Order','order_id','id');
    }

    static public function prepareGrn($orderid="")
    {
		$data =  DB::table('order_items as oi')
						->rightJoin('order_grns as og','og.item_row_id','=','oi.id')
						->select(DB::raw('oi.qty-SUM(COALESCE(og.qty,0)) as qty_left'),DB::raw('SUM(COALESCE(og.qty,0)) as qty_avail'), "oi.*")
						->groupBy('og.item_row_id')
						->where('oi.order_id',$orderid)
						->get();
		if($data->count() == 0)
			$data =  DB::table('order_items as oi')
						->select("oi.*", DB::raw("qty as qty_left"),DB::raw("0 as qty_avail"))
						->where('oi.order_id',$orderid)
						->get();
			
		return $data;
    }

    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

   static public function getGrns($grn_ids)
   {
   		return Self::select('id','order_id' ,'total_price')->whereIn('id',$grn_ids)->orderBy('created_at','asc')->get();
   }
    
}
