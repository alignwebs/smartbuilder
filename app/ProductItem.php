<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    
   	static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    public function department()
    {
    	return $this->belongsTo('App\Department','department_id');
    }

    public function item()
    {
    	return $this->belongsTo('App\OrderItem','item_id','item_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','user_id');
    }
}
