<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPropertyHeader extends Model
{
   	 use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    static public function scopeofProject($query,$project_id="")
    {
        $project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

        return $query->where('project_id',$project_id);
    }
}
