<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeRequestFile extends Model
{
    public $timestamps = false;
}
