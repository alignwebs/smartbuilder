<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\OrderGrn;
use App\ProductItem;

class Inventory extends Model
{
	protected $table = "inventory";

	public function item()
    {
    	return $this->belongsTo('App\OrderItem','item_id','item_id');
    }

    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }
	
	static public function scopeinStock($query)
	{
		return $query->where('avail_qty', ">" , 0);
	}

    static public function itemStatus($item_id)
    {
    	if($item_id > 0)
    	{
    		$where['org_id'] = getOrganization('org_id');
    		$where['project_id'] = getProject('proj_id');
    		$where['item_id'] = $item_id;
    	
    		$data['total_qty'] = OrderGrn::select('qty')->ofProject($where['project_id'])->where('item_id',$item_id)->sum('qty');
    		$data['used_qty'] = ProductItem::select('qty')->ofProject($where['project_id'])->where('item_id',$item_id)->sum('qty');
    		$data['avail_qty'] = $data['total_qty'] - $data['used_qty'];

    		$inventory = Inventory::updateOrInsert($where,$data);

    		return $inventory->first();

    	}
    }

    static public function updateInventory($project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	$grnItems = OrderGrn::select('item_id')->ofProject($project_id)->groupBy('item_id')->get();

    	foreach ($grnItems as $item) {
    		Self::itemStatus($item->item_id);
    	}

    }	

    
}
