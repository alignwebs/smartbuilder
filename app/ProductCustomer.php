<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCustomer extends Model
{
    use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];


    public function product()
    {
    	return $this->belongsTo('App\Product','product_id');
    }

    public function customer()
    {
    	return $this->belongsTo('App\Customer','customer_id');
    }

}
