<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Order extends Model
{
	use SoftDeletes;
	use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];
    
  
	public function items()
	{
		return $this->hasMany("App\OrderItem");
	}

	public function draftItems()
	{
		return $this->hasMany("App\OrderItem")->select("supplier_id as supplierid", "supplier_code as suppliercode","supplier_name as suppliername", "item_id as itemid","item_code as itemcode", "item_name as itemname", "unit as itemunit", "qty as itemqty" ,"unit_price as itemunitprice", "discount as itemdiscount", "tax_name as taxname", "tax","net_unit_price as itemnetunitprice", "total_price as total");
	}
  
	public function user()
	{
		return $this->belongsTo("App\User","user_id");
	}

	public function grn()
	{
		return $this->hasMany("App\OrderGrn","order_id");
	}

	public function grnItemQty($itemid)
	{
		return $this->grn()->where('item_id',$itemid)->sum('qty');
	}

	static public function scopeofProject($query,$project_id="")
   	{
      	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

      	return $query->where('project_id',$project_id);
   	}

    static public function scopeisDraft($query)
    {
      return $query->where('draft', 1);
    }

   	static public function orderSuppliers($order_id)
   	{
   		return OrderItem::where('order_id',$order_id)->groupBy('supplier_id')->pluck('supplier_name','supplier_id');
   	}

   	static public function orderGrnSuppliers($order_id)
   	{
   		$items = OrderGrn::select('item_id')->where('order_id',$order_id)->groupBy('item_id')->get()->toArray();
   		
   		return OrderItem::whereIn('item_id',$items)->where('order_id',$order_id)->groupBy('supplier_id')->pluck('supplier_name','supplier_id');

   	}

   	static public function ordersByGrn()
   	{
   		$order_ids = OrderGrn::select('order_id')->ofProject()->groupBy('order_id')->get()->toArray();

   		return Order::select('code','id')->whereIn('id',$order_ids)->pluck('code','id');
   	}

   	static public function suppliersGrn($supplier_id,$order_id="")
   	{
      $query = new OrderItem;

      $query = $query->select('id')->ofProject()->where('supplier_id',$supplier_id);

      if($order_id != "")
      {
        $query = $query->where('order_id',$order_id);
      }
   		
      $query = $query->get()->toArray();
      
   		return OrderGrn::select('id')->whereIn('item_row_id',$query)->get()->toArray();
   	}


}
