<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class SupplierPayment extends Model
{
    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    static public function total($supplier_id)
    {
    	return Self::ofProject()->where('supplier_id',$supplier_id)->sum('amount');
    }

    static public function Balance($supplier_id)
    {
       return Self::ofProject()->where('type','advance')->where('supplier_id',$supplier_id)->sum('amount');
    }

    static public function paidGrnAmount($supplier_id,$order_id="")
    {
        $query = new Self;
        $query = $query->ofProject()->where('type','grn')->where('supplier_id',$supplier_id);

        if($order_id != "")
            $query = $query->where('order_id',$order_id);

        return $query->sum('amount');
    }


    static public function stats($supplier_id,$order_id="")
    {
        $Order_Amount   = 0;

		$Supplier_AllGrn = array_flatten(Order::suppliersGrn($supplier_id,$order_id));

        $Supplier_GrnAmount = OrderGrn::whereIn('id',$Supplier_AllGrn)->sum('total_price');

        $Supplier_PaidGrnAmount = Self::paidGrnAmount($supplier_id,$order_id);
        
        $Supplier_PaidAdvanceAmount = Self::supplierBalance($supplier_id);

        if($order_id != "")
        {
            $Order = Order::find($order_id);
            $Order_Amount = $Order->invoice_total;
            $Order_Amount_Unpaid    =   $Order_Amount - $Supplier_PaidGrnAmount;
        }

       	$data['grn_total']	=	$Supplier_GrnAmount;
        $data['grn_paid'] 	= 	$Supplier_PaidGrnAmount;
 		$data['grn_due'] 	=   $data['grn_total'] - $data['grn_paid'];
        $data['order_total']  =   $Order_Amount;
        $data['order_due']     =   $Order_Amount_Unpaid;
        $data['order_paid']     =   $data['order_total'] - $data['order_due'];;
        $data['advance']    =   $Supplier_PaidAdvanceAmount;

        return $data;
    }

    static public function supplierGrnStats($supplier_id,$order_id="")
    {
    	$Supplier_PaidGrn = Self::select('grn_id')->ofProject()->where('supplier_id',$supplier_id);

        if($order_id != "")
        {
            $Supplier_PaidGrn = $Supplier_PaidGrn->where('order_id',$order_id);
        }

        $Supplier_PaidGrn = $Supplier_PaidGrn->get()->toArray();
	   
        $Supplier_AllGrn = Order::suppliersGrn($supplier_id,$order_id);

	    $Supplier_UnPaidGrnIDs = array_diff(array_flatten($Supplier_AllGrn),array_flatten($Supplier_PaidGrn));

        $data['paid']   =   $Supplier_PaidGrn;
        $data['unpaid'] =   $Supplier_UnPaidGrnIDs;
        $data['all']    =   $Supplier_AllGrn;

	    return $data;
	  
    }


    static public function addTransaction($params=[])
    { 
        $org_id     = getOrganization('org_id');
        $project_id = getProject('proj_id');

        $PaySettings['org_id']              = $org_id;
        $PaySettings['project_id']          = $project_id;
        $PaySettings['supplier_id']         = $params['supplier_id'];
        $PaySettings['payment_id']          = $params['payment_id'];
        $PaySettings['type']                = $params['type'];
        $PaySettings['amount']              = $params['amount'];

        Self::insert($PaySettings);
    }


}
