<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDepartment extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;

    public $timestamps = false;

    protected $fillable = ['item_id','department_id'];

    public function item()
    {
    	return $this->belongsTo('App\Items','item_id','id');
    }
    
    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }
}
