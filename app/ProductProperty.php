<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{
	use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    //protected $appends = ['property_header_name'];

  	public function ProductPropertyHeader()
  	{
  		return $this->hasOne('product_property_headers');
  	}
}
