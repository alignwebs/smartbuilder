<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Payment extends Model
{

    public function bank()
    {
        return $this->belongsTo('App\BankAccount','bank_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }


    public function paymentMethod()
    {
        return $this->belongsTo('App\PaymentMethod','payment_method_id');
    }

    public function paymentType()
    {
        return $this->belongsTo('App\PaymentType','payment_type');
    }

    public function materialpayment_grns()
    {
        return $this->hasMany('App\OrderGrn','payment_id');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier','ref_id');
    }

    public function supplier_payment()
    {
        return $this->hasMany('App\SupplierPayment','payment_id');
    }

    public function contractor()
    {
        return $this->belongsTo('App\Contractor','ref_id');
    }

    public function contractor_payment()
    {
        return $this->hasMany('App\ContractorPayment','payment_id');
    }

    public function contractor_product_stages()
    {
        return $this->hasMany('App\ProductStage','payment_id');
    }

    public function customer_payment()
    {
        return $this->hasMany('App\CustomerPayment','payment_id');
    }
    
    public function external_payment()
    {
        return $this->hasOne('App\ExternalPayment','payment_id');
    }


    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    static public function addTransaction($params=[])
    {
    	$user_id = Auth::id();
    	$org_id = getOrganization('org_id');
    	$project_id = getProject('proj_id');

        	$transaction = new Self();

    		$transaction->user_id			=	$user_id;
    		$transaction->org_id			=	$org_id;
    		$transaction->project_id		=	$project_id;
    		$transaction->bank_id			=	$params['bank_account_id'];
    		$transaction->payment_type		=	$params['payment_type_id'];
    		$transaction->payment_method_id	=	$params['payment_method_id'];
            $transaction->payment_method_ref =   $params['payment_method_ref'];
    		$transaction->amount			=	$params['amount'];
    		$transaction->type				=	$params['type'];
    		$transaction->remark			=	$params['remark'];
    		$transaction->date				=	$params['date'];
            $transaction->paid_to           =  (isset($params['paid_to']) ? $params['paid_to'] : null);
            $transaction->paid_by           =  (isset($params['paid_by']) ? $params['paid_by'] : null);
            $transaction->ref_id            =   $params['ref_id'];
            $transaction->approved_by       =  (isset($params['approved_by']) ? $params['approved_by'] : null);

    		$transaction->save();

            $transaction->gen_payment_id = getProject('proj_code').date('Ymd').str_pad($transaction->id, 4, "0",STR_PAD_LEFT);

            $transaction->save();

    		Self::calculateBankBalance($params['bank_account_id']);

    		return $transaction->id;

    }

    static public function calculateBankBalance($bank_account_id)
    {
    	$transactions = Self::ofProject()->where('bank_id',$bank_account_id)->get();

    	$data['credits']	=	$transactions->where('type','credit')->sum('amount');
    	$data['debits']		=	$transactions->where('type','debit')->sum('amount');
    	$data['balance']	=	$data['credits'] - $data['debits'];

    	BankAccount::updateBankBalance($bank_account_id,$data['balance']);

    	return $data;

    }

    static public function PayAmount($data,$amount)
    {
           
        return Self::addTransaction($data['bank_account_id'],$data['payment_type_id'],$data['payment_subtype_id'],$amount,$data['type']="debit",$data['remark'],$data['date']);
     
    }


    static public function bankTransaction($bank_id)
    {
        $Payments = Self::where('bank_id',$bank_id)->get();

        return $Payments;
    }

     
}
