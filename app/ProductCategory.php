<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
   use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    
   protected $appends = ['property_header_title'];

   public function getPropertyHeaderTitleAttribute()
   {
   		if($this->product_properties_header_id != NULL)
   		{
   			$PropertyHeader = ProductPropertyHeader::find($this->product_properties_header_id);

   			return $PropertyHeader->name;
   		}
   }

   public function propertyheader()
   {
   		return $this->hasOne('App\ProductPropertyHeader','id','product_properties_header_id');
   }

   public function products()
   {
      return $this->hasMany('App\Product','product_category_id');
   }
   
   static public function scopeofProject($query,$project_id="")
   {
      $project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

      return $query->where('project_id',$project_id);
   }

   public function stages()
   {
      return $this->hasMany('App\Stage','product_category_id');
   }


}
