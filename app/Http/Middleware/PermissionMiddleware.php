<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class PermissionMiddleware
{
    public function handle($request, Closure $next,  $permission)
    {
        if (Auth::guest()) {
            return redirect('/');
        }

        if (! $request->user()->can($permission)) {
           Auth::logout();
           return redirect('/');
        }

        return $next($request);
    }
}
