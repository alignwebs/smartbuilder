<?php

namespace App\Http\Middleware;

use Closure;

use Auth;

use App\User;
use App\Project;
use App\UserHasProject;
use App\Http\Controllers\ProjectController; 
use App\Http\Controllers\OrganizationController; 


class OrgProjectIfSet
{
    /**
     * Handle an incoming request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $user = User::find(Auth::id());

       

        if($user->hasRole('owner') or $user->hasRole('admin'))
        {
            if(empty($request->session()->get('selectedOrg')))
            {
                return redirect()->route('OrgSelect');
            }

            if(!empty($request->session()->get('selectedOrg')))
            {
                if(empty($request->session()->get('selectedProject')))
                    return redirect()->route('ProjectSelect');
            }
        }
        
        if($user->hasRole('manager'))
        {
           $UserProjectId = UserHasProject::where('user_id',Auth::id())->pluck('project_id');

            $Project = Project::find($UserProjectId)->first();
           
            $OrganizationController = new OrganizationController();
            $OrganizationController->setOrganization($Project->org_id);

            $ProjectController = new ProjectController();
            $ProjectController->setProject($Project->id);
        }

        return $next($request);
    }

}
