<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
class RoleMiddleware
{  
    public function handle($request, Closure $next, $role, $permission=null)
    {
        if (Auth::guest()) {
            return redirect('/');
        }

        $role = is_array($role)
            ? $role
            : explode('|', $role);

        if (! $request->user()->hasAnyRole($role)) {
            abort(403);
        }

        if ($permission && ! $request->user()->can($permission)) {
            abort(403);
        }

        return $next($request);
    }
}
