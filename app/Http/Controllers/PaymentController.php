<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use Datatables;
use Form;
use PDF;
use URL;

use App\Organization;
use App\Project;
use App\Payment;
use App\PaymentType;
use App\PaymentMethod;
use App\BankAccount;
use App\Supplier;
use App\SupplierPayment;
use App\Contractor;
use App\ContractorPayment;
use App\ServicePayment;
use App\ServicePaymentType;
use App\ExternalPayment;
use App\CustomerPayment;
use App\Order;
use App\OrderGrn;
use App\Product;
use App\ProductStage;
use App\Customer;
use App\ChangeRequest;


class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Payments = Payment::ofProject()->get();

        return view('payment.index', compact('Payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $PaymentTypes = PaymentType::pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');

        return view('payment.add', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts]);
    }

    public function materialPayment()
    {
        $payment_type_id = 1; // MATERIAL PAYMENT
        $PaymentTypes = PaymentType::where('id',$payment_type_id)->pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');
        $PaymentMethods = PaymentMethod::pluck('name','id');
        $Suppliers = Supplier::ofOrg()->pluck('name','id');

        return view('payment.materialpayment.add', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts, 'payment_type_id' => $payment_type_id, 'PaymentMethods' => $PaymentMethods, 'Suppliers'=>$Suppliers]);
    }

    public function contractorPayment()
    {
        $payment_type_id = 2; // CONTRACTOR PAYMENT
        $PaymentTypes = PaymentType::where('id',$payment_type_id)->pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');
        $PaymentMethods = PaymentMethod::pluck('name','id');
        $Contractors = Contractor::ofOrg()->pluck('name','id');

        return view('payment.contractor.add', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts, 'payment_type_id' => $payment_type_id, 'PaymentMethods' => $PaymentMethods, 'Contractors'=>$Contractors]);
    }

    public function servicePayment()
    {
        $payment_type_id = 3; // SERVICE PAYMENT
        $PaymentTypes = PaymentType::where('id',$payment_type_id)->pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');
        $PaymentMethods = PaymentMethod::pluck('name','id');
        $ServicePaymentType = ServicePaymentType::pluck('name','name');
        
        return view('payment.service.add', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts, 'payment_type_id' => $payment_type_id, 'PaymentMethods' => $PaymentMethods, 'ServicePaymentType'=>$ServicePaymentType]);
    }

    public function externalPayment()
    {
        $payment_type_id = 4; // external PAYMENT
        $PaymentTypes = PaymentType::where('id',$payment_type_id)->pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');
        $PaymentMethods = PaymentMethod::pluck('name','id');
        
        return view('payment.external.add', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts, 'payment_type_id' => $payment_type_id, 'PaymentMethods' => $PaymentMethods]);
    }

    public function externalIncome()
    {
        $payment_type_id = 6; // external Income
        $PaymentTypes = PaymentType::where('id',$payment_type_id)->pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');
        $PaymentMethods = PaymentMethod::pluck('name','id');
        
        return view('payment.external.income', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts, 'payment_type_id' => $payment_type_id, 'PaymentMethods' => $PaymentMethods]);
    }

    public function customerPayment()
    {
        $payment_type_id = 5; // CUSTOMER PAYMENT
        $PaymentTypes = PaymentType::where('id',$payment_type_id)->pluck('name','id');
        $BankAccounts = BankAccount::ofProject()->pluck('name','id');
        $PaymentMethods = PaymentMethod::pluck('name','id');
        $Customers = Customer::ofProject()->pluck('name','id');
        
        return view('payment.customer.add', ['PaymentTypes' => $PaymentTypes,'BankAccounts' => $BankAccounts, 'payment_type_id' => $payment_type_id, 'PaymentMethods' => $PaymentMethods, 'Customers'=>$Customers]);
    }

    public function pay(Request $request)
    {
        //dd($request->all());

        $org_id = getOrganization('org_id');
        $project_id = getProject('proj_id');

        $bank_account = $request->input('bank_account');
        $payment_type = $request->input('payment_type');
        $payment_method = $request->input('payment_method');
        $payment_method_ref = $request->input('payment_method_ref');
        $total_payable_amount = $request->input('total_payable_amount');
        $advance_amount = $request->input('advance_amount');
        $old_advance_amount = $request->input('old_advance_amount');
        $remark = $request->input('remark');
        $date = $request->input('date');
        $paid_to = $request->input('paid_to');

        DB::beginTransaction();


        switch($payment_type)
        {
            case "1": // MATERIAL PAYMENT
            {
             $payment_id = 0;

             $supplier_id = $request->input('supplier_id');

             $Supplier = Supplier::findOrFail($supplier_id);

             $items = json_decode($request->input('items'), true);

             if(sizeof($items) > 0)
             {
                $pay_grn_ids = [];

                foreach($items as $item)
                {
                    $grnid = $item['grnid'];

                    $grn = OrderGrn::find($grnid);

                    if($grn->count() > 0)
                    {
                        if(empty($grn->payment_id))
                        {
                            $pay_grn_ids[] = $grn->id;
                        }
                    }
                }

                if(sizeof($pay_grn_ids) > 0)
                {
                        //$total_grn_amount = OrderGrn::whereIn('id', $pay_grn_ids)->sum('total_price');

                    $total_amount = $total_payable_amount;

                    $params['bank_account_id'] = $bank_account;
                    $params['payment_type_id'] = $payment_type;
                    $params['payment_method_id'] = $payment_method;
                    $params['payment_method_ref'] = $payment_method_ref;
                    $params['amount'] = $total_amount;
                    $params['type'] = "debit";
                    $params['remark'] = $remark;
                    $params['date'] = $date;
                    $params['paid_to'] = $paid_to;
                    $params['ref_id'] = $supplier_id;

                    $payment_id = Payment::addTransaction($params);

                    if($payment_id > 0)
                    {

                        OrderGrn::whereIn('id', $pay_grn_ids)->update(['payment_id' => $payment_id]);

                        $SupplierPaymentSettings['supplier_id']         = $supplier_id;
                        $SupplierPaymentSettings['payment_id']          = $payment_id;
                        $SupplierPaymentSettings['type']                = 'grn';
                        $SupplierPaymentSettings['amount']              = $total_payable_amount;

                        SupplierPayment::addTransaction($SupplierPaymentSettings);

                        if($old_advance_amount > 0)
                        {  
                            $paid_advance_amount = $old_advance_amount - $total_payable_amount;

                            if($paid_advance_amount > 0)
                            {
                                $new_advance_amount = 0 - $paid_advance_amount;
                            }
                            else
                            {
                                $new_advance_amount = 0 - $old_advance_amount;
                            }  

                            $SupplierPaymentSettings['type']                = 'advance';
                            $SupplierPaymentSettings['amount']              = $new_advance_amount;

                            SupplierPayment::addTransaction($SupplierPaymentSettings);

                        }


                        if($advance_amount > 0)
                        {
                            $SupplierPaymentSettings['type']                = 'advance';
                            $SupplierPaymentSettings['amount']              = $advance_amount;

                            SupplierPayment::addTransaction($SupplierPaymentSettings);
                        }
                    }

                }   


            }

            break;


        }

        case "2": // CONTRACTOR PAYMENT
        {
                 $payment_id = 0;

                 $contractor_id = $request->input('contractor_id');

                 $Contractor = Contractor::findOrFail($contractor_id);

                 $items = json_decode($request->input('items'), true);

                 foreach($items as $item)
                 {
                    $tasks[] = $item['taskid'];
                 }

                if(sizeof($tasks) > 0)
                {
                    $total_amount = $total_payable_amount;

                    $params['bank_account_id'] = $bank_account;
                    $params['payment_type_id'] = $payment_type;
                    $params['payment_method_id'] = $payment_method;
                    $params['payment_method_ref'] = $payment_method_ref;
                    $params['amount'] = $total_amount;
                    $params['type'] = "debit";
                    $params['remark'] = $remark;
                    $params['date'] = $date;
                    $params['paid_to'] = $paid_to;
                    $params['ref_id'] = $contractor_id;

                    $payment_id = Payment::addTransaction($params);

                    if($payment_id > 0)
                    {
                        ProductStage::whereIn('id', $tasks)->update(['payment_id' => $payment_id]);

                        $PaymentSettings['contractor_id']         = $contractor_id;
                        $PaymentSettings['payment_id']          = $payment_id;
                        $PaymentSettings['type']                = 'task';
                        $PaymentSettings['amount']              = $total_payable_amount;

                        ContractorPayment::addTransaction($PaymentSettings);

                        if($advance_amount > 0)
                        {
                            $PaymentSettings['type']                = 'advance';
                            $PaymentSettings['amount']              = $advance_amount;

                            ContractorPayment::addTransaction($PaymentSettings);
                        }

                    }

                }

                break;

        }

        case "3": // SERVICE PAYMENT
            {
                    $payment_id = 0;

                    $approved_by = $request->input('approved_by');


                    if($total_payable_amount > 0)
                    {
                        $total_amount = $total_payable_amount;

                        $params['bank_account_id'] = $bank_account;
                        $params['payment_type_id'] = $payment_type;
                        $params['payment_method_id'] = $payment_method;
                        $params['payment_method_ref'] = $payment_method_ref;
                        $params['amount'] = $total_amount;
                        $params['type'] = "debit";
                        $params['remark'] = $remark;
                        $params['date'] = $date;
                        $params['paid_to'] = $paid_to;
                        $params['ref_id'] = 0;
                        $params['approved_by'] = $approved_by;

                        $payment_id = Payment::addTransaction($params);

                        if($payment_id > 0)
                        {
                           $ServicePayment = new ServicePayment();

                           $ServicePayment->org_id = $org_id;
                           $ServicePayment->project_id = $project_id;
                           $ServicePayment->payment_id = $payment_id;
                           $ServicePayment->type = $request->input('service_payment_type');
                           $ServicePayment->amount = $total_amount;
                           $ServicePayment->save();
                        }

                    }

                break;
            }

         case "4": // EXTERNAL PAYMENT
            {
                    $payment_id = 0;

                    $approved_by = $request->input('approved_by');


                    if($total_payable_amount > 0)
                    {
                        $total_amount = $total_payable_amount;

                        $params['bank_account_id'] = $bank_account;
                        $params['payment_type_id'] = $payment_type;
                        $params['payment_method_id'] = $payment_method;
                        $params['payment_method_ref'] = $payment_method_ref;
                        $params['amount'] = $total_amount;
                        $params['type'] = "debit";
                        $params['remark'] = $remark;
                        $params['date'] = $date;
                        $params['paid_to'] = $paid_to;
                        $params['ref_id'] = 0;
                        $params['approved_by'] = $approved_by;

                        $payment_id = Payment::addTransaction($params);

                        if($payment_id > 0)
                        {
                           $ExternalPayment = new ExternalPayment();

                           $ExternalPayment->org_id = $org_id;
                           $ExternalPayment->project_id = $project_id;
                           $ExternalPayment->payment_id = $payment_id;
                           $ExternalPayment->type = $request->input('purpose');
                           $ExternalPayment->amount = $total_amount;
                           $ExternalPayment->save();
                        }

                    }

                break;
            }

            case "5": // CUSTOMER PAYMENT
            {
                
                    $payment_id = 0;

                    $approved_by = $request->input('approved_by');
                    $customer_id = $request->input('customer_id');
                    $product_id = $request->input('product_id');

                    if($total_payable_amount > 0)
                    {
                        $total_amount = $total_payable_amount;

                        $params['bank_account_id'] = $bank_account;
                        $params['payment_type_id'] = $payment_type;
                        $params['payment_method_id'] = $payment_method;
                        $params['payment_method_ref'] = $payment_method_ref;
                        $params['amount'] = $total_amount;
                        $params['type'] = "credit";
                        $params['remark'] = $remark;
                        $params['date'] = $date;
                        $params['paid_to'] = $paid_to;
                        $params['ref_id'] = 0;
                        $params['approved_by'] = "";

                        $payment_id = Payment::addTransaction($params);

                        if($payment_id > 0)
                        {
                           $type = $request->input('prod_pay_type');

                           if($type == 'product')
                           {
                               $CustomerPayment = new CustomerPayment();

                               $CustomerPayment->org_id = $org_id;
                               $CustomerPayment->project_id = $project_id;
                               $CustomerPayment->customer_id = $customer_id;
                               $CustomerPayment->product_id = $product_id;
                               $CustomerPayment->payment_id = $payment_id;
                               $CustomerPayment->type = $type;
                               $CustomerPayment->amount = $total_amount;
                               $CustomerPayment->save();

                                if($CustomerPayment->id > 0)
                                   {
                                        Product::where('id',$product_id)->update(['payment_id'=>$payment_id]);
                                   }
                           }
                           elseif($type == 'upgrade')
                           {
                                 $items = json_decode($request->input('items'), true);

                                 if(sizeof($items) > 0)
                                 {
                                     foreach($items as $item)
                                     {

                                       $CustomerPayment = new CustomerPayment();

                                       $CustomerPayment->org_id = $org_id;
                                       $CustomerPayment->project_id = $project_id;
                                       $CustomerPayment->customer_id = $customer_id;
                                       $CustomerPayment->product_id = $product_id;
                                       $CustomerPayment->change_request_id = $item['taskid'];
                                       $CustomerPayment->payment_id = $payment_id;
                                       $CustomerPayment->type = $type;
                                       $CustomerPayment->amount = $item['price'];
                                       $CustomerPayment->save();

                                       if($CustomerPayment->id > 0)
                                       {
                                            ChangeRequest::where('id',$item['taskid'])->update(['payment_id'=>$payment_id]);
                                       }
                                        
                                     }
                                 }

                           }
                           
                        }

                    }

                break;
            }


             case "6": // EXTERNAL INCOME
            {
                    $payment_id = 0;

                    $approved_by = $request->input('approved_by');
                    $paid_by = $request->input('paid_by');


                    if($total_payable_amount > 0)
                    {
                        $total_amount = $total_payable_amount;

                        $params['bank_account_id'] = $bank_account;
                        $params['payment_type_id'] = $payment_type;
                        $params['payment_method_id'] = $payment_method;
                        $params['payment_method_ref'] = $payment_method_ref;
                        $params['amount'] = $total_amount;
                        $params['type'] = "credit";
                        $params['remark'] = $remark;
                        $params['date'] = $date;
                        $params['paid_by'] = $paid_by;
                        $params['ref_id'] = 0;
                        $params['approved_by'] = $approved_by;

                        $payment_id = Payment::addTransaction($params);

                        
                    }

                break;
            }

    default:
        break;
}

DB::commit();

if($payment_id > 0)
{
    flash()->overlay('Payment created succesfully with reference ID #'.$payment_id, 'Payment Successful!');
    return route('payments.index');
}
else
    return URL::previous();
}



public function receipt($paymentid,$type="pdf")
{
    $payment = Payment::ofProject()->findOrFail($paymentid);
    $org = Organization::find(getOrganization('org_id'));

    $html = view('receipt.master', compact('payment','org'))->render();

    if($type == 'pdf')
    {
        $pdf = PDF::setOptions([])->loadHTML($html);
        return $pdf->stream();
    }
    else
        return $html;
}

public function voucher($paymentid,$type="pdf")
{
    $payment = Payment::ofProject()->findOrFail($paymentid);
    $org = Organization::find(getOrganization('org_id'));
    
    $html = view('voucher.master', compact('payment','org'))->render();

    if($type == 'pdf')
    {
        $pdf = PDF::loadHTML($html);
        return $pdf->stream();
    }
    else
        return $html;
}



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($paymentid)
    {
        $payment = Payment::ofProject()->findOrFail($paymentid);

        return view('payment.view', compact('payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
