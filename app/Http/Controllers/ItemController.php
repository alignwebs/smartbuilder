<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

use App\Supplier;

use App\Department;

use App\Item;
use App\ItemDepartment;
use App\ItemStaff;
use App\ItemSupplier;
use App\Tax;
use App\Staff;

class ItemController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Items = Item::ofOrg()->get();

        return view('item.list',['Items'=>$Items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indiaStates = getIndiaStates();

        $Suppliers = Supplier::ofOrg()->orderBy('name','asc')->get()->pluck('name','id');

        $Departments = Department::ofOrg()->get()->pluck('name','id');

        $Tax = Tax::ofOrg()->get()->pluck('fullname','id');

        $Staff = Staff::fromOrg()->pluck('fullname','id');
  
        return view('item.add',['indiaStates'=>$indiaStates, 'suppliers'=>$Suppliers, 'departments'=>$Departments,'tax'=>$Tax,'staff'=>$Staff]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'code' => 'required',
            'name' => 'required',
            'supplier' => 'required',
            'unit' => 'required',
            'unit_price' => 'required',
            'tax' => 'required',
            'departments' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $project_id = getProject('proj_id');
        $org_id = getOrganization('org_id');

        $Item = new Item();

        $Item->org_id = $org_id;
        $Item->project_id = $project_id;
        $Item->tax_id = $request->input('tax');
        $Item->code = $request->input('code');
        $Item->name = $request->input('name');
        $Item->description = $request->input('description');
        $Item->unit = $request->input('unit');
        $Item->unit_price = $request->input('unit_price');
        $Item->discount = $request->input('discount');
        $Item->roq = $request->input('roq');
    
        $Item->save();

        $ItemID = $Item->id;

        /*Add Suppliers to Items*/

         foreach ($request->input('supplier') as $supplier) {
            $itemSuppl[] = array('item_id'=>$ItemID, 'supplier_id'=>$supplier);
        }

        $ItemSupplier = ItemSupplier::insert($itemSuppl);

        /*Add Item to Departments*/

        foreach ($request->input('departments') as $department_id) {
            $itemDept[] = array('project_id'=>$project_id, 'org_id'=>$org_id,'item_id'=>$ItemID, 'department_id'=>$department_id);
        }

        $ItemDepartment = ItemDepartment::insert($itemDept);


        /*Add Contact to Departments*/
        if(!empty($request->input('staff') ))
        {
             foreach ($request->input('staff') as $staff) {
                $itemStaff[] = array('staff_id'=>$staff, 'item_id'=>$ItemID);
            }

             ItemStaff::insert($itemStaff);
            
        }

         return redirect()->route('item.index','created=true');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Item = Item::find($id);

        $project_id = $Item->project_id;

        $org_id = $Item->org_id;

        $indiaStates = getIndiaStates();

        $Suppliers = Supplier::ofOrg()->orderBy('name','asc')->get()->pluck('name','id');

        $ItemSupplier = $Item->suppliers->pluck('supplier_id')->toArray();

        $Departments = Department::ofOrg()->get()->pluck('name','id');

        $ItemDepartment = $Item->departments->pluck('department_id')->toArray();

        $Tax = Tax::ofOrg()->get()->pluck('fullname','id');

        $Staff = Staff::fromOrg()->pluck('fullname','id');

        $ItemStaff = $Item->staff->pluck('staff_id')->toArray();

        return view('item.edit',['indiaStates'=>$indiaStates, 'suppliers'=>$Suppliers, 'departments'=>$Departments,'tax'=>$Tax,'item'=>$Item,'ItemSupplier'=>$ItemSupplier,'ItemDepartment'=>$ItemDepartment,'staff'=>$Staff, 'ItemStaff'=> $ItemStaff]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            'code' => 'required',
            'name' => 'required',
            'supplier' => 'required',
            'unit' => 'required',
            'unit_price' => 'required',
            'tax' => 'required',
            'departments' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Item = Item::findOrFail($id);

        $Item->tax_id = $request->input('tax');
        $Item->code = $request->input('code');
        $Item->name = $request->input('name');
        $Item->description = $request->input('description');
        $Item->unit = $request->input('unit');
        $Item->unit_price = $request->input('unit_price');
        $Item->discount = $request->input('discount');
        $Item->roq = $request->input('roq');
        
        $Item->save();

        $ItemID = $id;

        /*Add Suppliers to Items*/

        foreach ($request->input('supplier') as $supplier) {
            $itemSuppl[] = array('item_id'=>$ItemID, 'supplier_id'=>$supplier);
        }

        ItemSupplier::where('item_id',$ItemID)->delete();

        $ItemSupplier = ItemSupplier::insert($itemSuppl);

        /*Add Departments*/

        foreach ($request->input('departments') as $department_id) {
            $itemDept[] = array('project_id'=>$Item->project_id, 'org_id'=>$Item->org_id,'item_id'=>$ItemID, 'department_id'=>$department_id);
        }

        ItemDepartment::where('item_id',$ItemID)->delete();
        $ItemDepartment = ItemDepartment::insert($itemDept);


        /*Add Staff*/
        
        ItemStaff::where('item_id',$ItemID)->delete();

        if(sizeof($request->input('staff')) > 0)
        {
            foreach ($request->input('staff') as $staff) {
                $itemStaff[] = array('staff_id'=>$staff, 'item_id'=>$ItemID);
            }
            
            ItemStaff::insert($itemStaff);
        }

        

         return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Item::destroy($id);
        ItemSupplier::where('item_id',$id)->delete();

        return redirect()->back();
    }

    public function itemIncharge(Request $request)
    {
        $item_id = $request->input('id');

        $ItemContacts = ItemStaff::where('item_id',$item_id)->get();

        return view('item.incharge', ['incharges'=>$ItemContacts]);
    }

    public function supplierItems($supplierid="",Request $request)
    {
        $supplierid = ($supplierid != "" ? $supplierid : $request->input('supplier_id'));

        if($supplierid > 0)
        {
            $ItemSuppliers = ItemSupplier::where('supplier_id',$supplierid)->get();

            foreach ($ItemSuppliers as $row) {
                @$items[] = array("item_id"=>$row->item->id,"item_name"=>$row->item->name,"item_code"=>$row->item->code,"item_unit"=>$row->item->unit,"item_unit_price"=>$row->item->unit_price,"item_discount"=>$row->item->discount, "tax_name"=>$row->item->tax->name, "tax"=>$row->item->tax->percent, "roq"=>$row->item->roq);
            }
         
            $Supplier = Supplier::find($supplierid);

            if( $request->input('table') == true)
                return view('item.suppliersItem',['items'=>$items,'Supplier'=>$Supplier]);
            else
                return $items;
        }
    }

    public function RemoveSupplierItem(Request $request)
    {
        $supplier_id = $request->input('supplier_id');
        $item_id = $request->input('item_id');

        ItemSupplier::where('item_id',$item_id)->where('supplier_id',$supplier_id)->delete();

    }

}
