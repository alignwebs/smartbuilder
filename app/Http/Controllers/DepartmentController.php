<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

/*LOAD MODELS*/

use App\Department;

use App\DepartmentContact;

use App\Item;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Departments = Department::ofOrg()->orderby('name','asc')->get();

        return view('department.list', ["Departments"=>$Departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indiaStates = getIndiaStates();

        return view('department.add',['indiaStates'=>$indiaStates]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'code' => 'required',
            'name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }


        $department = new Department();

        $department->org_id = getOrganization('org_id');
        $department->project_id = getProject('proj_id');
        $department->code = $request->input('code');
        $department->name = $request->input('name');
        
        $department->save();

        if($request->input('contact_name') != "")
        {
            $departmentContact = new DepartmentContact();

            $departmentContact->department_id = $department->id;    
            $departmentContact->name = $request->input('contact_name');
            $departmentContact->email = $request->input('contact_email');
            $departmentContact->phone = $request->input('contact_phone');
            $departmentContact->address = $request->input('contact_address');
            $departmentContact->state = $request->input('contact_state');
            $departmentContact->save();
        }

        return redirect()->route('department.index','created=true');
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ItemsID = [];

        $Department = Department::findOrFail($id);

        $projectItems = $Department->projectItems;

        foreach($projectItems as $projectItem)
        {
            $ItemsID[] = $projectItem->item_id;
        }

        $Items = Item::find($ItemsID);

        $title = $Department->name;

        
        return view('department.view', ['Department'=>$Department, 'Items'=>$Items, 'title'=>$title]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Department = Department::findOrFail($id);
        return view('department.edit', ['Department'=>$Department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validator = Validator::make($request->all(), [

            'code' => 'required',
            'name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }


        $department = Department::find($id);

        $department->code = $request->input('code');
        $department->name = $request->input('name');
        $department->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fetch = Department::find($id);
        $fetch->contacts()->delete();
        $fetch->delete();

        return redirect()->route('department.index', 'deleted=true');
    }
}
