<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tax;

use Auth;

use Session;

use Validator;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Taxes = Tax::where('org_id',getOrganization('org_id'))->get();
        return view('tax.index',['Taxes'=>$Taxes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [

            'name' => 'required',
            'percent' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Tax = new Tax();
        $Tax->org_id = getOrganization('org_id');
        $Tax->project_id = getProject('proj_id');
        $Tax->name = $request->input('name');
        $Tax->percent = $request->input('percent');
        $Tax->description = $request->input('description');
        $Tax->save();

        return redirect()->back();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Tax    =   Tax::findOrFail($id);

        return view('tax.edit', ['tax'=>$Tax]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $Tax = Tax::findOrFail($id);

         $validator = Validator::make($request->all(), [

            'name' => 'required',
            'percent' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

       
        $Tax->org_id = getOrganization('org_id');
        $Tax->name = $request->input('name');
        $Tax->percent = $request->input('percent');
        $Tax->description = $request->input('description');
        $Tax->save();

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tax::destroy($id);

        return redirect()->back();//
    }
}
