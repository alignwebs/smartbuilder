<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserHasProject;
use App\UserDetail;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {


        $user = User::where('email',$data['email'])->first();

        if(!empty($user))
        {
            if(!$user->hasRole('customer'))
            {
                return "Existing user must be of type Customer";
            }

            return Validator::make($data, []);

        }
        else
        {
    
              return Validator::make($data, [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|confirmed',
                ]);
            
        }
      
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $isCutomer = false;

        $user = User::where('email',$data['email'])->first();

        if(!empty($user))
        {
            if(!$user->hasRole('customer'))
            {
                return "Existing user must be of type Customer";
            }
            else
                $isCutomer = true; 
        }
        else
        {
    
            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
            
        }


        if($user->id > 0)
        {
            if(@$data['projectmanager'] == true)
            {
                $user->assignRole('manager');
                $user->addProject(getProject('proj_id'));

                $UserDetail = new UserDetail();
                $UserDetail->user_id = $user->id;
                $UserDetail->phone = $data['phone'];
                $UserDetail->address = $data['address'];
                $UserDetail->district = $data['district'];
                $UserDetail->state = $data['state'];
                $UserDetail->role_title = $data['role_title'];
                $UserDetail->save();

                flash()->overlay('Project Manager has been created successfully!', 'User Creation Successful!');
            }

            if(@$data['customer'] == true)
            {
                if(!$isCutomer)
                    $user->assignRole('customer');

                $Customer = new Customer();
                $Customer->org_id = getOrganization('org_id');
                $Customer->project_id = getProject('proj_id');
                $Customer->user_id = $user->id;
                $Customer->name = $data['name'];
                $Customer->phone = $data['phone'];
                $Customer->email = $data['email'];
                $Customer->city = $data['city'];
                $Customer->state = @$data['state'];
                $Customer->name_sec = @$data['name_sec'];
                $Customer->phone_sec = @$data['phone_sec'];
                $Customer->email_sec = @$data['email_sec'];
                $Customer->save();

                flash()->overlay('Project Customer has been created successfully!', 'User Creation Successful!');
            }

        }

        return $user;
    }


}
