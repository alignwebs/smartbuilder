<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stage;
use App\Department;
use App\ProductCategory;

use Validator;
use Auth;
use Form;
class StageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Departments = Department::ofOrg()->get()->pluck('name','id');
        $ProductCategory = ProductCategory::ofProject()->get()->pluck('product_category_name','id');

        $Stages = Stage::ofProject()->get();

        return view('stage.index', ['Departments'=>$Departments, 'ProductCategory'=>$ProductCategory, 'Stages'=>$Stages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'department' => 'required',
            'productcategory' => 'required',
            'code' => 'required',
            'name' => 'required',
            'rate' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Stage = new Stage();

        $Stage->project_id = getProject('proj_id');
        $Stage->department_id = $request->input('department');
        $Stage->product_category_id = $request->input('productcategory');
        $Stage->code = $request->input('code');
        $Stage->name = $request->input('name');
        $Stage->rate = $request->input('rate');
        $Stage->description = $request->input('description');
        $Stage->comment = $request->input('comment');

        $Stage->save();

        return redirect()->route('stages.index','created=true');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Departments = Department::ofOrg()->get()->pluck('name','id');
        $ProductCategory = ProductCategory::ofProject()->get()->pluck('product_category_name','id');

        $Stage = Stage::ofProject()->findOrFail($id);
        
        return view('stage.edit', ['Departments'=>$Departments, 'ProductCategory'=>$ProductCategory, 'Stage'=>$Stage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            'department' => 'required',
            'productcategory' => 'required',
            'code' => 'required',
            'name' => 'required',
            'rate' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Stage = Stage::find($id);

        $Stage->department_id = $request->input('department');
        $Stage->product_category_id = $request->input('productcategory');
        $Stage->code = $request->input('code');
        $Stage->name = $request->input('name');
        $Stage->rate = $request->input('rate');
        $Stage->description = $request->input('description');
        $Stage->comment = $request->input('comment');

        $Stage->save();

        return redirect()->route('stages.index','updated=true');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listByDepartment(Request $request)
    {
        $project_id = getProject('proj_id');
        $product_category_id = $request->input('product_category_id');
        $department_id = $request->input('department_id');

        $data = Stage::where('product_category_id', $product_category_id)->where('department_id', $department_id)->get()->pluck('name','id')->toArray();

        return Form::select('stage', $data, null,['class'=>'form-control' , 'required'=>'', 'onchange'=>'getStagerate($(this).val())' , 'placeholder'=>'Select Stage']);
     }

     public function standardRate(Request $request)
    {
        $stage_id = $request->input('stage_id');
        return Stage::find($stage_id)->rate;
    }
}
