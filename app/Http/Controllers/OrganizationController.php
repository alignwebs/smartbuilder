<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

/*LOAD MODELS*/

use App\Organization;

use App\OrganizationContact;

/*END LOAD MODELS*/

class OrganizationController extends Controller
{
    



    public function __construct()
    {
       
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = $this->getOrganizations();
        
        return view('organization.list', ['organizations'=>$organizations]);
    }

    public function select()
    {
        return view('organization.select');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indiaStates = getIndiaStates();

        return view('organization.add',['indiaStates'=>$indiaStates]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*VALIDATE NEW ORGANIZATION DATA*/

        $validator = Validator::make($request->all(), [

            'org_code' => 'required|unique:organizations',
            'org_name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->route('organization.create')
                        ->withErrors($validator)
                        ->withInput();
        }

        /*ADD NEW ORGANIZATION DATA*/

        $organization = new Organization();

        $organization->user_id = Auth::id();
        $organization->org_code = $request->input('org_code');
        $organization->org_name = $request->input('org_name');
        $organization->org_address_one = $request->input('org_address_one');
        $organization->org_address_two = $request->input('org_address_two');
        $organization->org_city = $request->input('org_city');
        $organization->org_district = $request->input('org_district');
        $organization->org_state = $request->input('org_state');
        $organization->org_pincode = $request->input('org_pincode');
        $organization->org_tin = $request->input('org_other_tin');
        $organization->org_billing_address = $request->input('org_other_billing');
        $organization->org_code = $request->input('org_code');

        $organization->save();

        $org_id = $organization->id;    #FETCH LAST INSERTED ID

        if($request->input('org_contact_name') != "")
        {
            $orgContact = new OrganizationContact();

            $orgContact->org_id = $org_id;
            $orgContact->name = $request->input('org_contact_name');
            $orgContact->phone = $request->input('org_contact_phone');
            $orgContact->email = $request->input('org_contact_email');
            $orgContact->state = $request->input('org_contact_state');
            $orgContact->pincode = $request->input('org_contact_pincode');

            $orgContact->save();

        }



        if($request->hasFile('org_logo'))
        {

            $org_logo_file = $request->file('org_logo');

            $org_logo = $org_logo_file->store('org-logos');

            $organization->logo = $org_logo;

            $organization->save();
            
        }

       $this->setOrganization($org_id); #SET THE CURRENT ORGANIZATION IN SESSION

       flash()->overlay("New <b>".$request->input('org_name')."</b> organization has been created successfully.", 'Alert');

       return redirect()->action("OrganizationController@index");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Organization = Organization::findOrFail($id);

        return view('organization.view',['organization'=>$Organization]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Organization = Organization::findOrFail($id);

        $indiaStates = getIndiaStates();

        return view('organization.edit',['organization'=>$Organization,'indiaStates'=>$indiaStates]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*VALIDATE NEW ORGANIZATION DATA*/

        $validator = Validator::make($request->all(), [

            'org_name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        /*ADD NEW ORGANIZATION DATA*/

        $organization = Organization::find($id);

        $organization->org_name = $request->input('org_name');
        $organization->org_address_one = $request->input('org_address_one');
        $organization->org_address_two = $request->input('org_address_two');
        $organization->org_city = $request->input('org_city');
        $organization->org_district = $request->input('org_district');
        $organization->org_state = $request->input('org_state');
        $organization->org_pincode = $request->input('org_pincode');
        $organization->org_tin = $request->input('org_other_tin');
        $organization->org_billing_address = $request->input('org_other_billing');

        $organization->save();


        if($request->hasFile('org_logo'))
        {

            $org_logo_file = $request->file('org_logo');

            $org_logo = $org_logo_file->store('org-logos');

            $organization->logo = $org_logo;

            $organization->save();


            $selected_org = getOrganization('org_id');

            if($selected_org == $id)
            {
            	$this->setOrganization($id);
            }
            
        }

        flash()->overlay("Organization details for <b>".$request->input('org_name')."</b> has been updated successfully.", 'Alert');

        return redirect()->action("OrganizationController@index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Organization::destroy($id);

        if($id == @getOrganization('org_id'))
            $this->unsetOrganization();

        return route('organization.index', 'deleted=true');
    }

    /*SET ORGANIZATION FUNCTIONS*/

    public function setOrganization($org_id)
    {
        if($org_id > 0)
        {
            $orgFetch = Organization::where('id', $org_id)->first();

            if(isset($orgFetch))
            {
                $data = array("org_id"=>$orgFetch->id, "org_code"=>$orgFetch->org_code,"org_name"=>$orgFetch->org_name,"org_state"=>$orgFetch->org_state, "org_logo"=>$orgFetch->logo);

               session(['selectedOrg' => $data]);

            }
        }
    }

    public function unsetOrganization()
    {
        session(['selectedOrg' => ""]);
    }

    public function setOrganizationViaAjax(Request $request)
    {
        $org_id = $request->input('org_id');

        $this->setOrganization($org_id);
    }

    /*ENDS SET ORGANIZATION FUNCTIONS*/

    public function getOrganizations($fields="*")
    {
       $fetch = Organization::select($fields)->where('user_id', Auth::id())->orderby('org_name','asc')->get();

       return $fetch;
    }

    public function OrganizationWithID()
    {
        $data = $this->getOrganizations(['id','org_name','org_code']);

        return $data;
    }

}
