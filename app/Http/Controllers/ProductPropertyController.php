<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

use App\ProductPropertyHeader;

use App\ProductProperty;

class ProductPropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $pph = ProductPropertyHeader::where('project_id',Session('selectedProject')['proj_id'])->orderBy('name','desc')->get();

       return view('product.property.add', ['pph'=>$pph]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'pph' => 'required',
            'pphp' => 'required'
        
        ]);

        if ($validator->fails()) {
           return $validator->errors();
        }

        $pphp = json_decode($request->input('pphp'),true);

        $property_header_id = $request->input('pph');

        foreach($pphp as $productproperty)
        {
            if(!empty($productproperty))
                $data_pphp[] = array("property_header_id"=> $property_header_id,"property_name"=>$productproperty['name'],"property_type"=>$productproperty['type'],"qty"=>$productproperty['qty'],"unit"=>$productproperty['unit']);
        }

        /*DELETE ALL THE PRODUCT PROPERTIES OF PROPERTY HEADER*/
        ProductProperty::where('property_header_id',$property_header_id)->delete();

        /*INSERT NEW PRODUCT PROPERTIES TO PROPERTY HEADER*/
        if(ProductProperty::insert($data_pphp))
            return "ok";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listProductProperties(Request $request)
    {
        $fetch = ProductProperty::where('property_header_id',$request->pph)->orderBy('property_name','asc')->get();

        return $fetch;
    }
}
