<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProductCustomer;
use App\Product;
use Validator;
use Form;

class ProductCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validator = Validator::make($request->all(), [

            'product' => 'required|unique:product_customers,product_id'
            
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $ProductCustomer = new ProductCustomer();

        $ProductCustomer->product_id = $request->input('product');
        $ProductCustomer->customer_id = $request->input('customer_id');
        $ProductCustomer->date = $request->input('date');
        $ProductCustomer->remark = $request->input('remark');

        $ProductCustomer->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function customerProductsCombo(Request $request)
    {
        $customer_id = $request->input('customer_id');
        $CustomerProducts = ProductCustomer::where('customer_id',$customer_id)->pluck('product_id')->toArray();

        $Products = Product::whereIn('id', $CustomerProducts)->pluck('product_name','id');

        if($Products->count() > 0)

            return Form::select('product_id', $Products, null, ['id'=>'product_id','class'=>'form-control', 'placeholder'=>'Select Product']);
    }
}
