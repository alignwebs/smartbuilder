<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ReportType;
use App\Project;
use App\Customer;

class ReportController extends Controller
{
    public function index()
    {
    	$types = ReportType::all()->sortBy('name')->pluck('name','id');
    	$projects = Project::listProjects()->pluck('proj_name','id');
    	return view('reports.index', compact('types','projects'));
    }

    public function generate(Request $request)
    {
    	$report_type = $request->input('report_type');
    	$project = $request->input('project');

    	$report_title = ReportType::find($report_type);

    	switch ($report_type) {

    		case '1': // CUSTOMER DETAILS
    		{
    			if($project > 0)
    				$Customers = Customer::ofProject($project)->get();
    			else
    				$Customers = Customer::ofOrg()->get();

    			$html = view('reports.customer-details', compact('Customers','report_title'))->render();
    			
    			return $html;
    			break;
    		}
    		
    		default:
    			# code...
    			break;
    	}
    }
}
