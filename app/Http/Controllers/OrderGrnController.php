<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\OrderItem;
use App\OrderGrn;
use App\Inventory;

use Validator;

class OrderGrnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input('data');
        $grnitems = [];

        if(sizeof($data) > 0)
        {
            $project_id = getProject('proj_id');
            $org_id = getOrganization('org_id');

            foreach ($data as $item_row_id => $grn) {
                $rows[] = array(
                        "project_id" => $project_id,
                        "org_id" => $org_id,
                        "order_id" => $grn['orderid'],
                        "item_id" => $grn['itemid'],
                        "item_row_id" => $item_row_id,
                        "qty" => $grn['raisegrnqty'],
                        "total_price" => $grn['total']
                    );
            }

            OrderGrn::insert($rows);
            Inventory::updateInventory();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


   public function supplierGrn(Request $request)
   {

      $supplier_id = $request->input('supplier_id');
      
      $OrderItems = OrderItem::select('id')->ofProject()->where('supplier_id',$supplier_id)->get();

      if($OrderItems->count() > 0)
      {
          $GrnItems = OrderGrn::whereIn('item_row_id',$OrderItems->pluck('id'))->where('payment_id',null)->get();

          return view('supplier.unpaidgrn', compact('GrnItems'));
      }
      else
        return "No records found";

   }

}
