<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Auth;

use Session;

use App\ProductCategory;

use App\ProductPropertyHeader;

use App\ProductProperty;

use App\Product;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $project_id  = Session('selectedProject')['proj_id'];

        $ProductCategories = $this->getProductCategories($project_id);
        
        $PPheaders = ProductPropertyHeader::where('project_id', $project_id)->orderBy('name','asc')->get();

        return view('product.category.add', ['project_id'=>$project_id,'product_caregories' => $ProductCategories, 'ppheaders'=> $PPheaders]);  
    }

    public function getProductCategories($project_id="")
    {
        return ProductCategory::where('project_id', $project_id)->orderBy('product_category_name','asc')->get();
    }

    public function getProductCategoriesviaAjax(Request $request)
    {
        $rows = $this->getProductCategories($request->input('project_id'));

        return view('product.category.ajax.productcategory', compact('rows'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [

            'product_category_name' => 'required',
        
        ]);

        if ($validator->fails()) {
            return redirect()->action('ProductCategoryController@create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $ProductCategory = new ProductCategory();
        $ProductCategory->project_id = Session('selectedProject')['proj_id'];
        $ProductCategory->product_category_name = $request->input('product_category_name');
        
        $ProductCategory->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ProductCategory = ProductCategory::findOrFail($id);
        $ProductProperties = ProductProperty::where('property_header_id',$ProductCategory->product_properties_header_id)->get();
        $Products = Product::where('project_id', Session('selectedProject')['proj_id'])->get();

        $ProductsAvailable = $Products->where('product_category_id',null);
        $ProductsAssigned = $Products->where('product_category_id',$id);

        return view('product.category.manage',['ProductProperties'=>$ProductProperties, 'ProductCategory'=>$ProductCategory, 'ProductsAvailable'=>$ProductsAvailable, 'ProductsAssigned'=>$ProductsAssigned]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $ProductCategory = ProductCategory::findOrFail($id);

         return  view('product.category.edit',['fetch'=>$ProductCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            'product_category_name' => 'required',
        
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $ProductCategory = ProductCategory::find($id);
        
        $ProductCategory->product_category_name = $request->input('product_category_name');
        
        $ProductCategory->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductCategory::destroy($id);

        Product::removeProductCategory($id);

        return redirect()->route('product-category.create', 'deleted=true');
    }



    public function assignPropertyHeader(Request $request)
    {
        $ProductCategory = ProductCategory::find($request->input('pc'));
        $ProductCategory->product_properties_header_id = $request->input('tpl');
        $ProductCategory->save();
    }

    public function AddProdToCategory(Request $request)
    {
        $product_id = $request->input('prod_id');
        $product_category_id = $request->input('cat_id');

        if(isset($product_category_id) and isset($product_id))
        {
            $Product = Product::find($product_id);
            $Product->product_category_id = $product_category_id;
            $Product->save();
        }

    }

    public function DeleteProdFromCategory(Request $request)
    {
        $product_id = $request->input('prod_id');

        if(isset($product_id))
        {
            $Product = Product::find($product_id);
            $Product->product_category_id = null;
            $Product->save();
        }
    }

}
