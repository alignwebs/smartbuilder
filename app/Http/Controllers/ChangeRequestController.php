<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ChangeRequest;
use App\ChangeRequestFile;

use Auth;
use Storage;

class ChangeRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $instructions = $request->input('instructions');
        $product_id = $request->input('productid');
        $customer_id = $request->input('customerid');

    
        if(empty($title) or empty($instructions) or empty($product_id) or empty($customer_id))
            return redirect()->bsack();

        $date = $request->input('date');


        $ChangeRequest = new ChangeRequest();

        $ChangeRequest->org_id = getOrganization('org_id');
        $ChangeRequest->project_id = getProject('proj_id');
        $ChangeRequest->product_id = $product_id;
        $ChangeRequest->customer_id = $customer_id;
        $ChangeRequest->user_id = Auth::id();
        $ChangeRequest->customer_id = $customer_id;
        $ChangeRequest->title = $title;
        $ChangeRequest->instruction = $instructions;
        $ChangeRequest->date = $date;

        $ChangeRequest->save();

        if($ChangeRequest->id > 0)
        {
            $files = $request->file('files');

            if($request->hasFile('files'))
            {
                foreach ($files as $file) {
                    $rows[] = array("change_request_id"=>$ChangeRequest->id, "file"=>$file->store('change-requests/'.$product_id));
                }

                ChangeRequestFile::insert($rows);
            }
          
        }


        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateChangeRequest(Request $request)
    {
        $id = $request->input('id');

        $status = $request->input('status');
        $charge = $request->input('charge');

        $ChangeRequest = ChangeRequest::find($id);
        $ChangeRequest->status = $status;
        $ChangeRequest->charge = $charge;
        $ChangeRequest->save();

        flash()->overlay('Change Request for <b>'.$ChangeRequest->title.'</b> has been updated successfully!', 'Change Request Updated!');

        return redirect()->back();
    }
}
