<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BankAccount;
use App\Payment;
use Auth;
use Validator;
use Illuminate\Validation\Rule;


class BankAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Banks = BankAccount::ofProject()->get();
        return view('bank.list',['BankAccounts'=>$Banks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bank.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $org_id = getOrganization('org_id');
        $project_id = getProject('proj_id');

        $BankAccount = new BankAccount();

        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'bank_name' => 'required',
            'account_number' => ['required', 
                                    Rule::unique($BankAccount->getTable())->where(function ($query) {
                                        $query->where('project_id', getProject('proj_id'));
                                    })
                                ],
            'ifsc' => 'required',
            'balance' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        
        $BankAccount->org_id = $org_id;
        $BankAccount->project_id =  $project_id;
        $BankAccount->name = $request->input('name');
        $BankAccount->bank_name = $request->input('bank_name');
        $BankAccount->account_number = $request->input('account_number');
        $BankAccount->ifsc = $request->input('ifsc');
        $BankAccount->balance = $request->input('balance');
        $BankAccount->save();

        $Payment = new Payment();
        $Payment->user_id = Auth::id();
        $Payment->org_id =  getOrganization('org_id');
        $Payment->project_id = getProject('proj_id');
        $Payment->bank_id = $BankAccount->id;
        $Payment->amount = $request->input('balance');
        $Payment->type = 'credit';
        $Payment->date = date('Y-m-d');
        $Payment->save();


        return redirect()->route('bank-accounts.index','created=true');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $BankAccount = BankAccount::ofProject()->findOrFail($id);

        $transactions = Payment::bankTransaction($id);
    

        return view('bank.view', compact('BankAccount','transactions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Bank = BankAccount::findOrFail($id);
        return view('bank.edit', ['Bank'=>$Bank]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $BankAccount = BankAccount::find($id);

        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'bank_name' => 'required',
            'account_number' => ['required', 
                                    Rule::unique($BankAccount->getTable())->ignore($id,'id')->where(function ($query) {
                                        $query->where('project_id', getProject('proj_id'));
                                    })
                                ],
            'ifsc' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        
        $BankAccount->name = $request->input('name');
        $BankAccount->bank_name = $request->input('bank_name');
        $BankAccount->account_number = $request->input('account_number');
        $BankAccount->ifsc = $request->input('ifsc');
        $BankAccount->save();

        return redirect()->route('bank-accounts.index','updated=true');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajax(Request $request)
    {
        $type = $request->input('type');

        switch($type)
        {
            case "account_details":
            {
                $id = $request->input('id');
                $bank_account = BankAccount::find($id);
                $bank_account->balance = currency($bank_account->balance);
                $output = $bank_account;

                break;
            }

            default:
                break;
        }

        return $output;

    }
}
