<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Datatables;
use LoginActivity;

class LoginLogsController extends Controller
{
    public function logs(Request $request)
    {
    	$user_id = $request->input('user_id');

        $logs = LoginActivity::getLogs()->where('user_id',$user_id)->orderBy('created_at','desc')->get();

        return Datatables::of($logs)->make(true);
    }

}
