<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\ItemDepartment;

class InventoryController extends Controller
{

    public function items($department_id="", Request $request)
    {
    	$department_id = (!empty($department_id) ? $department_id : $request->input('department_id'));

    	//FIND ALL THE ITEM_ID IN A DEPARTMENT
    	$item_ids = ItemDepartment::select('item_id')->ofProject()->where('department_id', $department_id)->get()->toArray();

    	// FIND ITEMS IN INVENTORY
        $items = Inventory::ofProject()->whereIn('item_id',$item_ids)->get();
        
        return view('inventory.items', ['items'=>$items]);
    }

    public function index()
    {
    	$items = Inventory::ofProject()->get();

    	return view('inventory.index', ['Items'=>$items]);
    }

}
