<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Session;
use Datatables;

use App\Customer;

use App\Product;
use App\ChangeRequest;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org_id = getOrganization('org_id');
        $Customers = Customer::where('org_id', $org_id)->orderBy('name')->get();
        return view('customer.list', ['Customers'=>$Customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customer.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
       $Customer = Customer::ofProject()->findOrFail($id);
       $Products = Product::availableProducts();
       $ChangeRequests = ChangeRequest::where('user_id', $id)->get();
       
       return view('customer.view', compact('Customer', 'Products','ChangeRequests'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Customer = Customer::ofProject()->find($id);

        return view('customer.edit', compact('Customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [

            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Customer = Customer::find($id);

        $Customer->name     =   $request->input('name');
        $Customer->email     =   $request->input('email_primary');
        $Customer->phone     =   $request->input('phone');
        $Customer->city     =   $request->input('city');
        $Customer->state     =   $request->input('state');
        $Customer->name_sec     =   $request->input('name_sec');
        $Customer->phone_sec     =   $request->input('phone_sec');
        $Customer->email_sec     =   $request->input('email_sec');

        $Customer->save();

        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
