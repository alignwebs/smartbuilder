<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;        

use App\Product;

use App\Contractor;

use App\Stage;

use App\Department;

use App\ChangeRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::ofProject()->get();

        return view('product.index',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'product_name' => 'required',
        
        ]);

        if ($validator->fails()) {
            return redirect()->action('ProductController@create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $Product = new Product();
        $Product->org_id = getOrganization('org_id');
        $Product->project_id = getProject('proj_id');
        $Product->product_name = $request->input('product_name');
        $Product->product_details = $request->input('product_details');
        $Product->save();

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Product = Product::ofProject()->findOrFail($id);
        $Departments = Department::ofOrg()->get()->pluck('name','id');
        //Product::calculateProductCost($id);

        return view('product.view',['Product'=>$Product, 'Departments'=>$Departments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Product = Product::findOrFail($id);

        return view('product.edit', ['Product'=>$Product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all(), [

            'product_name' => 'required',
        
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Product = Product::find($id);
        $Product->product_name = $request->input('product_name');
        $Product->product_details = $request->input('product_details');
        $Product->save();

        return redirect()->route('product.index', 'edited='.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect()->route('product.index', 'deleted=true');
    }

    public function action(Request $request)
    {
        $action = $request->input('action');

        $product_id = $request->input('productid');

        switch ($action) {
            case 'setSalePrice':
            {
                $sale_price = $request->input('sale_price');
                Product::where('id', $product_id)->update(['sale_price' => $sale_price]);

                return redirect()->back();
                break;
            }
            
            default:
                # code...
                break;
        }
    }

    public function cost(Request $request)
    {
        $product_id = $request->input('product_id');
        $Product = Product::where('payment_id',null)->find($product_id);
      
        return $Product->sale_price;
    }

    public function upgrades(Request $request)
    {
        $product_id = $request->input('product_id');
        $data = ChangeRequest::where('product_id',$product_id)->where('payment_id',null)->get();
        return view('product.changerequest', compact('data'));
    }
}
