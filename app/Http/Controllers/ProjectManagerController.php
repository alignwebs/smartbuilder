<?php

namespace App\Http\Controllers;

use App\User;
use App\UserHasProject;
use App\UserDetail;
use Illuminate\Http\Request;


class ProjectManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ProjectManagers = UserHasProject::ofProject()->get();

        return view('project.manager.list',['ProjectManagers'=>$ProjectManagers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indiaStates = getIndiaStates();
        return view('project.manager.add',['indiaStates'=>$indiaStates]);
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
         $User = UserHasProject::ofProject()->where('user_id',$id)->firstOrFail();

         return view('project.manager.view',['User'=>$User]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProjectManager  $projectManager
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $User = UserHasProject::ofProject()->where('user_id',$id)->firstOrFail();

       return view('project.manager.edit',['User'=>$User]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProjectManager  $projectManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $User = User::find($id);
        $User->name = $request->input('name');
        $User->save();

        $UserDetail = UserDetail::where('user_id',$id)->first();
        $UserDetail->phone = $request->input('phone');
        $UserDetail->address = $request->input('address');
        $UserDetail->district = $request->input('district');
        $UserDetail->state = $request->input('state');
        $UserDetail->role_title = $request->input('role_title');
        $UserDetail->save();

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProjectManager  $projectManager
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       User::find($id)->delete();
       UserHasProject::ofProject()->where('user_id',$id)->delete();

       return redirect()->route('project-managers.index','deleted=true');
    }

   
}
