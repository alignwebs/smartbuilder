<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use Session;

/*LOAD MODELS*/

use App\Project;

use App\ProjectRole;

use App\Tax;

use App\Department;

use App\Supplier;

use App\Product;

use App\ProductPropertyHeader;


class SiteController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $project = Project::find(getProject('proj_id'));
        
        return view('home', ['project'=>$project]);
    }


    public function setupAlert()
    {
        $alert = [];

        $data = array(
                    array("title"=>"tax","count"=>Tax::ofOrg()->count(),"link"=>route('tax.index')),
                    array("title"=>"department","count"=>Department::ofProject()->count(),"link"=>route('department.create')),
                    array("title"=>"product","count"=>Product::ofProject()->count(),"link"=>route('product.index')),
                    array("title"=>"supplier","count"=>Supplier::count(),"link"=>route('supplier.create'))
            );
        
        foreach ($data as $key => $value) {
            
            if($value['count'] == 0)
                $alert[] = $value;

        }

        return view('setupalert',['data'=>$alert]);

    }

}
