<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

use App\Project;

class ProjectController extends Controller
{


    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::listProjects();
        
        return view('project.list', ['projects'=>$projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indiaStates = getIndiaStates();

        return view('project.add',['indiaStates'=>$indiaStates]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(empty(getOrganization('org_id'))) {
            dd("No Organization ID Set");
        }

        /*VALIDATE NEW PROJECT DATA*/

        $validator = Validator::make($request->all(), [

            'proj_code' => 'required|unique:projects',
            'proj_name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->route('project.create')
                        ->withErrors($validator)
                        ->withInput();
        }
   
        /*ADD NEW PROJECT DATA*/

        $Project = new Project();

        $Project->user_id = Auth::id();
        $Project->org_id = getOrganization('org_id');
        $Project->proj_code = $request->input('proj_code');
        $Project->proj_name = $request->input('proj_name');
        $Project->address_one = $request->input('proj_address_one');
        $Project->address_two = $request->input('proj_address_two');
        $Project->city = $request->input('proj_city');
        $Project->district = $request->input('proj_district');
        $Project->state = $request->input('proj_state');
        $Project->pincode = $request->input('proj_pincode');
    
        $Project->save();

        $proj_id = $Project->id;    #FETCH LAST INSERTED ID

        $this->setProject($proj_id); #SET THE CURRENT Project IN SESSION


        return redirect()->action("ProjectController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Project = Project::find($id);
        $indiaStates = getIndiaStates();

        return view('project.edit',['Project'=>$Project, 'indiaStates'=>$indiaStates]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Project = Project::find($id);

        $Project->address_one = $request->input('proj_address_one');
        $Project->address_two = $request->input('proj_address_two');
        $Project->city = $request->input('proj_city');
        $Project->district = $request->input('proj_district');
        $Project->state = $request->input('proj_state');
        $Project->pincode = $request->input('proj_pincode');
    
        $Project->save();

        return Redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::destroy($id);

        if($id == @getProject('proj_id'))
            $this->unsetProject();


        return redirect()->route('project.index', 'deleted=true');
    }

    public function select()
    {
        return view('project.select');
    }


     /*SET ORGANIZATION FUNCTIONS*/

    public function setProject($proj_id)
    {
        if($proj_id > 0)
        {
            $projFetch = Project::where('id', $proj_id)->first();

            if(isset($projFetch))
            {
                $this->unsetProject();

                $data = array("proj_id"=>$projFetch->id, "proj_code"=>$projFetch->proj_code,"proj_name"=>$projFetch->proj_name,"proj_state"=>$projFetch->state);

               session(['selectedProject' => $data]);

            }
        }
    }

    public function unsetProject()
    {
        session(['selectedProject' => ""]);
    }

    public function setProjectViaAjax(Request $request)
    {
        $proj_id = $request->input('proj_id');

        $this->setProject($proj_id);
    }

    /*ENDS SET ORGANIZATION FUNCTIONS*/

    public function getProjects($fields="*")
    {
       $fetch = Project::select($fields)->where('user_id', Auth::id())->where('org_id', getOrganization('org_id'))->orderby('proj_name','asc')->get();

       return $fetch;
    }

    public function ProjectWithID()
    {
        $data = $this->getProjects(['id','proj_name','proj_code']);

        return $data;
    }

}
