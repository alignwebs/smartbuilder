<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

/*LOAD MODELS*/

use App\Supplier;

use App\SupplierPayment;

use App\SupplierContact;

/*END LOAD MODELS*/

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::ofOrg()->orderby('name','asc')->get();  


        return view('supplier.list',['Suppliers'=>$suppliers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $indiaStates = getIndiaStates();

        return view('supplier.add',['indiaStates'=>$indiaStates]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [

            'suplr_code' => 'required',
            'suplr_name' => 'required',
            'suplr_email' => 'required',
            'suplr_phone' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $prefix = "suplr_";

        $supplier = new Supplier();

        $supplier->code = $request->input($prefix.'code');
        $supplier->org_id = getOrganization('org_id');
        $supplier->project_id = getProject('proj_id');
        $supplier->name = $request->input($prefix.'name');
        $supplier->email = $request->input($prefix.'email');
        $supplier->phone = $request->input($prefix.'phone');
        $supplier->address_one = $request->input($prefix.'address_one');
        $supplier->address_two = $request->input($prefix.'address_two');
        $supplier->city = $request->input($prefix.'city');
        $supplier->district = $request->input($prefix.'district');
        $supplier->state = $request->input($prefix.'state');
        $supplier->pincode = $request->input($prefix.'pincode');
        $supplier->meta_tin = $request->input($prefix.'meta_tin');
        $supplier->meta_bank_acc = $request->input($prefix.'meta_bank_acc');
        $supplier->meta_bank_name = $request->input($prefix.'meta_bank_name');
        $supplier->meta_bank_branch = $request->input($prefix.'meta_bank_branch');
        $supplier->meta_bank_acc_type = $request->input($prefix.'meta_bank_acc_type');
        $supplier->meta_bank_ifsc = $request->input($prefix.'meta_bank_ifsc');

        $supplier->save();

        $supplier_id = $supplier->id;

        if($request->input($prefix.'contact_name') != "")
        {
            $supplierContact = new SupplierContact();

            $supplierContact->supplier_id = $supplier_id;

            $supplierContact->name = $request->input($prefix.'contact_name');
            $supplierContact->phone = $request->input($prefix.'contact_phone');
            $supplierContact->email = $request->input($prefix.'contact_email');
            $supplierContact->address = $request->input($prefix.'contact_address');
            $supplierContact->district = $request->input($prefix.'contact_district');
            $supplierContact->state = $request->input($prefix.'contact_state');
            $supplierContact->pincode = $request->input($prefix.'contact_pincode');

            $supplierContact->save();

        }


        return redirect()->route('supplier.index', 'created=true');
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Supplier = Supplier::find($id);

        return view('supplier.view', ['Supplier'=>$Supplier]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Supplier = Supplier::findOrFail($id);

        $indiaStates = getIndiaStates();

        return view('supplier.edit', compact('Supplier','indiaStates'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

            'suplr_code' => 'required',
            'suplr_name' => 'required',
            'suplr_email' => 'required',
            'suplr_phone' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $prefix = "suplr_";

        $supplier = Supplier::findOrFail($id);

        $supplier->code = $request->input($prefix.'code');
        $supplier->name = $request->input($prefix.'name');
        $supplier->email = $request->input($prefix.'email');
        $supplier->phone = $request->input($prefix.'phone');
        $supplier->address_one = $request->input($prefix.'address_one');
        $supplier->address_two = $request->input($prefix.'address_two');
        $supplier->city = $request->input($prefix.'city');
        $supplier->district = $request->input($prefix.'district');
        $supplier->state = $request->input($prefix.'state');
        $supplier->pincode = $request->input($prefix.'pincode');
        $supplier->meta_tin = $request->input($prefix.'meta_tin');
        $supplier->meta_bank_acc = $request->input($prefix.'meta_bank_acc');
        $supplier->meta_bank_name = $request->input($prefix.'meta_bank_name');
        $supplier->meta_bank_branch = $request->input($prefix.'meta_bank_branch');
        $supplier->meta_bank_acc_type = $request->input($prefix.'meta_bank_acc_type');
        $supplier->meta_bank_ifsc = $request->input($prefix.'meta_bank_ifsc');

        $supplier->save();

        
        return redirect()->route('supplier.index', 'updated=true&id='.$supplier->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function balance(Request $request)
    {
        $supplier_id = $request->input('supplier_id');

        return SupplierPayment::Balance($supplier_id);
    }
}
