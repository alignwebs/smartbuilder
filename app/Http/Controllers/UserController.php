<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;


class UserController extends Controller
{
    public function checkUser(Request $request)
    {
    	switch ($request->input('type')) {
    			case 'customer':
    			{
    				$User = User::withTrashed()->where('email',$request->input('email'))->first();

    				if(!empty($User))
    				{
    					if($User->hasRole('customer'))
    					{
			    			$data['status'] = 'exists';
			    			$data['customer'] = $User;
    					}
    					else
    						$data['status'] = 'locked';

    				}
			    	else
			    		$data['status'] = 'available';

    				break;
    			}
    			
    			default:
    				# code...
    				break;
    		}

    		return $data;
    }
}
