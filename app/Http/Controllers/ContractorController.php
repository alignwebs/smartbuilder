<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Session;
use Form;

use App\Contractor;
use App\ContractorPayment;
use App\Department;





class ContractorController extends Controller
{
   
    public function index()
    {
        $org_id = getOrganization('org_id');

        $Contractors = $this->orgContractors($org_id);
        
        return view('contractor.list', ['Contractors'=>$Contractors]);
    }

    public function orgContractors($org_id,$fields="*")
    {
       $fetch = Contractor::select($fields)->where('org_id', $org_id)->orderby('name','asc')->get();

       return $fetch;
    }

    public function create()
    {
        $Departments = Department::ofOrg()->pluck('name','id');

        return view('contractor.add', ['Departments'=>$Departments]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'code' => 'required',
            'name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Contractor = new Contractor();

        $Contractor->code = $request->input('code');
        $Contractor->org_id = getOrganization('org_id');
        $Contractor->department_id = $request->input('department');
        $Contractor->name = $request->input('name');
        $Contractor->phone = $request->input('phone');
        $Contractor->email = $request->input('email');
        $Contractor->address = $request->input('address');
        $Contractor->save();

        return redirect()->route('contractor.index');
    }

    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        $Contractor = Contractor::ofOrg()->find($id);
        $Departments = Department::ofOrg()->pluck('name','id');

        return view('contractor.edit', ['Contractor'=>$Contractor,'Departments'=>$Departments]);
    }


    public function update(Request $request, $id)
    {
       $validator = Validator::make($request->all(), [

            'code' => 'required',
            'name' => 'required'

        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $Contractor = Contractor::find($id);

        $Contractor->code = $request->input('code');
        $Contractor->department_id = $request->input('department');
        $Contractor->name = $request->input('name');
        $Contractor->phone = $request->input('phone');
        $Contractor->email = $request->input('email');
        $Contractor->address = $request->input('address');
        $Contractor->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        Contractor::find($id)->delete();
        return redirect()->route('contractor.index','deleted=true');
    }

    public function listByDepartment(Request $request)
    {
        $data = Contractor::where('department_id',$request->input('department_id'))->get()->pluck('name','id')->toArray();
        return Form::select('contractor', $data, null,['class'=>'form-control' , 'required'=>'', 'placeholder'=>'Select Contractor']);
    }

    public function balance(Request $request)
    {
        $contractor_id = $request->input('contractor_id');

        return ContractorPayment::Balance($contractor_id);
    }

    public function unpaidTask(Request $request)
    {
        $contractor_id = $request->input('contractor_id');

        $data = Contractor::unpaidTask($contractor_id);

        return view('contractor.unpaidtask', compact('data'));
    }

}
