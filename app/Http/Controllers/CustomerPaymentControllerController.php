<?php

namespace App\Http\Controllers;

use App\CustomerPaymentController;
use Illuminate\Http\Request;

class CustomerPaymentControllerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CustomerPaymentController  $customerPaymentController
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerPaymentController $customerPaymentController)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CustomerPaymentController  $customerPaymentController
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerPaymentController $customerPaymentController)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CustomerPaymentController  $customerPaymentController
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerPaymentController $customerPaymentController)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CustomerPaymentController  $customerPaymentController
     * @return \Illuminate\Http\Response
     */
    public function destroy(CustomerPaymentController $customerPaymentController)
    {
        //
    }
}
