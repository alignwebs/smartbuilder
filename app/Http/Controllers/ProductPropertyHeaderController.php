<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

use App\ProductPropertyHeader;


class ProductPropertyHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pph = ProductPropertyHeader::where('project_id',Session('selectedProject')['proj_id'])->orderBy('name','desc')->get();
        return view('product.header.list', ['pph'=>$pph]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), [

            'pph_name' => 'required',
        
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $pph = new ProductPropertyHeader();
        $pph->project_id = Session('selectedProject')['proj_id'];
        $pph->name = $request->input('pph_name');
        $pph->description = $request->input('pph_desc');
        $pph->save();

        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pph = ProductPropertyHeader::ofProject()->find($id);

        return view('product.header.edit', ['pph'=>$pph]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validator = Validator::make($request->all(), [

            'pph_name' => 'required',
        
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $pph = ProductPropertyHeader::find($id);
        $pph->name = $request->input('pph_name');
        $pph->description = $request->input('pph_desc');
        $pph->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductPropertyHeader::destroy($id);

        return redirect()->route('header.index', 'deleted=true');
    }
}
