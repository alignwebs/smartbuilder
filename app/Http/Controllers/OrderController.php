<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

use Auth;

use Session;

use Form;

use App\Order;
use App\OrderItem;
use App\OrderGrn;
use App\Supplier;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Orders = Order::where('project_id',getProject('proj_id'))->get();

        return view('order.list',['Orders'=>$Orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $Suppliers = Supplier::ofOrg()->get()->pluck('name','id');
        
        return view('order.add',['Suppliers'=>$Suppliers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = json_decode($request->input('items'),true);
        
        $uid = Auth::id();
        $project_id = getProject('proj_id');
        $org_id = getOrganization('org_id');

        if(sizeof($items) > 0)
        {   
            $invoice_total = 0; 


            if(empty($request->input('order_id')))
            {
                /*FIRST CREATE ORDER*/

                $Order = new Order();
                $Order->user_id = $uid;
                $Order->project_id = $project_id;
                $Order->org_id = $org_id;
                $Order->draft = $request->input('draft');
                $Order->save();

                $order_id = $Order->id;
                $Order->code = $this->generateOrderCode($order_id);
                $Order->save();
            }
            else {

                $order_id = $request->input('order_id');
                $Order = Order::ofProject()->find($order_id);
                $Order->draft = 0;
                $Order->save();
            }

            

            foreach ($items as $item) {
                
                if(sizeof($item) > 0)
                {
                    $data[] = array(
                            "project_id" => $project_id,
                            "org_id" => $org_id,
                            "order_id"  =>  $order_id,
                            "supplier_id" => $item['supplierid'],
                            "supplier_code" => $item['suppliercode'],
                            "supplier_name" => $item['suppliername'],
                            "item_id" => $item['itemid'],
                            "item_code" => $item['itemcode'],
                            "item_name" => $item['itemname'],
                            "unit" => $item['itemunit'],
                            "qty" => $item['itemqty'],
                            "tax_name" => $item['taxname'],
                            "tax" => $item['tax'],
                            "unit_price" => $item['itemunitprice'],
                            "discount" => $item['itemdiscount'],
                            "net_unit_price" => $item['itemnetunitprice'],
                            "total_price" =>  $item['total']
                        );

                    $invoice_total  += $item['total'];
                    
                }

            }

            OrderItem::where('order_id',$order_id)->delete();

            $OrdeItems = OrderItem::insert($data);

            $Order->invoice_total = $invoice_total;
            $Order->Save();

            if($request->input('type') == 'draft')
                return action("OrderController@edit",[$Order->code]);
            else
                return action("OrderController@show",[$Order->code]);

        }
    }

    public function generateOrderCode($orderid)
    {
        $code = getOrganization('org_code')."-".getProject('proj_code')."-".str_pad($orderid,4,"0",STR_PAD_LEFT);

        return $code;
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Order = Order::where("code",$id)->firstOrFail();

        if($Order->draft > 0)
            return redirect()->action('OrderController@edit',$Order->code);

        return view('order.view',["Order"=>$Order]);
    }

    public function prepareGrn($Orderid)
    {
       $data = OrderGrn::prepareGrn($Orderid);
       return view('order.grn-prepare',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*FOR DRAFTED ORDER*/

        $Order = Order::where("code",$id)->where('draft',1)->first();
        $Suppliers = Supplier::all()->pluck('name','id');

        return view('order.edit',["Order"=>$Order, 'Suppliers'=>$Suppliers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::find($id)->isDraft()->delete();
        return route('order.index','deleted=true');
    }

    public function draftItems(Request $request)
    {
        $order_id = $request->input('orderid');
    }

    public function ajax(Request $request)
    {
        $type = $request->input('type');
        $id = $request->input('id');
        $output = "";

        switch($type)
        {
            case "suppliersByOrder":
            {
               
                $data = Order::orderSuppliers($id);

                $output = Form::select('supplier',$data, null, ['class'=>'form-control', 'placeholder' => 'Select Supplier', 'onchange'=>' getSupplierStats(this)']);

                break;
            }

           
            default:
                break;
        }

        return $output;

    }
}
