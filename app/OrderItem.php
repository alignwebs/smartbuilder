<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
  protected $guarded = ['id','created_at','updated_at'];
	
	static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    static public function calculateItemCost($item_id,$qty)
    {
    	$query = Self::select('unit_price')->ofProject()->where('item_id',$item_id)->get();
    }

  
}
