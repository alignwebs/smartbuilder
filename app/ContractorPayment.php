<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ContractorPayment extends Model
{
    static public function Balance($contractor_id)
    {
       return Self::ofProject()->where('type','advance')->where('contractor_id',$contractor_id)->sum('amount');
    }

    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    static public function total($contractor_id)
    {
    	return Self::ofProject()->where('contractor_id',$contractor_id)->sum('amount');
    }

    static public function addTransaction($params=[])
    { 
        $org_id     = getOrganization('org_id');
        $project_id = getProject('proj_id');

        $PaySettings['org_id']              = $org_id;
        $PaySettings['project_id']          = $project_id;
        $PaySettings['contractor_id']       = $params['contractor_id'];
        $PaySettings['payment_id']          = $params['payment_id'];
        $PaySettings['type']                = $params['type'];
        $PaySettings['amount']              = $params['amount'];

        Self::insert($PaySettings);
    }

   

}
