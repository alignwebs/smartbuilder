<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contractor extends Model
{
	use SoftDeletes;
	use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];

	protected $appends = ['codename'];

	static public function scopeofOrg($query,$org_id="")
	{
	  $org_id = (!empty($org_id) ? $org_id : getOrganization('org_id'));

	  return $query->where('org_id',$org_id);
	}

	public function setCodenameAttribute()
	{
			return $this->code." - ".$this->name;
	}

	static public function unpaidTask($contractor_id)
    {
    	$unpaid_stage_task = ProductStage::ofProject()->where('contractor_id', $contractor_id)->where('payment_id', null)->get();

    	return $unpaid_stage_task;
    }
}
