<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\DB;


class Product extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    
	 use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $appends = ['name_desc'];
    
    public function getNameDescAttribute()
    {
        return $this->product_name." - ".$this->product_details;
    }

    public function productcategory()
    {
    	return $this->belongsTo('App\ProductCategory','product_category_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');
    }

    public function organization()
    {
        return $this->belongsTo('App\Organization','org_id');
    }

    public function customers()
    {
        return $this->hasMany('App\ProductCustomer', 'product_id');
    }
    
    public function stages()
    {
        return $this->hasMany('App\ProductStage','product_id');
    }

    public function changerequest()
    {
        return $this->hasMany('App\ChangeRequest','product_id');
    }

    public function customerpayments()
    {
        return $this->hasMany('App\CustomerPayment','product_id');
    }
    
    public function items()
    {
        return $this->hasMany("App\ProductItem","product_id");
    }

    static public function scopeofProject($query,$project_id="")
    {
        $project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

        return $query->where('project_id',$project_id);
    }

    static public function scopeisSold($query)
    {
        return $query->where('payment_id', '>', '0');
    }

    static public function removeProductCategory($product_category_id)
    {
        return Self::where('product_category_id',$product_category_id)->update(['product_category_id' => null]);
    }

    static public function availableProducts()
    {
        $product_ids = Self::ofProject()->select('id')->get()->toArray();

        $sold_product_ids = ProductCustomer::select('product_id')->whereIn('product_id', $product_ids)->get()->toArray();

        $availableProducts = Self::ofProject()->whereNotIn('id',$sold_product_ids)->get();

        return $availableProducts;
    }

    static public function calculateProductCost($product_id)
    {
        $materials = ProductItem::selectRaw('item_id, SUM(qty) as qty')
                        ->where('product_id',$product_id)
                        ->groupBy('item_id')
                        ->get();
        
        if($material->count() > 0)
        {
            foreach ($materials as $material) {
                
            }
        }
    }

}
