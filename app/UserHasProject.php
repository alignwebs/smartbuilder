<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserHasProject extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];

    protected $fillable = ['user_id', 'project_id'];

    public function user()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    static public function scopeofProject($query, $project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

}
