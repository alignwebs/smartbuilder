<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];
    
    public function projectItems()
    {
    	$project_id = getProject('proj_id');
    	return $this->hasMany('App\ItemDepartment','department_id','id')->where('project_id',$project_id);
    }
    public function contacts()
    {
        return $this->hasMany('App\DepartmentContact','department_id');
    }    
    /*Get Departments of Projects*/

    static public function scopeofProject($query,$project_id="")
    {
    	$project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

    	return $query->where('project_id',$project_id);
    }

    static public function scopeofOrg($query,$org_id="")
    {
        $org_id = (!empty($org_id) ? $org_id : getOrganization('org_id'));

        return $query->where('org_id',$org_id);
    }

    public function setCodeAttribute($value)
    {
      $this->attributes['code'] = strtoupper($value);
    }

    public function setNameAttribute($value)
    {
      $this->attributes['name'] = ucwords($value);
    }
}
