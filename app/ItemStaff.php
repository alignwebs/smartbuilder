<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemStaff extends Model
{
	use \Venturecraft\Revisionable\RevisionableTrait;
    
    protected $revisionCreationsEnabled = true;
    

    public function staff()
    {
    	return $this->belongsTo('App\Staff','staff_id');
    }
}
