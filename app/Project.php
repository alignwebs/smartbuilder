<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $dates = ['deleted_at'];
    protected $revisionCreationsEnabled = true;

	public function orders()
	{
		return $this->hasMany('App\Order','project_id');
	}

	public function products()
	{
		return $this->hasMany('App\Product','project_id');
	}

	public function customers()
    {
        return $this->hasMany('App\Customer', 'project_id');
    }
    
	public function productcategories()
	{
		return $this->hasMany('App\ProductCategory','project_id');
	}

	public function setProjCodeAttribute($value)
    {
        $this->attributes['proj_code'] = strtoupper($value);
    }

    public function setProjNameAttribute($value)
    {
        $this->attributes['proj_name'] = ucwords($value);
    }

    static public function listProjects($fields="*")
    {
       $fetch = Self::select($fields)->where('org_id', getOrganization('org_id'))->orderby('proj_name','asc')->get();

       return $fetch;
    }
}
