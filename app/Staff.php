<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Staff extends Model
{
    use SoftDeletes;
    use \Venturecraft\Revisionable\RevisionableTrait;

    protected $revisionCreationsEnabled = true;
    protected $dates = ['deleted_at'];

	protected $appends = ['fullname'];

    public function genStaffCode()
    {
    	$staff_id = $this->id;

    	$org_code  = getOrganization('org_code');
    	
    	$this->code = $org_code."-S".$staff_id;

    	return $this->save();
    }

    static public function scopeofProject($query,$project_id="")
    {
        $project_id = (!empty($project_id) ? $project_id : getProject('proj_id'));

        return $query->where('project_id',$project_id);
    }

    static public function fromOrg()
    {
    	$org_id  = getOrganization('org_id');
    	$Staff = Self::where('org_id',$org_id)->get();

    	return $Staff;
    } 

    public function getFullnameAttribute()
    {
    	return $this->code." - ".$this->name;
    }
}
